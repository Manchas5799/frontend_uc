# Primera etapa: compilar la aplicación Angular
FROM nginx:latest

WORKDIR /usr/share/nginx/html

COPY dist/ExPortal/. .

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
