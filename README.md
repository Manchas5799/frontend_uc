[//]: # (**Pases produccion TI**)

# Project:

* **[UCPortal - Frontent Aplicacion Portal del Estudiante de Posgrado - Administrativos](https://gitlab.com/uc-cau/posgrado/portal-estudiante-frontend-administrativos.git)** 
revisar su [documentación](https://drive.google.com/drive/folders/1Fb1azDlyTfb5W0RjThHxYsPCMV9ck92k?usp=sharing), https://drive.google.com/drive/folders/1Fb1azDlyTfb5W0RjThHxYsPCMV9ck92k?usp=sharing

**Funcionalidades Agregadas:**

1. **................** - ................

---

# APLICACIÓN FRONTEND PORTAL DEL ESTUDIANTE POSGRADO -ADMINISTRATIVOS

Descripcion:

> Interfaces del Portal del Estudiante de Posgrado - Administrativos consume servicios del Api Portal Estudiante Posgrado. 
> [Angular CLI](https://github.com/angular/angular-cli) version 12.2.4.

GRUPO:
https://gitlab.com/uc-cau/posgrado/portal-estudiante-webapi
https://gitlab.com/uc-cau/posgrado/portal-estudiante-frontend


* PROD:
  * IP : 172.16.99.71
  * URI: https://administrativospc.continental.edu.pe
  * Usuario bd: continental\ssapppeepgec

* TEST: Publicado en Servidor: 172.16.3.107
  * IP      : 172.16.99.68
  * IIS Site: estudiantesepgts   
  * URI     : https://estudiantesepgts.continental.edu.pe
  

## CONFIGURACION
## INSTALL

* Verificar que el IIS esté instalado, .net framework 4 instalado.
* En el IIS crear un grupo de aplicaciones para el API Ejm: Name:webapiAP, .NET CLR Version: .NET CLR Version v4.0.30319, Managed pipeline mode: Integrated.
* Crear dentro de la carpeta `"\inetpub\wwwroot"` , la carpeta UCPortal, se debe copiar en esta carpeta el contenido de la publicacion.
* Configurar los permisos de la carpeta `"\inetpub\wwwroot\UCPortal"`  para el usuario IIS_IUSRS (usuario del IIS).
* Crear el directorio virtual o website con el grupo de aplicación creado.

## DEPLOY CON ANGULAR 12
* PRODUCCION
    * Configuracion de Google Auth
        * Creación del proyecto
        * Comenzamos creando un nuevo proyecto en la Consola de Firebase
        * Con el proyecto creado podemos entrar a la sección de autenticación y ubicar la configuración de inicio de sesión.
        * Esta sección nos permite habilitar o deshabilitar diferentes alternativas para que nuestros usuarios se registren a la aplicación. Podemos usar correo y contraseña, redes sociales como Twitter y Facebook, con mensajes de texto al teléfono, links de autenticación por correo electrónico, entre otras.
        * Incluso podemos configurar nuestra aplicación para soportar múltiples métodos de registro y darle más comodidades a nuestros usuarios. 
        * Habilitamos el registro de usuarios con correo y contraseña.

    * Configuracion de google auth en la aplicación:
        * Editar el archivo: `"src/environments/environment.prod.ts"`
            ```json
            export const environment = {               
                production: true,
                apiUrl: `"<uri del Webapi de produccion>"`,
                studentPortalUrl: `"<uri del Frontend del Portal de Estudiantes de produccion>"`,                
                googleClientId: `"<googleClientId obtenido para produccion>"`
            }
            ```     
        * En la seccion googleClientId colocar el id de cliente de firebase obtenido en el paso anterior
        * En la seccion apiUrl colocar la URI del WebApi de produccion
        * En la seccion studentPortalUrl colocar la URI del Frontend del Portal de Estudiantes de produccion

    * Para probar aplicativo en entorno local/prod ingresar por el terminal a la carpeta de la solucion y ejecutar en el siguiente orden:
        * npm install 
        * npm run prod

    * Para traspilar aplicativo iingresar por el terminal a la carpeta de la solucion y ejecutar en el siguiente orden:
        * npm install 
        * npm audit fix
        * npm run build:prod

    * Para publicar ingresar a la carpeta `"dist/EXPortal"` copiar los archivos generarados y pegarlos en la carpeta del servidor de produccion con IIS `"\inetpub\wwwroot\EXPortal"` 


* QA
      * Configuracion de Google Auth
        * Creación del proyecto
        * Comenzamos creando un nuevo proyecto en la Consola de Firebase
        * Con el proyecto creado podemos entrar a la sección de autenticación y ubicar la configuración de inicio de sesión.
        * Esta sección nos permite habilitar o deshabilitar diferentes alternativas para que nuestros usuarios se registren a la aplicación. Podemos usar correo y contraseña, redes sociales como Twitter y Facebook, con mensajes de texto al teléfono, links de autenticación por correo electrónico, entre otras.
        * Incluso podemos configurar nuestra aplicación para soportar múltiples métodos de registro y darle más comodidades a nuestros usuarios. 
        * Habilitamos el registro de usuarios con correo y contraseña.

    * Configuracion de google auth en la aplicación:
        * Editar el archivo: `"src/environments/environment.qa.ts"`
            ```json
            export const environment = {
                production: false,            
                apiUrl: `"<uri del Webapi de test>"`,
                studentPortalUrl: `"<uri del Frontend del Portal de Estudiantes de Test>"`,
                googleClientId: `"<googleClientId obtenido para test>"`     
                
            }
            ```     
        * En la seccion googleClientId colocar el id de cliente de firebase obtenido en el paso anterior
        * En la seccion apiUrl colocar la URI del WebApi de produccion

    * Para probar aplicativo en entorno local/test ingresar por el terminal a la carpeta de la solucion y ejecutar en el siguiente orden:
        * npm install 
        * npm run qa
   
    * Para traspilar aplicativo ingresar por el terminal a la carpeta de la solucion y ejecutar en el siguiente orden:
        * npm install 
        * npm audit fix
        * npm run build:qa

    * Para publicar ingresar a la carpeta `"dist/EXPortal"` copiar los archivos generarados y pegarlos en la carpeta del servidor de produccion con IIS `"\inetpub\wwwroot\EXPortal"` 

## PARA PUBLICACIONES DEL SERVICIO ANTE GESTIONES DE CAMBIO 
**Para publicacion con Nueva Version de Codigo**
  * Verificar el status de codigo ejecutando: git status
  * Respaldar cambios del archivo de configuracion `"src/environments/environment.prod.ts"` 
  * Revertir cambios ejecutando: git checkout -- src/environments/environment.prod.ts
  * Obtener ultima version del codigo ejecutando: git pull origin master
  * Editar archivo de configuracion `"src/environments/environment.prod.ts"` igual a la version de respaldo.  
  * Para obtener nuevo traspilado ejecutar: 
    * npm install 
    * npm audit fix
    * npm run build:prod    
  * Para publicar ingresar a la carpeta `"dist/EXPortal"` copiar los archivos generarados y pegarlos reemplazando los archivos en la carpeta del servidor de produccion con IIS `"\inetpub\wwwroot\EXPortal"` 
