// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: 'https://apiepgpets.continental.edu.pe',
  studentPortalUrl: 'https://cau20-4571c.web.app',
  googleClientId: '367414571491-tp1n99hm5ek0525sflr0c0jro3040hpk.apps.googleusercontent.com',
  studentType : 'estudiante',
  executiveType : 'administrativo',
  pregradoLevel: "pregrado",
  posgradoLevel: "posgrado",
  continuaLevel: "continua",
  requestCriticality: {
    red: 90,
    yellow: 50,
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
