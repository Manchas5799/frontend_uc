export const environment = {
  production: true,
  apiUrl: 'https://apiepgpets.continental.edu.pe',
  studentPortalUrl: 'https://cau20-4571c.web.app',
  googleClientId: '367414571491-tp1n99hm5ek0525sflr0c0jro3040hpk.apps.googleusercontent.com',
  studentType : 'estudiante',
  executiveType : 'administrativo',
  pregradoLevel: "pregrado",
  posgradoLevel: "posgrado",
  continuaLevel: "continua",
  requestCriticality: {
    red: 90,
    yellow: 50,
  },
};
