import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ui-p404',
  templateUrl: './p404.component.html',
  styleUrls: ['./p404.component.sass']
})
export class P404Component implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  goBack() {
    window.history.back();
  }

}
