import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthUiModule } from '@app/auth/models/AuthUiModule';
import { UcProfile } from 'src/app/auth/models/UcProfile';
import { AuthService } from 'src/app/auth/services/auth.service';
import { ModulesService } from 'src/app/executive/home/services/modules.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'ui-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.sass']
})
export class SideBarComponent implements OnInit {
  @Input() campus: string = 'HYO';
  @Input() campuses: string[] = [];
  @Input() modules: AuthUiModule[] = [];
  @Input() pathModule: string = '';
  level = '';
  continua = environment.continuaLevel;
  posgrado = environment.posgradoLevel;
  url: any = {};
  profile!: UcProfile;
  selectedCampus?: any;
  optCampus: any[] = [];
  constructor(
    private authService: AuthService,
    private modulesService: ModulesService,
    private router: Router,
  ) {
    this.campuses.forEach(campus => {
      this.optCampus.push({ code: campus, name: campus });
    });
    this.selectedCampus = { code: this.campus, name: this.campus };
  }

  ngOnInit(): void {
    console.log(this.modules);
    this.authService.getProfile()
      .subscribe(profile => {
        this.profile = profile!;
        this.level = profile!.level??'no-level';
      });
  }
  changeCampus(event: any) {
    console.log(event);

    const val = event.target.value;
    this.campus = val;
    this.modulesService.setCampus(val);
    this.router.navigate(['/','administrativo',this.level]);
  }

  getCampusName(campus: string) {
    switch (campus) {
      case 'S01':
        return 'Huancayo';
      case 'F01':
        return 'Arequipa';
      case 'F02':
        return 'Lima';
      case 'F03':
        return 'Cuzco';
      case 'V00':
        return 'Virtual';
      default:
        return campus;
    }
  }

}
