import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UcProfile } from 'src/app/auth/models/UcProfile';
import { AuthService } from 'src/app/auth/services/auth.service';

@Component({
  selector: 'ui-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.sass']
})
export class NavBarComponent implements OnInit {
  profile!: UcProfile;
  level = '';
  constructor(private authService: AuthService) {}

  ngOnInit(): void {
    this.authService.getProfile()
      .subscribe(profile => {
        this.level = profile!.level??'no-level';
        this.profile = profile!;
      });
  }
  toggleSidebar() {
    document.body.classList.toggle('sidebar-collapse');
  }

}
