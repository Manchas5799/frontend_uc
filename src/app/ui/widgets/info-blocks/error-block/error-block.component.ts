import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'ui-error-block',
  templateUrl: './error-block.component.html',
  styleUrls: ['./error-block.component.sass']
})
export class ErrorBlockComponent implements OnInit {
  @Input()
  mainText = "¡Oops!";
  @Input()
  text = "Revisa tu conexión de Internet e intenta nuevamente.";
  
  constructor() { }

  ngOnInit(): void {
  }

}
