import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'ui-blank-block',
  templateUrl: './blank-block.component.html',
  styleUrls: ['./blank-block.component.sass']
})
export class BlankBlockComponent implements OnInit {
  @Input()
  mainText = "La lista esta vacia";
  @Input()
  text = "No se encontro la información que esta buscando.  Vuelve a intentar en unos minutos.";
  constructor() { }

  ngOnInit(): void {
  }

}
