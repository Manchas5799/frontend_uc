import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BlankListComponent } from './blank-list.component';

describe('BlankListComponent', () => {
  let component: BlankListComponent;
  let fixture: ComponentFixture<BlankListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BlankListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BlankListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
