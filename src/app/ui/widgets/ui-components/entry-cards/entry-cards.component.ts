import { Component, Input, OnInit } from '@angular/core';
import { AuthUiModule } from 'src/app/auth/models/AuthUiModule';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'ui-entry-cards',
  templateUrl: './entry-cards.component.html',
  styleUrls: ['./entry-cards.component.sass']
})
export class EntryCardsComponent implements OnInit {
  @Input() type: string = environment.studentType;
  @Input() level: string = environment.posgradoLevel;
  @Input() modules: AuthUiModule[] = [];
  url: any = {};
  posgrado = environment.posgradoLevel;
  continua = environment.continuaLevel;
  constructor() { }

  ngOnInit(): void {
  }

  
}
