import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LeftAcademicBlockComponent } from './left-academic-block.component';

describe('LeftAcademicBlockComponent', () => {
  let component: LeftAcademicBlockComponent;
  let fixture: ComponentFixture<LeftAcademicBlockComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LeftAcademicBlockComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LeftAcademicBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
