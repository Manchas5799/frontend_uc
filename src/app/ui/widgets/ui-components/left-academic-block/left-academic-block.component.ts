import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'ui-left-academic-block',
  templateUrl: './left-academic-block.component.html',
  styleUrls: ['./left-academic-block.component.sass']
})
export class LeftAcademicBlockComponent implements OnInit {
  @Input()
  program!: any;
  @Input()
  section!: any;
  @Input()
  color = 'orange';
  constructor() { }

  ngOnInit(): void {
  }

}
