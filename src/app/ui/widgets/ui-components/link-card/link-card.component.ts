import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'ui-link-card',
  templateUrl: './link-card.component.html',
  styleUrls: ['./link-card.component.sass']
})
export class LinkCardComponent implements OnInit {
  @Input() title: string = '';
  @Input() description: string = '';
  @Input() icon: string = '';
  @Input() color: string = '';

  constructor() { }

  ngOnInit(): void {
  }

}
