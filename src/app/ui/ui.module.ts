import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// ng modules
import { DialogModule } from 'primeng/dialog';
import { ToastModule } from 'primeng/toast';
import { CalendarModule } from 'primeng/calendar';
import { DropdownModule } from 'primeng/dropdown';

// screens
import { P404Component } from './screens/p404/p404.component';
import { P401Component } from './screens/p401/p401.component';
import { EntryCardsComponent } from './widgets/ui-components/entry-cards/entry-cards.component';
import { FooterComponent } from './widgets/layout-componts/footer/footer.component';
import { LinkCardComponent } from './widgets/ui-components/link-card/link-card.component';
import { NavBarComponent } from './widgets/layout-componts/nav-bar/nav-bar.component';
import { SideBarComponent } from './widgets/layout-componts/side-bar/side-bar.component';
import { LoaderComponent } from './widgets/info-blocks/loader/loader.component';
import { BlankListComponent } from './widgets/info-blocks/blank-list/blank-list.component';
import { ErrorBlockComponent } from './widgets/info-blocks/error-block/error-block.component';
import { BlankBlockComponent } from './widgets/info-blocks/blank-block/blank-block.component';
import { LeftAcademicBlockComponent } from './widgets/ui-components/left-academic-block/left-academic-block.component';
import { RouterModule } from '@angular/router';
import { ReportFilterComponent } from '../executive/request-reports/widgets/report-filter/report-filter.component';
import { ReportListItemComponent } from '../executive/request-reports/widgets/report-list-item/report-list-item.component';
import { FormsModule } from '@angular/forms';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
// widgets


@NgModule({
  declarations: [
    EntryCardsComponent,
    FooterComponent,
    LinkCardComponent,
    NavBarComponent,
    P401Component,
    P404Component,
    SideBarComponent,
    LoaderComponent,
    BlankListComponent,
    ErrorBlockComponent,
    BlankBlockComponent,
    LeftAcademicBlockComponent,
    ReportFilterComponent,
    ReportListItemComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    ToastModule,
    DialogModule,
    CalendarModule,
    DropdownModule,
    ConfirmDialogModule,
  ],
  exports: [
    EntryCardsComponent,
    FooterComponent,
    LinkCardComponent,
    NavBarComponent,
    SideBarComponent,
    LoaderComponent,
    ErrorBlockComponent,
    BlankBlockComponent,
    LeftAcademicBlockComponent,
    ToastModule,
    DialogModule,
    CalendarModule,
    DropdownModule,
    ReportFilterComponent,
    ReportListItemComponent,
    ConfirmDialogModule,
  ]
})
export class UiModule { }
