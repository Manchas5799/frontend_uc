import { TestBed } from '@angular/core/testing';

import { DinamicScriptLoaderService } from './dinamic-script-loader.service';

describe('DinamicScriptLoaderService', () => {
  let service: DinamicScriptLoaderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DinamicScriptLoaderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
