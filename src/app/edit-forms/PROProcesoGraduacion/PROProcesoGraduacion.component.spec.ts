import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PROProcesoGraduacionEditComponent } from './PROProcesoGraduacion.component';

describe('PROProcesoGraduacionComponent', () => {
  let component: PROProcesoGraduacionEditComponent;
  let fixture: ComponentFixture<PROProcesoGraduacionEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PROProcesoGraduacionEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PROProcesoGraduacionEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
