import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { EditBaseComponent } from '../../shared/base/edit-base.component';
import { GridBaseComponent } from '../../shared/base/grid-base/grid-base.component';
import { XtFieldCellComponent } from '../../shared/controls/xt-field-cell/xt-field-cell.component';
import { DetalleParametroService } from '@svc/DetalleParametro.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { PROProcesoGraduacionService } from '@svc/PROProcesoGraduacion.service';
import { PEREstudianteService } from '@svc/PEREstudiante.service';
@Component({
  selector: 'app-proprocesograduacion-edit',
  templateUrl: './proprocesograduacion.component.html',
  styleUrls: ['./proprocesograduacion.component.css']
})
export class PROProcesoGraduacionEditComponent extends EditBaseComponent implements OnInit {

  constructor(
    public service: PROProcesoGraduacionService,
    protected router: Router,
    protected route: ActivatedRoute,
    public PEREstudianteSvc: PEREstudianteService,
    public DetalleParametroSvc: DetalleParametroService
  ) {
    super();
  }
  //Codigo
  @ViewChild('FieldCodigo')
  public FieldCodigo: XtFieldCellComponent;
  //Estudiante
  @ViewChild('FieldEstudiante')
  public FieldEstudiante: XtFieldCellComponent;
  //FechaInscripcion
  @ViewChild('FieldFechaInscripcion')
  public FieldFechaInscripcion: XtFieldCellComponent;
  //NumeroGrupo
  @ViewChild('FieldNumeroGrupo')
  public FieldNumeroGrupo: XtFieldCellComponent;
  //TituloTesis
  @ViewChild('FieldTituloTesis')
  public FieldTituloTesis: XtFieldCellComponent;
  //GestionTesis
  @ViewChild('FieldGestionTesis')
  public FieldGestionTesis: XtFieldCellComponent;
  //Etapa
  @ViewChild('FieldEtapa')
  public FieldEtapa: XtFieldCellComponent;
  //FechaFinalizacionUltimaAsignatura
  @ViewChild('FieldFechaFinalizacionUltimaAsignatura')
  public FieldFechaFinalizacionUltimaAsignatura: XtFieldCellComponent;
  //FechaLimiteGraduarse
  @ViewChild('FieldFechaLimiteGraduarse')
  public FieldFechaLimiteGraduarse: XtFieldCellComponent;
  //NotaArea
  @ViewChild('FieldNotaArea')
  public FieldNotaArea: XtFieldCellComponent;
  //FechaInicioAsesoria
  @ViewChild('FieldFechaInicioAsesoria')
  public FieldFechaInicioAsesoria: XtFieldCellComponent;
  //FechaFinAsesoria
  @ViewChild('FieldFechaFinAsesoria')
  public FieldFechaFinAsesoria: XtFieldCellComponent;
  //EstadoSustentacion
  @ViewChild('FieldEstadoSustentacion')
  public FieldEstadoSustentacion: XtFieldCellComponent;
  //Asistencia
  @ViewChild('FieldAsistencia')
  public FieldAsistencia: XtFieldCellComponent;
  //EstadoSustentacionFinal
  @ViewChild('FieldEstadoSustentacionFinal')
  public FieldEstadoSustentacionFinal: XtFieldCellComponent;
  //EstadoExpediente
  @ViewChild('FieldEstadoExpediente')
  public FieldEstadoExpediente: XtFieldCellComponent;
  //FechaExpediente
  @ViewChild('FieldFechaExpediente')
  public FieldFechaExpediente: XtFieldCellComponent;
  //EstadoDiploma
  @ViewChild('FieldEstadoDiploma')
  public FieldEstadoDiploma: XtFieldCellComponent;
  //FechaDiploma
  @ViewChild('FieldFechaDiploma')
  public FieldFechaDiploma: XtFieldCellComponent;
  //NotaAreaDiploma
  @ViewChild('FieldNotaAreaDiploma')
  public FieldNotaAreaDiploma: XtFieldCellComponent;
  //NotaAreaEtapa3
  @ViewChild('FieldNotaAreaEtapa3')
  public FieldNotaAreaEtapa3: XtFieldCellComponent;
  //NotaAreaEtapa2
  @ViewChild('FieldNotaAreaEtapa2')
  public FieldNotaAreaEtapa2: XtFieldCellComponent;

  //Integrante
  @ViewChild('GridIntegrante')
  public GridIntegrante : GridBaseComponent;
  //GraduacionRequisito
  @ViewChild('GridGraduacionRequisito')
  public GridGraduacionRequisito : GridBaseComponent;
  //GraduacionObservacion
  @ViewChild('GridGraduacionObservacion')
  public GridGraduacionObservacion : GridBaseComponent;
  //GraduacionSustentacion
  @ViewChild('GridGraduacionSustentacion')
  public GridGraduacionSustentacion : GridBaseComponent;
  //GraduacionDiploma
  @ViewChild('GridGraduacionDiploma')
  public GridGraduacionDiploma : GridBaseComponent;
  //GraduacionResponsable
  @ViewChild('GridGraduacionResponsable')
  public GridGraduacionResponsable : GridBaseComponent;
  
  ngOnInit(): void {
    super.ngOnInit();
  }
  ngAfterViewInit() {
    super.ngAfterViewInit();
    super.FillDropDown(this.FieldEstudiante, this.PEREstudianteSvc.selAll());
    super.FillDropDown(this.FieldGestionTesis, this.DetalleParametroSvc.selxParametro(this.FieldGestionTesis.listid));
    super.FillDropDown(this.FieldEtapa, this.DetalleParametroSvc.selxParametro(this.FieldEtapa.listid));
    //super.FillDropDown(this.FieldEstadoSustentacion, this.DetalleParametroSvc.selxParametro(this.FieldEstadoSustentacion.listid));
    //super.FillDropDown(this.FieldAsistencia, this.DetalleParametroSvc.selxParametro(this.FieldAsistencia.listid));
    //super.FillDropDown(this.FieldEstadoSustentacionFinal, this.DetalleParametroSvc.selxParametro(this.FieldEstadoSustentacionFinal.listid));
    //super.FillDropDown(this.FieldEstadoExpediente, this.DetalleParametroSvc.selxParametro(this.FieldEstadoExpediente.listid));
    //super.FillDropDown(this.FieldEstadoDiploma, this.DetalleParametroSvc.selxParametro(this.FieldEstadoDiploma.listid));
  }

  fnValidar = () => {
    alert("fnValidar")
  }

  fnInscribirPlan = () => {
    alert("fnInscribirPlan")
  }

  fnFinalizarEtapa = () => {
    alert("fnFinalizarEtapa")
  }

  getFormFields() : Array<XtFieldCellComponent> {
  	return [
      this.FieldCodigo,
      this.FieldEstudiante,
      this.FieldFechaInscripcion,
      this.FieldNumeroGrupo,
      this.FieldTituloTesis,
      this.FieldGestionTesis,
      this.FieldEtapa,
      this.FieldFechaFinalizacionUltimaAsignatura,
      this.FieldFechaLimiteGraduarse,
      this.FieldNotaArea,
      this.FieldFechaInicioAsesoria,
      this.FieldFechaFinAsesoria,
      this.FieldEstadoSustentacion,
      this.FieldAsistencia,
      this.FieldEstadoSustentacionFinal,
      this.FieldEstadoExpediente,
      this.FieldFechaExpediente,
      this.FieldEstadoDiploma,
      this.FieldFechaDiploma,
      this.FieldNotaAreaDiploma,
      this.FieldNotaAreaEtapa3,
      this.FieldNotaAreaEtapa2,

    ];
  }

  getGridFields(): Array<GridBaseComponent> {
    return [
      this.GridIntegrante,
      this.GridGraduacionRequisito,
      this.GridGraduacionObservacion,
      this.GridGraduacionSustentacion,
      this.GridGraduacionDiploma,
      this.GridGraduacionResponsable,

    ];
  }

  setDefaultData() {

  }

  setFieldAvailability() {
    if (this.id == 0) {
      this.FieldCodigo.invisible = true;
      this.FieldEstudiante.invisible = true;
      this.FieldEtapa.disabled = true;

    } else {
      this.FieldCodigo.invisible = true;
      this.FieldEstudiante.invisible = true;
      this.FieldEtapa.disabled = true;

    }
  }
}
