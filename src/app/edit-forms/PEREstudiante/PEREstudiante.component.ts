﻿import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { EditBaseComponent } from '../../shared/base/edit-base.component';
import { GridBaseComponent } from '../../shared/base/grid-base/grid-base.component';
import { XtFieldCellComponent } from '../../shared/controls/xt-field-cell/xt-field-cell.component';
import { DetalleParametroService } from '@svc/DetalleParametro.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { PEREstudianteService } from '@svc/PEREstudiante.service';
import { UBIPaisService } from '@svc/UBIPais.service';
import { UBIDepartamentoService } from '@svc/UBIDepartamento.service';
import { UBIProvinciaService } from '@svc/UBIProvincia.service';
import { UBIDistritoService } from '@svc/UBIDistrito.service';
import { MAESeccionService } from '@svc/MAESeccion.service';
import { MAESedeService } from '@svc/MAESede.service';
import { MAEProgramaService } from '@svc/MAEPrograma.service';
import { MAEMaestriaService } from '@svc/MAEMaestria.service';
@Component({
  selector: 'app-perestudiante-edit',
  templateUrl: './perestudiante.component.html',
  styleUrls: ['./perestudiante.component.css']
})
export class PEREstudianteEditComponent extends EditBaseComponent implements OnInit {

  constructor(
    public service: PEREstudianteService,
    protected router: Router,
    protected route: ActivatedRoute,
    public UBIPaisSvc: UBIPaisService,
    public UBIDepartamentoSvc: UBIDepartamentoService,
    public UBIProvinciaSvc: UBIProvinciaService,
    public UBIDistritoSvc: UBIDistritoService,
    public MAESeccionSvc: MAESeccionService,
    public MAESedeSvc: MAESedeService,
    public MAEProgramaSvc: MAEProgramaService,
    public MAEMaestriaSvc: MAEMaestriaService,
    public DetalleParametroSvc: DetalleParametroService
  ) {
    super();
  }
  //Codigo
  @ViewChild('FieldCodigo')
  public FieldCodigo: XtFieldCellComponent;
  //Nombre
  @ViewChild('FieldNombre')
  public FieldNombre: XtFieldCellComponent;
  //Descripcion
  @ViewChild('FieldDescripcion')
  public FieldDescripcion: XtFieldCellComponent;
  //FechaNacimiento
  @ViewChild('FieldFechaNacimiento')
  public FieldFechaNacimiento: XtFieldCellComponent;
  //Apellidos
  @ViewChild('FieldApellidos')
  public FieldApellidos: XtFieldCellComponent;
  //Sexo
  @ViewChild('FieldSexo')
  public FieldSexo: XtFieldCellComponent;
  //TipoDocumento
  @ViewChild('FieldTipoDocumento')
  public FieldTipoDocumento: XtFieldCellComponent;
  //NumeroDocumento
  @ViewChild('FieldNumeroDocumento')
  public FieldNumeroDocumento: XtFieldCellComponent;
  //Telefono
  @ViewChild('FieldTelefono')
  public FieldTelefono: XtFieldCellComponent;
  //Correo
  @ViewChild('FieldCorreo')
  public FieldCorreo: XtFieldCellComponent;
  //UbigeoNacimiento
  @ViewChild('FieldUbigeoNacimiento')
  public FieldUbigeoNacimiento: XtFieldCellComponent;
  //Pais
  @ViewChild('FieldPais')
  public FieldPais: XtFieldCellComponent;
  //Departamento
  @ViewChild('FieldDepartamento')
  public FieldDepartamento: XtFieldCellComponent;
  //Provincia
  @ViewChild('FieldProvincia')
  public FieldProvincia: XtFieldCellComponent;
  //Distrito
  @ViewChild('FieldDistrito')
  public FieldDistrito: XtFieldCellComponent;
  //Direccion
  @ViewChild('FieldDireccion')
  public FieldDireccion: XtFieldCellComponent;
  //Seccion
  @ViewChild('FieldSeccion')
  public FieldSeccion: XtFieldCellComponent;
  //Sede
  @ViewChild('FieldSede')
  public FieldSede: XtFieldCellComponent;
  //NombrePrograma
  @ViewChild('FieldNombrePrograma')
  public FieldNombrePrograma: XtFieldCellComponent;
  //Modalidad
  @ViewChild('FieldModalidad')
  public FieldModalidad: XtFieldCellComponent;
  //PeriodoAcademico
  @ViewChild('FieldPeriodoAcademico')
  public FieldPeriodoAcademico: XtFieldCellComponent;
  //PlanEstudios
  @ViewChild('FieldPlanEstudios')
  public FieldPlanEstudios: XtFieldCellComponent;
  //Sector
  @ViewChild('FieldSector')
  public FieldSector: XtFieldCellComponent;
  //AreaEmpresa
  @ViewChild('FieldAreaEmpresa')
  public FieldAreaEmpresa: XtFieldCellComponent;
  //NombreEmpresa
  @ViewChild('FieldNombreEmpresa')
  public FieldNombreEmpresa: XtFieldCellComponent;
  //Profesion
  @ViewChild('FieldProfesion')
  public FieldProfesion: XtFieldCellComponent;
  //Cargo
  @ViewChild('FieldCargo')
  public FieldCargo: XtFieldCellComponent;
  //Deuda
  @ViewChild('FieldDeuda')
  public FieldDeuda: XtFieldCellComponent;
  //Moneda
  @ViewChild('FieldMoneda')
  public FieldMoneda: XtFieldCellComponent;
  //Maestria
  @ViewChild('FieldMaestria')
  public FieldMaestria: XtFieldCellComponent;

  //PROProcesoGraduacion
  @ViewChild('GridPROProcesoGraduacion')
  public GridPROProcesoGraduacion : GridBaseComponent;
  //EstudianteDetalleDeuda
  @ViewChild('GridEstudianteDetalleDeuda')
  public GridEstudianteDetalleDeuda : GridBaseComponent;
  //Integrante
  @ViewChild('GridIntegrante')
  public GridIntegrante : GridBaseComponent;
  
  ngOnInit(): void {
    super.ngOnInit();
  }
  ngAfterViewInit() {
    super.ngAfterViewInit();
    super.FillDropDown(this.FieldPais, this.UBIPaisSvc.selAll());
    super.FillDropDown(this.FieldDepartamento, this.UBIDepartamentoSvc.selAll());
    super.FillDropDown(this.FieldProvincia, this.UBIProvinciaSvc.selAll());
    super.FillDropDown(this.FieldDistrito, this.UBIDistritoSvc.selAll());
    super.FillDropDown(this.FieldSeccion, this.MAESeccionSvc.selAll());
    super.FillDropDown(this.FieldSede, this.MAESedeSvc.selAll());
    super.FillDropDown(this.FieldNombrePrograma, this.MAEProgramaSvc.selAll());
    super.FillDropDown(this.FieldMaestria, this.MAEMaestriaSvc.selAll());
    super.FillDropDown(this.FieldSexo, this.DetalleParametroSvc.selxParametro(this.FieldSexo.listid));
    super.FillDropDown(this.FieldTipoDocumento, this.DetalleParametroSvc.selxParametro(this.FieldTipoDocumento.listid));
    super.FillDropDown(this.FieldModalidad, this.DetalleParametroSvc.selxParametro(this.FieldModalidad.listid));
    super.FillDropDown(this.FieldMoneda, this.DetalleParametroSvc.selxParametro(this.FieldMoneda.listid));
  }

  getFormFields() : Array<XtFieldCellComponent> {
  	return [
      this.FieldCodigo,
      this.FieldNombre,
      this.FieldDescripcion,
      this.FieldFechaNacimiento,
      this.FieldApellidos,
      this.FieldSexo,
      this.FieldTipoDocumento,
      this.FieldNumeroDocumento,
      this.FieldTelefono,
      this.FieldCorreo,
      this.FieldUbigeoNacimiento,
      this.FieldPais,
      this.FieldDepartamento,
      this.FieldProvincia,
      this.FieldDistrito,
      this.FieldDireccion,
      this.FieldSeccion,
      this.FieldSede,
      this.FieldNombrePrograma,
      this.FieldModalidad,
      this.FieldPeriodoAcademico,
      this.FieldPlanEstudios,
      this.FieldSector,
      this.FieldAreaEmpresa,
      this.FieldNombreEmpresa,
      this.FieldProfesion,
      this.FieldCargo,
      this.FieldDeuda,
      this.FieldMoneda,
      this.FieldMaestria,

    ];
  }

  getGridFields(): Array<GridBaseComponent> {
    return [
      this.GridPROProcesoGraduacion,
      this.GridEstudianteDetalleDeuda,
      this.GridIntegrante,

    ];
  }

  setDefaultData() {

  }

  setFieldAvailability() {
    if (this.id == 0) {

    } else {

    }
  }
}
