import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PEREstudianteEditComponent } from './PEREstudiante.component';

describe('PEREstudianteEditComponent', () => {
  let component: PEREstudianteEditComponent;
  let fixture: ComponentFixture<PEREstudianteEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PEREstudianteEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PEREstudianteEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
