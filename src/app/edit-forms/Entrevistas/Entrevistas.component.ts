import { Component, Inject, OnInit, LOCALE_ID } from '@angular/core';
import {
  MAT_MOMENT_DATE_FORMATS,
  MomentDateAdapter,
  MAT_MOMENT_DATE_ADAPTER_OPTIONS,
} from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
//import { GridBaseComponent } from '@shared/base/grid-base/grid-base.component';
import { ActivatedRoute } from '@angular/router';
import { RequestsService } from '@app/svc/services/Requests.service';
import { InterviewService } from '@app/svc/services/Interview.service';
import * as moment from 'moment';
import { AuthService } from '@app/auth/services/auth.service';
import { UcProfile } from '@app/auth/models/UcProfile';
import { MatDialog } from '@angular/material/dialog';
import { DialogEntrevistaEditComponent } from '../dialog-entrevista/dialog-entrevista.component';
import { ENTFechaService } from '../../svc/services/ENTFecha.service';

import localeEs from '@angular/common/locales/es';
import { registerLocaleData } from '@angular/common';
import { ENTEntrevistaService } from '../../svc/services/ENTEntrevista.service';
import { GlobalsComponent } from '../../globals.component';
import { InjectorInstance } from '../../app.module';
import { DialogConfirmEditComponent } from '../dialog-confirm/dialog-confirm.component';
registerLocaleData(localeEs, 'es');


export const MYFormat = {
  parse: {
    dateInput: 'DD/MM/YYYY'
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMMM Y',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM Y'
  }
};

@Component({
  selector: 'app-entrevistas',
  templateUrl: './entrevistas.component.html',
  styleUrls: ['./entrevistas.component.sass'],
  providers: [
    //{ provide: MAT_DATE_LOCALE, useValue: 'es-GB' },
    { provide: LOCALE_ID, useValue: 'es' },
    { provide: MAT_DATE_FORMATS, useValue: MYFormat },
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
    },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
  ],
})
export class EntrevistasComponent implements OnInit {
  selected: Date | null = new Date(new Date().setHours(0, 0, 0, 0));
  today: Date = new Date();
  data: any[] = []
  groupDate: any[] = []
  public datEntrevistas: any[] = [];
  public fechaActual: any = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');


  protected globals: GlobalsComponent = InjectorInstance.get(GlobalsComponent);
  constructor(
    public service: ENTFechaService,
    private readonly authService: AuthService,
    public entrevistaSvc: ENTEntrevistaService,
    protected route: ActivatedRoute,
    private _adapter: DateAdapter<any>,
    @Inject(MAT_DATE_LOCALE) private _locale: string,
    public dialog: MatDialog,
  ) { }

  ngOnInit(): void {
    this.service.selByFilters({ "Search": this.selected }).subscribe((dat: any) => {
      this.data = dat;
    })
    this.entrevistaSvc.selAll().subscribe((dat: any) => {
      console.log("dat entrevistas", dat)
      this.datEntrevistas = dat?.filter((d: any) =>
        d.Fecha_text > this.fechaActual
      );
      if (this.datEntrevistas.length > 0) {
        this.datEntrevistas.forEach((dat: any) => {
          let fecha = new Date(new Date(dat.Fecha_text).setHours(0, 0, 0, 0));
          //let dateDesc = dat.Fecha_text.split(' ')[0].split('/')
          //let fecha = new Date(parseInt(dateDesc[2]), parseInt(dateDesc[1]), parseInt(dateDesc[0]), 0, 0, 0);
          if (this.groupDate.find(element => element.getTime() == fecha.getTime()) == undefined)
            this.groupDate.push(fecha)
        })
        console.log('this.groupDate', this.groupDate);

      }
      console.log('this.datSolicitudes', this.datEntrevistas);
    });
  }

  agruparFechas(fechaGrupo: Date, fechaItem: Date) {
    let onlyfechagrupo = moment(fechaGrupo).format('YYYY-MM-DD')
    let onlyfechaitem = moment(fechaItem).format('YYYY-MM-DD')
    console.log("onlyfechagrupo.toString() == onlyfechaitem.toString()", onlyfechagrupo.toString() == onlyfechaitem.toString())
    return onlyfechagrupo.toString() == onlyfechaitem.toString()
  }

  dateFilter = (date: Date) => {
    const currentDate = new Date();
    currentDate.setHours(0, 0, 0, 0); // Establecer horas, minutos, segundos y milisegundos a cero para comparación
    return date >= currentDate; // Devuelve true si la fecha es igual o posterior a la fecha actual
  };

  onSelect(event: any) {
    this.service.selByFilters({ "Search": event }).subscribe((dat: any) => {
      this.data = dat;
    })
  }

  deleteFecha(event: any, fecha: any) {
    let dialogRef = this.dialog.open(DialogConfirmEditComponent
      , {
        disableClose: true,
        data: { },
        height: '250px',
        width: '400px',
      }
    );
    dialogRef.afterClosed().subscribe(result => {
      if (result.flag) { //true
        this.entrevistaSvc.selxFecha(fecha.Id).subscribe((dat: any) => {
          if (dat.length == 0) {
            this.service.del(fecha.Id, null).subscribe((dat: any) => {
              console.log("delete")
              this.service.selByFilters({ "Search": this.selected }).subscribe((dat: any) => {
                this.data = dat;
              })
            })
          } else {
            this.globals.notify(`No se puede eliminar esta fecha porque tiene ${dat.length} entrevista(s) enlazadas`)
          }
        })
      }
    });


  }

  addFecha($event: any) {
    let dialogRef = this.dialog.open(DialogEntrevistaEditComponent
      , {
        disableClose: true,
        data: {
          "select": new Date(this.selected)
        }
        //height: '400px',
        //width: '600px',
      }
    );
    dialogRef.afterClosed().subscribe(result => {
      if (result.flag) { //true
        this.service.selByFilters({ "Search": this.selected }).subscribe((dat: any) => {
          console.log("fecha dat", dat)
          this.data = dat;
        })
      }
    });
  }
}
