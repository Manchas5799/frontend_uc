import { Component, OnInit, Inject, ViewChild, ElementRef, ViewContainerRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import * as moment from 'moment';
import { GlobalsComponent } from '../../globals.component';
import { XtFieldCellComponent } from '../../shared/controls/xt-field-cell/xt-field-cell.component';
import { ENTFechaService } from '../../svc/services/ENTFecha.service';
import { IntegranteEditComponent } from '../Integrante/Integrante.component';

@Component({
  selector: 'app-dialog-entrevista',
  templateUrl: './dialog-entrevista.component.html',
  styleUrls: ['./dialog-entrevista.component.css']
})
export class DialogEntrevistaEditComponent implements OnInit {

  //Codigo
  @ViewChild('FieldHoraInicio')
  public FieldHoraInicio: XtFieldCellComponent;
  //Codigo
  @ViewChild('FieldHoraFin')
  public FieldHoraFin: XtFieldCellComponent;

  public select: Date;
  constructor(
    public service: ENTFechaService,
    public dialogRef: MatDialogRef<DialogEntrevistaEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public globals: GlobalsComponent,
    private vref: ViewContainerRef
  ) {
  }

  @ViewChild('ref', { static: true, read: ViewContainerRef }) vcRef: ViewContainerRef;

  ngOnInit(): void {
    this.select = this.data.select;

  }

  ngAfterViewInit() {
    //super.ngafterviewinit();
    console.log("dialog data", this.data)
    //console.log("dialog select", this.select)
  }

  cerrar() {
    console.log("dialog")
    this.dialogRef.close({ flag: false });
  }

  onBlur(event: any) {
    console.log(this.FieldHoraInicio.value)
    var addMlSeconds = 15 * 60000;
    let HoraFin = new Date(this.FieldHoraInicio.value.getTime() + addMlSeconds)
    this.FieldHoraFin.value = HoraFin
    console.log(HoraFin)
  }


  guardar(): any {
    console.log(this.FieldHoraInicio.value)
    console.log(this.select.toLocaleDateString())
    if (this.FieldHoraInicio.value == '' || this.FieldHoraInicio.value == null || this.FieldHoraInicio.value == undefined) return this.globals.notify("Debe ingresar una hora válida");
    var mom = moment(this.select.toLocaleDateString() + ' ' + this.FieldHoraInicio.value.toLocaleTimeString(), 'DD/MM/YYYY HH:mm:ss');

    //let newValue = new Date(this.FieldHoraInicio.value.getTime())
    var values: Object = {
      FechaEntrevista: mom.toDate(),
      HoraEntrevista: mom.toDate(),
    };
    this.service.ins(values).subscribe((dat: any) => {
      console.log(dat)
      if(dat.Id)
        this.dialogRef.close({ flag: true });
      else
        this.dialogRef.close({ flag: false });
    })
    //this.dialogRef.close({ flag: true });
  }

}
