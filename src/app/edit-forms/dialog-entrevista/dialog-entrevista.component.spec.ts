import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogEntrevistaEditComponent } from './dialog-entrevista.component';

describe('DialogEntrevistaEditComponent', () => {
  let component: DialogEntrevistaEditComponent;
  let fixture: ComponentFixture<DialogEntrevistaEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DialogEntrevistaEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogEntrevistaEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
