import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogIntegranteEditComponent } from './dialog-integrante.component';

describe('DialogIntegranteComponent', () => {
  let component: DialogIntegranteEditComponent;
  let fixture: ComponentFixture<DialogIntegranteEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DialogIntegranteEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogIntegranteEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
