import { Component, OnInit, Inject, ViewChild, ElementRef, ViewContainerRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { GlobalsComponent } from '../../globals.component';
import { IntegranteEditComponent } from '../Integrante/Integrante.component';

@Component({
  selector: 'app-dialog-integrante',
  templateUrl: './dialog-integrante.component.html',
  styleUrls: ['./dialog-integrante.component.css']
})
export class DialogIntegranteEditComponent implements OnInit {
  
  constructor(
    public dialogRef: MatDialogRef<DialogIntegranteEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public globals: GlobalsComponent,
    private vref: ViewContainerRef
  ) {
  }

  @ViewChild('ref', { static: true, read: ViewContainerRef }) vcRef: ViewContainerRef;

  ngAfterViewInit() {
    //super.ngafterviewinit();
    console.log("dialog data",this.data)
  }
  
  cerrar() {
    console.log("dialog")
    this.dialogRef.close({ flag: false });
  }
   
  ngOnInit(): void {


  }

}
