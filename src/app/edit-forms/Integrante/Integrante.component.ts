import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { EditBaseComponent } from '../../shared/base/edit-base.component';
import { GridBaseComponent } from '../../shared/base/grid-base/grid-base.component';
import { XtFieldCellComponent } from '../../shared/controls/xt-field-cell/xt-field-cell.component';
import { DetalleParametroService } from '@svc/DetalleParametro.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { IntegranteService } from '@svc/Integrante.service';
import { PEREstudianteService } from '@svc/PEREstudiante.service';
import { PROProcesoGraduacionService } from '@svc/PROProcesoGraduacion.service';
import { MAESeccionService } from '@svc/MAESeccion.service';
import { MAESedeService } from '@svc/MAESede.service';
import { MAEMaestriaService } from '@svc/MAEMaestria.service';
@Component({
  selector: 'app-integrante-edit',
  templateUrl: './integrante.component.html',
  styleUrls: ['./integrante.component.css']
})
export class IntegranteEditComponent extends EditBaseComponent implements OnInit {

  constructor(
    public service: IntegranteService,
    protected router: Router,
    protected route: ActivatedRoute,
    public PEREstudianteSvc: PEREstudianteService,
    public PROProcesoGraduacionSvc: PROProcesoGraduacionService,
    public MAESeccionSvc: MAESeccionService,
    public MAESedeSvc: MAESedeService,
    public MAEMaestriaSvc: MAEMaestriaService,
    public DetalleParametroSvc: DetalleParametroService
  ) {
    super();
  }
  //Codigo
  @ViewChild('FieldEstudiante')
  public FieldEstudiante: XtFieldCellComponent;
  //PROGraduacion
  @ViewChild('FieldPROGraduacion')
  public FieldPROGraduacion: XtFieldCellComponent;
  //Seccion
  @ViewChild('FieldSeccion')
  public FieldSeccion: XtFieldCellComponent;
  //Nombre
  @ViewChild('FieldNombre')
  public FieldNombre: XtFieldCellComponent;
  //Apellidos
  @ViewChild('FieldApellidos')
  public FieldApellidos: XtFieldCellComponent;
  //Sede
  @ViewChild('FieldSede')
  public FieldSede: XtFieldCellComponent;
  //NombreMaestria
  @ViewChild('FieldNombreMaestria')
  public FieldNombreMaestria: XtFieldCellComponent;
  //PeriodoAcademico
  @ViewChild('FieldPeriodoAcademico')
  public FieldPeriodoAcademico: XtFieldCellComponent;
  ////Estudiante
  //@ViewChild('FieldEstudiante')
  //public FieldEstudiante: XtFieldCellComponent;
  //Modalidad
  @ViewChild('FieldModalidad')
  public FieldModalidad: XtFieldCellComponent;
  ////Graduacionvista
  //@ViewChild('FieldGraduacionvista')
  //public FieldGraduacionvista: XtFieldCellComponent;


  ngOnInit(): void {
    super.ngOnInit();
  }
  ngAfterViewInit() {
    this.FieldPROGraduacion.invisible = true;
    super.ngAfterViewInit();
    super.FillDropDown(this.FieldEstudiante, this.PEREstudianteSvc.selAll());
    super.FillDropDown(this.FieldPROGraduacion, this.PROProcesoGraduacionSvc.selAll());
    super.FillDropDown(this.FieldSeccion, this.MAESeccionSvc.selAll());
    super.FillDropDown(this.FieldSede, this.MAESedeSvc.selAll());
    super.FillDropDown(this.FieldNombreMaestria, this.MAEMaestriaSvc.selAll());
    //super.FillDropDown(this.FieldEstudiante, this.PEREstudianteSvc.selAll());
    //super.FillDropDown(this.FieldGraduacionvista, this.PROProcesoGraduacionSvc.selAll());
  }
  searchEstudiante(event: any): any {
    console.log("event", event.value)
    if (event.value) {
      this.PEREstudianteSvc.sel(event.value).subscribe((dat: any) => {
        console.log('dat', dat)
        this.FieldPROGraduacion.value = dat.PROGraduacion;
        this.FieldSeccion.value = dat.Seccion;
        this.FieldNombre.value = dat.Nombre;
        this.FieldApellidos.value = dat.Apellidos;
        this.FieldSede.value = dat.Sede;
        this.FieldNombreMaestria.value = dat.Maestria;
        this.FieldPeriodoAcademico.value = dat.PeriodoAcademico;
        this.FieldEstudiante.value = event.value;
        this.FieldEstudiante.value = event.value;
        this.FieldModalidad.value = dat.Modalidad;
      })

    } else {
      this.FieldPROGraduacion.value = null;
      this.FieldSeccion.value = null;
      this.FieldNombre.value = null;
      this.FieldApellidos.value = null;
      this.FieldSede.value = null;
      this.FieldNombreMaestria.value = null;
      this.FieldPeriodoAcademico.value = null;
      this.FieldEstudiante.value = null;
      this.FieldModalidad.value = null;
    }
  }
  getFormFields(): Array<XtFieldCellComponent> {
    return [
      this.FieldEstudiante,
      this.FieldPROGraduacion,
      this.FieldSeccion,
      this.FieldNombre,
      this.FieldApellidos,
      this.FieldSede,
      this.FieldNombreMaestria,
      this.FieldPeriodoAcademico,
      this.FieldEstudiante,
      this.FieldModalidad,
      //this.FieldGraduacionvista,

    ];
  }

  getGridFields(): Array<GridBaseComponent> {
    return [

    ];
  }

  setDefaultData() {

  }

  setFieldAvailability() {
    //if (this.id == 0) {
    //  this.FieldPROGraduacion.invisible = true;

    //} else {
    //  this.FieldPROGraduacion.invisible = true;

    //}
  }
}
