import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntegranteEditComponent } from './Integrante.component';

describe('IntegranteComponent', () => {
  let component: IntegranteEditComponent;
  let fixture: ComponentFixture<IntegranteEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IntegranteEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntegranteEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
