import { Component, OnInit, Inject, ViewChild, ElementRef, ViewContainerRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import * as moment from 'moment';
import { GlobalsComponent } from '../../globals.component';
import { XtFieldCellComponent } from '../../shared/controls/xt-field-cell/xt-field-cell.component';
import { ENTFechaService } from '../../svc/services/ENTFecha.service';
import { IntegranteEditComponent } from '../Integrante/Integrante.component';

@Component({
  selector: 'app-dialog-confirm',
  templateUrl: './dialog-confirm.component.html',
  styleUrls: ['./dialog-confirm.component.css']
})
export class DialogConfirmEditComponent implements OnInit {

  public select: Date;
  constructor(
    public service: ENTFechaService,
    public dialogRef: MatDialogRef<DialogConfirmEditComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public globals: GlobalsComponent,
    private vref: ViewContainerRef
  ) {
  }

  ngOnInit(): void {

  }

  ngAfterViewInit() {
  }

  cerrar() {
    console.log("dialog")
    this.dialogRef.close({ flag: false });
  }

  guardar() {
    console.log("dialog")
    this.dialogRef.close({ flag: true });
  }

}
