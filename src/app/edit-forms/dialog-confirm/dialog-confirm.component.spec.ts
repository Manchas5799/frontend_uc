import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogConfirmEditComponent } from './dialog-confirm.component';

describe('DialogConfirmEditComponent', () => {
  let component: DialogConfirmEditComponent;
  let fixture: ComponentFixture<DialogConfirmEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DialogConfirmEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogConfirmEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
