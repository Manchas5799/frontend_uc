export enum RolSegEnum {
  Consultora = 80,
  Titular = 74,
  Administrador = 81
}

export interface ILoginResponse {
  SId: string;
  User: string;
  FullName: string;
  Email: string;
  Cliente: string;
  TipoSite: string;
  TipoUser: string;
  IP: string;
}
