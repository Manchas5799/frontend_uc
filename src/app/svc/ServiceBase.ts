import { Injectable } from "@angular/core";
import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { Observable, of, throwError } from "rxjs";
import { InjectorInstance } from "@app/app.module";
//import { AuthService } from "@app/core/security/auth.service";
//import { GlobalsComponent } from "../globals.component";
import { catchError } from "rxjs/operators";
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: "root",
})
export class ServiceBase {

  //public accessPointUrl: string = "api/v1/";
  public accessPointUrl: string = `${environment.apiUrl}/api/v1/`;
  protected apiPrefix: string;
  protected http: HttpClient;
  //protected authService: AuthService;
  //protected globals: GlobalsComponent;

  constructor() {
    //if (!this.authService) {
    //  this.authService = InjectorInstance.get(AuthService);
    //}
    //if (!this.globals) {
    //  this.globals = InjectorInstance.get(GlobalsComponent);
    //}
  }

  private getHeaders(): HttpHeaders {
    return null;
    let httpHeaders = new HttpHeaders();
    //let sesionDecodificada = this.authService.idSesionDeUsuario;

    //return httpHeaders.set('sessionid', sesionDecodificada);
  }

  protected get(urlPostfix: string = null): Observable<Object> {
    console.log("llamada back " + this.accessPointUrl +
      this.apiPrefix +
      (urlPostfix == null ? "" : "/" + urlPostfix));

    return this.http.get(
      this.accessPointUrl +
      this.apiPrefix +
      (urlPostfix == null ? "" : "/" + urlPostfix),
      { headers: this.getHeaders() }
    );
  }

  public post(urlPostfix: string = null, body: any = null): Observable<Object> {

    return this.http.post(
      this.accessPointUrl +
      this.apiPrefix +
      (urlPostfix == null ? "" : "/" + urlPostfix),
      body,
      { headers: this.getHeaders() }
    );
  }

  protected put(urlPostfix: string = null, body: any = null): Observable<Object> {
    return this.http.put(
      this.accessPointUrl +
      this.apiPrefix +
      (urlPostfix == null ? "" : "/" + urlPostfix),
      body,
      { headers: this.getHeaders() }
    ).pipe(
      catchError(error => {
        let errorMsg: string;
        if (error.error instanceof ErrorEvent) {
          errorMsg = `Error: ${error.error.message}`;
        } else {
          errorMsg = this.getServerErrorMessage(error);
        }
        return throwError(errorMsg);
      })
    );

  }
  private getServerErrorMessage(error: HttpErrorResponse): string {
    switch (error.status) {
      case 404: {
        return `Not Found: ${error.message}`;
      }
      case 403: {
        return `Access Denied: ${error.message}`;
      }
      case 500: {
        return `Internal Server Error: ${error.message}`;
      }
      default: {
        return `Unknown Server Error: ${error.message}`;
      }

    }
  }
  private delete(urlPostfix: string = null, body: any = null): Observable<Object> {
    return this.http.delete(
      this.accessPointUrl +
      this.apiPrefix +
      (urlPostfix == null ? "" : "/" + urlPostfix),
      { headers: this.getHeaders() }
    );
  }

  public getByText(attribute: string, valor: string): Observable<Object> {
    return this.get(`by${attribute}/${valor}`);
  }
  public getBy(attribute: string, id: number): Observable<Object> {
    return this.get(`by${attribute}/${id}`);
  }
  public getTieneAcceso(usuario: string): Observable<Object> {
    return this.get(`byTieneAcceso/${usuario}`);
  }
  public getValidarUser(obj: any): Observable<Object> {
    if (!(obj instanceof FormData)) {
      obj = this.toForm(obj);
    }
    return this.http.post(
      this.accessPointUrl +
      this.apiPrefix + "/getLogin",
      obj,
      { headers: this.getHeaders() }
    );
  }

  public selVisiblexReactivacion(idDeclaracion: number, idAnexo: number, idUsuario: string): Observable<Object> {
    //return this.get(`visiblexReactivacion/${idDeclaracion}/${idAnexo}/${idUsuario}`);
    return this.http.get(
      this.accessPointUrl +
      this.apiPrefix + `/visiblexReactivacion/${idDeclaracion}/${idAnexo}/${idUsuario}`,
      { headers: this.getHeaders() }
    );
  }

  public sel(id: number) {
    return this.get(id.toString());
  }

  public selOrEmpty(id: number) {
    return this.get(`selorempty/${id}`);
  }

  public selAll(): Observable<Object> {
    return this.get();
  }

  public selByFilters(obj: any): Observable<Object> {
    if (!(obj instanceof FormData)) {
      obj = this.toForm(obj);
    }
    return this.post("byFilters", obj);
  }

  public ins(obj: any): Observable<Object> {
    if (!(obj instanceof FormData)) {
      obj = this.toForm(obj);
    }
    //obj.append('AudUser', this.globals.currentUser.User);
    return this.post(null, obj);
  }

  public del(id: number, audUser: string): Observable<Object> {
    return this.delete(`${id}/${audUser}`);
  }

  public delByKey(keyName: string, keyValue: number, audUser: string): Observable<Object> {
    return this.delete(`delByKey/${keyName}/${keyValue}/${audUser}`);
  }

  public upd(obj: any): Observable<Object> {
    if (!(obj instanceof FormData)) {
      obj = this.toForm(obj);
    }
    //obj.append('AudUser', this.globals.currentUser.User);
    return this.put(null, obj);
  }

  public updClaveUser(obj: any) {
    //obj.AudUser = this.globals.currentUser.User;
    if (!(obj instanceof FormData)) {
      obj = this.toForm(obj);
    }
    return this.put('updClave', obj);
  }

  public deleteFile(id: any, idRegistro: any, idCampo: any) {
    return this.http.get(this.accessPointUrl +
      this.apiPrefix + '/delete/' + id + '/' + idRegistro + '/' + idCampo); //cambiar a interplacion
  }

  public updBatch(objects: Array<any>, attributes: Array<string>, files: Array<any> = [], fileIndexes: Array<number> = [], fileField: string = null): Observable<Object> {
    //objects.forEach(e => {
    //  e.AudUser = this.globals.currentUser.User;
    //  e.AudIp = this.globals.currentUser.IP;
    //});
    let obj = this.toForm(
      {
        entItems: objects,
        attributes: attributes,
        files: files,
        fileIndexes: fileIndexes,
        fileField: fileField
      }
    );

    return this.put('batchupd', obj);
  }

  public toForm(values: any) {
    var resp = new FormData();
    var keys = Object.keys(values);

    for (var i = 0; i <= keys.length - 1; i++) {
      let key = keys[i];
      let val = values[key];
      if (val === null) continue;

      if (Array.isArray(val)) {
        val.forEach((item, index) => {
          if (item instanceof Object && !(item instanceof File)) {
            Object.keys(item).forEach(oKey => {
              if (item[oKey] !== undefined && item[oKey] !== null) //agregar validacion para guardar 0 (|| item[oKey] === 0)

                resp.append(`${key}[${index}][${oKey}]`, item[oKey]);
            });
          } else if (item instanceof File) {
            resp.append(`${key}`, item);

          } else {
            resp.append(`${key}[${index}]`, item);
          }
        });

      } else {
        resp.append(key, val);
      }
    }

    return resp;
  }

  public insAnexoXPNTConcesion(obj: any): Observable<Object> {
    if (!(obj instanceof FormData)) {
      obj = this.toForm(obj);
    }
    return this.post("anexoByConcesion", obj);
  }

  public insAnexoXISOConcesion(obj: any): Observable<Object> {
    if (!(obj instanceof FormData)) {
      obj = this.toForm(obj);
    }
    return this.post("anexoByISOConcesion", obj);
  }

  public insVentaXCargaMasiva(obj: any): Observable<Object> {
    if (!(obj instanceof FormData)) {
      obj = this.toForm(obj);
    }

    return this.post("CargaMasiva", obj);
  }

  public getSessionInfo(): Observable<Object> {

    return this.http.get(
      this.accessPointUrl +
      this.apiPrefix +
      "/getSessionInfo",
      { headers: this.getHeaders() });
  }

  public login(usuario: any): Observable<Object> {
    return this.http.get(
      this.accessPointUrl +
      this.apiPrefix +
      "/usuario/" + usuario,
      { headers: this.getHeaders() });
  }

  protected getConsolidado(urlPostfix: string = null, filename: string, type: string, body: any) {

    return this.http.post(
      this.accessPointUrl +
      this.apiPrefix +
      (urlPostfix == null ? "" : "/" + urlPostfix),
      body,
      {
        headers: this.getHeaders(),
        responseType: 'arraybuffer'
      })
    //.subscribe((response: any) => {
    //  const binaryData = [];
    //  binaryData.push(response);
    //  let downloadLink = document.createElement('a');
    //  downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, { type: type }));
    //  //if (filename)
    //  //  downloadLink.setAttribute('download', filename);
    //  document.body.appendChild(downloadLink);
    //  downloadLink.click();
    //});


    //console.log(`***GET MDS: ${this.apiPrefix} (${urlPostfix})`);
    //console.log("Llamada de get", this.accessPointUrl +
    //  this.apiPrefix +
    //  (urlPostfix == null ? "" : "/" + urlPostfix), new Date());

    //this.http.get(this.accessPointUrl + this.apiPrefix + (urlPostfix == null ? "" : "/" + urlPostfix), {
    //  headers: this.getHeaders(),
    //  responseType: 'arraybuffer'
    //}).subscribe((response: any) => {
    //  const binaryData = [];
    //  binaryData.push(response);
    //  let downloadLink = document.createElement('a');
    //  downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, { type: type }));
    //  if (filename)
    //    downloadLink.setAttribute('download', filename);
    //  document.body.appendChild(downloadLink);
    //  downloadLink.click();
    //});
  }

  protected getDescargarArchivo(urlPostfix: string = null, filename: string, type: string): void {
    console.log(`***GET MDS: ${this.apiPrefix} (${urlPostfix})`);
    console.log("Llamada de get", this.accessPointUrl +
      this.apiPrefix +
      (urlPostfix == null ? "" : "/" + urlPostfix), new Date());

    this.http.get(this.accessPointUrl + this.apiPrefix + (urlPostfix == null ? "" : "/" + urlPostfix), {
      headers: this.getHeaders(),
      responseType: 'arraybuffer'
    }).subscribe((response: any) => {
      const binaryData = [];
      binaryData.push(response);
      let downloadLink = document.createElement('a');
      downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, { type: type }));
      if (filename)
        downloadLink.setAttribute('download', filename);
      document.body.appendChild(downloadLink);
      downloadLink.click();
    });
  }

  //public selPlantilla(codInterfaz: string, filename: string, type: string) {
  //  return this.http.get(this.accessPointUrl +
  //    this.apiPrefix + `/Plantilla/${codInterfaz}`
  //    , {
  //    headers: this.getHeaders()
  //      ,
  //    responseType: 'arraybuffer'
  //  }).subscribe((response: any) => {
  //    console.log("plantilla response", response);
  //    const binaryData = [];
  //    binaryData.push(response);
  //    let downloadLink = document.createElement('a');
  //    downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, { type: type }));
  //    //if (filename)
  //      downloadLink.setAttribute('download', response.NombrePlantilla);
  //    document.body.appendChild(downloadLink);
  //    downloadLink.click();
  //  });
  //}
  public getCodInterfazBase(idAnexo: number) {
    return this.http.get(
      this.accessPointUrl +
      this.apiPrefix + `/codInterfaz/${idAnexo}`
      ,
      { headers: this.getHeaders() }
    );
  }
  public selPlantilla(codInterfaz: string, filename: string, type: string) {
    return this.http.get(this.accessPointUrl +
      this.apiPrefix + `/Plantilla/${codInterfaz}`
      , {
        headers: this.getHeaders()
        ,
        responseType: 'arraybuffer'
      }).subscribe((response: any) => {
        this.http.get(this.accessPointUrl +
          this.apiPrefix + `/dataPlantilla/${codInterfaz}`
          , {
            headers: this.getHeaders()
          }).subscribe((responseData: any) => {
            if (responseData.length > 0) {
              console.log("plantilla response", response);
              const binaryData = [];
              binaryData.push(response);
              let downloadLink = document.createElement('a');
              downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, { type: type }));
              //if (filename)
              var filename = responseData[0].Documento.split("[||]");
              downloadLink.setAttribute('download', filename[0]);
              document.body.appendChild(downloadLink);
              downloadLink.click();
            }
          });
      });
  }
  public selInformacionEstamin(codInterfaz: string, type: string, idPantalla: number, nombrePaquete: string, nombreEntidad: string) {
    return this.http.get(this.accessPointUrl +
      this.apiPrefix + `/InformacionEstamin/${nombrePaquete}/${nombreEntidad}/${codInterfaz}/${idPantalla}`
      , {
        headers: this.getHeaders()
        ,
        responseType: 'arraybuffer'
      }).subscribe((response: any) => {
        this.http.get(this.accessPointUrl +
          this.apiPrefix + `/dataInformacionEstamin/${nombrePaquete}/${nombreEntidad}/${codInterfaz}/${idPantalla}`
          , {
            headers: this.getHeaders()
          }).subscribe((responseData: any) => {
            if (responseData.length > 0) {
              console.log("informacion estamin response", response);
              const binaryData = [];
              binaryData.push(response);
              let downloadLink = document.createElement('a');
              downloadLink.href = window.URL.createObjectURL(new Blob(binaryData, { type: type }));
              //if (filename)
              var filename = responseData[0].Documento.split("[||]");
              downloadLink.setAttribute('download', filename[0]);
              document.body.appendChild(downloadLink);
              downloadLink.click();
            }
          });
      });
  }

  get entidad(): string {
    return this.apiPrefix;
  }

}
