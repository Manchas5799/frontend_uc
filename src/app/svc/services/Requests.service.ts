import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ServiceBase } from '../ServiceBase';

export enum RequestInterviewStatus {
  EntrevistaActiva = 115,
  EntrevistaDesactivada = 116,
}

@Injectable({
  providedIn: 'root',
})
export class RequestsService extends ServiceBase {
  protected apiPrefix: string = 'POSPostulacion';

  constructor(protected http: HttpClient) {
    super();
  }

  //Filtros por FK
  public selxPantallaPrograma(id: number) {
    return this.getBy('PantallaPrograma', id);
  }

  public selPersonasByTipo(filter: string) {
    return this.getByText('PersonasByTipo', filter);
  }

  public selxDeclaracion(id: number) {
    return this.getBy('Declaracion', id);
  }

  public selCounts() {
    console.log('selCounts', this.accessPointUrl + this.apiPrefix);
    return this.http.get(this.accessPointUrl + this.apiPrefix);
  }

  public updEstadoEntrevista({ id, estado }: { id: number; estado: number }) {
    return this.put('updStatusInterview', { Id: id, EstadoEntrevista: estado });
  }

  public updAsignar({ Id, AsignadoC }: { Id: any; AsignadoC: any }) {
    return this.put('updAsignar', { Id: Id, AsignadoC: AsignadoC });
  }

  public updAsignarRA({ Id, AsignadoRA }: { Id: any; AsignadoRA: any }) {
    return this.put('updAsignarRA', { Id: Id, AsignadoRA: AsignadoRA });
  }
}
