import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ServiceBase } from '../ServiceBase';

@Injectable({
  providedIn: 'root'
})
export class RequestsCountService extends ServiceBase {
  protected apiPrefix: string = 'POSPostulacionCounts';

  constructor(protected http: HttpClient) {
    super();
  }
	
	//Filtros por FK
  public selCounts() {
    console.log("selCounts", this.accessPointUrl + this.apiPrefix);
    return this.http.get(
      this.accessPointUrl +
      this.apiPrefix + '/byCounts'
    );
  }
  
}
