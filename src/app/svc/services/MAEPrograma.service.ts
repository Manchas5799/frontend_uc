﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ServiceBase } from '../ServiceBase';

@Injectable({
  providedIn: 'root'
})
export class MAEProgramaService extends ServiceBase {
  protected apiPrefix: string = 'MAEPrograma';

  constructor(protected http: HttpClient) {
    super();
  }
}
