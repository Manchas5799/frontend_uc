import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ServiceBase } from '../ServiceBase';

@Injectable({
  providedIn: 'root'
})
export class InterviewService extends ServiceBase {
  protected apiPrefix: string = 'ENTEntrevista';

  constructor(protected http: HttpClient) {
    super();
  }

  //Filtros por FK
  public selxPOSPostulacion(id: number) {
    return this.getBy('POSPostulacion', id);
  }
  public selxFecha(id: number) {
    return this.getBy('Fecha', id);
  }
}
