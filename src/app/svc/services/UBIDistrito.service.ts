﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ServiceBase } from '../ServiceBase';

@Injectable({
  providedIn: 'root'
})
export class UBIDistritoService extends ServiceBase {
  protected apiPrefix: string = 'UBIDistrito';

  constructor(protected http: HttpClient) {
    super();
  }
}
