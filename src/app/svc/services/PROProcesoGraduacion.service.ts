﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ServiceBase } from '../ServiceBase';

@Injectable({
  providedIn: 'root'
})
export class PROProcesoGraduacionService extends ServiceBase {
  protected apiPrefix: string = 'PROProcesoGraduacion';

  constructor(protected http: HttpClient) {
    super();
  }
	
	//Filtros por FK
  public selxEstudiante(id: number) {
    return this.getBy('Estudiante', id);
  }
}
