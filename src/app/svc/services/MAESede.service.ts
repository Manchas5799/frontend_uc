﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ServiceBase } from '../ServiceBase';

@Injectable({
  providedIn: 'root'
})
export class MAESedeService extends ServiceBase {
  protected apiPrefix: string = 'MAESede';

  constructor(protected http: HttpClient) {
    super();
  }
}
