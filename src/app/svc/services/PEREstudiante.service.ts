﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ServiceBase } from '../ServiceBase';

@Injectable({
  providedIn: 'root'
})
export class PEREstudianteService extends ServiceBase {
  protected apiPrefix: string = 'PEREstudiante';

  constructor(protected http: HttpClient) {
    super();
  }
	
	//Filtros por FK
  public selxPais(id: number) {
    return this.getBy('Pais', id);
  }
  public selxDepartamento(id: number) {
    return this.getBy('Departamento', id);
  }
  public selxProvincia(id: number) {
    return this.getBy('Provincia', id);
  }
  public selxDistrito(id: number) {
    return this.getBy('Distrito', id);
  }
  public selxSeccion(id: number) {
    return this.getBy('Seccion', id);
  }
  public selxSede(id: number) {
    return this.getBy('Sede', id);
  }
  public selxNombrePrograma(id: number) {
    return this.getBy('NombrePrograma', id);
  }
  public selxMaestria(id: number) {
    return this.getBy('Maestria', id);
  }
}
