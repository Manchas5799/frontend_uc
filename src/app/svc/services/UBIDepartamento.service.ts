﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ServiceBase } from '../ServiceBase';

@Injectable({
  providedIn: 'root'
})
export class UBIDepartamentoService extends ServiceBase {
  protected apiPrefix: string = 'UBIDepartamento';

  constructor(protected http: HttpClient) {
    super();
  }
}
