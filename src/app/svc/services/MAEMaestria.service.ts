﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ServiceBase } from '../ServiceBase';

@Injectable({
  providedIn: 'root'
})
export class MAEMaestriaService extends ServiceBase {
  protected apiPrefix: string = 'MAEMaestria';

  constructor(protected http: HttpClient) {
    super();
  }
}
