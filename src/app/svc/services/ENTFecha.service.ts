﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ServiceBase } from '../ServiceBase';

@Injectable({
  providedIn: 'root'
})
export class ENTFechaService extends ServiceBase {
  protected apiPrefix: string = 'ENTFecha';

  constructor(protected http: HttpClient) {
    super();
  }
}
