﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ServiceBase } from '../ServiceBase';

@Injectable({
  providedIn: 'root'
})
export class MAESeccionService extends ServiceBase {
  protected apiPrefix: string = 'MAESeccion';

  constructor(protected http: HttpClient) {
    super();
  }
}
