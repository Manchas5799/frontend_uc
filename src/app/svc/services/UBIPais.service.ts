﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ServiceBase } from '../ServiceBase';

@Injectable({
  providedIn: 'root'
})
export class UBIPaisService extends ServiceBase {
  protected apiPrefix: string = 'UBIPais';

  constructor(protected http: HttpClient) {
    super();
  }
}
