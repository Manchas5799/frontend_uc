import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ServiceBase } from '../ServiceBase';

@Injectable({
  providedIn: 'root'
})
export class DetalleParametroService extends ServiceBase {
  protected apiPrefix: string = 'DetalleParametro';

  constructor(protected http: HttpClient) {
    super();
  }
	
	//Filtros por FK
  public selxParametro(id: number) {
    return this.getBy('Parametro', id);
  }
}
