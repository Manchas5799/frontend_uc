﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ServiceBase } from '../ServiceBase';

@Injectable({
  providedIn: 'root'
})
export class UBIProvinciaService extends ServiceBase {
  protected apiPrefix: string = 'UBIProvincia';

  constructor(protected http: HttpClient) {
    super();
  }
}
