﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ServiceBase } from '../ServiceBase';

@Injectable({
  providedIn: 'root'
})
export class IntegranteService extends ServiceBase {
  protected apiPrefix: string = 'Integrante';

  constructor(protected http: HttpClient) {
    super();
  }
	
	//Filtros por FK
  public selxCodigo(id: number) {
    return this.getBy('Codigo', id);
  }
  public selxPROGraduacion(id: number) {
    return this.getBy('PROGraduacion', id);
  }
  public selxSeccion(id: number) {
    return this.getBy('Seccion', id);
  }
  public selxSede(id: number) {
    return this.getBy('Sede', id);
  }
  public selxNombreMaestria(id: number) {
    return this.getBy('NombreMaestria', id);
  }
  public selxEstudiante(id: number) {
    return this.getBy('Estudiante', id);
  }
  public selxGraduacionvista(id: number) {
    return this.getBy('Graduacionvista', id);
  }
}
