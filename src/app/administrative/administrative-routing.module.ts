import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HasExecutiveProfileGuard } from '../auth/guards/has-executive-profile.guard';
import { IsLoginGuard } from '../auth/guards/is-login.guard';
import { SolicitudesComponent } from './solicitudes/solicitudes.component';
import { AdministrativeLayoutComponent } from './administrative-layout/administrative-layout.component';

const routes: Routes = [
  {
    path: '',
    component: AdministrativeLayoutComponent,
    children: [
      {
        path: 'requests',
        component: SolicitudesComponent,
        canActivate: [IsLoginGuard, HasExecutiveProfileGuard],
        children: [
          //   { path: '', redirectTo: `/auth/escoger-perfil`, pathMatch: 'full' },
        ],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdministrativeRoutingModule {}
