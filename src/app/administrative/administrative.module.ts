import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AdministrativeRoutingModule } from './administrative-routing.module';
import { AdministrativeLayoutComponent } from './administrative-layout/administrative-layout.component';
import { SolicitudesComponent } from './solicitudes/solicitudes.component';
import { JWTInterceptor } from '@app/auth/services/jwt.interceptor';
import { SharedModule } from '@app/shared/shared.module';
import { UiModule } from '@app/ui/ui.module';
import { BaseModule } from '@app/shared/base/base.module';

@NgModule({
  declarations: [AdministrativeLayoutComponent, SolicitudesComponent],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
    AdministrativeRoutingModule,
    SharedModule,
    UiModule,
    BaseModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JWTInterceptor,
      multi: true,
    },
  ],
})
export class AdministrativeModule {}
