import { NgModule, CUSTOM_ELEMENTS_SCHEMA,NO_ERRORS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import { ControlsModule } from './controls/controls.module';
import { BaseModule } from './base/base.module';
//import { ErrorDialogComponent } from './errors/error-dialog/error-dialog.component';
//import { ErrorDialogService } from "./errors/error-dialog.service";
import { LoadingDialogComponent } from './loading/loading-dialog/loading-dialog.component';
import { LoadingDialogService } from "./loading/loading-dialog.service";
import { AgmCoreModule } from '@agm/core';
import { MaterialModule } from './material/material.module';
//import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
//import { DirectiveModule } from './directive/directive.module';

@NgModule({
  declarations: [
    //ErrorDialogComponent,
    LoadingDialogComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MaterialModule,
    ControlsModule,
    BaseModule,
    //PerfectScrollbarModule,
    AgmCoreModule
  ],
  exports: [
    ControlsModule,
    BaseModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MaterialModule,
    //DirectiveModule,
    //PerfectScrollbarModule,
    //ErrorDialogComponent,
    LoadingDialogComponent
  ],
  providers: [
    //ErrorDialogService,
    LoadingDialogService
  ],
  entryComponents: [
    //ErrorDialogComponent,
    LoadingDialogComponent
  ],
  schemas:  [ CUSTOM_ELEMENTS_SCHEMA,NO_ERRORS_SCHEMA ]
})
export class SharedModule { }
