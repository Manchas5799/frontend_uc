import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XtFieldPanelComponent } from './xt-field-panel.component';

describe('XtFieldPanelComponent', () => {
  let component: XtFieldPanelComponent;
  let fixture: ComponentFixture<XtFieldPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XtFieldPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XtFieldPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
