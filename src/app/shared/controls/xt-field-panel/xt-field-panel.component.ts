import { Component, OnInit, Input, ViewChild, ElementRef, HostBinding, EventEmitter, Output, OnChanges, SimpleChanges, AfterViewInit, Inject, ChangeDetectorRef } from '@angular/core';
import { MatDatepicker } from '@angular/material/datepicker';
import { MatSelect, MatSelectChange } from '@angular/material/select';
import { MatAutocompleteTrigger } from '@angular/material/autocomplete';
import { FormControl } from '@angular/forms';
import { GlobalsComponent } from "@app/globals.component";
import { fromEvent, Observable } from 'rxjs';
import { map, startWith, tap } from 'rxjs/operators';
import { MatSlideToggle } from '@angular/material/slide-toggle';
import { MatCheckbox } from '@angular/material/checkbox';
import { _isNumberValue } from '@angular/cdk/coercion';
import * as moment from 'moment';
//import { default as _rollupMoment } from 'moment';
import { APP_BASE_HREF } from '@angular/common';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { XtGridColumnComponent } from '../xt-grid-column/xt-grid-column.component';


//const moment = _rollupMoment || _moment;
export const MYFormat = {
  parse: {
    dateInput: 'DD/MM/YYYY'
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMMM Y',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM Y'
  }
};

@Component({
  selector: 'xt-field-panel',
  templateUrl: './xt-field-panel.component.html',
  styleUrls: ['./xt-field-panel.component.css'],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'es-GB' },
    { provide: MAT_DATE_FORMATS, useValue: MYFormat },
    { provide: DateAdapter, useClass: MomentDateAdapter },
  ],
})
export class XtFieldPanelComponent implements OnInit, AfterViewInit, OnChanges {

  @Input() title: string;
  @Input() titleArchivo: string;
  titleArchivoSaltoLinea: string;
  @Input() id: string;
  @Input() datatype: string;
  @Input() datasource: Array<any>;
  @Input() public _disabled: boolean = false;
  @HostBinding('class.xt-invisible')
  @Input()
  invisible: boolean = false;
  @Input() public required: boolean = false;
  @Input() public designer: string = null;
  @Input() public formatValue: boolean = true;
  @Input() public maxlength: number = 150;
  public characterCount: number;
  @Input() public checkedValue: number = -1001; //Sí
  @Input() public uncheckedValue: number = -1002; //No
  @Input() public affectedfields: string = null;
  @Input() minval: string = null;
  @Input() maxval: string = null;
  @Input() NroEnteros: number = null;
  @Input() NroDecimales: number = null;
  @Input() public maxsize: number = null;
  @Input() public extensions: string = null;
  @Input() public mask: string = null;
  @Input() public rowInfo: any = null;
  @Input() public colInfo: any = null;
  @Input() public fileLabel: string = "Adjuntar archivo";
  @Input() public placeholder: string = null;
  @Input() public yearList: any[];
  @Input() validationBlur: boolean = true;
  private valorNumerico: number;
  private valorFormateado: string;
  @Input() public decimals: number = 2; //default cantidad decimales
  public $: any = (window as any)["jQuery"]
  afterAVInit: boolean = false;
  @Input() public optionDefault: string = "(Seleccione un valor)";


  esconderClave = true;


  selectedValue: string;
  public meses: any[] = [];
  public anios: any[] = [];
  public anioSelected: any = null;
  public mesSelected: any;

  public minDate() {
    if (!this.maxval) return null;
    let dateMinDac = this.minval.split('/')
    return new Date(parseInt(dateMinDac[2]), 0, 1)
  }

  public maxDate() {
    if (!this.maxval) return null;
    let dateMaxDac = this.maxval.split('/')
    return new Date(parseInt(dateMaxDac[2]), 11, 31)
  }

  date = new FormControl(moment());

  chosenYearHandler(normalizedYear: MatSelectChange, dateInput: any) {

    if (normalizedYear.value !== '') {
      let fechaCadena = `01/01/${normalizedYear.value}`;
      this.mesSelected = this.meses[0].value;
      this.RefControlMonth.value = this.meses[0].value;
      this.anioSelected = normalizedYear.value;
      dateInput.value = fechaCadena;
    } else {
      this.anioSelected = null;
      this.mesSelected = null;
      dateInput.value = null;
    }
  }

  chosenMonthHandler(normalizedMonth: any, dateInput: any) {
    this.mesSelected = normalizedMonth.value;
    let fechaCadena = `01/${normalizedMonth.value}/${this.anioSelected}`;
    dateInput.value = fechaCadena;
  }

  public isDirty: boolean = false; //para identificar si alguien cambió

  @HostBinding('class.require-empty')
  @Input() public invalid: boolean = false; //estilo subrayado al pasar por un campo requerido y dejarlo vacío

  @Input()
  customBlurFn: (fieldPanel: XtFieldPanelComponent, invalid: boolean) => Observable<boolean>;

  @HostBinding('class.xt-disabled')
  @Input() public get disabled(): boolean {
    return this._disabled;
  }

  public set disabled(v: boolean) {
    this._disabled = v;
    this.colocarRequeridoAVI();
  }


  public get ctx(): any {
    return this._ctx;
  }

  @Input()
  public set ctx(v: any) {
    //console.log(`Context received`);
    this._ctx = v;
    if (v && v.val !== undefined) this.value = v.val;
    //if ((v && v.val !== undefined) || v.val === 0) {
    //  this.value = v.val;
    //}
  }

  private globals: GlobalsComponent;
  private viewInitialized: boolean = false;
  private newValue: any = undefined;
  private _ctx: any;

  filteredDatasource: Observable<any[]>;

  @Output()
  public selectionChange = new EventEmitter();

  @Output()
  public blur = new EventEmitter();
  @Output()
  public deleteFile = new EventEmitter();

  @Output()
  public keypress = new EventEmitter();

  @Output()
  public keydown = new EventEmitter();

  @ViewChild('RefControl')
  public RefControl: MatSelect;

  //fecha monthyear
  @ViewChild('RefControlYear')
  public RefControlYear: MatSelect;

  @ViewChild('RefControlMonth')
  public RefControlMonth: MatSelect;

  @ViewChild(MatAutocompleteTrigger)
  public RefControlAC: MatAutocompleteTrigger;

  @ViewChild(MatSlideToggle)
  public RefControlToggle: MatSlideToggle;

  @ViewChild(MatCheckbox)
  public RefControlCheck: MatCheckbox;

  @ViewChild('DateTimeControl')
  DateTimeControl: MatDatepicker<Date>;

  @ViewChild('DateControl')
  DateControl: MatDatepicker<Date>;

  @ViewChild('DateControlMY')
  DateControlMY: MatDatepicker<Date>;

  @ViewChild('DateControlDM')
  DateControlDM: MatDatepicker<Date>;

  @ViewChild('FileControl')
  FileControl: ElementRef;

  @ViewChild('TextControl')
  TextControl: ElementRef;

  @ViewChild('TextControlTA')
  TextControlTA: ElementRef;

  @ViewChild('IntegerControl')
  IntegerControl: ElementRef;

  @ViewChild('DecimalControl')
  DecimalControl: ElementRef;

  @ViewChild('ACTextControl')
  ACTextControl: ElementRef;

  @ViewChild('DateTimeInput')
  DateTimeInput: ElementRef;

  @ViewChild('DateInput')
  DateInput: ElementRef;

  @ViewChild('TimeControl')
  TimeControl: ElementRef;

  //public datetimevalue: Date;
  public refValue: number;
  public fileValue: string;
  public filenoderef: string;
  public filealias: string;
  private acDirty: boolean = false;


  constructor(
    //private globals: GlobalsComponent,
    @Inject(APP_BASE_HREF) public baseHref: string,
    private changeDetectorRef: ChangeDetectorRef) {
  }

  //Integer
  public get ValueControl(): any {
    var resp = null;
    switch (this.datatype.toLowerCase()) {
      case 'reference':
        switch (this.effectiveDesigner) {
          case 'autocomplete':
            resp = this.RefControlAC;
            break;
          case 'toggle':
            resp = this.RefControlToggle;
            break;
          case 'checkbox':
            resp = this.RefControlCheck;
            break;
          default:
            resp = this.RefControl;
            break;
        }
        break;
      case 'datetime':
        console.log('datetime')
        return this.DateTimeControl;
        break;
      case 'date':
        console.log('date')
        if (this.designer == "monthyear")
          return this.DateControlMY;
        if (this.designer == "daymonth")
          return this.DateControlDM;
        return this.DateControl;
        break;
      case 'time':
        return this.TimeControl;
        break;
      case 'file':
        resp = this.FileControl;
        break;
      case 'integer':
        resp = this.IntegerControl;
        break;
      case 'decimal':
        resp = this.DecimalControl;
        break;
      default:
        switch (this.effectiveDesigner) {
          case 'textarea':
            resp = this.TextControlTA;
            break;
          default:
            resp = this.TextControl;
            break;
        }
        break;
    }
    return resp;
  }

  @Input()
  public get value(): any {
    var resp = null;
    switch (this.datatype.toLowerCase()) {
      case 'reference':
        switch (this.effectiveDesigner) {
          case 'autocomplete':
            resp = this.refValue;
            //resp = this.RefControlAC.activeOption.value;
            break;
          case 'toggle':
            resp = this.refValue;
            break;
          case 'checkbox':
            resp = this.refValue;
            break;
          default:
            resp = this.RefControl.value;
            break;
        }

        //if (this.effectiveDesigner == 'autocomplete') {
        //  resp = this.refValue;
        //  //resp = this.RefControlAC.activeOption.value;
        //} else {
        //  resp = this.RefControl.value;
        //}
        break;
      case 'datetime':
        //resp = this.datetimevalue;
        //var mom = this.DateTimeControl._selected;
        //if (mom) resp = mom.toDate();
        return null;
        //return this.DateTimeControl._selected;
        break;
      case 'date':
        //if (this.designer == "monthyear") {
        //  let fechaDM = new Date(this.anioSelected, parseInt(this.mesSelected) - 1, 1);
        //  return moment(fechaDM).toDate();
        //} if (this.designer == "daymonth")
        //  return moment(this.DateControlDM._selected).toDate();
        //return moment(this.DateControl._selected).toDate();
        //console.log('date', moment(this.DateInput.nativeElement.value).toDate())
        //console.log('date', new Date(this.DateInput.nativeElement.value))
        //if (this.DateInput.nativeElement.value != "" && this.DateInput.nativeElement.value != null && this.DateInput.nativeElement.value != undefined)
        //  return moment(this.DateInput.nativeElement.value).toDate();
        //else
          return this.DateInput.nativeElement.value;
        break;
      case 'time':
        resp = this.TimeControl.nativeElement.value;
        var mom = moment('01/01/1950 ' + resp, 'DD/MM/YYYY h:mm A');
        if (!mom.isValid() || resp == '')
          resp = null;
        else
          resp = mom.toDate();

        return resp;
        break;
      case 'file':
        if (this.fileValue)
          resp = this.fileValue;
        else if (this.FileControl.nativeElement.files.length != 0)
          resp = this.FileControl.nativeElement.files[0].name + '[||][||]' + this.btConvert(this.FileControl.nativeElement.files[0].size);
        break;
      case 'integer':
        resp = this.IntegerControl.nativeElement.value;
        if (resp !== "")
          resp = this.quitarFormatoNumero(resp);
        break;
      case 'decimal':
        resp = this.DecimalControl.nativeElement.value;
        if (resp !== "")
          resp = this.quitarFormatoNumero(resp);
        break;
      //case 'decimal':
      //  resp = this.DecimalControl.nativeElement.value;
      //  let decimal = resp.split('.')
      //  let respE = parseInt(decimal[0])
      //  let respD = parseInt(decimal[1])
      //  let respd1 = (respD / 1000000)
      //  let respF= (respE + respd1)
      //  if (resp != "")
      //    resp = respF;
      //  break;
      default:
        switch (this.effectiveDesigner) {
          case 'textarea':
            resp = this.TextControlTA.nativeElement.value;
            break;
          default:
            resp = this.TextControl.nativeElement.value;
            break;
        }
        break;
    }
    if (resp === '' || resp === undefined) resp = null;
    return resp;
  }

  public set value(v: any) {
    if (!this.viewInitialized) {
      //if ((this.value == 0 && v == null) && (this.datatype.toLowerCase() == "integer" || this.datatype.toLowerCase() == "decimal"))
      //  v = 0;
      this.newValue = v;
      return;
    }
    switch (this.datatype.toLowerCase()) {
      case 'reference':
        switch (this.effectiveDesigner) {
          case 'autocomplete':
            this.refValue = v;
            this.updateAutocompleteText();
            break;
          case 'toggle':
            this.refValue = v;
            this.RefControlToggle.checked = v == this.checkedValue;
            break;
          case 'checkbox':
            if (v === true) v = this.checkedValue;
            if (v === false) v = this.uncheckedValue;
            this.refValue = v;
            this.RefControlCheck.checked = v == this.checkedValue;
            break;
          default:
            this.RefControl.value = v;
            break;
        }
        //if (this.effectiveDesigner == 'autocomplete') {
        //  this.refValue = v;
        //  this.updateAutocompleteText();
        //} else {
        //  this.RefControl.value = v;
        //}
        break;
      case 'datetime':
        //this.datetimevalue = v;
        //this.DateTimeControl.nativeElement.value = v;
        //this.DateTimeControl.
        var d: Date = null;
        if (v != null) {
          d = moment(v).toDate();
          //this.DateTimeControl.select(d);
        } else {
          //this.DateTimeControl.select(null);
        }
        this.DateTimeControl.select(d);
        break;
      case 'date':
        var d: Date = null;
        if (v != null) {
          d = moment(v).toDate();
        }
        if (this.designer == "monthyear") {
          this.asignarMesAnioList(v, d);
        } else if (this.designer == "daymonth") {
          this.DateControlDM.select(d);
        } else {
          this.DateControl.select(d);
          //this.DateInput.nativeElement.value = v
        }
        break;
      case 'time':
        var d: Date = null;
        if (v != null) {
          let mom = moment(v);
          d = moment(v).toDate();
          this.TimeControl.nativeElement.value = mom.format('h:mm A');
        } else {
          this.TimeControl.nativeElement.value = '';
        }
        break;
      case 'file':
        this.fileValue = v;
        if (v) {
          let parts = (v as string).split("[||]");
          console.log("xt-field-panel", parts)
          this.filealias = parts[0];
          this.filenoderef = parts[1];
          //aqui deberia traer el archivo y guardarlo en FileControl para evitar error al darle al boton guardar
          //y ya hay un archivo. VALIDAR SI ES LA MEJOR MANERA
          //if (parts.length > 2) {
          //  let djiSvc = InjectorInstance.get(PNTDeclJuradaInversionService); //obtengo el archivo
          //  djiSvc.downloadTwo(parts[1], parts[0]).subscribe(data => { //busco en laserfiche
          //    if (data) { //si hay archivo (retorne en blob)
          //      //Convertir Blob a file y añadirlo a FileControl
          //      let list = new DataTransfer();
          //      let file = new File([data], parts[0]);
          //      list.items.add(file);

          //      this.FileControl.nativeElement.files = list.files;
          //    }
          //  })
          //}
          this.fileLabel = `${parts[0]} (${this.btConvert(parseInt(parts[2]))})`;
          console.log("xt-field-panel", this.filealias)
          console.log("xt-field-panel", this.filenoderef)
        } else {
          this.filealias = null;
          this.filenoderef = null;
          this.FileControl.nativeElement.value = "";
          this.fileLabel = "Adjuntar archivo";
          //quitando el file de FileControl
          //this.FileControl.nativeElement.files = new DataTransfer().files;
        }

        break;
      case 'integer':
        //this.valorNumerico = v;
        this.IntegerControl.nativeElement.type = 'text';
        this.IntegerControl.nativeElement.value = this.formatearNumero(parseInt(v === null ? '' : v));
        break;
      case 'decimal':
        this.DecimalControl.nativeElement.type = 'text';
        this.DecimalControl.nativeElement.value = this.formatearNumero(parseFloat(v === null ? '' : v));
        break;
      default:

        switch (this.effectiveDesigner) {
          case 'textarea':
            this.TextControlTA.nativeElement.value = v;
            this.changeCountCharacterTA();

            break;
          default:
            if (this.TextControl === undefined) return;
            this.TextControl.nativeElement.value = v;
            break;
        }
        break;
    }

    this.colocarRequerido();
    this.afterAVInit = true;
  }
  formatearNumero(numero: number): string {
    if (!this.formatValue) return numero.toString();
    if (numero !== null && !isNaN(numero)) {
      this.valorNumerico = numero;
      if (this.datatype.toLowerCase() == 'integer') this.valorFormateado = `${numero}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      if (this.datatype.toLowerCase() == 'decimal') {
        let temp = (Math.round(numero * (10 ** this.decimals)) / (10 ** this.decimals)).toString();//.toFixed(this.decimals);
        let parts = temp.split('.');
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ","); //formatear parte entera

        this.valorFormateado = parts.join('.');
      }
      return this.valorFormateado;
    }
    return '';
  }
  quitarFormatoNumero(numero: string): number {
    if (numero == this.valorFormateado) return this.valorNumerico;
    if (numero && this.datatype.toLowerCase() == "integer") return parseInt(numero);
    if (numero && this.datatype.toLowerCase() == "decimal") return parseFloat(numero);
    return null;
  }
  onFocus($event: any) {
    if (this.datatype.toLowerCase() == "integer" || this.datatype.toLowerCase() == "decimal") {
      let control = this.datatype.toLowerCase() == "integer" ? this.IntegerControl : this.DecimalControl

      let numero = this.quitarFormatoNumero(control.nativeElement.value)
      control.nativeElement.type = 'number';
      control.nativeElement.value = numero;
      //quitar evento scroll al enfocar un input type number y asi evitar aumentar o disminuir el valor del input
      let $ = this.$;
      $('input[type=number]').on('mousewheel', function (e: any) {
        e.preventDefault();
      });
    }

  }
  btConvert(bytes: number, decimals = 2) {
    if (bytes === 0) return '0 Bytes';

    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return Math.ceil(parseFloat((bytes / Math.pow(k, i)).toFixed(dm))) + ' ' + sizes[i];
  }

  asignarMesAnioList(v: any, d: any) {
    console.log(typeof (v))
    if (v) {
      if (typeof (v) != "object") { //diferenciar de cuando llega de la db o cuando se hace el cambio en el front (entra al if al cargar la data)
        this.DateControlMY.select(d);
        let fechaTemp = v.toString().split('-')
        this.anioSelected = fechaTemp[0];
        this.mesSelected = parseInt(fechaTemp[1]) <= 10 ? `0${parseInt(fechaTemp[1])}` : `${parseInt(fechaTemp[1])}`;
        this.RefControlYear.value = fechaTemp[0];
        this.RefControlMonth.value = parseInt(fechaTemp[1]) <= 10 ? `0${parseInt(fechaTemp[1])}` : `${parseInt(fechaTemp[1])}`;
      }
    }
    else {
      this.anioSelected = null;
      this.mesSelected = null;
      this.RefControlYear.value = null;
      this.RefControlMonth.value = null;
      this.DateInput.nativeElement.value = null;
    }
  }

  ngOnInit(): void {

  }

  ngAfterViewInit() {

    this.viewInitialized = true;
    if (this.RefControl) {
      this.RefControl.selectionChange.subscribe($event => {
        console.log(`VALUE: ${$event.value}`);
        // console.log(`VALUE: ${$event}`);
      });
    }
    if (this.maxval === null) {
      if (this.datatype.toLowerCase() == "integer") this.maxval = "999999999";
      if (this.datatype.toLowerCase() == "decimal") this.maxval = "9999999999";
    }
    if (this.minval === null) {
      this.minval = "0";
    }
    if (this.newValue || this.newValue === 0) { //agregar validacion para guardar 0 ( || this.newValue === 0)
      this.value = this.newValue;
      this.newValue = undefined;
    }
    if (this.effectiveDesigner?.toLowerCase() == "textarea") this.changeCountCharacterTA();
    //if (this.datatype.toLowerCase() == "textarea")
    //  this.changeCountCharacterTA();
    if (this.datatype.toLowerCase() == "date" && this.designer == "monthyear") {
      this.meses = [
        //{ value: "", viewValue: '(Mes)' },
        { value: "01", viewValue: 'Enero' },
        { value: "02", viewValue: 'Febrero' },
        { value: "03", viewValue: 'Marzo' },
        { value: "04", viewValue: 'Abril' },
        { value: "05", viewValue: 'Mayo' },
        { value: "06", viewValue: 'Junio' },
        { value: "07", viewValue: 'Julio' },
        { value: "08", viewValue: 'Agosto' },
        { value: "09", viewValue: 'Setiembre' },
        { value: "10", viewValue: 'Octubre' },
        { value: "11", viewValue: 'Noviembre' },
        { value: "12", viewValue: 'Diciembre' }
      ]
      //llamado a la base de datos para obtener el año de la declaracion

      //this.yearList = this.defaultYearList();
    }
    if (this.titleArchivo) this.titleArchivoSaltoLinea = `<br/><p><strong>${this.titleArchivo}&nbsp;&nbsp</strong></p>`
    this.changeDetectorRef.detectChanges();

    this.colocarRequerido();
  }
  public colocarRequerido() {
    if (this.required.toString() == "true" && this.invisible === false && this.value === null && this.disabled.toString() !== "true") { //inputs en la grilla tienen ctx, los que no tienen pertenecen a algun formulario
      this.invalid = true;
      //this.isDirty = true;
    } else {
      this.invalid = false;

    }
  }
  public colocarRequeridoAVI() {
    if (this.required.toString() == "true" && this.invisible === false && this.disabled.toString() == "true") { //inputs en la grilla tienen ctx, los que no tienen pertenecen a algun formulario
      this.invalid = false;
    } else {
      this.invalid = true;

    }
  }

  public simularEventoClick() { //simular evento click
    let evObj = document.createEvent('MouseEvents');
    evObj.initMouseEvent('click', true, true, window, 1, 12, 345, 7, 220, false, false, true, false, 0, null);
    return evObj;
  }

  effectiveYearList(): any[] {
    let rv = this.yearList || this.defaultYearList();
    console.log(rv, "aaaa")
    return rv
  }

  defaultYearList(): any[] {
    let anios: any[] = [];
    let anio = new Date().getFullYear()
    anios.push({ value: '', viewValue: '(Seleccione un valor)' })
    for (let i = anio + 4; i >= anio - 4; i--) {
      //debugger
      anios.push({ value: `${i}`, viewValue: i })
    }
    return anios;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.datatype.toLowerCase() == 'reference' && changes['datasource'] && this.datasource != null && this.effectiveDesigner == 'autocomplete') {
      this.updateAutocompleteText();
      //this.onChangesDirty();
      this.filteredDatasource = fromEvent<any>(this.ACTextControl.nativeElement, 'input').pipe(
        tap(ev => {
          this.acDirty = true;
        }),
        startWith(''),
        map(list => {
          return this.datasource.filter((e) => e.RowText.toLowerCase().includes(this.ACTextControl.nativeElement.value.toLowerCase()));
        })
      );

    }
  }

  onChangesDirty() {
    if (this.ctx?.search === false)
      this.isDirty = true;
  }

  updateAutocompleteText() {
    if (this.refValue === null) { //agregar validacion para guardar 0 ( triple igual )
      this.ACTextControl.nativeElement.value = '';
    } else if (this.datasource != null && this.ACTextControl.nativeElement.value === '') {//agregar validacion para guardar 0 ( triple igual )
      //console.log(`Searching for text for field id "${this.id}" value ${this.refValue}`)
      var option = this.datasource.find((e, i) => e.Id == this.refValue);
      if (option)
        this.ACTextControl.nativeElement.value = option.RowText;
    }
  }

  assignAutocompleteValues(value: number, text: string) {
    this.refValue = value;
    this.ACTextControl.nativeElement.value = text;
    this.acDirty = false;
  }

  public getFile() {
    //if (this.FileControl.nativeElement.files) return null
    var files = this.FileControl.nativeElement.files;
    if (files.length != 0)
      return files[0];

    return null;
  }

  public onSelectionChange($event: any) {
    this.selectionChange.emit($event);
    this.onChangesDirty();
  }

  public get effectiveDesigner(): string {
    //return this.designer;
    //return 'autocomplete';
    if (this.designer === null || this.designer === undefined) {
      if (this.title.toLowerCase().startsWith('No hay información')) {
        return 'toggle';
      }
    }
    return this.designer;
  }

  public onOptionSelected($event: any) {
    this.refValue = $event.option.id;
    this.acDirty = false;
    this.selectionChange.emit($event);
    this.onChangesDirty();
  }

  onACTextBlur($event: any) {
    if (this.acDirty) {
      var curText = this.ACTextControl.nativeElement.value;
      if (curText === '' || this.datasource === null || this.datasource === undefined) {
        this.assignAutocompleteValues(null, '');
      } else {
        var matches = this.datasource.filter(e => e.RowText && e.RowText.toLowerCase().includes(curText.toLowerCase()));
        if (matches.length == 1) {
          this.assignAutocompleteValues(matches[0].Id, matches[0].RowText);
        } else {
          this.assignAutocompleteValues(null, '');
        }
      }
    }
  }

  public onBlur($event: any) {
    //if (!this.validationBlur) return;
    this.invalid = false;
    if ((this.datatype.toLowerCase() == 'integer' || this.datatype.toLowerCase() == 'decimal') && !this.ValueControl.nativeElement.validity.valid) {

      if (this.ValueControl.nativeElement.validity.badInput) {
        this.value = null;
        this.globals.notify(`El valor del campo "${this.effectiveTitle}" debe ser un número válido`);
      }
      if (this.ValueControl.nativeElement.validity.rangeUnderflow) {
        this.value = null;
        this.globals.notify(`El valor mínimo del campo "${this.effectiveTitle}" es ${this.minval}`);
      }
      if (this.ValueControl.nativeElement.validity.rangeOverflow) {
        this.value = null;
        this.globals.notify(`El valor máximo del campo "${this.effectiveTitle}" es ${this.maxval}`);
      }


    }
    if (this.datatype.toLowerCase() == 'integer' || this.datatype.toLowerCase() == 'decimal') {
      let control = this.datatype.toLowerCase() == 'integer' ? this.IntegerControl : this.DecimalControl
      control.nativeElement.type = 'text';
      control.nativeElement.value = this.formatearNumero(this.value);
    }
    //Validar fechas
    if (this.datatype.toLowerCase() == 'date') {
      var date = this.testDate(this.DateInput.nativeElement.value);
      if (date == null) {
        this.DateInput.nativeElement.value = this.testDate(moment(new Date()).format('DD/MM/YYYY'));
        this.DateInput.nativeElement.value = null;
        this.globals.notify(`El valor del campo "${this.effectiveTitle}" debe ser una fecha válida`);
      } else {
        console.log("this.value", moment(this.value).format('DD/MM/YYYY'));
        console.log("date     :", moment(date).format('DD/MM/YYYY'));
        if (moment(this.value).format('DD/MM/YYYY') !== moment(date).format('DD/MM/YYYY')) this.value = date;
        if (this.value !== null && this.minval !== null && this.minval !== "") {
          if (this.value < this.testDate(this.minval)) {
            this.globals.notify(`El valor del campo "${this.effectiveTitle}" debe ser mayor a "${this.minval}".`);
            this.value = null;
          }
        }
        if (this.value !== null && this.maxval !== null && this.maxval !== "") {
          if (this.value > this.testDate(this.maxval)) {
            this.globals.notify(`El valor del campo "${this.effectiveTitle}" debe ser menor a "${this.maxval}".`);
            this.value = null;
          }
        }
      }
    }
    if (this.datatype.toLowerCase() == 'datetime' && !this.testDate(this.DateTimeInput.nativeElement.value)) {
      var date = this.testDate(this.DateTimeInput.nativeElement.value);
      if (date == null) {
        this.DateTimeInput.nativeElement.value = null;
        this.globals.notify(`El valor del campo "${this.effectiveTitle}" debe ser una fecha válida`);
      } else {
        this.value = date;
      }
    }

    //Validar hora
    if (this.datatype.toLowerCase() == 'time' && this.TimeControl.nativeElement.value != '') {
      console.log("time")
      let val: string = this.TimeControl.nativeElement.value;
      var mom = moment('01/01/1950 ' + val, 'DD/MM/YYYY h:m A');
      if (!mom.isValid() || !val.includes(':')) {
        alert(`El valor del campo "${this.effectiveTitle}" debe ser una hora válida (hh:mm AM/PM)`);
        this.TimeControl.nativeElement.value = '';
      } else {
        this.value = mom.toDate();
      }
    }

    //Validar mascara de entrada
    if (this.mask && this.value) {
      var pattern = this.mask;
      var regEx;
      if (pattern.toLowerCase() == 'email') regEx = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      else if (pattern.toLowerCase() == 'cellphone') regEx = /^[0-9]+$/;
      else if (pattern.toLowerCase() == 'dni') regEx = /^[0-9]{8}$/;
      else if (pattern.toLowerCase() == 'ruc') regEx = /^[0-9]{11}$/;
      else regEx = new RegExp(pattern);


      if (!(this.value as string).match(regEx)) {
        this.value = null;
        this.globals.notify(`El valor del campo "${this.effectiveTitle}" debe tener un formato válido`);
      }
    }


    if (this.required.toString() == "true" && this.value === null) //inputs en la grilla tienen ctx, los que no tienen pertenecen a algun formulario
      this.invalid = true;


    if (this.value && (this.datatype.toLowerCase() == "textarea" || this.datatype.toLowerCase() == "text")) this.value = this.value.trim(); //solucion a guardar pnt con espacios delante
    //if (this.value)
    //  this.onChangesDirty();
    $event.field = this;
    if (this.customBlurFn) {
      this.customBlurFn(this, this.invalid).subscribe((data: boolean) => {
        this.blur.emit($event);
      });
    }
    else
      this.blur.emit($event);
  }

  onToggleChange($event: any) {
    console.log("onToggleChange: ", this.RefControlToggle.checked)
    this.refValue = this.RefControlToggle.checked ? this.checkedValue : this.uncheckedValue;
    $event.fieldPanel = this;
    this.selectionChange.emit($event);
    this.onChangesDirty();
  }

  onCheckChange($event: any) {
    console.log("onCheckChange: ", this.RefControlCheck.checked)
    this.refValue = this.RefControlCheck.checked ? this.checkedValue : this.uncheckedValue;
    $event.checked = this.RefControlCheck.checked;
    this.selectionChange.emit($event);
    this.onChangesDirty();
  }

  onFileChange($event: any) {
    //preguntar si es un archivo, tengo que hacer lo que hacía en el otro lado
    //validar formato file
    if (this.datatype.toLowerCase() == 'file') {
      if (this.FileControl.nativeElement.files.length != 0) {
        let newName = this.FileControl.nativeElement.files[0].name.replaceAll("+", "_").replaceAll(";", "_").replaceAll("&", "_").replaceAll("{", "_").replaceAll("}", "_").replaceAll("[", "_").replaceAll("]", "_").replaceAll("*", "_").replaceAll("|", "_").replaceAll("%", "_").replaceAll("#", "_").replaceAll("@", "_").replaceAll("<", "_").replaceAll(">", "_").replaceAll("=", "_").replaceAll("^", "_").replaceAll("~", "_");
        const previousFile = this.FileControl.nativeElement.files[0];
        const newFile = new File([previousFile], newName);

        // hack to update the selected file
        const dT = new DataTransfer();
        dT.items.add(newFile);
        this.FileControl.nativeElement.files = dT.files;

        var file = this.FileControl.nativeElement.files[0];
        var ext = file.name.split('.').pop();

        if (ext) {

          if (this.extensions) {

            var exts = this.extensions.split(",");
            var valido = false;
            for (let j = 0; j < exts.length; j++) {

              if (ext.toLowerCase() == exts[j].trim()) {
                valido = true;
                break;
              }
            }
            if (valido == false) {
              this.value = null
              this.globals.notify(`El archivo debe contar con extensión "${this.extensions}"`);
              return;
            }
          }
        }

        if (file.size > this.maxsize * 1024 * 1024) {
          this.value = null
          this.globals.notify(
            `El tamaño del archivo no debe exceder ${this.maxsize} Mb`
          );
          return;
        }

      }
    }
    if (this.FileControl.nativeElement.files.length > 0) {
      let nombre = this.value.split('[||]');
      this.fileLabel = `${nombre[0]} (${nombre[2]})`;
      //let archivo = fields[i].FieldPanel.FileControl.nativeElement.files[0];
      //this.FieldExtension.value = archivo.name.split('.').pop();
      //this.FieldTamano.value = archivo.size;
      //this.buttonDelete = true;
    }
    this.selectionChange.emit($event);
    this.onChangesDirty();
  }

  onDateChange($event: any) {
    this.selectionChange.emit($event);
    //if (this.datatype.toLowerCase() == "date") {
    //debugger
    //if (this.value) {
    //  let minD = this.value.split('/');
    //  this.minDate = new Date(parseInt(minD[2]), parseInt(minD[1]) - 1, parseInt(minD[0]));
    //}

    //}
    this.onChangesDirty();
  }

  onTextChange($event: any) {
    this.onChangesDirty();
  }

  changeCountCharacterTA() {
    if (this.value === null || this.value === "")
      this.characterCount = this.maxlength
    else
      this.characterCount = this.maxlength - this.value.length;

  }

  onNumberChange($event: any) {
    //this.value = $event.target.value;
    this.onChangesDirty();
  }

  //onKeypressEvent(e: any) {
  //  let valorTemp: number;
  //  if (this.datatype.toLowerCase() == 'integer') {
  //    if (e.key == '.') return false;//no deja ingresar . en fields de tipo integer
  //    if (e.key == '-') return;//no deja ingresar . en fields de tipo integer
  //    if (this.IntegerControl.nativeElement.value && this.IntegerControl.nativeElement.value.includes('-')) {
  //      valorTemp = this.IntegerControl.nativeElement.value.substring(1)
  //      if (this.NroEnteros === null) {
  //        if (valorTemp &&
  //          valorTemp.toString().length + 1 > this.maxval.toString().length) {
  //          event.preventDefault(); //solo ingresar n digitos y no pasarse de la cantidad de valor "maximo"
  //        }
  //      }
  //      else {
  //        if (valorTemp &&
  //          valorTemp.toString().length + 1 > this.NroEnteros) {
  //          event.preventDefault(); //solo ingresar n digitos y no pasarse de la cantidad de valor "maximo"
  //        }
  //      }
  //    }
  //    else {
  //      if (this.NroEnteros === null) {
  //        if (this.IntegerControl.nativeElement.value &&
  //          this.IntegerControl.nativeElement.value.toString().length + 1 > this.maxval.toString().length) {
  //          event.preventDefault(); //solo ingresar n digitos y no pasarse de la cantidad de valor "maximo"
  //        }
  //      }
  //      else {
  //        if (this.IntegerControl.nativeElement.value &&
  //          this.IntegerControl.nativeElement.value.toString().length + 1 > this.NroEnteros) {
  //          event.preventDefault(); //solo ingresar n digitos y no pasarse de la cantidad de valor "maximo"
  //        }
  //      }

  //    }


  //  }

  //  if (e.key == '+') return false;//no deja ingresar + en fields de tipo integer o decimal
  //}

  onKeyupEvent(e: any) {
    if (this.designer?.toLowerCase() == "textarea") this.changeCountCharacterTA();

    if (this.datatype.toLowerCase() == 'decimal') {
      if (this.DecimalControl.nativeElement.value) {
        let partsInput = this.DecimalControl.nativeElement.value.split('.');
        let enteroI = partsInput[0];
        let decimalI = partsInput[1] ? partsInput[1] : 0;
        let partsMV = this.maxval.toString().split('.');
        let enteroMV = partsMV[0];
        let decimalMV = partsMV[1] ? partsMV[1] : 0;

        if (this.NroEnteros === null && this.NroDecimales === null) {
          if (!this.DecimalControl.nativeElement.value.toString().includes('.')) {
            //no incluye coma decimal
            if (enteroI.toString().length > enteroMV.toString().length && e.key != '.') this.DecimalControl.nativeElement.value = this.DecimalControl.nativeElement.value.slice(0, enteroMV.length);
          }
          else {
            //incluye coma decimal
            if (enteroI.toString().length > enteroMV.toString().length || decimalI.toString().length > decimalMV.toString().length) {
              enteroI = enteroI.slice(0, enteroMV.toString().length);
              decimalI = decimalI.slice(0, decimalMV.toString().length);
              this.DecimalControl.nativeElement.value = enteroI + '.' + decimalI;
            }
          }
        } else {
          if (!this.DecimalControl.nativeElement.value.toString().includes('.')) {
            //no incluye coma decimal
            if (enteroI.toString().length > this.NroEnteros && e.key != '.') this.DecimalControl.nativeElement.value = this.DecimalControl.nativeElement.value.slice(0, this.NroEnteros);
          }
          else {
            //incluye coma decimal
            if (enteroI.toString().length > this.NroEnteros || decimalI.toString().length > this.NroDecimales) {
              enteroI = enteroI.slice(0, this.NroEnteros);
              decimalI = decimalI.slice(0, this.NroDecimales);
              this.DecimalControl.nativeElement.value = enteroI + '.' + decimalI;
            }
          }
        }
      }
    }

    //if (this.datatype?.toLowerCase() == "integer" && this.designer?.toLocaleLowerCase() == "comamiles") this.format(e);

    //var entrada = e.target.value.split('.').join('');
    //entrada = entrada.split('').reverse();

    //var salida = [];
    //var aux = '';

    //var paginador = Math.ceil(entrada.length / 3);

    //for (let i = 0; i < paginador; i++) {
    //  for (let j = 0; j < 3; j++) {

    //    if (entrada[j + (i * 3)] != undefined) {
    //      aux += entrada[j + (i * 3)];
    //    }
    //  }
    //  salida.push(aux);
    //  aux = '';

    //  e.target.value = salida.join('.').split("").reverse().join('');
    //}
  }
  //format(input) {
  //  debugger
  //  let nStr = input.target.value + '';
  //  nStr = nStr.replace(/\,/g, "");
  //  let x = nStr.split('.');
  //  let x1 = x[0];
  //  let x2 = x.length > 1 ? '.' + x[1] : '';
  //  let rgx = /(\d+)(\d{3})/;
  //  while (rgx.test(x1)) {
  //    x1 = x1.replace(rgx, '$1' + ',' + '$2');
  //  }
  //  input.target.value = x1 + x2;
  //}

  onKeypress($event: any) {
    this.keypress.emit($event);
  }

  onKeydown($event: any) {
    this.keydown.emit($event);
  }
  public onDeleteFile($event: any) {
    $event.FieldPanel = this;
    this.deleteFile.emit($event);
  }
  public clean() {
    this.isDirty = false;
  }

  private testDate(val: any) {
    let anoActual = new Date().getFullYear();
    if (val === '') return null;
    let parts = val.split('/');

    if (parts.length != 3)
      return null;

    if (parseInt(parts[2]) < 1900 || parseInt(parts[2]) > anoActual + 8) //cambiar aqui si se necesita un rango mayor del año ingresado
      return null;

    if (isNaN(parts[0]) || isNaN(parts[1]) || isNaN(parts[2]))
      return null;

    let test = new Date(parts[2], parseFloat(parts[1]) - 1, parts[0]);
    if (test.getDate() != parseFloat(parts[0]) || test.getMonth() + 1 != parseFloat(parts[1]) || test.getFullYear() != parseInt(parts[2])) {
      return null;
    }
    return test;
  }

  public focus(): void {
    this.ValueControl?.nativeElement?.focus();
  }

  public get effectiveTitle(): string {
    let title = this.title;
    if (title === undefined || title === '')
      title = this.ctx?.col?.title;

    return title;
  }

}
