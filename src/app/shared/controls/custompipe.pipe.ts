import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common'

@Pipe({
  name: 'custompipe'
})
export class CustompipePipe extends DatePipe implements PipeTransform {
  transform(value: any, args?: any): any {
    //  console.log('CustompipePipe value', value)
    // console.log('CustompipePipe args', args) cambias aqui si piden otro formato de fecha en la grilla / tabla
    if (value) {
      if (args != null) {
        switch (args.toLowerCase()) {
          case 'date': {
            console.log('CustompipePipe date', value)

            let hora = new Date(value).getHours();
            let min = new Date(value).getMinutes();
            let seg = new Date(value).getSeconds();
            console.log('CustompipePipe hora', hora)
            if (hora || min || seg) {
              return super.transform(value, 'dd/MM/yyyy hh:mm:ss a');
            } else {
              return super.transform(value, 'dd/MM/yyyy');
            }

          }
          case 'decimal': {
            let parts = value.toString().split('.');
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ","); //formatear parte entera

            return parts.join('.');
          }
          case 'integer': return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
          default: return value;
        }
      }
    }
    return value;
  }
}
