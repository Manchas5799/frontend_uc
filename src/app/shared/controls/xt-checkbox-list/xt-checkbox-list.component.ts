import { Component, OnInit, Input, AfterViewChecked, HostBinding, OnChanges, SimpleChange, SimpleChanges, DoCheck } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { InjectorInstance } from '../../../app.module';
import { GlobalsComponent } from '../../../globals.component';

@Component({
  selector: 'xt-checkbox-list',
  templateUrl: './xt-checkbox-list.component.html',
  styleUrls: ['./xt-checkbox-list.component.css']
})
export class XtCheckboxListComponent implements OnInit, AfterViewChecked, DoCheck {
  protected globals: GlobalsComponent;

  @Input() dataAll: any[];
  @Input() dataSelected: any[];
  @Input() ValueField: string = "Id";
  @Input() TextField: string = "RowText";
  @Input() DescField: string = undefined;
  @Input() FieldVisible: boolean = true;
  @Input() BaseEntity: string;
  @Input() designer: string = "";
  @Input() disabled: boolean = false;
  @Input() clean: boolean = false;
  public datoAllValue: boolean = false;

  @Input()
  actualizarEstado: boolean = true;

  @Input()
  @HostBinding('style.--columns-num')
  columnsNum: number = 1;

  @Input() NameLabel: string = "";
  @Input() DescriptionLabel: string = "Descripción";
  @Input() minSelection: number = 0;
  @Input() maxSelection: number = 0;
  allSelection: number = 0;

  dinamycColumns: any[] = [0];
  @Input() AtributeOne: string;
  @Input() AtributeTwo: string;

  Validator: boolean = false;
  ValidatorHistory: boolean = false;
  CheckedField: any
  TextValueOperations: string = "operaciones"
  ValueFieldFirstTable: string;
  dataHistory: any[]

  constructor(protected route: ActivatedRoute,) {
    this.CheckedField = "CheckedField"
  }

  ngDoCheck() {
    if (this.dataAll && this.dataSelected && this.Validator == false) {
      setTimeout(() => {
        this.saveInitialState();
      }, 0);
      this.Validator = true;
    }

    if (this.dataAll ) {
      if (this.dataAll.length > 0) {
        if (this.dataAll[0].Descripcion === "") {
          this.FieldVisible = false;
        }

      }
    }
  }

  ngAfterViewChecked () {
    //if (this.dataAll && this.dataSelected && this.Validator == false) {
    //  setTimeout(() => {
    //    this.saveInitialState();
    //  }, 0);
    //  this.Validator = true;
    //}

    //if (this.dataAll) {
    //  if (this.dataAll.length > 0) {
    //    if (this.dataAll[0].Descripcion === "") {
    //      this.FieldVisible = false;
    //    }

    //  }
    //}
  }
  changeValueAll(event: any) {
    console.log(this, event)
    if (event == false) {
      //this.dataSelected = [];
      this.dataAll.forEach(dato => {
        if (dato.CheckedField != false) this.changeValue(dato, false);
      })
    }
    else {
      this.dataAll.forEach(dato => {
        //this.dataSelected.push(dato);
        if (dato.CheckedField != true) this.changeValue(dato, true)
      })
    }

  }
  saveInitialState() {

    this.dataAll.forEach(dato => {
      let checked = this.dataSelected.some(elem => {
        return elem[this.AtributeTwo] == dato[this.ValueField]
      });
      if (checked) this.allSelection++
      dato[this.CheckedField] = checked;
    })
    if (this.allSelection == this.dataAll.length) this.datoAllValue = true;
    this.dataHistory = this.dataAll.map(dato => {

      return dato = { ...dato }
    })

  }

  captureState(Id?: number, op?: string, cambio?: any) {

    if (op == 'ins')
      this.dataSelected.push({ [this.ValueField]: Id, [this.AtributeOne]: cambio[this.AtributeOne], [this.AtributeTwo]: cambio[this.AtributeTwo] })
    else {
      let index = this.dataSelected.findIndex(dato => dato[this.ValueField] == Id)
      this.dataSelected.splice(index, 1)
    }

    for (var i = 0; i < this.dataAll.length; i++) {
      if (this.dataHistory[i][this.CheckedField] != this.dataAll[i][this.CheckedField])
        this.dataHistory[i][this.CheckedField] = !this.dataHistory[i][this.CheckedField]
    }

  }
  ngOnInit() {
    this.globals = InjectorInstance.get(GlobalsComponent);
    this.ValueFieldFirstTable = this.route.snapshot.paramMap.get("id")
  }

  verCambios(idPrincipalTable: number = 0): Array<any> {

    let dataOperations: Array<any> = []

    if (this.clean === false) {
      if (this.dataAll) {
        for (let i = 0; i < this.dataAll.length; i++) {
          if (this.dataAll[i][this.CheckedField] != this.dataHistory[i][this.CheckedField]) {
            let Id: any;
            if (this.dataHistory[i][this.CheckedField] == true) {
              let index = this.dataSelected.findIndex(dato => dato[this.AtributeTwo] == this.dataAll[i][this.ValueField])
              Id = this.dataSelected[index][this.ValueField]
              dataOperations.push({ [this.TextValueOperations]: 'D', [this.ValueField]: Id })
            } else {
              dataOperations.push({ [this.TextValueOperations]: 'I', [this.AtributeOne]: Number(this.ValueFieldFirstTable) == 0 ? idPrincipalTable : Number(this.ValueFieldFirstTable), [this.AtributeTwo]: this.dataAll[i][this.ValueField] }) //Ids de la tabla todos y de la tabla seleccionados
            }
          }
        }
      }
      return dataOperations;
    } else {
      return []
    }

  }

  get isDirty(): boolean {
    return this.verCambios(0).length > 0;
  }


  public fnClean() {
    this.clean = true;
  }

  CheckedInList(dato: any) {
    return this.dataSelected.some(elem => {
      elem[this.AtributeTwo] === dato[this.ValueField]
    });

  }

  changeValue(dato: any, evento: any) {


    console.log({ dato: dato });
    dato[this.CheckedField] = evento;
    if (dato[this.CheckedField] === true) {
      //let dt = { ...dato, [this.AtributeTwo]: dato.Id }
      //this.dataSelected.push(dt)
      this.allSelection++;
    } else {
      //let index = this.dataSelected.findIndex(dat => dat[this.ValueField] == dato.Id)
      //this.dataSelected.splice(index, 1)

      this.allSelection--;
    }
    if (this.allSelection >= this.dataAll.length)
      this.datoAllValue = true;
    else
      this.datoAllValue = false;
  }
}

