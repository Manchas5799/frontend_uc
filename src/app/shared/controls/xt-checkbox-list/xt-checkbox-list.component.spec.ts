import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XtCheckboxListComponent } from './xt-checkbox-list.component';

describe('XtCheckboxListComponent', () => {
  let component: XtCheckboxListComponent;
  let fixture: ComponentFixture<XtCheckboxListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XtCheckboxListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XtCheckboxListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
