import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XtFieldCellComponent } from './xt-field-cell.component';

describe('XtFieldCellComponent', () => {
  let component: XtFieldCellComponent;
  let fixture: ComponentFixture<XtFieldCellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XtFieldCellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XtFieldCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
