import {
  Component,
  OnInit,
  Input,
  ElementRef,
  HostBinding,
  ViewChild,
  Output,
  EventEmitter,
} from '@angular/core';
import { XtFieldPanelComponent } from '../xt-field-panel/xt-field-panel.component';

@Component({
  selector: 'xt-field-cell',
  templateUrl: './xt-field-cell.component.html',
  styleUrls: ['./xt-field-cell.component.scss'],
})
export class XtFieldCellComponent implements OnInit {
  @Input()
  @HostBinding('style.--cols')
  cols: number = 1;
  public nextVal: any = null;

  @HostBinding('style.--rows')
  @Input()
  rows: number = 1;

  @Input() title: string;
  @Input() titleArchivo: string;
  @Input() id: string;
  @Input() datatype: string;
  @Input() optionDefault: string = '(Seleccione un valor)';
  @Input() datasource: Array<any>;
  @Input() listid: number;
  @Input() public required: boolean = false;
  @Input() public designer: string;
  @Input() maxlength: number = null;
  @Input() public affectedfields: string = null;
  @Input() minval: string = null;
  @Input() maxval: string = null;
  @Input() public maxsize: number = null;
  @Input() public extensions: string = null;
  @Input() public mask: string = null;
  @Input() public columnId: string = null;
  @Input() public yearList: any[];
  @Input() public placeholder: string = null;
  @Input() public decimals: number = 2;

  public getColumnId(): string {
    return this.columnId ?? this.id.substr(5);
  }

  //@Input() public dateformat: string = null; //monthyear

  public get devHint(): string {
    //if(this.datatype == 'reference' && this.listid)
    //  return `(List ${this.listid})`;

    return null;
  }

  @Output()
  public selectionChange = new EventEmitter();
  @Output()
  public deleteFile = new EventEmitter();

  @Output()
  public blur = new EventEmitter();

  @ViewChild(XtFieldPanelComponent)
  public FieldPanel: XtFieldPanelComponent;

  @HostBinding('class.xt-disabled')
  @Input()
  disabled: boolean = false;

  @HostBinding('class.xt-invisible')
  @Input()
  invisible: boolean = false;

  get value(): any {
    return this.FieldPanel.value;
  }
  set value(v: any) {
    this.FieldPanel.value = v;
  }

  constructor() {}

  ngOnInit(): void {}

  public onSelectionChange($event: any) {
    $event.fieldcell = this;
    this.selectionChange.emit($event);
  }

  public onBlur($event: any) {
    this.blur.emit($event);
  }
  public onDeleteFile($event: any) {
    $event.fieldcell = this;
    this.deleteFile.emit($event);
  }
  get effectivedesigner() {
    if (this.designer) return this.designer;
    if (this.datatype == 'text' && this.rows > 1) return 'textarea';
    return this.designer;
  }
  get effectivemaxlength() {
    if (this.maxlength) return this.maxlength;
    if (this.datatype == 'text' && this.rows > 1) return 2000;
    return 150;
  }
  get effectiveminval(): string {
    if (this.minval) return this.minval;
    return '0';
  }
  get effectivemaxval(): string {
    if (this.maxval) return this.maxval;
    if (this.datatype == 'integer') return '999999999';
    if (this.datatype == 'decimal') return '9999999999';
    return null;
  }
  get effectivemaxsize() {
    if (this.maxsize) return this.maxsize;
    return 100;
  }
  get effectiveextensions() {
    if (this.extensions) return this.extensions;
    return null;
  }
}
