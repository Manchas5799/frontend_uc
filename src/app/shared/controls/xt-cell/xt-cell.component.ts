import { Component, OnInit, Input, HostBinding } from '@angular/core';

@Component({
  selector: 'xt-cell',
  templateUrl: './xt-cell.component.html',
  styleUrls: ['./xt-cell.component.scss']
})
export class XtCellComponent implements OnInit {
  @Input()
  @HostBinding('style.--cols')
  cols: number = 1;

  @HostBinding('style.--rows')
  @Input()
  rows: number = 1;

  @Input()
  title: string = null;

  @HostBinding('class.xt-invisible')
  @Input()
  invisible: boolean = false;
  //@Input() xtclass: string;
  constructor() { }

  ngOnInit(): void {
  }

}
