import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XtCellComponent } from './xt-cell.component';

describe('XtCellComponent', () => {
  let component: XtCellComponent;
  let fixture: ComponentFixture<XtCellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XtCellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XtCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
