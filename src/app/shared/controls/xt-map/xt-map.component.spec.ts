import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XtMapComponent } from './xt-map.component';

describe('XtMapComponent', () => {
  let component: XtMapComponent;
  let fixture: ComponentFixture<XtMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XtMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XtMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
