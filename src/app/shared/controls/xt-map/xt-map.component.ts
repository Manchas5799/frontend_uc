import { AgmMap } from '@agm/core';
import { Component, OnInit, ElementRef, AfterViewInit, Input } from '@angular/core';
//import { Constants } from '@app/common/constant';
//import proj4 from 'proj4';

//import { MouseEvent } from '@agm/core';
@Component({
  selector: 'xt-map',
  templateUrl: './xt-map.component.html',
  styleUrls: ['./xt-map.component.css']
})

export class XtMapComponent implements OnInit {

  /*@Input() parameterPolygon = [];
  @Input() lat: number = -8.2417608;
  @Input() lon: number = -71.8960003;
  @Input() zoom: number = 6;
  @Input() zona: number = 0;
  @Input() datum: number = 0;*/
  red: string = "#FF0000"
  black: string = "#000000"
  tipeMap: string="satellite";
  tipeMaps=[{codigo: 'satellite', descripcion: 'Satelital', checked:true}, {codigo: 'terrain', descripcion: 'Calles',checked:false}];

  cargandoMapa = false;

  etiquetaLatitud: string;
  etiquetaLongitud: string;

  parameterPolygon: any = [];
  lat: number = -8.2417608;
  lon: number = -71.8960003;
  zoom: number = 13;
  zona: number = 0;
  datum: number = 0;
  showVertice: number = -1002;
  poligono: any = [
  ];
  map: any;
  mapClickListener: any;
  mostrarCoordenadas: string = '';
  constructor() { }

  ngOnInit(): void {
    this.cargandoMapa = true;
    setInterval( () => { this.mostrarCoordenadas=''; }, 250);
  }

  ngAfterViewInit(): void {

    //   this.mapLoad();

  }

  seleccionMapa(mapa: any){
    this.tipeMap = mapa;
  }

  public mapReload(parameterPolygon: any, zona: number, datum: number, showVertice: any) {
    this.parameterPolygon = parameterPolygon;
    this.zona = zona;
    this.datum = datum;
    this.mapLoad();
    this.showVertice = showVertice;
  }

  mapLoad() {
    this.poligono = [];

    console.log("XtMapComponent zona", this.zona)

    console.log("XtMapComponent parameterPolygon", this.datum)
    //switch (this.datum) {
    //  case Constants.DATUM_ID_PASAT56:
    //    var utm = "+proj=utm +zone=" + this.zona + " +south";
    //    console.log("XtMapComponent parameterPolygon ordenado", this.parameterPolygon)
    //    this.parameterPolygon.forEach(element => {

    //      if (element.Este && element.Norte) {
    //        var pasat56 = "+proj=longlat +ellps=intl +units=m +towgs84=-288,175,-376 +no_defs";//LatLon PSAD56
    //        let resultLatLng = proj4(utm, pasat56, [element.Este, element.Norte])
    //        console.log("XtMapComponent proj4", element);
    //        this.poligono.push({
    //          lat: resultLatLng[1],
    //          lng: resultLatLng[0],
    //          nro: element.RowText + ''
    //        })
    //        this.lat = resultLatLng[1];
    //        this.lon = resultLatLng[0];
    //      }
    //    });
    //    break;
    //  case Constants.DATUM_ID_WGS84:
    //    var utm = "+proj=utm +zone=" + this.zona + " +south";
    //    this.parameterPolygon.forEach(element => {
    //      if (element.Este && element.Norte) {
    //        var wgs84 = "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs";
    //        let resultLatLng = proj4(utm, wgs84, [element.Este, element.Norte])
    //        console.log("XtMapComponent proj4", resultLatLng);
    //        this.poligono.push({
    //          lat: resultLatLng[1],
    //          lng: resultLatLng[0],
    //          nro: element.RowText + ''
    //        })
    //        this.lat = resultLatLng[1];
    //        this.lon = resultLatLng[0];
    //      }
    //    });
    //    break;

    //  default:
    //    break;
    //}
  }

  mapClicked($event: any) {
    console.log("mapClicked $event", $event);
    this.mostrarCoordenadas = this.mostrarCoordenadas;
  
  }

  mapMouseover($event: any) {
    $event.addListener("mouseover", () => {
      //const infowindow = new google.maps.InfoWindow({
      //  content: "hola"
      //});
      //infowindow.open();
    });

  }


  onMouseOver(infoWindow: any, gm: any) {

    if (gm.lastOpen != null) {
      gm.lastOpen.close();
    }

    gm.lastOpen = infoWindow;

    infoWindow.open();
  }


  mapReady($event: any): void {
    this.cargandoMapa = false;
    
    var wgs84 = "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs";
    $event.addListener('mousemove', (e: any) => {  //mousemove
      console.log("mousemove $event", e);
      var utm = "+proj=utm +zone="+this.zona + " +south";
      //let resultLatLng = proj4(wgs84, utm, [e.latLng.lng(), e.latLng.lat()]);

      //this.mostrarCoordenadas = 'Este=' + parseFloat(resultLatLng[0]).toFixed(2) +'; Norte=' + parseFloat(resultLatLng[1]).toFixed(2);
      
      //console.log('Norte='+resultLatLng[1]);
      //console.log('Este=' +resultLatLng[0]);
  });
  }
  
}


