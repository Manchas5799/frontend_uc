import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XtButtonComponent } from './xt-button.component';

describe('XtButtonComponent', () => {
  let component: XtButtonComponent;
  let fixture: ComponentFixture<XtButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XtButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XtButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
