import { Component, OnInit, Input, Output, EventEmitter, HostBinding } from '@angular/core';
import { InjectorInstance } from '@app/app.module';
import { GlobalsComponent } from '@app/globals.component';

@Component({
  selector: 'xt-button',
  templateUrl: './xt-button.component.html',
  styleUrls: ['./xt-button.component.css']
})
export class XtButtonComponent implements OnInit {

  @Input() public action: string;

  @Input() deshabilitado: boolean = false;

  @Input() color: '' | 'primary' | 'accent' | 'warn' = '';

  @Input() tipoBoton: 'mat-button' | 'mat-raised-button' | 'mat-stroked-button' | 'mat-flat-button' | 'mat-icon-button' | 'mat-fab' | 'mat-mini-fab' | 'mat-menu-item' = 'mat-raised-button' ;

  @Input() nombreIcono = '';

  @Output() public buttonclicked = new EventEmitter();

  protected globals: GlobalsComponent = InjectorInstance.get(GlobalsComponent);
  constructor() { }

  @HostBinding('style.--icono')
  get icono(): string {
    return `'${this.nombreIcono}'`;
  }

  ngOnInit(): void {
  }

  handleClick($event:any) {
    this.buttonclicked.emit($event);
  }
}
