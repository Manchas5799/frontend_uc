import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XtGridComponent } from './xt-grid.component';

describe('XtGridComponent', () => {
  let component: XtGridComponent;
  let fixture: ComponentFixture<XtGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XtGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XtGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
