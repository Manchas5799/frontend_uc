import {
  AfterViewInit,
  Component,
  ContentChildren,
  EventEmitter,
  AfterContentInit,
  forwardRef,
  HostBinding,
  Input,
  OnInit,
  Output,
  QueryList,
  ChangeDetectorRef,
  TemplateRef,
  ContentChild,
} from '@angular/core';
import { XtFieldPanelComponent } from '../xt-field-panel/xt-field-panel.component';
import { XtGridColumnComponent } from '../xt-grid-column/xt-grid-column.component';
import { XtButtonComponent } from '../xt-button/xt-button.component';
import {
  MatCheckboxChange,
  MatCheckboxModule,
} from '@angular/material/checkbox';

// HDT Inicio.06.10.2021: Sin esta sección los estilos de Angular Material para las mat-table no se aplican
export interface PeriodicElement {
  position: number;
}

const ELEMENT_DATA: PeriodicElement[] = [{ position: 1 }];
// HDT Fin

@Component({
  selector: 'xt-grid',
  templateUrl: './xt-grid.component.html',
  styleUrls: ['./xt-grid.component.css'],
})
export class XtGridComponent
  implements OnInit, AfterViewInit, AfterContentInit
{
  // HDT Inicio.06.10.2021: Sin esta sección los estilos de Angular Material para las mat-table no se aplican
  displayedColumns: string[] = ['position'];
  dataSource = ELEMENT_DATA;
  enProceso = true;
  @Input()
  messageDelete: string = '¿Está seguro que desea eliminar este registro?';
  // HDT Fin.

  @ContentChildren(forwardRef(() => XtGridColumnComponent))
  columns: QueryList<XtGridColumnComponent>;
  @ContentChildren(XtFieldPanelComponent, { descendants: true })
  fields: QueryList<XtFieldPanelComponent>;

  @ContentChild('tools', { read: TemplateRef })
  tools: TemplateRef<any>;
  private _data: Array<any>;

  @Input()
  get data(): Array<any> {
    return this._data;
  }

  set data(value: Array<any>) {
    this.columns.forEach((c) => {
      c.clearContexts();
    });
    value.forEach((v) => {
      v.selected = false;
      if (v.RowText) v.RowText = v.RowText.trim();
    });
    this._data = value;
  }

  @Input()
  CountedRows: number = -1;
  @Input()
  actionsvisible: boolean = true;
  @Input()
  selectCheckEnd: boolean = false;
  @Input()
  selectCheckStart: boolean = true;
  @Input()
  showCheckboxes: boolean = false;
  @Input()
  footerVisible: boolean = false;
  @Input()
  editVisible: boolean = true;
  @Input()
  editLupaVisible: boolean = false;
  @Input()
  deleteVisible: boolean = true;
  @Input()
  deleteMultiple: boolean = false;
  @Input()
  groupColumns: Array<string> = [];
  @Input()
  showSubTotals: boolean = false;
  @Input()
  showSearch: boolean = false;
  @Input()
  subtotalGroupLevel: number = 0;
  @Input()
  totalLabel: string = ''; //Total
  @Input()
  subTotalLabel: string = ''; //Subtotal
  @Input()
  designer: string = 'table';
  @Input()
  nombreColumnaOpcion: string = 'Opción';
  @Input()
  groupHeader: boolean = false;
  @Input()
  validarTodosFields: boolean = true;
  @Input()
  actualizarEstado: boolean = true;
  @Input()
  saltarActualizarEstadoBtnGuardar: boolean = false;
  @Input()
  configHeader: any[] = [];

  @Input()
  tooltipColumn: string = null;
  @Input()
  cssClass: () => any;

  @Input()
  readOnly: boolean = false;

  @HostBinding('class')
  get themeClass() {
    let rv: any = new Object();
    if (this.cssClass) rv = this.cssClass();
    rv[`xt-des-${this.designer}`] = true;

    rv[`xt-readonly`] = this.readOnly;

    return rv;
  }

  @Input()
  pageStyle: string = null; //server - client - null
  @Input()
  pageSize: number = 25;
  currentPage: number = 1;

  @Input()
  subTotalFn: (
    groupColumn: string,
    col: XtGridColumnComponent,
    lastRow: any,
    index: number,
    gridInstance: XtGridComponent,
    item: any
  ) => number;

  @Input()
  totalFn: (col: XtGridColumnComponent, grid: XtGridComponent) => any;

  @Input()
  dataRowClassFn: (row: any, gridInstance: XtGridComponent) => object;

  @Input()
  indentFn: (row: any, gridInstance: XtGridComponent) => number;

  @Input()
  editVisibleFn: (
    col: XtGridColumnComponent,
    gridInstance: XtGridComponent,
    item: any
  ) => boolean;

  @Input()
  editLupaVisibleFn: (
    col: XtGridColumnComponent,
    gridInstance: XtGridComponent,
    item: any
  ) => boolean;

  @Input()
  deleteVisibleFn: (
    col: XtGridColumnComponent,
    gridInstance: XtGridComponent,
    item: any
  ) => boolean;

  @Input() foundRows: number;

  @HostBinding('class.xt-grouped')
  get grouped(): boolean {
    return this.groupColumns.length != 0;
  }

  @Output()
  EditRow = new EventEmitter();
  @Output()
  DeleteRow = new EventEmitter();
  @Output()
  Search = new EventEmitter();
  @Output()
  PageChange = new EventEmitter();

  constructor(private changeDetectorRef: ChangeDetectorRef) {}

  effectiveEditVisible(col: XtGridColumnComponent, item: any): boolean {
    return (
      this.editVisible &&
      (this.editVisibleFn === undefined || this.editVisibleFn(col, this, item))
    );
  }
  effectiveEditLupaVisible(col: XtGridColumnComponent, item: any): boolean {
    return (
      this.editLupaVisible &&
      (this.editLupaVisibleFn === undefined ||
        this.editLupaVisibleFn(col, this, item))
    );
  }
  effectiveDeleteVisible(col: XtGridColumnComponent, item: any): boolean {
    return (
      this.deleteVisible &&
      (this.deleteVisibleFn === undefined ||
        this.deleteVisibleFn(col, this, item))
    );
  }
  public getToolsContext(item: any) {
    return {
      $implicit: {
        item: item,
      },
      item: item,
    };
  }
  ngOnInit(): void {
    console.log(this.cssClass);
  }

  ngAfterViewInit(): void {
    let that = this;
    this.columns.forEach((col) => {
      col.grid = that;
    });

    this.enProceso = false;

    this.changeDetectorRef.detectChanges();
  }
  cleanColumnSearch() {
    this.columns.forEach((col) => {
      if (col.EffectiveSearchField) col.EffectiveSearchField.value = null;
    });
  }
  ngAfterContentInit(): void {}
  fnInnerHtml(item: any, col: XtGridColumnComponent) {
    return item[col.textSource || col.valueSource];
  }
  getRowClass(item: any) {
    let rv: any = {};
    if (this.dataRowClassFn) {
      rv = this.dataRowClassFn(item, this);
    }
    rv['xt-selected'] = item.selected;
    return rv;
  }

  selectedRows(): any[] {
    return this.data.filter((d) => d.selected == true);
  }

  onCheckChangeAll($event: any) {
    this.data.forEach((d, i) => {
      if (this.rowStyle(i, d) == 'none') d.selected = false;
      else d.selected = $event.checked;
    });
  }

  onCheckChange($event: MatCheckboxChange, item: any) {
    item.selected = $event.checked;
  }

  editRow(id: number, item: any) {
    console.log('XtGridComponent id', id);
    this.EditRow.emit({ id: id, row: item });
  }
  deleteRow(id: number, item: any) {
    this.DeleteRow.emit({
      id: id,
      row: item,
      messageDelete: this.messageDelete,
    });
  }
  pageChange(page: number) {
    this.PageChange.emit({ page: page });
  }

  realoadGrid() {
    this.Search.emit();
  }

  columnFields(col: any): XtFieldPanelComponent[] {
    if (this.fields === undefined || this.fields.length == 0) return []; //es necesario un flag? Pero si es modo lectura o no igual
    //debe de diferenciarse ya que en modo lectura no todos se deben sumar
    if (col instanceof XtGridColumnComponent) {
      return this.fields.filter((item) => item.ctx.col == col);
    } else {
      return this.fields.filter(
        (item) =>
          item.ctx.col.valueSource == col || item.ctx.col.textSource == col
      );
    }
  }

  trunc(x: any, posiciones = 0) {
    var s = x.toString();
    var l = s.length;
    var decimalLength = s.indexOf('.') + 1;

    if (l - decimalLength <= posiciones) {
      return x;
    }
    // Parte decimal del número
    var isNeg = x < 0;
    var decimal = x % 1;
    var entera = isNeg ? Math.ceil(x) : Math.floor(x);
    // Parte decimal como número entero
    // Ejemplo: parte decimal = 0.77
    // decimalFormated = 0.77 * (10^posiciones)
    // si posiciones es 2 ==> 0.77 * 100
    // si posiciones es 3 ==> 0.77 * 1000
    var decimalFormated = Math.floor(
      Math.abs(decimal) * Math.pow(10, posiciones)
    );
    // Sustraemos del número original la parte decimal
    // y le sumamos la parte decimal que hemos formateado
    var finalNum =
      entera + (decimalFormated / Math.pow(10, posiciones)) * (isNeg ? -1 : 1);

    return finalNum;
  }
  columnSum(col: XtGridColumnComponent, grid: XtGridComponent): any {
    if (this.totalFn) {
      return this.totalFn(col, grid);
    }
    let decimals = -1;
    let sum = 0;
    this.columnFields(col).forEach((item) => {
      decimals =
        decimals < item?.value?.toString().split('.')[1]?.length
          ? item?.value?.toString().split('.')[1]?.length
          : decimals;
      sum += item.value;
    });

    return decimals <= 0 ? sum : this.trunc(sum, decimals);
  }

  getClass(col: XtGridColumnComponent): any {
    let rv: any = {
      'xt-edited': col.EffectiveEditor !== undefined && !this.readOnly,
    };

    if (col.datatype) {
      rv[`xt-dt-${col.datatype}`] = col.datatype;
    }

    return rv;
  }

  get isDirty(): boolean {
    return this.fields.find((fd) => fd.isDirty === true) !== undefined;
  }

  public clean() {
    this.fields.forEach((fd) => fd.clean());
    this.data?.forEach((d) => (d.changed = false));
  }

  public rowStyle(rowIndex: number, row: any): string {
    return this.data[rowIndex].matched === undefined &&
      (this.pageStyle != 'client' ||
        this.rowPage(rowIndex + 1, row) == this.currentPage)
      ? 'table-row'
      : 'none';
  }

  getGroups(row: any) {
    if (this.groupColumns.length == 0) return undefined;

    let prevRow: any = null;

    let idx = this.data.indexOf(row);
    if (idx > 0) prevRow = this.data[idx - 1];

    let groups: Array<any> = [];
    let changed = false;
    this.groupColumns.forEach((e, i) => {
      if (changed || prevRow == null || prevRow[e] != row[e]) {
        if (row[e] != '') {
          //ocultar grupo con cadena vacía
          groups.push({ text: row[e], level: i + 1 });
          changed = true;
        }
      }
    });

    return groups;
  }

  getSubTotals(row: any) {
    console.log('getSubTotals');
    let item = row;
    if (!this.showSubTotals) return [];

    if (this.groupColumns.length == 0) return undefined;

    let nextRow = null;

    let idx = this.data.indexOf(row);
    if (idx < this.data.length) nextRow = this.data[idx + 1];

    let groups: Array<any> = [];
    let changed = false;
    for (var i = 0; i < this.groupColumns.length; i++) {
      if (i >= this.subtotalGroupLevel) {
        let g = this.groupColumns[i];

        if (changed || nextRow == null || nextRow[g] != row[g]) {
          let st: any = { text: row[g], level: i + 1, cells: [] };
          groups.push(st);
          changed = true;
          this.columns.forEach((col) => {
            let cell = { text: ' ' };
            if (st.cells.length == 0) {
              //
              if (this.subTotalLabel != '')
                cell.text = `${this.subTotalLabel} ${row[g].toLowerCase()}`;
              else cell.text = `Subtotal ${row[g]}`;
            } else if (col.showTotal) {
              cell.text = this.columnSubTotal(
                g,
                col,
                row,
                idx,
                item
              ).toString();
            }

            st.cells.push(cell);
          });
        }
      }
    }

    return groups.reverse();
  }

  columnSubTotal(
    groupColumn: string,
    col: XtGridColumnComponent,
    lastRow: any,
    index: number,
    item: any
  ): number {
    if (this.subTotalFn) {
      return this.subTotalFn(groupColumn, col, lastRow, index, this, item);
    }

    let sum = 0;
    let fields = this.columnFields(col);
    for (var i = index; i >= 0; i--) {
      let changed = false;
      this.groupColumns.every((g) => {
        if (this.data[i][g] != lastRow[g]) {
          changed = true;
          return false;
        }
        if (g == groupColumn) return false;
        return true;
      });
      //if (this.data[i][groupColumn] != lastRow[groupColumn])
      if (changed) break;
      if (fields.length > i) sum += fields[i].value;
    }

    return sum;
  }

  public rowPage(rowIndex: number, row: any): number {
    return Math.floor(
      ((row.filteredIndex || rowIndex) - 1) / this.pageSize + 1
    ); //math floor devuelve entero más próximo inferior (parte entera)
  }

  public totalPages(paginacionServidor: boolean = false): number {
    //validaciones del boton final y siguiente
    if (!paginacionServidor)
      return Math.ceil(
        this.data === undefined
          ? 1
          : (this.foundRows || this.data.length) / this.pageSize
      ); //math ceil devuelve entero mayor más próximo
    else
      return Math.ceil(
        this.data === undefined ? 1 : this.CountedRows / this.pageSize
      ); //math ceil devuelve entero mayor más próximo
  }
  public search() {
    if (this.showSearch) {
      setTimeout((_: any) => {
        this.foundRows = 0;
        this.data.forEach((row) => {
          let notFound = this.columns.some((col) => {
            let field = col.EffectiveSearchField;
            if (field) {
              let val = row[col.EffectiveSearchAttribute]
                ?.toString()
                ?.toLowerCase();
              if (
                field.value == null ||
                field.value === '' ||
                (val && val.includes(field.value.toString().toLowerCase()))
              )
                return false;
              else {
                this.go('first');
                return true;
              }
            } else {
              console.log('no search');
            }
            return false;
          });
          if (!notFound) this.foundRows++;
          row.matched = notFound ? false : undefined;
          row.filteredIndex = notFound ? undefined : this.foundRows;
        });

        //recorrer fields
        this.fields.forEach((fd) => {
          if (fd.ctx.search) {
          }
        });
      }, 0);
    }
  }
  nombreArchivo(nombreCompleto: any) {
    if (nombreCompleto === undefined) return '';
    let nombre = nombreCompleto.split('[||]');
    return nombre[0];
  }
  public go(navegacion: string) {
    if (this.pageStyle == 'client')
      switch (navegacion) {
        case 'first':
          this.currentPage = 1;
          break;
        case 'previous':
          this.currentPage--;
          break;
        case 'next':
          this.currentPage++;
          break;
        case 'last':
          this.currentPage = this.totalPages();
          break;
        default:
          break;
      }
    else if (this.pageStyle == 'server') {
      switch (navegacion) {
        case 'first':
          this.currentPage = 1;
          break;
        case 'previous':
          this.currentPage--;
          break;
        case 'next':
          this.currentPage++;
          break;
        case 'last':
          this.currentPage = this.totalPages(true);
          break;
        default:
          break;
      }
      this.pageChange(this.currentPage);
    }
  }
}
