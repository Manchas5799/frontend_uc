import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XtGridColumnComponent } from './xt-grid-column.component';

describe('XtGridColumnComponent', () => {
  let component: XtGridColumnComponent;
  let fixture: ComponentFixture<XtGridColumnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XtGridColumnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XtGridColumnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
