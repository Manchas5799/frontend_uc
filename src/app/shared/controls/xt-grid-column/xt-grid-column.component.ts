import { Component, ContentChild, Input, OnInit, Output, TemplateRef, ViewChild, EventEmitter } from '@angular/core';
import { ColumnEditorDirective } from '../column-editor.directive';
import { XtFieldPanelComponent } from '../xt-field-panel/xt-field-panel.component';
import { XtGridComponent } from '../xt-grid/xt-grid.component';

@Component({
  selector: 'xt-grid-column',
  templateUrl: './xt-grid-column.component.html',
  styleUrls: ['./xt-grid-column.component.css']
})
export class XtGridColumnComponent implements OnInit {
  @Input() title: string;
  @Input() textSource: string;
  @Input() valueSource: string;
  @Input() toolTip: string = "";
  @Input() editor: TemplateRef<any>;
  @Input() showTotal: boolean = false;
  @Input() datatype: string;
  @Input() format: string;
  @Input() textHtml: string;
  @Input() searchAttribute: string;
  @Output()
  public search = new EventEmitter();

  @ContentChild(ColumnEditorDirective, { read: TemplateRef })
  detectedEditor: TemplateRef<any>;

  @ContentChild('search', { read: TemplateRef })
  detectedSearch: TemplateRef<any>;

  @ViewChild('defSearch', { read: TemplateRef })
  defSearch: TemplateRef<any>;

  @ViewChild('defField')
  public defField: XtFieldPanelComponent;

  @ContentChild(XtFieldPanelComponent)
  searchField: XtFieldPanelComponent;

  private contexts: Array<any> = [];
  private lastId = -10000;
  @Input() public grid: XtGridComponent;

  @Input()
  public minWidth: string = 'auto';
  @Input()
  public width: string = 'auto';
  @Input()
  public maxWidth: string = 'auto';

  constructor() { }

  ngOnInit(): void {
  }
  public clearContexts() {
    this.contexts = [];
  }
  public getSearchContext() {
    return {
      $implicit: {
        col: this,
        search: true
      },
      col: this,
      search: true
    };
  }
  public getContext(row: any) {
    if (row.Id == null) {
      row.Id = this.lastId;
      this.lastId--;
    }
    let ctx = this.contexts.find((item) => item.id == row.Id);
    //console.log(`Context found: ${ctx == null}`);
    if (ctx == null) {
      ctx = {
        id: row.Id,
        row: row,
        col: this,
        $implicit: {
          row: row,
          col: this,
          value: row[this.valueSource],
          text: row[this.textSource],
          val: row[this.valueSource || this.textSource],
          search: false
        },
        value: row[this.valueSource],
        text: row[this.textSource],
        val: row[this.valueSource || this.textSource],
        search: false
      };
      this.contexts.push(ctx);
    }

    return ctx;
  }

  public get source(): string {
    return this.valueSource || this.textSource;
  }

  get EffectiveEditor(): TemplateRef<any> {
    return this.editor || this.detectedEditor;
  }
  get EffectiveSearch(): TemplateRef<any> {
    return this.detectedSearch || this.defSearch;
  }
  get EffectiveSearchField(): XtFieldPanelComponent {
    return this.defField || (this.searchField?.ctx?.search ? this.searchField : undefined);
  }
  get EffectiveSearchAttribute(): string {
    return this.searchAttribute || this.valueSource || this.textSource;
  }
  get EffectiveFormat(): string {
    if (this.format) return this.format;
    return null;

  }
}
