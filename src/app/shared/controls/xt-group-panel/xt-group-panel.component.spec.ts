import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XtGroupPanelComponent } from './xt-group-panel.component';

describe('XtGroupPanelComponent', () => {
  let component: XtGroupPanelComponent;
  let fixture: ComponentFixture<XtGroupPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XtGroupPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XtGroupPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
