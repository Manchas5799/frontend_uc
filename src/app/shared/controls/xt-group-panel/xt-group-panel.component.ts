import { Component, OnInit, Input, HostBinding, ViewChild, ViewContainerRef } from '@angular/core';

@Component({
  selector: 'xt-group-panel',
  templateUrl: './xt-group-panel.component.html',
  styleUrls: ['./xt-group-panel.component.scss']
})
export class XtGroupPanelComponent implements OnInit {
  @Input() title: string;

  nuevos: object[] = [];
  enProceso = false;

  tieneElevacion = false;
  @Input() expanded: boolean = true;

  @Input()
  @HostBinding('style.--fieldcols')
  fieldcols: number = 2;

  @HostBinding('class.xt-panel')
  @HostBinding('class.xt-group')
  classAsigned = true;

  @ViewChild('ref', { static: true, read: ViewContainerRef }) vcRef: ViewContainerRef;

  @Input() notaVisible: boolean = true;
  @Input() notaInicial: string;
  @Input() notaInicialStyle: string;
  @Input() notaFinal: string;
  @Input() notaFinalStyle: string;

  @Input()
  @HostBinding('class.xt-group-anexo')
  classGroupAnexo = false;

  @Input()
  @HostBinding('class.xt-group-no-visible')
  classGroupNoVisible = false;

  @HostBinding('class.xt-no-title')
  get noTitle() {
    return this.title == '';
  }

  constructor() { }

  ngOnInit(): void {

  }
 
}
