import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { XtToolbarComponent } from './xt-toolbar/xt-toolbar.component';
import { XtButtonComponent } from './xt-button/xt-button.component';
import { XtGroupPanelComponent } from './xt-group-panel/xt-group-panel.component';
import { XtFieldCellComponent } from './xt-field-cell/xt-field-cell.component';
import { XtFieldPanelComponent } from './xt-field-panel/xt-field-panel.component';
import { XtCellComponent } from './xt-cell/xt-cell.component';
import { XtGridColumnComponent } from './xt-grid-column/xt-grid-column.component';
import { XtCheckboxListComponent } from './xt-checkbox-list/xt-checkbox-list.component';
import { ColumnEditorDirective } from './column-editor.directive';
import { XtMapComponent } from './xt-map/xt-map.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { CustompipePipe } from './custompipe.pipe';
import { AgmCoreModule } from '@agm/core';
import { MaterialModule } from '../material/material.module';
import { XtGridComponent } from './xt-grid/xt-grid.component';

@NgModule({
  declarations: [
    XtToolbarComponent,
    XtButtonComponent,
    XtGroupPanelComponent,
    XtFieldCellComponent,
    XtFieldPanelComponent,
    XtCellComponent,
    XtGridComponent,
    XtGridColumnComponent,
    XtCheckboxListComponent,
    ColumnEditorDirective,
    XtMapComponent,
    CustompipePipe
  ],
  imports: [
    CommonModule,
    AgmCoreModule,
    MaterialModule,
    MatCheckboxModule,
    FormsModule
  ],
  exports: [
    XtToolbarComponent,
    XtButtonComponent,
    XtGroupPanelComponent,
    XtFieldCellComponent,
    XtFieldPanelComponent,
    XtCellComponent,
    XtGridComponent,
    XtGridColumnComponent,
    XtCheckboxListComponent,
    XtMapComponent,
    ColumnEditorDirective,
    MatCheckboxModule
  ]
})
export class ControlsModule { }
