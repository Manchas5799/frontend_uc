import { Component, HostBinding, Input, OnInit } from '@angular/core';

@Component({
  selector: 'xt-toolbar',
  templateUrl: './xt-toolbar.component.html',
  styleUrls: ['./xt-toolbar.component.css']
})
export class XtToolbarComponent implements OnInit {

  constructor() { }

  @Input() title: string;

  // @HostBinding('style.--title')
  // @Input()
  // get titleProp(): string {
  //   return `'${this.title}'`;
  // }

  ngOnInit(): void {
  }

}
