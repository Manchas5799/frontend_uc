import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XtToolbarComponent } from './xt-toolbar.component';

describe('XtToolbarComponent', () => {
  let component: XtToolbarComponent;
  let fixture: ComponentFixture<XtToolbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XtToolbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XtToolbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
