import { Component, OnInit, ViewChild, AfterViewInit, AfterViewChecked, HostBinding, ContentChildren, QueryList, OnDestroy, ChangeDetectorRef, Input } from "@angular/core";
import { forkJoin, Observable, Subscriber, Subscription } from "rxjs";
import { XtFieldCellComponent } from "../controls/xt-field-cell/xt-field-cell.component";
import { FormBuilder } from "@angular/forms";
import { EditFormComponent } from "./edit-form/edit-form.component";
import { ServiceBase } from "../../svc/ServiceBase";
import { ActivatedRoute, Event, NavigationStart, Router, UrlSegment } from "@angular/router";
import { GridBaseComponent } from "./grid-base/grid-base.component";
import { GlobalsComponent } from "../../globals.component";
//import { DACAnexoService } from "@svc/services/DACAnexo.service";
//import { GENAnexoNotaService } from "@svc/services/GENAnexoNota.service";
import { InjectorInstance } from "../../app.module";
import { MatTabGroup } from "@angular/material/tabs";
import { MatDialog } from "@angular/material/dialog";
import { XtCheckboxListComponent } from "../controls/xt-checkbox-list/xt-checkbox-list.component";
//import { DACDeclaracionService } from "@app/svc/services/DACDeclaracion.service";
import { XtGroupPanelComponent } from "../controls/xt-group-panel/xt-group-panel.component";
import { tap } from "rxjs/operators";
import { XtFieldPanelComponent } from "../controls/xt-field-panel/xt-field-panel.component";
//import { HomeService } from "@app/svc/services/home.service";
//import { Constants } from "../../common/constant";
import { error } from "@angular/compiler/src/util";
import { LoadingDialogService } from "../loading/loading-dialog.service";

@Component({
  selector: "app-edit-base",
  template: ` <p>edit-base works!</p> `,
  styles: [],
})
export class EditBaseComponent
  implements OnInit, AfterViewInit, AfterViewChecked, OnDestroy {
  protected fb: FormBuilder;

  public service: ServiceBase;
  protected route: ActivatedRoute;
  protected router: Router;
  public $: any = (window as any)["jQuery"]

  enProceso = false;

  @HostBinding('class.xt-edit-form') true: any;

  @ViewChild(EditFormComponent, { static: true })
  public formComp: EditFormComponent;

  @ViewChild(MatTabGroup)
  public RootTab: MatTabGroup;

  @ViewChild(MatDialog)
  public dialogo: MatDialog;

  public conEventos: boolean = true;

  @ContentChildren(XtGroupPanelComponent, { descendants: true }) fieldsGroupB: QueryList<XtGroupPanelComponent>;

  private _key: string;

  @HostBinding('class')
  get key() {
    return `xt-key-${this._key}`;
  };

  set key(value: string) {
    this._key = value;
  };





  public modal: boolean = false;
  public keynameM: string = null;
  public keyvalueM: number = 0;
  public id: number = 0;
  public nombrePantalla: string;
  public mode: string = null;
  public modoLecturaValue: boolean = false;
  public nietos = [
    //hijo, padre, atributo que lleva al padre --hasta llegar a una PNT
    //indice 1 - Informacion del titular
    ["ITMAccionista", 'PNTTitularMinero', 'TitularMinero'],
    ["ITMResponsable", 'PNTTitularMinero', 'TitularMinero'],
    ["ITMConcesion", 'PNTConcesion', 'PantallaConcesion'],
    ["ITMInversion", 'PNTITMInversion', 'PantallaInversion'],
    ["ITMEstudio", 'PNTITMEstudio', 'PantallaEstudio'],
    ["FINActivoCorriente", 'PNTSituacionFinanciera', 'PantallaSituacionFinanciera'],
    ["FINEstadoResultadoN", 'PNTEstadoResultadoN', 'EstadoResultadoN'],
    ["FINEstadoResultadoF", 'PNTEstadoResultadoF', 'EstadoResultadoF'],
    ["FINMovimientoExist", 'PNTMovimientoExist', 'MovimientoExist'],
    ["FINInmueble", 'PNTInmueble', 'Inmueble'],
    ["FINDepreciacion", 'PNTDepreciacion', 'Depreciacion'],
    ["FINMetalicaRG", 'PNTMetalicaRG', 'PntMetalicaRegimenGeneral'],
    ["FINMetalicaDetalleRG", 'FINMetalicaRG', 'MetalicaRG'],
    ["FINMetalicaPPMPMA", 'PNTMetalicaPPMPMA', 'PantallaMetalicaPPMPMA'],
    ["FINVentaNoMetalica", 'PNTVentaNoMetalica', 'PantallaVentaNoMetalica'],
    ["FINVentaIndustrial", 'PNTVentaIndustrial', 'PantallaVentaIndustrial'],
    ["FINCostoProduccion", 'PNTVentaIndustrial', 'PantallaCostoProduccion'],
    ["FINVentaCarbonifero", 'PNTVentaCarbonifero', 'PantallaVentaCarbonifero'],
    ["FINOroAluvial", 'PNTOroAluvial', 'PantallaOroAluvial'],
    ["FINDividendo", 'PNTDividendo', 'Dividendo'],
    ["FINCambioPatrimonio", 'PNTCambioPatrimonio', 'CambioPatrimonio'],
    ["FINImpuestoEmpresa", 'PNTImpuestoEmpresa', 'ImpuestoEmpresa'],
    ["FINGastoPersonal", 'PNTGastoPersonal', 'PntGastoPersonal'],
    ["FINCategoriaPersonal", 'PNTCategoriaPersonal', 'PntCategoriaPersonal'],
    ["FINBalanceComprobacion", 'PNTBalanceComprobacion', 'BalanceComprobacion'],
    ["EXPReservaNoMetalica", 'PNTReservaNoMetalica', 'PantallaReservaNoMetalica'],
    //["EXPReservaCarbonifera", 'PNTReservaCarbonifera', 'PantallaReservaMetalica'],
    ["EXPProduccionNoMetalica", 'PNTProduccionNoMetalica', 'PntProduccionNoMetalica'],
    //["EXPProduccionCarbonifera", 'PNTProduccionCarbonifera', 'PntProduccionNoMetalica'],
    ["EXPProdCarbonDestino", 'PNTProdCarbonDestino', 'ProdCarbonDestino'],
    ["EXPCompra", 'PNTOrigenCompra', 'PantallaOrigenCompra'],
    ["EXPProduccionObtenida", 'PNTProduccionObtenida', 'PantallaProduccionObtenida'],
    ["EXPProduccionDestino", 'PNTProduccionDestino', 'PantallaProduccionDestino'],
    ["EXPReserva", 'PNTReservaMetalica', 'PantallaReservaMetalica'],
    ["EXPRecursoMetalico", 'PNTRecursoMetalico', 'PantallaRecursoMetalico'],
    ["EXPRecursoNoMetalico", 'PNTRecursoNoMetalico', 'PantallaRecursoNoMetalico'],
    ["EXPRecursoCarbonifero", 'PNTRecursoCarbonifero', 'PantallaRecursoCarbonifero'],
    ["EXPBalanceMetalurgico", 'PNTBalanceMetalurgico', 'PntBalanceMetalurgico'],
    ["EXPBalanceMetalurgicoDetalle", 'EXPBalanceMetalurgico', 'BalanceMetalurgico'],
    ["ADSPrograma", 'PNTProgramaLargoPlazo', 'PantallaPrograma'],
    ["ADSActividad", 'PNTBalanceSocialAct', 'PntBalanceSocialActividad'],
    ["ADSActividadImpacto", 'ADSActividad', 'Actividad'],
    ["ASAAreaSuperficial", 'PNTAreaSuperActMinera', 'PntAreaSuperactividadMinera'],
    ["ASAAreaSuperficial", 'PNTAreaSuperUsoMinero', 'PntAreaSuperUsoMinero'],
    ["ISOPerforacionRealizada", 'PNTPerforacionRealizada', 'PantallaPerforacionRealizada'],
    ["ISOGeologiaSuperficie", 'PNTGeologiaSuperficie', 'PntGeologiaSuperficie'],
    ["ISOGeoquimicaSuper", 'PNTGeoquimicaSuper', 'PntGeoquimicaSuperficie'],
    ["ISOVetaAflorante", 'PNTVetaAflorante', 'PantallaVetaAflorante'],
    ["ISOUbicacion", 'PNTUbicacion', 'Ubicacion'],
    ["ISOSondaje", 'PNTSondaje', 'PantallaSondaje'],
    ["ISOConcesion", 'PNTISOConcesion', 'PNTISOConcesion']
  ];
  public pntUpdEstadoInsertNoInicianPantalla = [
    ['EstadoResultadoN'],
    ['EstadoResultadoF'],
    ['MovimientoExist'],
    ['Inmueble'],
    ['Depreciacion'],
    ['Dividendo'],
    ['CambioPatrimonio'],
    ['ImpuestoEmpresa'],
    ['BalanceComprobacion'],
    ['ProdCarbonDestino'],
    ['Ubicacion'],
    ['Actividad'],
    ['PantallaSondaje'],
    ['PNTISOConcesion'],
  ];

  public pntNoUpdEstadoInsert = [
    //hijo, padre, atributo que lleva al padre --hasta llegar a una PNT
    //indice 1 - Informacion del titular
    //['TitularMinero'], //tiene dos grillas
    //['PantallaConcesion'], //tiene validacion personalizada
    //['PantallaVentaIndustrial'], //tiene dos grillas
    //['PantallaInversion'], //tiene validación personalizada
    //['PantallaCostoProduccion'], //tiene dos grillas
    //['PantallaReservaMetalica'], //para completar pantalla depende de otros inputs
    //['PntBalanceMetalurgico'], //para completar pantalla depende de otros inputs
    ['BalanceMetalurgico'], //para completar pantalla depende de otros inputs
    //['PntProduccionNoMetalica'], //error not found
    //['PantallaOrigenCompra'],  //tiene validacion personalizada
    ['PantallaValorVenta'],  //tiene validacion personalizada
    //['PantallaConcesion'],  //tiene validacion personalizada
    //["PNTTitularMinero", 'TitularMinero'], //tiene dos grillas
    //["PNTConcesion",'PantallaConcesion'], //tiene validacion personalizada
    //["PNTVentaIndustrial", 'PantallaVentaIndustrial'], //tiene dos grillas
    //["PNTVentaIndustrial", 'PantallaCostoProduccion'] //tiene dos grillas
  ];
  public pntCompletarCerosAlGuardar = [ //Va a contener a todas las pantallas, ya sean con confirmacion o sin confirmacion
    ['PNTSituacionFinanciera'],
    ['PNTEstadoResultadoN'],
    ['PNTEstadoResultadoF'],
    ['PNTMovimientoExist'],
    ['PNTInmueble'],
    ['PNTDepreciacion'],
    ['PNTDividendo'],
    ['PNTCambioPatrimonio'],
    ['PNTImpuestoEmpresa'],
    ['PNTGastoPersonal'],
    ['PNTCategoriaPersonal'],
    ['PNTITMPersonalOcupado'],
    ['PNTPersonalOcupado'],
    ['PNTInversion'],
    ['PNTConsumo'],
    ['ITMInversion'],
    ['PNTITMInversion'],
    ['PNTAccidente'],

  ];
  public pntCompletarCerosAlGuardarConfirmacion = [ //Solo los que piden confirmacion
    ['PNTITMPersonalOcupado'],
    ['PNTPersonalOcupado'],
    ['PNTInversion'],
    ['ITMInversion'],
    ['PNTConsumo'],
    ['PNTITMInversion'],
    ['PNTAccidente'],
  ];
  public pnts41 = [
    ['PNTMetalicaRG'],
    ['FINMetalicaRG'],
    ['FINMetalicaDetalleRG'],
    ['PNTMetalicaPPMPMA'],
    ['FINMetalicaPPMPMA'],
    ['PNTVentaNoMetalica'],
    ['FINVentaNoMetalica'],
    ['PNTVentaIndustrial'],
    ['FINVentaIndustrial'],
    ['FINCostoProduccion'],
    ['PNTVentaCarbonifero'],
    ['FINVentaCarbonifero'],
    ['PNTOroAluvial'],
    ['FINOroAluvial'],
  ];
  public pnts41ExportarExcel = [
    ['FINMetalicaRG'],
    ['FINMetalicaPPMPMA'],
    ['FINVentaNoMetalica'],
    ['FINVentaIndustrial'],
    ['FINCostoProduccion'],
    ['FINVentaCarbonifero'],
    ['FINOroAluvial'],
  ];
  public pnts4Sin42 = [
    //['PNTValorVenta'],
    ['PNTAuditor'],
    ['PNTDeclJuradaInversion'],
  ];
  public pnts4 = [
    ['PNTValorVenta'],
    ['PNTAuditor'],
    ['PNTDeclJuradaInversion'],
  ];

  public pntEliminacionMasiva = [ //pantallas que requieren eliminar todo pero no tiene el botno "eliminar todo" visible, solo se eliminará al activar el check No declarar
    ['PNTSituacionFinanciera'],
    ['PNTEstadoResultadoN'],
    ['PNTInmueble'],
    ['PNTDepreciacion'],
  ]

  public keyname: string = null;
  public keyvalue: number = 0;
  //private afterViewInitComplete: boolean = false;
  private pobladoPendiente: boolean = false;

  protected globals: GlobalsComponent;
  private loadingDialogService: LoadingDialogService
  private loading: boolean = false;
  private esCapitulo: boolean = false;
  private reqFieldsEmpty: boolean = false;
  public mostrarDACDeclaracion: boolean = false;
  private routeSub: Subscription;
  public data: any;
  public _declaracionRow: any = undefined;
  public anios: any[] = undefined;

  get declaracionRow(): any {
    var resp = this._declaracionRow
    return resp;
  }

  set declaracionRow(value) {
    this._declaracionRow = value;
  }

  public flgConsolidado: boolean = false;


  /*******************************LISTA DE CABECERAS*******************************************************************/
  public readonly CT_BalanceMetalurgico_UnidadAlimentacion = 1;
  public readonly CT_BalanceMetalurgico_UnidadLey = 3;
  public readonly CT_Transversal_EstadoRegistro = 4;
  public readonly CT_Transversal_SiNo = 2;
  public readonly CT_GENAnexo_Nivel = 8;
  public readonly CT_GENAnexoNota_Posicion = 9;
  public readonly CT_DACAnexo_Estado = 10;
  public readonly CT_GENTipoPersonal_Grupo = 11;
  public readonly CT_ITMRepresentanteLegal_TipoDocumento = 12;
  public readonly CT_ITMConcesion_MotivoObtencionConcesionUEA = 16;
  public readonly CT_ITMPersonalOcupado_TipoTrabajador = 19;
  public readonly CT_FINPersonalOcupado_TipoTrabajador = 21;
  public readonly CT_EXPCargaFamiliar_TipoCargaFamiliar = 35;
  public readonly CT_EXPPeriodoActividad_Concepto = 36;
  public readonly CT_EXPCompra_TipoCompra = 53;
  public readonly CT_PNTDeclJuradaInversion_TipoDocumento = 56;
  public readonly CT_ITMInversion_EtapaProyecto = 104;
  public readonly CT_ADSActividad_OrigenFondo = 57;
  public readonly CT_ASAAreaSuperficial_UsoMinero = 59;
  public readonly CT_ISOPerforacionRealizada_Diametro = 63;
  public readonly CT_ISOPerforacionRealizada_MetodoPerforacion = 64;
  public readonly CT_ISOGeologiaSuperficie_Intensidad = 67;
  public readonly CT_ISOGeologiaSuperficie_Mineralogia = 68;
  public readonly CT_ISOGeologiaSuperficie_MetodoDatacion = 69;
  public readonly CT_ISOGeologiaSuperficie_Veta = 70;
  public readonly CT_RPMPadronMEM_EstadoTramitacion = 74;
  public readonly CT_RPMTitularPadron_TipoPersona = 76;
  public readonly CT_RPMPadronUEAS_TipoSustancia = 78;
  public readonly CT_RPMPadronUEAS_TipoTitular = 79;
  public readonly CT_RPMPadronUEADetalle_TipoMovimiento = 81;
  public readonly CT_RPMPadronUEADemaRes_EmisorResolucion = 84;
  public readonly CT_PNTTitularMinero_SituacionComplementaria = 91;
  public readonly CT_PNTTitularMinero_CambioEstructura = 92;
  public readonly CT_ISOGeologiaSuperficie_Regolito = 93;
  public readonly CT_ISOGeologiaSuperficie_Datacion = 94;
  public readonly CT_Transversal_ActivoInactivo = 95;
  public readonly CT_ADSActividad_Compromiso = 96;
  public readonly CT_TipoAsignación = 90;
  public readonly CT_DACDeclaracion_Estado = 97;
  public readonly CT_DACSolicitudReactivacion_TipoPlazo = 98;
  public readonly CT_DACSolicitudReactivacion_Estado = 99;
  public readonly CT_DACSolicitudReactivacion_TipoReactivacion = 107;
  public readonly CT_DACProcesoPAS_Criterio = 101;
  public readonly CT_DACConcesion_EstadoTramite = 102;
  public readonly CT_DACRegistro_Estado = 103;
  public readonly CT_GENTipoArchivo_Formato = 38;
  public readonly CT_ADSActividadImpacto_NivelImpacto = 58;
  public readonly CT_ISOGeologiaSuperficie_TipoAleacion = 66;
  public readonly CT_RPMPadronMEM_SituacionExpediente = 75;
  public readonly CT_RPMPadronUEADemaRes_TipoMovimiento = 83;
  public readonly CT_Cuenta_Seccion = 110;



  ngOnInit(): void {
    this.globals = InjectorInstance.get(GlobalsComponent);

    //this.flgConsolidado = this.route.snapshot.routeConfig.path.toLowerCase().includes("dacconsolidado");
    if (!this.modal) {
      var paramId = this.route.snapshot.paramMap.get("id");
      console.log("paramId", paramId);
      if (paramId) {
        let parts = paramId.split('_');
        if (parts.length > 1)
          this.mode = parts[1];
        this.id = parseInt(parts[0]);
        //this.nombrePantalla = parte[];
      }
      //let urlActual = this.router.url; de aquí podría obtener el nombre de la pantalla?
    }

    //this.route.params.subscribe((respuesta: any) => {
    //  console.log("this.route.params.subscribe: ", respuesta)
    //  //var paramId = this.route.snapshot.paramMap.get("id");
    //  //console.log("paramId", paramId);
    //  //if (paramId) {
    //  //  let parts = paramId.split('_');
    //  //  //console.log(parts)
    //  //  if (parts.length > 1)
    //  //    this.mode = parts[1];
    //  //  this.id = parseInt(parts[0]);

    //  //  this.ejecutarPoblado();
    //  //}
    //});

  }

  ngAfterViewInit() {

    this.ejecutarPoblado();
  }

  loadId(id: number) {
    this.id = id
    //this.flgConsolidado = this.route.snapshot.routeConfig.path.toLowerCase().includes("dacconsolidado");
    this.pobladoPendiente = true;
    this.modal = true;
    this.formComp.mostrarClose = false;
    this.formComp.mostrarSaveClose = false;
  }
  loadNew(keynameM: string, keyvalueM: number) {
    this.id = 0
    //this.flgConsolidado = this.route.snapshot.routeConfig.path.toLowerCase().includes("dacconsolidado");
    this.pobladoPendiente = true;
    this.modal = true;
    this.keynameM = keynameM;
    this.keyvalueM = keyvalueM
    this.formComp.mostrarClose = false;
    this.formComp.mostrarSaveClose = false;
  }
  //modalAct() {
  //}

  ngAfterViewChecked() {
  }

  afterObtenerNotaFormulario() {

  }

  obtenerNotaFormulario(dataPNT: any): null {
    return null;
    //if (this.globals.tipoSite != 'E' || this.flgConsolidado) return;
    ////Valida si la tabla contiene el texto PNT
    //let entidad = this.service.entidad;
    //if (entidad) {
    //  let contains = entidad.startsWith("PNT");
    //  if (contains) {
    //    let srv = InjectorInstance.get(DACAnexoService);
    //    //consultando Anexo_message al API
    //    console.log("obtenerNotaFormulario this.data.DeclaracionAnexo ", this.data.DeclaracionAnexo)
    //    console.log("obtenerNotaFormulario this.data ", this.data)
    //    if (this.data.DeclaracionAnexo)
    //      srv.sel(this.data.DeclaracionAnexo).subscribe((data: any) => {
    //        if (data) {
    //          //obtener observaciones
    //          console.log(data);
    //          if (data.Observacion && data.Observacion !== "") this.formComp.observacionAnexo = data.Observacion;
    //          //obtener notas
    //          let srv2 = InjectorInstance.get(GENAnexoNotaService);

    //          srv2.selxAnexo(data.Anexo).subscribe((data2: any) => {
    //            data2.forEach(nota => {
    //              console.log("data2 Nota: ", nota);
    //              console.log(this.formComp.fieldsGroup)
    //              let i = 1;
    //              this.formComp.fieldsGroup.forEach(fg => {
    //                if (nota.Seccion == 0) {
    //                  if (nota.Posicion == 1) {
    //                    this.formComp.message = nota.Nota;
    //                    this.formComp.messageStyle = nota.NombreClase?.toLowerCase();
    //                  }
    //                  if (nota.Posicion == 2) {
    //                    this.formComp.messageEnd = nota.Nota;
    //                    this.formComp.messageEndStyle = nota.NombreClase?.toLowerCase();
    //                  }
    //                } else {
    //                  if (nota.Seccion == i) {//dos if para la seccion que necesite ambas notas
    //                    if (nota.Posicion == 1) {//1 Nota Inicial... si no es 1, poner aquí el código que debería tener la nota inicial
    //                      fg.notaInicial = nota.Nota;
    //                      fg.notaInicialStyle = nota.NombreClase?.toLowerCase();
    //                    }
    //                    if (nota.Posicion == 2) {//2 Nota Final... si no es 2, poner aquí el código que debería tener la nota inicial
    //                      fg.notaFinal = nota.Nota;
    //                      fg.notaFinalStyle = nota.NombreClase?.toLowerCase();
    //                    }
    //                  }
    //                }

    //                i++;
    //              })

    //            });
    //          })
    //          this.formComp.message = data.Anexo_message;//Nota inicial formulario
    //          this.afterObtenerNotaFormulario();
    //        }
    //      });
    //  }
    //}
  }

  get isDirty(): boolean {
    //aquí debo preguntar por los dirty de los hijos
    //alert(`Se han realizado cambios en la pantalla. ¿Desea guardar antes de salir?`);
    //this.dialogo
    //  .open(DialogGuardarAlSalirComponent, {
    //    data: `Se han realizado cambios en la pantalla. ¿Desea guardar antes de salir?`
    //  })
    //  .afterClosed()
    //  .subscribe((confirmado: Boolean) => {
    //    if (confirmado) {
    //      //si
    //      alert("¡Llamar funciión guardar!");
    //    } else {
    //      //No
    //      alert("Salir");
    //    }
    //  });
    //let hayCambios: boolean = false;

    //Preguntar si hay cambios en los checkboxlist
    let cbs = this.getCheckboxLists();
    if (cbs != null) {
      for (let i = 0; i < cbs.length; i++) {
        if (cbs[i].isDirty) { //se realizaron cambios en algún checkbox?
          //alert("\'hay cambios en el checkboxList (data).\'");
          return true;
        }
      }
      //cbs.forEach(cb => {
      //  if (cb.isDirty) { //se realizaron cambios en algún checkbox?
      //    alert("\'hay cambios en el checkboxList (data).\'");
      //    return true;
      //  }
      //})
    }

    //Preguntar si hay cambios en las grillas
    let grids = this.getGridFields();
    if (grids !== undefined) {
      for (let i = 0; i < grids.length; i++) {
        if (grids[i] !== undefined) {
          let isDirty = grids[i].formComp.grid.isDirty;
          if (isDirty) {
            console.log(grids[i].formComp);
            //alert("\'hay cambios en el grid.\'");
            return true;
          }
        }
      }
      //grids.forEach(grid => {
      //  if (grid !== undefined) {
      //    let isDirty = grid.formComp.grid.isDirty;
      //    if (isDirty) {
      //      alert("\'hay cambios en el grid.\'");
      //      return true;
      //    }
      //  }
      //})
    }


    //var grids = this.getGridFields();
    //if (grids !== undefined) {
    //  for (var i = 0; i < grids.length; i++) {
    //    //if (!grids[i].disabled) { //validación de grillas editable
    //    let j = 0;
    //    let isDirty = grids[i].formComp.grid.isDirty;
    //    if (isDirty) {
    //      alert("\'hay cambios en el grid.\'");
    //      return true;
    //    }
    //    //grids[j].formComp.grid.fields.forEach(fd => {
    //    //  if (fd.isDirty) {
    //    //    console.log(fd)
    //    //    alert("\'hay cambios en algún grid field (data).\'");
    //    //    return true;
    //    //  }
    //    //  j+=1;
    //    //})
    //    //}
    //  }


    //}

    //Preguntar si hay cambios en los field
    var fields = this.getFormFields();
    if (fields !== null) {
      for (var i = 0; i < fields.length; i++) {
        if (fields[i] !== undefined)
          if (fields[i].FieldPanel.isDirty === true) {
            console.log(fields[i].FieldPanel)
            //alert("\'hay cambios en el formfields (data).\'");
            return true;
          }
      }
    }

    return false;


  }

  clean() {
    //if (this.getCheckboxLists() !== null) this.getCheckboxLists().filter(gfd => gfd).forEach(gfd => gfd.clean());
    this.getGridFields().filter(gfd => gfd).forEach(gfd => gfd.formComp.grid.clean());
    this.getFormFields().filter(gfd => gfd).forEach(gfd => gfd.FieldPanel.clean());
  }

  ngOnDestroy() {
    //let Dirty = this.isDirty;
    //if (Dirty === true) {
    //  var r = confirm("se han detectado cambios en la pantalla. ¿guardar los cambios antes de salir?");
    //  //if (r == true) {
    //  //  this.save().subscribe(data => {
    //  //    if (data) {
    //  //      alert("\'cambios guardados (data).\'");
    //  //    }
    //  //    alert("\'cambios guardados (next if).\'");
    //  //  });
    //  //}
    //}

  }

  implementarCapitulos(): boolean {
    return false;
    //if (this.globals.currentUser.idRolSeg != RolSegEnum.Titular) {
    //  return false;
    //}

    var capitulos = [
      "CAPDescripcionProyecto",
      "CAPElaboracionLineaBase",
      "CAPParticipacionCiudadana",
      "CAPCaracterizacionIA",
      "CAPEstrategiaManejoAmbiental",
      "CAPValorizacionEconomicaIA",
      "CAPDatosTitular",
      "CAPRepresentante",
      "CAPConsultora",
      "CAPCorreoAlerta",
      "CAPAreaEfectivaProyectoTemp",
      "CAPAreaEstudioTemp",
      "CAPAreaInfluenciaTemp",
      "CAPPuntoReferencialTemp",
      "CAPResumenEjecutivo",
      "CAPDescripcionProyectoTDR",
      "CAPEstudioLineaBase",
      "CAPPlanParticipacionCiudadana",
      "CAPCaracterizacionIATDR",
      "CAPEstrategiaIA",
      "CAPValoracionEconomicaTDR",
      "CAPEmpresaConsultora",
      "CAPOtrasConsideraciones",
      "CAPOpinionesTecnicas",
      "CAPBibliografia",
      "CAPAnexos",
      "CAPTitulosHabitantes",
      "CAPPagosOtrosRequisitosTUPA",
      "CAPAdjuntarDocumentos",
    ];
    var svc: ServiceBase = this.service as ServiceBase;

    var cap = capitulos.find((e) => e == svc.entidad);

    if (cap == null) return false;
    console.log(`capitulo:"${cap}"`);
    this.esCapitulo = true;

    //var svcCap: SOLCapituloService = InjectorInstance.get(SOLCapituloService);
    //svcCap.sel(this.keyvalue).subscribe((data: any) => {
    //  if (data.Estado == -650) {
    //    //finalizado
    //    this.formComp.mostrarFinalizar = false;
    //    this.formComp.mostrarAbrir = true;
    //    this.disableForm(true, true);
    //  } else {
    //    this.formComp.mostrarFinalizar = true;
    //    this.formComp.mostrarAbrir = false;
    //  }
    //});

    return true;
  }

  abrirCapitulo() {
    this.save().subscribe((data) => {
      if (data) {
        //var svc: SOLCapituloService = InjectorInstance.get(SOLCapituloService);
        //svc.Upd_Estado(this.keyvalue, -649).subscribe(() => { //Abierto
        //  //abierto
        //  console.log("Capitulo abierto." + this.keyvalue);
        //  this.globals.notify("El capitulo esta abierto");
        //  this.formComp.mostrarFinalizar = true;
        //  this.formComp.mostrarAbrir = false;

        //  //var tree = this.router.parseUrl(this.router.url);
        //  //tree.root.children["primary"].segments[2].path = this.id.toString();
        //  var url = this.router.url;
        //  this.router.onSameUrlNavigation = "reload";
        //  this.router
        //    .navigateByUrl("/", { skipLocationChange: true })
        //    .then(() => {
        //      this.router.navigateByUrl(url);
        //    }
        //    );
        //});
      }
    });
  }

  finalizarCapitulo() {
    this.save().subscribe((data) => {
      if (data) {
        //Validar grillas
        var grids = this.getGridFields();
        for (var i = 0; i <= grids.length; i++) {
          var grid = grids[i];
          if (grid) {
            if (!grid.skipValidation && grid.data.length == 0) {
              this.globals.notify(`Debe agregar al menos un item en "${grid.formComp.title}"`);
              return;
            }
          }
        }

        //Actualizar estado
        //var svc: SOLCapituloService = InjectorInstance.get(SOLCapituloService);
        //svc.Upd_Estado(this.keyvalue, -650).subscribe(() => {
        //  //finalizado
        //  console.log("Capitulo finalizado." + this.keyvalue);
        //  this.globals.notify("El capitulo esta finalizado");
        //  this.formComp.mostrarFinalizar = false;
        //  this.formComp.mostrarAbrir = true;
        //  this.disableForm(true, true);
        //});
      }
    });
  }

  validarCoordenadas() { }

  enviarSenace() { }

  disableForm(disableGrids: boolean = true, readOnlyGrids: boolean = false) {
    //ocultar controles
    this.getFormFields().forEach((item) => {
      if (item) {
        item.disabled = true;
      }
    });

    //ocultar botones
    this.formComp.mostrarSave = false;
    this.formComp.mostrarSaveClose = false;

    if (disableGrids) {
      this.getGridFields().forEach((item) => {
        if (item) {
          item.formComp.insertvisible = false;
          item.formComp.disabled = true;
          if (readOnlyGrids) item.formComp.readonly = true;
        }
      });
    }
  }

  setValidarCoordenada() {
    this.formComp.mostrarValidacion = true;
  }

  setEnviarSenace() {
    this.formComp.mostrarEnviar = true;
  }

  unSetEnviarSenace() {
    this.formComp.mostrarEnviar = false;
  }

  setEnviarDoc() {
    this.formComp.mostrarEnviarDoc = false;
    this.formComp.mostrarSave = false;
    this.formComp.mostrarSaveClose = true;
  }
  setDeshabilitarGuardar() {
    this.formComp.mostrarSave = false;
    this.formComp.mostrarSaveClose = false;
    this.formComp.mostrarClose = false;
  }

  fechaMinDac(restarAnio: number = 0) {
    return this.declaracionRow ? `01/01/${this.declaracionRow.Ano - restarAnio}` : null;
  }

  fechaMinDac1900() {
    return this.declaracionRow ? `01/01/1900` : null;
  }

  fechaMaxDac() {
    return this.declaracionRow ? `31/12/${this.declaracionRow.Ano}` : null;
  }

  aniosDeclaracion(): any[] {
    if (this.declaracionRow === undefined) return [];

    if (this.anios) return this.anios;
    this.anios = [];
    this.anios.push({ value: '', viewValue: '(Seleccione un valor)' })
    for (let i = this.declaracionRow.Ano; i >= this.declaracionRow.Ano - 4; i--) {
      //debugger
      this.anios.push({ value: `${i}`, viewValue: i })
    }
    return this.anios;
  }
  modoLectura() {
    if (this.formComp) {
      this.modoLecturaValue = true; //para saber cuándo es modo lectura en algunos escenarios con bloqueos de fields personalizados x pantalla
      this.formComp.textSalirNoGrabar = "Regresar";
      if (this.esPNT() === false) this.formComp.salirNoGrabar = true;
      //this.formComp.toolbarVisible = false
      this.formComp.mostrarSave = false;
      this.formComp.mostrarSaveClose = false;
      this.formComp.mostrarBotonesPersonalizados = false;
      this.getFormFields().forEach(f => {
        if (f) f.disabled = true;
      });
      this.getGridFields().forEach(g => {
        if (g) {
          //g.formComp.toolbarVisible = true;
          g.formComp.insertvisible = false;
          g.formComp.deleteallvisible = false;
          g.formComp.deleteMultiple = false;
          g.formComp.searchvisible = false;
          g.formComp.deletevisible = false;
          g.formComp.toolVisible = false;
          g.formComp.toolVisible = this.pnts41ExportarExcel.find(pnt => pnt[0] == g.entName) !== undefined;
          g.formComp.exportvisible = false;
          if (g.formComp.grid.editVisible == false) {
            g.formComp.grid.editLupaVisible = false;
          } else {
            g.formComp.grid.editVisible = false;
            g.formComp.grid.editLupaVisible = true;
          }
          g.formComp.grid.deleteVisible = false;
          g.formComp.grid.deleteMultiple = false;
          g.formComp.deleteallvisible = false;
          g.formComp.deleteMultiple = false;
          g.formComp.grid.showCheckboxes = false;
          g.formComp.grid.readOnly = true;
          //g.maingrid.fields.forEach(fl => { //posible reemplazo readOnly, PERO funciona intermitente... por "activar y desactivar fields personalizados implementados por pantalla"
          //  fl.disabled = true
          //})
        }
      });
      this.getCheckboxLists()?.forEach(cbl => {
        if (cbl) {
          cbl.disabled = true;
        }
      });
    }
  }

  //async getIdDacAnexo(idDondeEstoy: number, dondeEstas: string) {
  //  //idITMInversionley, itmInversionLey, EXPInversion, Inversion
  //  let obsList = new Array<Observable<Object>>();
  //  let idAnexo: number;
  //  //cambiar array aquí
  //  let nietos = [
  //    //hijo, padre, atributo que lleva al padre --hasta llegar a una PNT
  //    //indice 1 - Informacion del titular
  //    ["ITMAccionista", 'PNTTitularMinero', 'TitularMinero'],
  //    ["ITMResponsable", 'PNTTitularMinero', 'TitularMinero'],
  //    ["ITMConcesion", 'PNTConcesion', 'PantallaConcesion'],
  //    ["ITMInversion", 'PNTITMInversion', 'PantallaInversion'],
  //    ["ITMEstudio", 'PNTITMEstudio', 'PantallaEstudio'],
  //    ["FINActivoCorriente", 'PNTSituacionFinanciera', 'PantallaSituacionFinanciera'],
  //    ["FINEstadoResultadoN", 'PNTEstadoResultadoN', 'EstadoResultadoN'],
  //    ["FINEstadoResultadoF", 'PNTEstadoResultadoF', 'EstadoResultadoF'],
  //    ["FINMovimientoExist", 'PNTMovimientoExist', 'MovimientoExist'],
  //    ["FINInmueble", 'PNTInmueble', 'Inmueble'],
  //    ["FINDepreciacion", 'PNTDepreciacion', 'Depreciacion'],
  //    ["FINMetalicaRG", 'PNTMetalicaRG', 'PntMetalicaRegimenGeneral'],
  //    ["FINMetalicaDetalleRG", 'FINMetalicaRG', 'MetalicaRG'],
  //    ["FINMetalicaPPMPMA", 'PNTMetalicaPPMPMA', 'PantallaMetalicaPPMPMA'],
  //    ["FINVentaNoMetalica", 'PNTVentaNoMetalica', 'PantallaVentaNoMetalica'],
  //    ["FINVentaIndustrial", 'PNTVentaIndustrial', 'PantallaVentaIndustrial'],
  //    ["FINCostoProduccion", 'PNTVentaIndustrial', 'PantallaCostoProduccion'],
  //    ["FINVentaCarbonifero", 'PNTVentaCarbonifero', 'PantallaVentaCarbonifero'],
  //    ["FINOroAluvial", 'PNTOroAluvial', 'PantallaOroAluvial'],
  //    ["FINDividendo", 'PNTDividendo', 'Dividendo'],
  //    ["FINCambioPatrimonio", 'PNTCambioPatrimonio', 'CambioPatrimonio'],
  //    ["FINImpuestoEmpresa", 'PNTImpuestoEmpresa', 'ImpuestoEmpresa'],
  //    ["FINGastoPersonal", 'PNTGastoPersonal', 'PntGastoPersonal'],
  //    ["FINCategoriaPersonal", 'PNTCategoriaPersonal', 'PntCategoriaPersonal'],
  //    ["FINBalanceComprobacion", 'PNTBalanceComprobacion', 'BalanceComprobacion'],
  //    ["EXPReservaNoMetalica", 'PNTReservaNoMetalica', 'PantallaReservaNoMetalica'],
  //    ["EXPReservaCarbonifera", 'PNTReservaCarbonifera', 'PantallaReservaMetalica'],
  //    ["EXPProduccionNoMetalica", 'PNTProduccionNoMetalica', 'PntProduccionNoMetalica'],
  //    ["EXPProduccionCarbonifera", 'PNTProduccionCarbonifera', 'PntProduccionNoMetalica'],
  //    ["EXPProdCarbonDestino", 'PNTProdCarbonDestino', 'ProdCarbonDestino'],
  //    ["EXPCompra", 'PNTOrigenCompra', 'PantallaOrigenCompra'],
  //    ["EXPProduccionObtenida", 'PNTProduccionObtenida', 'PantallaProduccionObtenida'],
  //    ["EXPProduccionDestino", 'PNTProduccionDestino', 'PantallaProduccionDestino'],
  //    ["EXPReserva", 'PNTReservaMetalica', 'PantallaReservaMetalica'],
  //    ["EXPReservaNoMetalica", 'PNTReservaNoMetalica', 'PantallaReservaNoMetalica'],
  //    ["ADSPrograma", 'PNTProgramaLargoPlazo', 'PantallaPrograma'],
  //    ["ADSActividad", 'PNTBalanceSocialAct', 'PntBalanceSocialActividad'],
  //    ["ADSActividadImpacto", 'ADSActividad', 'Actividad'],
  //    ["ASAAreaSuperficial", 'PNTAreaSuperActMinera', 'PntAreaSuperactividadMinera'],
  //    ["ASAAreaSuperficial", 'PNTAreaSuperUsoMinero', 'PntAreaSuperUsoMinero'],
  //    ["ISOPerforacionRealizada", 'PNTPerforacionRealizada', 'PantallaPerforacionRealizada'],
  //    ["ISOGeologiaSuperficie", 'PNTGeologiaSuperficie', 'PntGeologiaSuperficie'],
  //    ["ISOGeoquimicaSuper", 'PNTGeoquimicaSuper', 'PntGeoquimicaSuperficie'],
  //    ["ISOVetaAflorante", 'PNTVetaAflorante', 'PantallaVetaAflorante'],
  //    ["ISOUbicacion", 'PNTUbicacion', 'Ubicacion'],
  //    ["ISOSondaje", 'PNTSondaje', 'PantallaSondaje']
  //  ];
  //  if (dondeEstas.startsWith('PNT')) { //si es un PNT -> seleccionar registro y return dacAnexo
  //    this.service.sel(this.id).subscribe((data2: any) => { //OBTENER LA DATA PARA CAPTURAR EL IDANEXO
  //      return data2.DeclaracionAnexo;
  //    });
  //    //let obs = this.service.sel(this.id);
  //    //obsList.push(obs.pipe(tap((data: any) => {
  //    //  idAnexo = data.DeclaracionAnexo
  //    //})));

  //    //await forkJoin(obsList).subscribe(_ => {
  //    //  return idAnexo;

  //    //});

  //  } else {//sino
  //    let srv = InjectorInstance.get(`${dondeEstas}Service`);
  //    srv.sel(idDondeEstoy).subscribe(data => {//selecciona al registro actual dondeestas.sel(this.id)...data
  //      let padre = nietos.find(n => n[0] == dondeEstas)
  //      if (padre === undefined || padre === null) return null; //ubicar en el array, //si no hay en el array devuelve null o undefined
  //      return this.getIdDacAnexo(data[srv.entidad], padre[1]);
  //      //averiguar id del padre (dentro de data del select anterior) (aprovecho nombre del padre)
  //      //return obtenerPadre(idPadre, entidadPadre);
  //    });
  //    //return 0; //eliminar luego de completar la funcion
  //  }
  //}

  async modoLecturaNoPNT(idDondeEstoy: number, dondeEstas: string): Promise<any> {
    return null;
    //let srvAnexo = InjectorInstance.get(DACAnexoService);
    //console.log(this)
    //let srv = InjectorInstance.get(`${dondeEstas}Service`);

    //if (dondeEstas.startsWith('PNT')) { //si es un PNT -> seleccionar registro y return dacAnexo
    //  srv.sel(idDondeEstoy).subscribe((data2: any) => { //OBTENER LA DATA PARA CAPTURAR EL IDANEXO
    //    srvAnexo.visiblexReactivacion(this.declaracionActual(), data2.DeclaracionAnexo, this.globals.currentUser.User).subscribe((data3: any) => {
    //      if (data3 == "lectura") {
    //        if (this.modoLecturaValue == false) this.modoLectura();
    //        //this.modoLectura();
    //      }
    //    })
    //  });

    //} else if (idDondeEstoy != 0) {//sino
    //  srv.sel(idDondeEstoy).subscribe(data => {//selecciona al registro actual dondeestas.sel(this.id)...data
    //    let padre: any;
    //    if (dondeEstas === 'ASAAreaSuperficial' && this.keyname === 'PntAreaSuperUsoMinero')
    //      padre = ["ASAAreaSuperficial", 'PNTAreaSuperUsoMinero', 'PntAreaSuperUsoMinero']
    //    else if (dondeEstas === 'ASAAreaSuperficial' && this.keyname === 'PntAreaSuperactividadMinera')
    //      padre = ["ASAAreaSuperficial", 'PNTAreaSuperActMinera', 'PntAreaSuperactividadMinera']
    //    else
    //      padre = this.nietos.find(n => n[0] == dondeEstas)

    //    if (padre === undefined || padre === null) {
    //      console.error(`Pantalla ${dondeEstas} no se ha enocntrado. Comunicarse con un administrador.`); //ubicar en el array, //si no hay en el array devuelve null o undefined
    //      return;
    //    }

    //    this.modoLecturaNoPNT(data[padre[2]], padre[1]);
    //  });
    //}
  }

  ejecutarPoblado() {
    this.pobladoPendiente = false;

    //if (this.declaracionActual() !== undefined && !this.flgConsolidado) {
    //  let srv = InjectorInstance.get(DACDeclaracionService);
    //  srv.sel(this.declaracionActual()).subscribe(async (dataDecAct: any) => { //renombrar data, data2 
    //    this.declaracionRow = dataDecAct;
    //    //if (dataDecAct.Estado == Constants.ESTADO_DAC_ENVIADO || dataDecAct.Estado == Constants.ESTADO_DAC_SOLICITUD_REACTIVACION) { // si ya se envió, todo va a estar en modo Lectura (verificar si se va a cambiar el miso estado de enviar a reactivado? o se usará otra columna de la tabla DACDeclaracion)
    //    //  this.modoLectura();
    //    //}
    //    if (dataDecAct.Estado == Constants.ESTADO_DAC_REACTIVADO) { //si el estado cambió de Enviado a Reactivado, verificar cuáles anexos están reactivados
    //      //fecha produccion afecta a 177, fecha inversion afecta a anexo 3
    //      //0 es porque ya venció, 1 es porque aún está dentro del plazo
    //      let pnt41: boolean = false;
    //      let pnt3: boolean = false;
    //      if (this.pnts41.find(d => d[0] == this.service.entidad)) {
    //        if (dataDecAct.AcredicacionProdMin == 0) {
    //          //todas las pantallas de 1.7.7 se ponen en modo lectura
    //          if (this.modoLecturaValue == false) this.modoLectura();
    //        }
    //        pnt41 = true;
    //      }
    //      if (this.pnts4.find(d => d[0] == this.service.entidad)) {
    //        //todas las pantallas de anexo 3 (4...) se ponen en modo lectura
    //        if (dataDecAct.AcredicacionInvMin == 0) {
    //          if (dataDecAct.AcredicacionProdMin == 1) {
    //            if (this.pnts4Sin42.find(d => d[0] == this.service.entidad)) {
    //              if (this.modoLecturaValue == false) this.modoLectura();
    //            }
    //          } else {
    //            if (this.pnts4.find(d => d[0] == this.service.entidad)) {
    //              if (this.modoLecturaValue == false) this.modoLectura();
    //            }
    //          }
    //        }
    //        pnt3 = true;
    //      }
    //      if (!pnt41 && !pnt3) {
    //        let srvAnexo = InjectorInstance.get(DACAnexoService);
    //        if (this.esPNT()) {
    //          this.service.sel(this.id).subscribe((data2: any) => { //OBTENER LA DATA PARA CAPTURAR EL IDANEXO
    //            srvAnexo.visiblexReactivacion(this.declaracionActual(), data2.DeclaracionAnexo, this.globals.currentUser.User).subscribe((dataEstadoAnexo: any) => {
    //              if (dataEstadoAnexo == "lectura") {
    //                if (this.modoLecturaValue == false) this.modoLectura();
    //              }
    //            })

    //          });
    //        } else {
    //          await this.modoLecturaNoPNT(this.id, this.service.entidad); //llamar a funcion recursiva, validar valor que retorna (si no devuelve se debe levantar un error)
    //        }
    //      }

    //    }
    //    else if (dataDecAct.Estado == Constants.ESTADO_DAC_PENDIENTE) {
    //      if (dataDecAct.AcredicacionInvMin == 1 && dataDecAct.AcredicacionProdMin == 0) {
    //        //todas las pantallas de 1.7.7 se ponen en modo lectura
    //        if (this.pnts41.find(d => d[0] == this.service.entidad)) {
    //          if (this.modoLecturaValue == false) this.modoLectura();
    //        }
    //      }
    //      if (dataDecAct.AcredicacionProdMin == 1 && dataDecAct.AcredicacionInvMin == 0) {
    //        //todas las pantallas de 1.7.7 se ponen en modo lectura
    //        if (this.pnts4Sin42.find(d => d[0] == this.service.entidad)) {
    //          if (this.modoLecturaValue == false) this.modoLectura();
    //        }
    //      }
    //      if (dataDecAct.AcredicacionProdMin == 0 && dataDecAct.AcredicacionInvMin == 0) {
    //        //todas las pantallas de 1.7.7 se ponen en modo lectura
    //        if (this.pnts4.find(d => d[0] == this.service.entidad)) {
    //          if (this.modoLecturaValue == false) this.modoLectura();
    //        }
    //        if (this.pnts41.find(d => d[0] == this.service.entidad)) {
    //          if (this.modoLecturaValue == false) this.modoLectura();
    //        }
    //      }
    //      if (this.globals.currentUser.TipoUser != 'P') {
    //        let srvAnexo = InjectorInstance.get(DACAnexoService);
    //        if (this.esPNT()) {
    //          this.service.sel(this.id).subscribe((data2: any) => { //OBTENER LA DATA PARA CAPTURAR EL IDANEXO
    //            srvAnexo.visiblexReactivacion(this.declaracionActual(), data2.DeclaracionAnexo, this.globals.currentUser.User).subscribe((dataEstadoAnexo: any) => {
    //              if (dataEstadoAnexo == "lectura") {

    //                if (this.modoLecturaValue == false) this.modoLectura();
    //              }
    //            })

    //          });
    //        } else {
    //          await this.modoLecturaNoPNT(this.id, this.service.entidad); //llamar a funcion recursiva, validar valor que retorna (si no devuelve se debe levantar un error)
    //        }
    //      }
    //    }
    //    else {
    //      if (this.modoLecturaValue == false) this.modoLectura();
    //    }
    //  })
    //}

    if (this.formComp) {
      //if (!this.router.url.includes("DACDeclaracion")) this.formComp.reiniciarEncabezadoFn();
      //if (this.esPNT()) {
      //  this.formComp.mostrarSaveClose = false;
      //  //this.formComp.mostrarClose = false;
      //}
      //else {
      //  this.formComp.notaVisible = false;
      //  this.formComp.salirNoGrabar = true;
      //}

      //this.formComp.toolbarVisible = !this.flgConsolidado;
      //let homeSvc = InjectorInstance.get(HomeService);
      //homeSvc.getxSessionInfo().subscribe((data: any) => {
      //this.globals.currentUser = data;
      //this.globals.tipoSite = data.TipoSite;

      //if (this.declaracionActual() !== undefined && this.globals.tipoSite != 'E' || this.flgConsolidado) {
      //  //setear solo lectura --- llamar funcion modoLectura
      //  this.modoLectura();
      //  //this.formComp.salirNoGrabar = true;
      //  //if (this.globals.tipoSite != 'E') { //intranet cambiar icono de lapiz a lupa
      //  //  this.getGridFields().forEach(g => {
      //  //    if (g) {
      //  //      if (g.formComp.grid.editVisible == false) {
      //  //        g.formComp.grid.editLupaVisible = false;
      //  //      } else {
      //  //        g.formComp.grid.editVisible = false;
      //  //        g.formComp.grid.editLupaVisible = true;
      //  //      }
      //  //    }
      //  //  });
      //  //}
      //  if (this.flgConsolidado) {
      //    //ocultar boton editar y eliminar
      //    this.formComp.toolbarVisible = false;
      //    this.formComp.salirNoGrabar = false;
      //    //this.getFormFields().forEach(fieldcell => { //pantallas con check no declarar (opcion a la funcion de la parte final de la funcion loadData())
      //    //  if (fieldcell) {
      //    //    if (fieldcell.affectedfields) {
      //    //      setTimeout(() => {
      //    //        if (fieldcell.FieldPanel.value == Constants.NO || fieldcell.FieldPanel.value == null) {
      //    //          fieldcell.invisible = true;
      //    //        }
      //    //      }, 1000);
      //    //    }
      //    //    return;
      //    //  }
      //    //})

      //    this.formComp.notaVisible = false;
      //    ////this.formComp.messageEnd = undefined;
      //    this.formComp.fieldsGroup.forEach(fg => {
      //      fg.notaVisible = false;
      //      ////  fg.notaFinal = null;
      //    })

      //    this.getGridFields().forEach(g => {
      //      if (g) {
      //        g.formComp.toolbarVisible = false;
      //        g.maingrid.actionsvisible = false;
      //        g.maingrid.deleteVisible = false;
      //        g.maingrid.editVisible = false;
      //        if (g.maingrid.pageStyle != "server") {
      //          g.maingrid.pageStyle = null;
      //          g.formComp.pageStyle = null;
      //          g.formComp.grid.pageStyle = null;
      //        }
      //        g.formComp.searchvisible = null;
      //        g.maingrid.showSearch = false;

      //      }
      //    });
      //  }
      //  //this.formComp.mostrarSave = false;
      //  //this.formComp.mostrarSaveClose = false;
      //  //this.getFormFields().forEach(f => {
      //  //  if (f) f.disabled = true;
      //  //});
      //  //this.getGridFields().forEach(g => {
      //  //  if (g) {
      //  //    g.formComp.insertvisible = false;
      //  //    g.formComp.deletevisible = false;
      //  //    g.formComp.grid.deleteVisible = false;
      //  //    g.formComp.grid.readOnly = true;
      //  //  }
      //  //});
      //  //this.getCheckboxLists()?.forEach(cbl => {
      //  //  if (cbl) {
      //  //    cbl.disabled = true;
      //  //  }
      //  //});
      //}
      //})
    }
    //suscribirse al evento de la grilla para actualizar la observacion
    this.getGridFields().forEach(g => {
      if (g) {
        //g._action.subscribe((dat: any) => {
        //  if (dat.eliminacionGrid) { //solo viene al eliminar un archivo deuna grilla (ejemplo 2.5)
        //    this.save().subscribe(_ => {

        //    })
        //  } else {
        //    this.fnObtenerObservacionAnexo(dat.DeclaracionAnexo, dat.messageIncompleto)

        //  }
        //})
        //console.log("grilla._saveForm: " + g.keyname)
        //g._saveForm.subscribe((dat: any) => {
        //  console.log("grilla._saveForm activado: " + g.keyname)
        //  g.searchObs().subscribe((dat2: any) => {
        //    setTimeout((_: any) => {
        //      console.log("grilla._saveForm activado actualizarEstado: " + g.keyname)
        //      this.actualizarEstadoAnexo().subscribe((result) => { });
        //    }, 250)
        //  })
        //})
        //g._saveGrid.subscribe((dat: any) => {
        //  console.log("grilla.sabeGrid activado: " + g.keyname)
        //  let vacio: number = 0;
        //  g.maingrid.fields.forEach((field: XtFieldPanelComponent) => {
        //    //field.isDirty = true;
        //    if (field.value === null || field.value == undefined) vacio++
        //  });
        //  if (vacio == 0) {
        //    this.saveGrids().subscribe((dat2: any) => {
        //      //setTimeout(_ => {
        //      //  console.log("grilla._saveForm activado actualizarEstado: " + dat2)
        //      //}, 250)
        //    })
        //  }
        //})
      }
    });


    //let urlActual = this.router.url;
    //const segments: UrlSegment[] = this.route.snapshot.url;
    //let urlComparar = "/" + segments[0].path + "/" + segments[1]?.path + "/" + segments[2]?.path;

    //if (urlActual === urlComparar) {
    //  this.mostrarDACDeclaracion = true;
    //  if (document.getElementById("IdDeclaracionForm")) {
    //    document.getElementById("IdDeclaracionForm").style.display = "";
    //  }

    //}
    //else {
    //  this.mostrarDACDeclaracion = false;
    //  if (document.getElementById("IdDeclaracionForm")) {
    //    document.getElementById("IdDeclaracionForm").style.display = "none";
    //  }
    //}

    //console.log("mostrarDACDeclaracion ", this.mostrarDACDeclaracion);

    if (this.id != 0) {
      this.loadData();
      this.keyname = this.modal ? this.keynameM : this.route.snapshot.queryParams["keyname"];
      if (this.keyname && this.keyname != 'null') {
        this.keyvalue = this.modal ? this.keyvalueM : parseInt(this.route.snapshot.queryParams["keyvalue"]);
        var field = this.getField(this.keyname);
        console.log("field", field);
        if (field) {
          field.disabled = true;
          field.invisible = true;
        } else {
          //throw new Error(`No se encontró el campo "${this.keyname}"`);
        }

      }

      if (this.mode == 'r') {
        this.disableForm(true, true);
      }

    } else {
      this.keyname = this.modal ? this.keynameM : this.route.snapshot.queryParams["keyname"];

      if (this.keyname && this.keyname != 'null') {
        this.keyvalue = this.modal ? this.keyvalueM : parseInt(this.route.snapshot.queryParams["keyvalue"]);
        console.log(`KV: ${this.keyvalue}`);
        var field = this.getField(this.keyname);
        if (field) {
          field.value = this.keyvalue;
          setTimeout(() => {
            field.disabled = true;
            field.invisible = true;
          }, 0);
        } else {
          //throw new Error(`No se encontró el campo "${this.keyname}"`);
        }
      }

      setTimeout(() => {
        this.setDefaultData();

      }, 100);
    }

    setTimeout(() => {
      //this.setFieldAvailability();
      if (document.getElementById("xtPanel")) {
        const inputsLength = document.getElementById("xtPanel").getElementsByTagName('input').length;
        const selectsLength = document.getElementById("xtPanel").getElementsByClassName("mat-select-value").length;
        const textAreaLength = document.getElementById("xtPanel").getElementsByTagName('textarea').length;
        const selectFieldDeclaracionAnexo = document.getElementById("FieldDeclaracionAnexo") === undefined ? 0 : 1;
        console.log("doc", inputsLength + textAreaLength);
        if ((inputsLength + textAreaLength + selectsLength - selectFieldDeclaracionAnexo) <= 0) {
          //this.setDeshabilitarGuardar();
        }
      }
    }, 100);
    if (this.formComp) {
      this.formComp.actionclicked.subscribe(($event) => {
        switch ($event.action) {
          case "save":
            //this.save(false, this.id == 0, !this.esCapitulo).toPromise();
            this.save(false, false, this.shouldValidateReqFields()).toPromise();
            break;
          case "saveclose":
            this.save(true, false, this.shouldValidateReqFields()).toPromise();
            break;
          case "close":
            this.close();
            break;
          case "closenosave":
            this.closenosave();
            break;
          case "finalizarCapitulo":
            this.finalizarCapitulo();
            break;
          case "abrirCapitulo":
            this.abrirCapitulo();
            break;
          case "validarCoordenadas":
            this.validarCoordenadas();
            break;
          case "enviarSenace":
            this.enviarSenace();
            break;
          case "enviarDoc":
            this.enviarDoc();
            break;
          default:
        }
      });

    }

    //Suscribirse a evento change
    let fields: any = this.getFormFields();
    for (let i = 0; i <= fields.length - 1; i++) {
      if (fields[i]) {
        fields[i].selectionChange.subscribe(($event: any) => {
          //this.conEventos = false;
          //console.log(this, $event)
          if ($event.checked) {
            //let srv = InjectorInstance.get(DACAnexoService);
            //if (this.data)
              //srv.verificarCheckNoDeclarar(this.data.DeclaracionAnexo).subscribe((data: any) => {
              //  if (data.Resultado)
              //    this.affectFields($event.fieldcell);
              //  else {
              //    this.globals.notify(data.Mensaje);
              //    setTimeout(() => {
              //      //fields[i].value = Constants.NO;
              //    }, 50);
              //  }

              //});

          } else {
            this.affectFields($event.fieldcell);
          }
          //this.conEventos = true;
        });
        fields[i].deleteFile.subscribe(($event:any) => {
          let idLaserfiche = $event.fieldcell.FieldPanel.filenoderef;
          this.EliminarFiles(idLaserfiche, $event.fieldcell).subscribe(data => {
            if (data) $event.fieldcell.FieldPanel.value = null
            //setTimeout(() => {
            //  this.save().subscribe(data => {
            //    console.log(data)
            //  })
            //}, 500)
          });
        });
      }
    }

    //Detectar click en tab group
    if (this.RootTab) {
      this.RootTab.selectedTabChange.subscribe($event => {

        var tree = this.router.parseUrl(this.router.url);
        tree.root.children["primary"].segments[2].path = `${this.id.toString()}_t${$event.index}`;
        var url = this.router.serializeUrl(tree);
        this.router.navigateByUrl(url, { replaceUrl: true });
      });

      //Seleccionar tab actual
      if (this.mode && this.mode.startsWith('t')) {
        //this.RootTab.selectedIndex = parseInt(this.mode.substr(1));
      }
    }

    this.configureAccess();
    this.implementarCapitulos();
  }

  private saveAffectedFields(): Observable<boolean> { //llamar en save editbase
    let resp = new Observable<boolean>((observer) => {
      this.getFormFields().forEach(async fieldcell => {
        if (fieldcell && fieldcell.affectedfields) {
          if (fieldcell.FieldPanel.value == 'Constants.SI') { //Si el form está desactivado, que recién ahí elimine los registros de la grilla y/o formulario
            var fields = fieldcell.affectedfields.split(',');
            let obsList = new Array<Observable<Object>>();
            let grids: GridBaseComponent[] = [];
            let noDeclararFields: boolean = false;
            for (var i = 0; i <= fields.length - 1; i++) {
              if (fields[i].startsWith('Field')) {
                noDeclararFields = true;
                let field = this.getFormFields().find((fl: any) => fl.FieldPanel.id == fields[i])
                if (field === undefined) throw new Error(`No se encontró el campo "${fields[i]}"`);
                field.value = null;
                field.FieldPanel.isDirty = false;
                observer.next(true);
                observer.complete();
              }

              if (fields[i].startsWith('Grid')) {
                let grid = this.getGridFields().find(e => e.id == fields[i]);
                if (grid === undefined) throw new Error(`No se encontró la grilla "${fields[i]}"`);
                grids.push(grid);
                //let srv = InjectorInstance.get(DACAnexoService);
                //let objGrid = grid as any;
                if (grid.formComp.deleteallvisible || this.pntEliminacionMasiva.find(e => e[0] === this.service.entidad)) { //buscamos en las pantallas que requieren eliminación masiva y no tienen el botón "eliminar todo" visible
                  obsList.push(grid.service.delByKey(grid.keyname, grid.keyvalue, this.globals.currentUser.User).pipe(tap(data => {
                    //this.onDataSaved(true);
                    grid.searchObs().subscribe(data => {
                      fieldcell.FieldPanel.clean()

                    })
                  })));

                } else {
                  grid.data.forEach(item => {
                    obsList.push(grid.service.del(item.Id, this.globals.currentUser.User).pipe(tap(data => {
                      //this.onDataSaved(true);
                      grid.searchObs().subscribe(data => {
                        fieldcell.FieldPanel.clean()

                      })
                    })));
                  })
                }
                grid.data = [];
              }
            }
            if (noDeclararFields) { //check en pantalla con solo fields
              var values: any = this.getValues();
              var svc: ServiceBase = this.service as ServiceBase;
              //Auditoria
              values['AudUser'] = this.globals.currentUser.User;
              values['AudIp'] = this.globals.currentUser.IP;
              var form = this.toForm(values);
              svc.upd(form).subscribe(async (data) => {
                this.getFormFields().filter(gfd => gfd).forEach(gfd => gfd.FieldPanel.clean()); //solucion al problema de titular minero al cambiar "datos complementarios"

                //await this.actualizarEstadoAnexo().toPromise();

                this.onDataSaved(false);
                this.clean();
                setTimeout(() => {
                  this.obtenerNotaFormulario(null);
                }, 600)
                observer.next(true);
                observer.complete();

              });
            }
            else {
              let loadingDialog = InjectorInstance.get(LoadingDialogService);
              loadingDialog.openDialog();

              await forkJoin(obsList).toPromise();
              setTimeout(() => {
                this.obtenerNotaFormulario(null);
              }, 600)
              //grids.forEach(g => {
              //  g.searchObs().subscribe(data => {
              //    fieldcell.FieldPanel.clean()
              //    observer.next(true);
              //    observer.complete();
              //  })
              //});
              //forkJoin(obsList).subscribe(() => {
              //  loadingDialog.hideDialog();
              //  console.log("eliminados, ", obsList);
              //  grids.forEach(g => {
              //    g.searchObs().subscribe(data => {
              //      fieldcell.FieldPanel.clean()
              //      observer.next(true);
              //      observer.complete();
              //      setTimeout(() => {
              //        this.obtenerNotaFormulario(null);
              //      }, 600)
              //    })
              //  });
              //});
            }
          }
          else {
            observer.next(false);
            observer.complete();
          }
        }
        //else {
        //  observer.next(false);
        //  observer.complete();
        //}
      });
      observer.next(false);
      observer.complete();
    });
    return resp;

  }
  private affectFields(fieldcell: XtFieldCellComponent) { //cambios
    //if (this.loading) return; //iff si no hy eventos directo return
    /*if (!this.conEventos) return;*/
    let confirmacionAceptada;
    let noDeclarar;
    let guardarUnaVez: number = 0;
    if (fieldcell.affectedfields) {
      var fields = fieldcell.affectedfields.split(',');
      for (var i = 0; i <= fields.length - 1; i++) {
        if (fields[i].startsWith('Field')) {
          let field = this.getFormFields().find((fl: any) => fl.FieldPanel.id == fields[i])
          if (field === undefined) throw new Error(`No se encontró el campo "${fields[i]}"`);
          let objField = field as any;
          if (objField.disabledInicial === undefined) objField.disabledInicial = field.disabled; //captura valor inicial
          if (objField.requiredInicial === undefined) objField.requiredInicial = field.required; //captura valor inicial
          if (objField.invisibleInicial === undefined) objField.invisibleInicial = field.invisible; //captura valor inicial
          //if (objField.valueInicial === undefined) objField.valueInicial = field.value; //captura valor inicial

          if (fieldcell.FieldPanel.value == 'Constants.SI') {
            noDeclarar = this.bloquearFields(field, null);
          } else {

            noDeclarar = this.bloquearFields(field, objField);
          }


          if (noDeclarar && !this.loading) {
            if (this.conEventos) {
              if (confirmacionAceptada === undefined)
                confirmacionAceptada = confirm('Al activar esta opción se eliminarán los registros de la pantalla. Esta acción es irreversible.')
              if (confirmacionAceptada) {
                //setTimeout(() => {//debería regresar el check a false y no se debe ocultar ningún botón, además el estado debe regresar a incompleto
                fieldcell.FieldPanel.value = 'Constants.SI'
                this.bloquearFields(field, null);
                //field.value = null;
              } else {
                fieldcell.FieldPanel.value = fieldcell.FieldPanel.uncheckedValue
                this.bloquearFields(field, objField);
                //field.value = objField.valueInicial;
                field.FieldPanel.isDirty = false;
              }
            }
          }

        }
        if (fields[i].startsWith('Grid')) { //recorrer las grillas
          let grid = this.getGridFields().find(e => e.id == fields[i]);
          if (grid === undefined) throw new Error(`No se encontró la grilla "${fields[i]}"`);
          //let srv = InjectorInstance.get(DACAnexoService);
          let objGrid = grid as any;
          if (objGrid.insertInicial === undefined) objGrid.insertInicial = grid.formComp.insertvisible; //captura valor inicial
          if (objGrid.deleteInicial === undefined) objGrid.deleteInicial = grid.formComp.deleteMultiple; //captura valor inicial
          grid.formComp.disabled = fieldcell.FieldPanel.value == fieldcell.FieldPanel.checkedValue;
          if (fieldcell.FieldPanel.value ==' Constants.SI') {
            grid.invisible = true;
            if (!this.modoLecturaValue) this.formComp.mostrarBotonesPersonalizados = false;
          } else {
            grid.invisible = false;
            if (!this.modoLecturaValue) this.formComp.mostrarBotonesPersonalizados = true;

            //if (objGrid.saveDesactivadoInicial !== undefined) {
            //}
            //objGrid.saveDesactivadoInicial = true
          }
          if (objGrid.insertInicial) grid.formComp.insertvisible = !grid.formComp.disabled;
          if (objGrid.deleteInicial) grid.formComp.deleteMultiple = !grid.formComp.disabled;
          if (grid.formComp.disabled && !this.loading) {
            if (grid.data) {
              //debugger
              if (grid.data.length != 0) {
                //falta otra validacion ? tabla con right join siempre va a tener data.length != 0, por eso casi siempre entra
                if (this.conEventos) {
                  if (confirmacionAceptada === undefined)
                    confirmacionAceptada = confirm('Al activar esta opción se eliminarán los registros de la grilla. Esta acción es irreversible.')
                  if (!confirmacionAceptada) {
                    //setTimeout(() => {//debería regresar el check a false y no se debe ocultar ningún botón, además el estado debe regresar a incompleto
                    grid.invisible = false;
                    //this.conEventos = false;
                    fieldcell.FieldPanel.value = fieldcell.FieldPanel.uncheckedValue; //No
                    grid.formComp.disabled = false;
                    //grid.maingrid.fields.forEach(item => {
                    //  item.disabled = false;
                    //});
                    //grid.lockfields(false);
                    grid.formComp.insertvisible = objGrid.insertInicial;
                    grid.formComp.deleteMultiple = objGrid.deleteInicial;
                    this.formComp.mostrarBotonesPersonalizados = true;
                    fieldcell.FieldPanel.clean()//limpiar el componente
                    //this.conEventos = true;
                    //}, 0);
                    return;
                  }
                }
                grid.invisible = true;
                grid.skipValidation = grid.formComp.disabled;
              }
              else {
                this.save(false, false, false).subscribe();
              }
            }
          }
          else {
            if (!this.loading) {
              if (guardarUnaVez === 0) {
                this.save(false, false, false).subscribe();
                guardarUnaVez++;
              }
            }
          }
        }
      }
      if (confirmacionAceptada) {
        this.save(false, false, false).subscribe();
      }
    }
  }


  bloquearFields(field: any, objField: any): boolean {
    if (objField === undefined || objField === null) {
      //this.getFormFields().forEach((field: any) => {
      if (field.FieldPanel.id !== "FieldDeclaracionAnexo" && field.FieldPanel.id !== "FieldNoDeclarar") {
        field.disabled = true;
        field.invisible = true;
        field.required = false;
        //field.value = null;
      }
      //})
      return true;
    } else {
      //this.getFormFields().forEach((field: any) => {
      if (field.FieldPanel.id !== "FieldDeclaracionAnexo" && field.FieldPanel.id !== "FieldNoDeclarar") {
        field.disabled = objField.disabledInicial;
        field.invisible = objField.invisibleInicial;
        field.required = objField.requiredInicial;
        //field.value = objField.valueInicial;
      }
      //})
      return false;
    }

  }
  public declaracionActual(): number {
    //recorrer segmentos y saber si
    let urlActual = this.router.url;
    const segments = urlActual.split('/');
    if (segments.length < 5) return undefined;
    if (segments[1].toString() != "edit-forms" || segments[2].toString() != "DACDeclaracion") return undefined;
    return parseInt(segments[3]?.toString());
  }
  public FillDropDown(
    field: XtFieldCellComponent,
    observableList: Observable<Object>
  ) {
    if (field) {
      //if (field.id == "FieldDeclaracionAnexo") return; //debería: en lugar de convertirse en texto, debe convertirse en campo numérico
      let quitarCombo = this.flgConsolidado &&
        field.datatype.toLocaleLowerCase() == "reference";

      if (quitarCombo) return;
      observableList.subscribe((data: any) => {

        field.datasource = data;
        //if (data && data.length === 1) {
        //  field.value = data[0].Id;
        //  field.disabled = true;
        //}
        if (field.nextVal != null) {
          field.value = field.nextVal;
          field.nextVal = null;
        }
      });
    }
  }

  public FillCheckBoxList(
    field: XtCheckboxListComponent,
    dataAll: Observable<Object>,
    dataSelected: Observable<Object>
  ) {

    //if (field) {
    //  dataAll.subscribe((dataAll: any) => {
    //    //field.dataAllTemp = dataAll
    //    field.dataAll = dataAll
    //    dataSelected.subscribe((dataSelected: any) => {
    //      field.dataSelected = dataSelected
    //    })
    //  })
    //}
    let obsList = new Array<Observable<Object>>();
    //obsList.push(obs);

    obsList.push(dataAll.pipe(tap((dataAll: any) => {
      field.dataAll = dataAll
    })));
    obsList.push(dataSelected.pipe(tap((dataSelected: any) => {
      field.dataSelected = dataSelected
    })));

    //Esperar a que terminen los updates
    forkJoin(obsList).subscribe(() => {
      //field.informacionCargada = true;
    });
  }
  //public FillCheckBoxList(
  //  field: XtCheckboxListComponent,
  //  dataAll: Observable<Object>,
  //  dataSelected: Observable<Object>
  //) {

  //  if (field) {
  //    let obsList = new Array<Observable<Object>>();
  //    let obs = dataAll;
  //    //obsList.push(obs);

  //    obsList.push(obs.pipe(tap((dataAll: any) => {
  //      field.dataAll = dataAll;
  //      dataSelected.subscribe((dataSelected: any) => {
  //        field.dataSelected = dataSelected
  //      })
  //    })));

  //    if (obsList.length == 0) {
  //      return
  //    }

  //    //Esperar que todos terminen
  //    forkJoin(obsList).subscribe(() => {
  //      //alert("checkbox mostrándose")
  //    });
  //  }
  //}
  public FillDropDownData(field: XtFieldCellComponent, data: any) {
    if (field) {
      if (data.length > 30) console.log("Data FILLDROPDOWN: ");
      field.datasource = data;
      if (data && data.length === 1) {
        field.value = data[0].Id;
        field.disabled = true;
      }
      if (field.nextVal != null) {
        field.value = field.nextVal;
        field.nextVal = null;
      }
    }
  }

  getFormFields(): Array<XtFieldCellComponent> {
    return new Array();
  }

  getGridFields(): Array<GridBaseComponent> {
    return new Array();
  }

  getRowText(obj: any) {
    let resp = obj["RowText"];
    if (!isNaN(resp)) {
      console.log(`RowText is a number: ${resp}`);
      resp = '';
    }
    return resp;
  }

  toFormCheckboxLists(values: any) {
    var resp = new FormData();
    var keys = Object.keys(values);

    for (var i = 0; i <= keys.length - 1; i++) {
      if (keys[i] != 'operaciones') {
        var val = values[keys[i]];
        if (val == null) continue;
        resp.append(keys[i], val);
      }
    }
    return resp;
  }
  public getCheckboxLists(): Array<XtCheckboxListComponent> {
    return new Array();
  }
  public saveCheckboxLists(id: number = 0): Observable<boolean> {
    let resp = new Observable<boolean>(observer => {
      let cbs = this.getCheckboxLists();
      if (cbs == null) {
        observer.next(false);
        observer.complete();
      }
      else {
        let obsList = new Array<Observable<Object>>();
        cbs.forEach(cb => {
          //codigo que no ingresa a capturestate pero tiene la funcion del forkjoin
          let svc: ServiceBase = InjectorInstance.get(cb.BaseEntity.concat("Service"));
          cb.verCambios(id).forEach(cambio => {
            if (cambio.operaciones == 'I') {
              cambio.AudIp = this.globals.currentUser.IP;
              var form = this.toFormCheckboxLists(cambio);
              let obs = svc.ins(form);
              obsList.push(obs.pipe(tap((data: any) => {
                //this.onDataSaved(true);
                cb.captureState(data["Id"], 'ins', cambio);
              })));

            } else {
              let obs = svc.del(cambio.Id, '1');
              obsList.push(obs.pipe(tap(data => {
                cb.captureState(cambio.Id, 'del', cambio);
              })));
            }
          })

          //codigo que ingresa al capturestate pero no tiene el forkjoin
          //let svc: ServiceBase = InjectorInstance.get(cb.BaseEntity.concat("Service"));
          //cb.verCambios(id).forEach(cambio => {
          //  if (cambio.operaciones == 'I') {
          //    var form = this.toFormCheckboxLists(cambio);
          //    svc.ins(form).subscribe(data => {
          //      this.onDataSaved(true);
          //      cb.captureState(data["Id"], 'ins', cambio);
          //    });
          //  } else {
          //    svc.del(cambio.Id, '1').subscribe(data => {
          //      this.onDataSaved(true);
          //      cb.captureState(cambio.Id, 'del', cambio);

          //    });
          //  }
          //})
        })
        //observer.next(true);
        //observer.complete();
        if (obsList.length == 0) {
          observer.next(false);
          observer.complete();
        }

        //Esperar que todos terminen
        forkJoin(obsList).subscribe(() => {
          observer.next(true);
          observer.complete();
        });


      }
    });

    return resp;
  }

  public saveGrids(id: number = 0, reqFields: boolean = true): Observable<boolean> {
    console.log("save grids");
    let resp = new Observable<boolean>(observer => {
      let grids = this.getGridFields();
      if (grids == null || grids[0] === undefined) {
        observer.next(false);
        observer.complete();
      }

      let obsList = new Array<Observable<Object>>();
      let confirmacion: boolean;
      grids.forEach(gridBase => {
        if (gridBase && reqFields) {
          if (this.pntCompletarCerosAlGuardar.find(pnt => pnt[0] == this.service.entidad)) { //Pantallas que requieren completar con 0
            let vacios: number = 0;
            gridBase.maingrid.fields.forEach(f => { //recorremos los fields del grid y verificamos si hay alguno vacio
              if (!f.invisible && !f.disabled && (f.value === null || f.value.toString() == "")) {
                vacios++;
              }
            })
            if (vacios !== 0) {//si hay alguno vacío, verificamos si requiere confirmación o no
              if (this.pntCompletarCerosAlGuardarConfirmacion.find(pnt => pnt[0] == this.service.entidad)) {
                if (confirmacion === undefined) { //pregunta por primera vez
                  if (confirm(`Hay campos en blanco en el formulario. ¿Desea completar los campos en blanco con ceros?`)) {
                    confirmacion = true;
                    gridBase.maingrid.fields.forEach(f => {
                      let val = f.value;
                      if (
                        !f.invisible &&
                        !f.disabled &&
                        (val === null || val.toString() == "")
                      ) {
                        f.value = 0;
                        f.isDirty = true;
                      }
                    })
                  }
                  else {
                    confirmacion = false;

                  }
                }
                else {
                  if (confirmacion) {
                    gridBase.maingrid.fields.forEach(f => {
                      let val = f.value;
                      if (
                        !f.invisible &&
                        !f.disabled &&
                        (val === null || val.toString() == "")
                      ) {
                        f.value = 0;
                        f.isDirty = true;
                      }
                    })
                  }
                }
              }
              else { //se guarda con 0 sin preguntar
                gridBase.maingrid.fields.forEach(f => {
                  let val = f.value;
                  if (
                    !f.invisible &&
                    !f.disabled &&
                    (val === null || val.toString() == "")
                  ) {
                    f.value = 0;
                    f.isDirty = true;
                  }
                })
              }
            }
          }
          if (gridBase.ShouldSave && !gridBase.invisible) {
            if (id != 0) gridBase.keyvalue = id;
            let ents = gridBase.changedEntities(true);
            if (ents.length != 0) {
              let fileField = null;
              let files: any[] = [];
              let fileIndexes: number[] = [];
              //obtener lista archivos, que campo es?, mandar lista indices, archivo y todo lo que se tiene que recibir
              ents.forEach((e, i) => {
                e.fields.forEach((f: XtFieldPanelComponent) => {
                  if (f.datatype.toLowerCase() == "file") {
                    fileField = f.ctx.col.source; //capturamos el nombre de la columna
                    let file = f.getFile();
                    if (file) files.push(file); //armamos el array de files
                    fileIndexes.push(i);
                  }
                })
              })
              let obs = gridBase.service.updBatch(ents, [], files, fileIndexes, fileField);
              //obsList.push(obs);

              obsList.push(obs.pipe(tap(data => {
                gridBase.search();
              })));

            }
          }
        }
      });

      if (obsList.length == 0) {
        observer.next(false);
        observer.complete();
      }

      //Esperar a que terminen los updates
      forkJoin(obsList).subscribe(() => {
        //this.deleteFileGrid();
        observer.next(true);
        observer.complete();
      });
    });

    return resp;
  }
  buscarPnt(keyname: any) {
    let pnt;

    if (this.router.url.includes("EXPReservaCarbonifera"))
      pnt = "PNTReservaCarbonifera"
    else if (this.router.url.includes("EXPProduccionCarbonifera"))
      pnt = "PNTProduccionCarbonifera"
    else {
      this.nietos.forEach(n => {
        console.log(n)
        if (n[2] == keyname)
          pnt = n[1];
      })

    }
    return pnt;
  }
  customActualizarEstadoInsert(): Observable<Object> {
    var resp = new Observable<Object>((observer: Subscriber<Object>) => {
      let obj: any = { Resultado: true, Mensaje: null };
      observer.next(obj);
      observer.complete();
    });
    return resp;
  }

  fnActualizarEstadoInsert(): any {
    return null;
    //if (this.keyname.toLowerCase().includes("pantalla") || this.keyname.toLowerCase().includes("titularminero") || this.keyname.toLowerCase().startsWith("pnt") || this.pntUpdEstadoInsertNoInicianPantalla.find(d => d[0] == this.keyname)) {
    //  let validar = this.pntNoUpdEstadoInsert.find(d => d[0] == this.keyname)
    //  if (validar !== undefined) return;
    //  let pnt = this.buscarPnt(this.keyname);
    //  if (pnt === undefined) return;
    //  //if (this.customActualizarEstadoInsert() == true) return
    //  let estadoInsert;
    //  let messageIncompleto: any = null;
    //  this.customActualizarEstadoInsert().subscribe((dt: any) => {
    //    if (dt.Resultado == 1) {
    //      estadoInsert = Constants.ESTADO_COMPLETO
    //    } else {
    //      estadoInsert = Constants.ESTADO_INCOMPLETO
    //      messageIncompleto = dt.Mensaje
    //    }
    //    if (!dt.saltarActualizacion) { //pantallas sin 2do detalle
    //      let srvAnexo = InjectorInstance.get(DACAnexoService);
    //      let srv = InjectorInstance.get(`${pnt}Service`);
    //      srv.sel(this.keyvalue).subscribe((data2: any) => { //OBTENER LA DATA PARA CAPTURAR EL IDANEXO
    //        srvAnexo.updEstado(data2.DeclaracionAnexo, estadoInsert, messageIncompleto).subscribe((data3: any) => {
    //          console.log("grid actualizar estado ", data2)
    //          this.reiniciarIndice();
    //          this.fnObtenerObservacionAnexo(data2.DeclaracionAnexo, messageIncompleto);
    //        })
    //      });
    //    }
    //    else { //pantalla con 2do detalle como ADSActividadareaImpacto (5.2.1)
    //      this.actualizarSegundoDetalle().subscribe((dt: any) => {

    //      })
    //    }
    //  });

    //}
  }

  actualizarSegundoDetalle(): Observable<Object> {
    var resp = new Observable<Object>((observer: Subscriber<Object>) => {
      let obj: any = { Resultado: true, Mensaje: null };
      observer.next(obj);
      observer.complete();
    });
    return resp;
  }
  beforeSave() {

  }
  public save(closeAfter: boolean = false, reload: boolean = false, reqFields: boolean = true): Observable<boolean> {
    this.beforeSave();
    var resp = new Observable<boolean>((observer) => {
      //if (!this.formComp.mostrarSave) { //se está comentando para los casos en los que el boton guardar se oculte pero el boton 'guardar y cerrar' esté visible
      //  observer.next(true);
      //  observer.complete();
      //  return;
      //}

      this.validateForm(reqFields).subscribe((esValido: boolean) => {

        if (!esValido) {
          observer.next(false);
          observer.complete();
          return;
        }

        var values = this.getValues();
        console.log("VALUES: " + JSON.stringify(values));
        var svc: ServiceBase = this.service as ServiceBase;

        //Auditoria
        //values['AudUser'] = this.globals.currentUser.User;
        //values['AudIp'] = this.globals.currentUser.IP;

        console.log("save values", values)
        var form = this.toForm(values);
        console.log("save form", form)
        if (this.id == 0) {
          svc.ins(form).subscribe(async (data: any) => {
            this.id = data["Id"];
            //await this.saveCheckboxLists(this.id).toPromise();
            //llamar funcion guardar ckbl
            await forkJoin([
              this.saveCheckboxLists(this.id),
              this.saveGrids(this.id)
            ]).toPromise();
            this.onDataSaved(true);

            console.log(data);
            this.globals.notify("Registro creado");
            console.log(`RESPONSE: ${JSON.stringify(data)}`);
            this.clean();

            let newUrl: string = this.router.url.replace("/0", `/${data["Id"]}`);
            let pathString = localStorage.getItem(`idx_${this.router.url}`);
            if (pathString) {
              localStorage.setItem(`idx_${newUrl}`, pathString);
            }

            this.router.navigateByUrl(newUrl, { replaceUrl: true });//reemplazo el 0 por el id creado, además navego y reemplazo para que ahora se quede en esta pantalla
            //actulizar estado a completo
            this.fnActualizarEstadoInsert();
            observer.next(true);
            observer.complete();
            if (closeAfter) {
              this.close();
            } else if (reload) {
              var tree = this.router.parseUrl(this.router.url);
              tree.root.children["primary"].segments[2].path = this.id.toString();
              var url = this.router.serializeUrl(tree);
              this.router.onSameUrlNavigation = "reload";
              this.router
                .navigateByUrl("/", { skipLocationChange: true })
                .then(() => {
                  this.router.navigateByUrl(url, { replaceUrl: true });
                }
                );
            } else {
              this.service.sel(this.id).subscribe((data) => {
                this.formComp.title2 = this.getRowText(data);// data["RowText"];
              });

              //Actualizar grids
              var grids = this.getGridFields();
              for (var i = 0; i <= grids.length; i++) {
                if (grids[i]) {
                  console.log("Grid refreshed");
                  grids[i].keyvalue = this.id;
                  grids[i].search();
                }
              }
              //this.deleteFileGrid();
            }
          });
        } else {
          svc.upd(form).subscribe(async (data) => {
            this.getFormFields().filter(gfd => gfd).forEach(gfd => gfd.FieldPanel.clean()); //solucion al problema de titular minero al cambiar "datos complementarios"

            //llamar funcion guardar ckbl
            await forkJoin([
              this.saveCheckboxLists(),
              this.saveGrids(0, reqFields)
            ]).toPromise();

            //await this.saveCheckboxLists().toPromise();

            await this.saveAffectedFields().toPromise();
            //await this.actualizarEstadoAnexo().toPromise();

            this.onDataSaved(false);
            this.globals.notify("Registro actualizado");
            this.clean();
            //this.deleteFileGrid();
            //alert("Registro actualizado");

            //this.lockField();//en grillas editables se debe bloquear al activar el toggle "no declarar"

            observer.next(true);
            observer.complete();
            //this.service.sel(this.id).subscribe((data) => {
            //  this.formComp.title2 = this.getRowText(data);// data["RowText"];
            //});
            if (closeAfter) this.close();


          });
        }
      });

    });
    return resp;
  }

  public customActualizarEstadoAnexoNoDeclarar(): Observable<Object> {
    return null;
    //var resp = new Observable<Object>((observer: Subscriber<Object>) => {
    //  let obj = { Resultado: true, Mensaje: null };
    //  observer.next(obj);
    //  observer.complete();
    //});
    //return resp;
  }

  fnObtenerObservacionAnexo(declaracionAnexo: number, mensajeIncompleto: string = null) { //recibe el id de la declaracionnexo para cuando es llamado desde grid_base
    //// volvemos a obtener la Observación para ver si sigue pintado o no en la pantalla
    //let srv = InjectorInstance.get(DACAnexoService);
    //srv.sel(declaracionAnexo).subscribe((data: any) => {
    //  if (data) {
    //    //obtener observaciones
    //    console.log(data);
    //    //setTimeout(() => {
    //    this.formComp.observacionAnexo = data.Observacion === "" ? null : data.Observacion;

    //    //}, 100)
    //  }
    //})
    //this.formComp.observacionAnexo = mensajeIncompleto;
  }

  public actualizarEstadoAnexo(): Observable<boolean> {
    return null;

    //var resp = new Observable<boolean>((observer: Subscriber<Boolean>) => {
    //  if (!this.esPNT() || (this.getGridFields() === undefined || this.getGridFields().length == 0 && this.getCheckboxLists() === null || this.getCheckboxLists() === undefined)) { //preguntar si están bien las condiciones después del &&
    //    observer.next(false);
    //    observer.complete();
    //    return;
    //  }
    //  let esValido = true;
    //  let messageIncompleto: any = null;
    //  let noDeclarar = false;
    //  let actualizarEstado: boolean = true;
    //  let saltarActualizarEstadoBtnGuardar: boolean = false;
    //  console.log("actualizarEstadoAnexo")
    //  this.getFormFields().forEach(fieldcell => { //pantallas con check no declarar
    //    if (fieldcell) {
    //      if (fieldcell.affectedfields) {
    //        if (fieldcell.FieldPanel.value == Constants.SI) {
    //          noDeclarar = true;
    //          let srv = InjectorInstance.get(DACAnexoService);
    //          this.customActualizarEstadoAnexoNoDeclarar().subscribe((dat: any) => {
    //            esValido = dat.Resultado;
    //            if (dat.Mensaje && dat.Mensaje !== "")
    //              messageIncompleto = dat.Mensaje;
    //            else
    //              messageIncompleto = null;
    //            //obtener el mensaje cuando esto es incompleto
    //            srv.updEstado(this.data.DeclaracionAnexo, esValido ? Constants.ESTADO_COMPLETO : Constants.ESTADO_INCOMPLETO, messageIncompleto).subscribe(data => {
    //              this.fnObtenerObservacionAnexo(this.data.DeclaracionAnexo, messageIncompleto);
    //              this.reiniciarIndice();
    //              //let srvD = InjectorInstance.get(DACDeclaracionService);
    //              //srvD.changeIdAnexo(this.data.DeclaracionAnexo, esValido ? Constants.ESTADO_COMPLETO : Constants.ESTADO_INCOMPLETO);
    //              observer.next(true);
    //              observer.complete();
    //              return;
    //            });
    //            return;
    //          })
    //          return;
    //        }
    //      }
    //      return;
    //    }
    //  })
    //  if (noDeclarar) return;
    //  this.getGridFields().forEach((grid: any) => {
    //    if (!esValido) return false; // si es falso no seguir buscando
    //    if (grid === undefined) {
    //      observer.next(true);
    //      observer.complete();
    //      return
    //    }
    //    if (grid.formComp.grid.data.length < grid.formComp.minRows) {
    //      //alert(`Debe ingresar al menos ${grid.formComp.minRows} registro(s) en la sección para que esté completa.`);

    //      esValido = false;
    //      if (grid.formComp.minRows !== 0) messageIncompleto = `Debe ingresar al menos ${grid.formComp.minRows} registro(s) en la sección  para que se encuentre completa.`;
    //      return false;
    //    }
    //    if (grid.maingrid.validarTodosFields) {
    //      grid.maingrid.fields.forEach((f, i) => { //como saltarme los fields del search en grillas con campos para editar?
    //        //if (f.title != "" && !f.invisible && !f.disabled && (f.value === undefined || f.value === null)) {
    //        if (!f.invisible && !f.disabled && (f.value === undefined || f.value === null)) {
    //          console.log("actualizarEstadoAnexo: 1.2: " + i + " " + f.id + " " + f.invisible + " " + f.disabled + " " + f.value)
    //          esValido = false;
    //          messageIncompleto = "Debe ingresar datos en los campos subrayados."
    //          return false;
    //        }
    //      })
    //    }
    //    actualizarEstado = grid.maingrid.actualizarEstado;
    //    saltarActualizarEstadoBtnGuardar = grid.maingrid.saltarActualizarEstadoBtnGuardar;
    //    console.log("actualizarEstadoAnexo: 2")

    //  })
    //  if (!saltarActualizarEstadoBtnGuardar) {
    //    this.getCheckboxLists().forEach(cb => {
    //      if (!esValido) return false; // si es falso no seguir buscando
    //      if (cb === undefined) {
    //        observer.next(true);
    //        observer.complete();
    //      }
    //      actualizarEstado = cb.actualizarEstado;
    //    })
    //    console.log("actualizarEstadoAnexo: 3")

    //    if (esValido)
    //      this.customValidarAnexo().subscribe((data: any) => { //cuando se requiere un customValidarAnexo
    //        console.log("actualizarEstadoAnexo: 4")

    //        esValido = data.Resultado;
    //        if (data.Mensaje && data.Mensaje !== "") messageIncompleto = data.Mensaje;
    //        //let srv = InjectorInstance.get(DACAnexoService);
    //        ////srv.updBatch([{ Id: this.data.DeclaracionAnexo, Estado: esValido ? Constants.ESTADO_COMPLETO : Constants.ESTADO_INCOMPLETO }], ["Estado"]).subscribe(data => {
    //        //srv.updEstado(this.data.DeclaracionAnexo, esValido ? Constants.ESTADO_COMPLETO : Constants.ESTADO_INCOMPLETO, messageIncompleto).subscribe(data => {
    //        //  //let srvD = InjectorInstance.get(DACDeclaracionService);
    //        //  //srvD.changeIdAnexo(this.data.DeclaracionAnexo, esValido ? Constants.ESTADO_COMPLETO : Constants.ESTADO_INCOMPLETO);
    //        //  if (actualizarEstado) this.reiniciarIndice();
    //        //  this.fnObtenerObservacionAnexo(this.data.DeclaracionAnexo, messageIncompleto);
    //        //  observer.next(true);
    //        //  observer.complete();

    //        //}); //Estado en la base de datos DAC
    //      })
    //    else {
    //      //let srv = InjectorInstance.get(DACAnexoService);
    //      if (messageIncompleto && messageIncompleto !== "")
    //        console.log("") // messageIncompleto = messageIncompleto.replaceAll('ñ', '{').replaceAll('<br>', '}').replaceAll('.', '_').replaceAll(';', ']').replaceAll(':', '[').replaceAll(' ', '-').replaceAll(',', '|');
    //      else
    //        messageIncompleto = null;
    //      //srv.updBatch([{ Id: this.data.DeclaracionAnexo, Estado: esValido ? Constants.ESTADO_COMPLETO : Constants.ESTADO_INCOMPLETO }], ["Estado"]).subscribe(data => {
    //      //srv.updEstado(this.data.DeclaracionAnexo, esValido ? Constants.ESTADO_COMPLETO : Constants.ESTADO_INCOMPLETO, messageIncompleto).subscribe(data => {
    //      //  //let srvD = InjectorInstance.get(DACDeclaracionService);
    //      //  //srvD.changeIdAnexo(this.data.DeclaracionAnexo, esValido ? Constants.ESTADO_COMPLETO : Constants.ESTADO_INCOMPLETO);
    //      //  this.reiniciarIndice();
    //      //  this.fnObtenerObservacionAnexo(this.data.DeclaracionAnexo, messageIncompleto);
    //      //  observer.next(true);
    //      //  observer.complete();
    //      //}); //Estado en la base de datos DAC
    //    }
    //  }
    //  else {
    //    this.customValidarAnexo().subscribe((data: any) => { //cuando se requiere un customValidarAnexo
    //      esValido = data.Resultado;
    //      if (data.Mensaje && data.Mensaje !== "") messageIncompleto = data.Mensaje;
    //      //let srv = InjectorInstance.get(DACAnexoService);
    //      //srv.updBatch([{ Id: this.data.DeclaracionAnexo, Estado: esValido ? Constants.ESTADO_COMPLETO : Constants.ESTADO_INCOMPLETO }], ["Estado"]).subscribe(data => {
    //      //srv.updEstado(this.data.DeclaracionAnexo, esValido ? Constants.ESTADO_COMPLETO : Constants.ESTADO_INCOMPLETO, messageIncompleto).subscribe(data => {
    //        //let srvD = InjectorInstance.get(DACDeclaracionService);
    //        //srvD.changeIdAnexo(this.data.DeclaracionAnexo, esValido ? Constants.ESTADO_COMPLETO : Constants.ESTADO_INCOMPLETO);
    //        //if (actualizarEstado) this.reiniciarIndice();
    //        //this.fnObtenerObservacionAnexo(this.data.DeclaracionAnexo, messageIncompleto);
    //        //observer.next(true);
    //        //observer.complete();
    //      //}); //Estado en la base de datos DAC
    //    })

    //  }
    //});
    //return resp;
  }
  public reiniciarIndice() {
    //let srvD = InjectorInstance.get(DACDeclaracionService);
    //srvD.reiniciarIndice();
  }
  public customValidarAnexo(): Observable<Object> {
    var resp = new Observable<Object>((observer: Subscriber<Object>) => {
      let obj: any = { Resultado: true, Mensaje: null };
      observer.next(obj);
      observer.complete();
    });
    return resp;
  }

  public enviarDoc(closeAfter: boolean = false, reload: boolean = false): Observable<boolean> {
    var resp = new Observable<boolean>((observer) => {
      //if (!this.formComp.mostrarEnviarDoc) {
      //  observer.next(true);
      //  observer.complete();
      //  return;
      //}

      //Esto hay que cambiar por un subscribe (Revisar la función Save);
      if (!this.validateForm()) {
        observer.next(false);
        observer.complete();
        return;
      }
      var values = this.getValues();
      console.log("VALUES: " + JSON.stringify(values));
      var svc: ServiceBase = this.service as ServiceBase;

      var form = this.toForm(values);
      if (this.id == 0) {
        svc.ins(form).subscribe((data: any) => {
          this.id = data["Id"];
          this.onDataSaved(true);
          this.globals.notify("Registro creado");
          //alert("Registro creado");
          console.log(`RESPONSE: ${JSON.stringify(data)}`);
          observer.next(true);
          observer.complete();
          if (closeAfter) {
            this.close();
          } else if (reload) {
            var tree = this.router.parseUrl(this.router.url);
            tree.root.children["primary"].segments[2].path = this.id.toString();
            var url = this.router.serializeUrl(tree);
            this.router.onSameUrlNavigation = "reload";
            this.router
              .navigateByUrl("/", { skipLocationChange: true })
              .then(() => {
                this.router.navigateByUrl(url, { replaceUrl: true });
              }
              );
          } else {
            this.service.sel(this.id).subscribe((data) => {
              this.formComp.title2 = this.getRowText(data);// data["RowText"];
            });

            //Actualizar grids
            var grids = this.getGridFields();
            for (var i = 0; i <= grids.length; i++) {
              if (grids[i]) {
                console.log("Grid refreshed");
                grids[i].keyvalue = this.id;
                grids[i].search();
              }
            }
          }
        });
      } else {
        svc.upd(form).subscribe((data) => {
          this.onDataSaved(false);
          this.globals.notify("Registro actualizado");
          //alert("Registro actualizado");
          observer.next(true);
          observer.complete();
          this.service.sel(this.id).subscribe((data) => {
            this.formComp.title2 = this.getRowText(data);// data["RowText"];
          });
          if (closeAfter) this.close();
        });
      }
    });
    return resp;
  }

  toForm(values: any) {
    var resp = new FormData();
    var keys = Object.keys(values);

    for (var i = 0; i <= keys.length - 1; i++) {
      var val = values[keys[i]];
      if (val === null) continue; //agregar validacion para guardar 0 (triple igual)
      resp.append(keys[i], val);
    }

    return resp;
  }

  close() {
    window.history.back();
  }

  closenosave() {
    this.clean();
    this.getCheckboxLists().forEach((dat: XtCheckboxListComponent) => {
      if (dat) {
        dat.fnClean();
      }
    })
    window.history.back();
  }

  getValues() {
    var resp: any = new Object();
    var fields: any = this.getFormFields();
    for (var i = 0; i <= fields.length - 1; i++) {
      if (fields[i]) {
        resp[fields[i].getColumnId()] = fields[i].value;
        if (fields[i].datatype == "file") {
          var file = fields[i].FieldPanel.getFile();
          resp[fields[i].getColumnId() + "_file"] = file;
        }
      }
    }

    if (this.id != 0) {
      resp["Id"] = this.id;
    } else if (this.keyname) {
      resp[this.keyname] = this.keyvalue;
    }

    return resp;
  }

  getField(id: string) {
    var fields = this.getFormFields();
    var resp = fields.find((e, i) => e !== undefined && e.id == `Field${id}`);
    if (resp) return resp;
    return undefined;
  }

  public loadData() {
    this.loading = true;
    this.conEventos = false;

    this.service.sel(this.id).subscribe((data: any) => {
      console.log(`DATA: ${JSON.stringify(data)}`);
      console.log("DATA this.formComp", this.formComp)
      if (this.globals.tipoSite !== "E")
        this.formComp.title2 = `${data["RowText"]} - ${data["TitularMinero_text"]}`;// data["RowText"];
      else
        this.formComp.title2 = this.getRowText(data);// data["RowText"];

      let keys = Object.keys(data);
      //var fields = this.getFormFields();
      for (let i = 0; i <= keys.length - 1; i++) {
        let field = this.getField(keys[i]);
        //var field = fields.find((e, idx) => e.id == `Field${ keys[i] } `)
        if (field) {
          let quitarCombo = this.flgConsolidado &&
            field.datatype.toLocaleLowerCase() == "reference" &&
            (field.designer === undefined ||
              field.designer === null || field.designer === "");
          field.value = data[keys[i]];
          if (quitarCombo) {
            field.datatype = "Text";
            //field.FieldPanel.datatype = "Text";

            setTimeout(function () {
              let d = data[`${keys[i]}_text`];
              if (d) {
                field.value = d;
              }
            }, 0);

          }
        }
      }

      //Activar evento change para toggles
      var fields = this.getFormFields();
      for (var i = 0; i <= fields.length - 1; i++) {
        if (fields[i] && (fields[i].FieldPanel.effectiveDesigner == 'toggle' || fields[i].FieldPanel.effectiveDesigner == 'checkbox')) {
          this.affectFields(fields[i]);
        }
      }
      this.data = data;
      //await this.actualizarEstadoAnexo().toPromise(); //validar cada vez que se cargue la información de la pantalla
      this.clean();
      setTimeout(() => {
        this.onReviewDataLoaded(keys, data);
      }, 600)
      this.onDataLoaded();
      //this.deleteFileGrid();
      //debugger
      //if (this.declaracionActual() !== undefined && this.globals.tipoSite != 'E') {

      //  //setear solo lectura
      //  this.formComp.mostrarSave = false;
      //  this.formComp.mostrarSaveClose = false;
      //  this.getFormFields().forEach(f => {
      //    if (f) f.disabled = true;
      //  })
      //  this.getGridFields().forEach(g => {
      //    if (g) {
      //      g.insertvisible = false;
      //      g.deletevisible = false;
      //    }
      //  })
      //}
      this.loading = false;
      this.conEventos = true;
      this.ocultarCheckNoDeclararConsolidado();
    });

  }
  ocultarCheckNoDeclararConsolidado() {
    if (this.formComp) {
      if (this.declaracionActual() !== undefined && this.globals.tipoSite != 'E' || this.flgConsolidado) {
        if (this.flgConsolidado) {
          this.getFormFields().forEach(fieldcell => { //pantallas con check no declarar
            if (fieldcell) {
              if (fieldcell.affectedfields) {
                //setTimeout(() => {
                if (fieldcell.FieldPanel.value == 'Constants.NO' || fieldcell.FieldPanel.value == null) {
                  fieldcell.invisible = true;
                }
                //}, 1000);
              }
              return;
            }
          })

        }
      }
    }
  }
  //public deleteFileGrid() {
  //  var grids = this.getGridFields();
  //  for (var i = 0; i < grids.length; i++) {
  //    if (grids[i]) {
  //      console.log(`fields en xtGrid: ${ grids[i].maingrid.fields.length } `)
  //      this.registrarEventosGrilla(grids[i].maingrid); //no encontro campos no se hace nada
  //      grids[i].maingrid.fields.changes.subscribe(_ => this.registrarEventosGrilla(grids[i].maingrid)) //registra los cambios
  //    }
  //  }
  //}
  //private registrarEventosGrilla(xtGrid: XtGridComponent) {
  //  xtGrid.fields.forEach(f => {
  //    if (f) {
  //      if (f.datatype?.toLowerCase() == "file") {
  //        f.deleteFile.subscribe($event => {
  //          let idLaserfiche = $event.FieldPanel.filenoderef;
  //          this.EliminarFilesGrids(idLaserfiche, $event.FieldPanel).subscribe(data => {
  //            if (data) $event.FieldPanel.value = null
  //          });
  //        });
  //      }
  //    }
  //  });
  //}
  onDataLoaded() { }

  onReviewDataLoaded(keys: any, data: any) {
    if (!this.flgConsolidado)
      this.obtenerNotaFormulario(data);
  }

  obtenerCaminoDeMigas(dataPNT: any): string {
    //Valida si la tabla contiene el texto PNT
    let caminomigas: string;
    //debugger


    let entidad = this.service.entidad;
    if (entidad) {
      let contains = entidad.startsWith("PNT");
      if (contains) {
        //this.idCaminoMigas = dataPNT.DeclaracionAnexo;
        //let srv = InjectorInstance.get(DACDeclaracionService);
        //caminomigas = this.declaracion.crearCaminoMigasPorIdDacAnexo(dataPNT.DeclaracionAnexo);

        //this.formComp.caminodemigas = caminomigas;
      }
    }
    return caminomigas;
  }
  setDefaultData() { }

  setFieldAvailability() { }

  //public validateForm(reqFields: boolean = true): Observable<boolean> {
  //  this.reqFieldsEmpty = false;

  //  var resp = new Observable<boolean>((observer) => {

  //    //Validar grids
  //    if (reqFields && this.getGridFields()) {
  //      this.getGridFields().forEach(gf => {
  //        if (gf) {
  //          gf.changedEntities(true).forEach(ent => ent.fields.forEach((field: XtFieldPanelComponent) => {
  //            let val = field.value;
  //            if (
  //              !field.invisible &&
  //              !field.disabled &&
  //              field.required &&
  //              (val == null || val.toString() == "")
  //            ) {
  //              this.reqFieldsEmpty = true;

  //              this.globals.notify(`El valor del campo "${field.effectiveTitle}" es requerido`);
  //              field.focus();
  //              observer.next(false);
  //              observer.complete();
  //              return;
  //            }
  //            return true;
  //          }));
  //        }
  //      });
  //    }

  //    //Validar campos
  //    var fields = this.getFormFields();

  //    for (let i = 0; i <= fields.length; i++) {
  //      let field = fields[i];
  //      if (field) {
  //        let val = field.value;
  //        //validar campos requeridos
  //        if (
  //          //reqFields &&
  //          !field.invisible &&
  //          !field.disabled &&
  //          field.required.toString() == "true" &&
  //          (val == null || val.toString().trim() == "")
  //        ) {
  //          this.reqFieldsEmpty = true;
  //          if (reqFields) {
  //            console.log(`Campo vacio: ${ field.id }.Valor: ${ field.value } `);
  //            this.globals.notify(`El valor del campo "${field.title}" es requerido`);
  //            //field.FieldPanel.focus();
  //            //alert(`El valor del campo "${field.title}" es requerido`);
  //            observer.next(false);
  //            observer.complete();
  //            return;
  //          }
  //        }
  //        //validar tamaño de archivos y extensiones
  //        if (field.datatype == "file") {
  //          if (field.FieldPanel.FileControl.nativeElement.files.length != 0) {
  //            var file = field.FieldPanel.FileControl.nativeElement.files[0];
  //            var ext = file.name.split('.').pop();

  //            if (ext) {

  //              if (field.FieldPanel.extensions) {

  //                var exts = field.FieldPanel.extensions.split(",");
  //                var valido = false;
  //                for (let j = 0; j < exts.length; j++) {

  //                  if (ext.toLowerCase() == exts[j]) {
  //                    valido = true;
  //                    break;
  //                  }
  //                }
  //                if (valido == false) {
  //                  this.globals.notify(
  //                    `El archivo "${field.title}" debe contar con extensión "${field.FieldPanel.extensions}"`
  //                  );
  //                  observer.next(false);
  //                  observer.complete();
  //                  return;
  //                }
  //              }
  //            }

  //            if (file.size > field.FieldPanel.maxsize * 1024 * 1024) {
  //              this.globals.notify(
  //                `El tamaño del archivo "${field.title}" no debe exceder ${ field.FieldPanel.maxsize } Mb`
  //              );
  //              observer.next(false);
  //              observer.complete();
  //              return;
  //            }

  //          }
  //        }

  //        //validar valor maximo campos numericos
  //        if (field.datatype == "integer" || field.datatype == "decimal") {
  //          if (field.value > Number(field.FieldPanel.maxval)) {
  //            this.globals.notify(
  //              `El valor máximo del campo "${field.title}" es "${field.FieldPanel.maxval} "`
  //            );
  //            observer.next(false);
  //            observer.complete();
  //            return;
  //          }
  //        }

  //        //Validar checkboxlists
  //        if (reqFields && this.getCheckboxLists()) {
  //          this.getCheckboxLists().forEach(cb => {
  //            if (cb.minSelection != 0 && cb.minSelection > cb.allSelection) {
  //              this.globals.notify(
  //                `Debe marcar como mínimo ${ cb.minSelection } opción(es) de la lista`
  //              );
  //              observer.next(false);
  //              observer.complete();
  //              return;
  //            } else if (cb.maxSelection != 0 && cb.maxSelection < cb.allSelection) {
  //              this.globals.notify(`Debe marcar como máximo ${ cb.maxSelection } opción(es) de la lista`);

  //              observer.next(false);
  //              observer.complete();
  //              return;
  //            }
  //          })
  //        }
  //      }
  //    }
  //    observer.next(true);
  //    observer.complete();

  //  });

  //  return resp;


  //}

  public validateForm(reqFields: boolean = true): Observable<boolean> { //validar formatos de golpe
    this.reqFieldsEmpty = false;
    let camposRequeridosVacios = 0;
    var resp = new Observable<boolean>((observer) => {
      //Validar grids
      if (reqFields && this.getGridFields()) {
        this.getGridFields().forEach(gf => {
          if (gf) {
            if (!this.pntCompletarCerosAlGuardar.find(pnt => pnt[0] == this.service.entidad)) {
              gf.changedEntities(true).forEach(ent => ent.fields.forEach((field: XtFieldPanelComponent) => {
                let val = field.value;
                if (
                  !field.invisible &&
                  !field.disabled &&
                  field.required &&
                  (val == null || val.toString() === "")
                ) {
                  this.reqFieldsEmpty = true;
                  field.invalid = true;
                  this.globals.notify(`Los valores subrayados son requeridos`);
                  //this.globals.notify(`Los campos con * son requeridos`);
                  //field.focus();
                  observer.next(false);
                  observer.complete();
                  return false;
                }
                return true;
              }));
            }
          }
        });
      }
      if (this.reqFieldsEmpty) {
        observer.next(false);
        observer.complete();
        return;
      }
      //Validar campos
      var fields = this.getFormFields();

      for (let i = 0; i <= fields.length; i++) {
        let field: any = fields[i];
        if (field) {
          if (field.id == "FieldDeclaracionAnexo" && field.value === null) {
            this.globals.notify("Field Declaracion Anexo is null")
            observer.next(false);
            observer.complete();
            return;
          }
          let val: any = field.value;
          //validar campos requeridos
          if (
            //reqFields &&
            !field.invisible &&
            !field.disabled &&
            field.required.toString() == "true" &&
            (val == null || val.toString().trim() === "" || val.toString().trim() === "Invalid Date")
          ) {
            this.reqFieldsEmpty = true;
            if (reqFields) {
              console.log(`Campo vacio: ${field.id}.Valor: ${field.value} `);
              //this.globals.notify(`El valor del campo "${field.title}" es requerido`);
              camposRequeridosVacios++;
              field.FieldPanel.focus();
              field.FieldPanel.invalid = true;
              //let idFd = field.FieldPanel.id;
              //let fd = document.getElementById(idFd);
              //fd.children[0].children[0].children[0].children[0].classList.add("require-empty");
              //console.log(fd)
              //observer.next(false);
              //observer.complete();
              //return;
            }
          } else {
            field.FieldPanel.invalid = false;
          }
          //validar tamaño de archivos y extensiones
          if (field.datatype == "file") {
            if (field.FieldPanel.FileControl.nativeElement.files.length != 0) {
              var file = field.FieldPanel.FileControl.nativeElement.files[0];
              var ext = file.name.split('.').pop();

              if (ext) {

                if (field.FieldPanel.extensions) {

                  var exts = field.FieldPanel.extensions.split(",");
                  var valido = false;
                  for (let j = 0; j < exts.length; j++) {

                    if (ext.toLowerCase() == exts[j].trim()) {
                      valido = true;
                      break;
                    }
                  }
                  if (valido == false) {
                    this.globals.notify(
                      `El archivo debe contar con extensión "${field.FieldPanel.extensions}"`
                    );
                    observer.next(false);
                    observer.complete();
                    return;
                  }
                }
              }

              if (file.size > field.FieldPanel.maxsize * 1024 * 1024) {
                this.globals.notify(
                  `El tamaño del archivo no debe exceder ${field.FieldPanel.maxsize} Mb`
                );
                observer.next(false);
                observer.complete();
                return;
              }

            }
          }

          //validar valor maximo campos numericos
          if (field.datatype == "integer" || field.datatype == "decimal") {
            if (field.value > Number(field.FieldPanel.maxval)) {
              this.globals.notify(
                `El valor máximo del campo "${field.title}" es "${field.FieldPanel.maxval} "`
              );
              observer.next(false);
              observer.complete();
              return;
            }
          }

          //Validar checkboxlists
          if (reqFields && this.getCheckboxLists()) {
            this.getCheckboxLists().forEach(cb => {
              //cb.validateCheckbox().subscribe(data => {
              //  if (!data) {
              //    observer.next(false);
              //    observer.complete();
              //    return
              //  }
              //})
              if (cb.minSelection != 0 && cb.minSelection > cb.allSelection) {
                this.globals.notify(
                  `Debe marcar como mínimo ${cb.minSelection} opción(es) en ${cb.NameLabel = "" ? "la lista" : cb.NameLabel} `
                );
                observer.next(false);
                observer.complete();
                return;
              } else if (cb.maxSelection != 0 && cb.maxSelection < cb.allSelection) {
                this.globals.notify(`Debe marcar como máximo ${cb.maxSelection} opción(es) en ${cb.NameLabel = "" ? "la lista" : cb.NameLabel} `);

                observer.next(false);
                observer.complete();
                return;
              }
            })
          }
        }
      }

      if (camposRequeridosVacios > 0) {
        this.globals.notify(`El campo con(*) es requerido`);
        observer.next(false);
        observer.complete();
        return
      }
      observer.next(true);
      observer.complete();

    });

    return resp;


  }

  public EliminarFiles(idLaserfiche: any, fieldcell: XtFieldCellComponent): Observable<boolean> {
    console.log(this)
    var resp = new Observable<boolean>((observer) => {
      if (confirm(`Se va a eliminar el archivo subido. Esta acción es irreversible`)) {
        //llamar funcion delete laserfiche
        //this.existeArchivo = this.FieldDocumento.FieldPanel.FileControl.nativeElement.files.length > 0;
        //if (this.existeArchivo) {
        //alert("existe archivo, voy a eliminar")
        //let idLaserfiche = this.FieldDocumento.value.split('|');
        this.service.deleteFile(idLaserfiche, this.id, fieldcell.getColumnId()).subscribe(data => { //mandar el id de laserfiche que se encuentra en el campo "Documento"
          //this.FieldDocumento.value = "";
          //this.FieldNombre.value = "";
          //this.FieldExtension.value = "";
          //this.FieldTamano.value = "";
          //this.buttonDelete = false;
          //this.save().subscribe(data2 => { //me muestra "El campo Documento es requerido"
          this.afterDeleteFile();
          observer.next(true);
          observer.complete();

          //let obj = { Id: this.id };
          //obj[fieldcell.getColumnId()] = null;
          //this.service.updBatch([obj], [fieldcell.getColumnId()]).subscribe(data2 => {
          //  //return true;
          //  observer.next(true);
          //  observer.complete();
          //})
        })
      } else {
        observer.next(false);
        observer.complete();
        //return
      }

    });
    return resp;
  }

  afterDeleteFile() {

  }

  async canDeactivate() {
    // you logic goes here, whatever that may be & must return either True or false
    //let srvD = InjectorInstance.get(DACDeclaracionService);

    //if (this.isDirty === true && (this.globals.tipoSite == "E" || (this.globals.tipoSite != "E" && !this.router.url.includes("DACDeclaracion")))) {
    if (this.isDirty === true) {
      if (confirm(`Se detectaron cambios en la pantalla.
        Aceptar: Se guardará los cambios, siempre y cuando haya completado todos los campos obligatorios.
          Cancelar: No graba los cambios y sale de la pantalla.`)) {

        let a = await forkJoin(
          this.save()
        ).toPromise();
        if (a[0]) {
          //srvD.changeCaminoMigas();
        }
        return a[0];
      }
    }
    //srvD.changeCaminoMigas();
    return true; // la pnt no está dirty o no quiere guardar los cambios al salir


  }

  //async canDeactivate() {
  //  // you logic goes here, whatever that may be & must return either True or false
  //  let srvD = InjectorInstance.get(DACDeclaracionService);
  //  let dialog = InjectorInstance.get(MatDialog)

  //  if (this.isDirty === true) {
  //    const dialogRef = dialog.open(DialogGuardarAlSalirComponent, {
  //      width: '500px',
  //      minHeight: "200px",
  //      disableClose: true,
  //      data: { mensaje: "Se detectaron cambios en la pantalla. ¿Desea guardar los cambios antes de salir?" }
  //    });
  //    return dialogRef.afterClosed().subscribe(async (result: any) => {
  //      if (result.respuesta) {
  //        let a = await forkJoin(
  //          this.save()
  //        ).toPromise();
  //        if (a[0]) {
  //          srvD.changeCaminoMigas();
  //        }
  //        return a[0]
  //      }
  //      srvD.changeCaminoMigas(); // no quiere guardar los cambios al salir
  //      return true
  //    });
  //  }
  //  srvD.changeCaminoMigas();
  //  return true; // la pnt no está dirty


  //}


  public onDataSaved(inserting: boolean) { }

  public esPNT(): boolean {
    return this.service.entidad.startsWith('PNT');
  }

  private nivelDescendenciaPNT(): number {
    if (this.esPNT()) return 0;

    //Completar escenarios de tablas hijas de PNT

    return -1;
  }

  private shouldValidateReqFields(): boolean {
    return true;
    //return !this.esPNT();
  }

  configureAccess() {
  }

}
