import { Component, OnInit, Input, Output, EventEmitter, HostBinding, ContentChild } from '@angular/core';
import { XtGridComponent } from '../../controls/xt-grid/xt-grid.component';

@Component({
  selector: 'xt-grid-form',
  templateUrl: './grid-form.component.html',
  styleUrls: ['./grid-form.component.css']
})
export class GridFormComponent implements OnInit {

  enProceso = false;

  @Input() title: string;
  @Input() titleSolicitudReactivacion: boolean;
  @Input() titleLong: string = `<strong>Reactivación automática:</strong> Mientras esté vigente el plazo de la declaración DAC.<br>
<strong>Reactivación previa evaluación:</strong> Cuando ha vencido el plazo de la declaración DAC.<br>
<strong>Nota:</strong> El Anexo III y la sección 1.7.7. Adjuntar Liquidaciones de Venta, serán activados dependiendo si están dentro de su periodo de declaración (acreditación inversión mínima y acreditación producción mínima).`;
  @Input() tieneElevacion = false;

  @Input() insertvisible: boolean = true;
  @Input() toolVisible: boolean = true;
  @Input() searchvisible: boolean = false;
  @Input() exportvisible: boolean = false;
  @Input() toolbarVisible: boolean = true;
  @Input() minRows: number = 1;
  @Input()
  pageStyle: string = null; //server - client - null
  @Input()
  pageSize: number = 25;
  public currentPage: number = 1;

  @HostBinding('class.xt-delete-visible')
  @Input()
  deletevisible: boolean = true;

  @Input()
  deleteMultiple: boolean = false;

  @Input()
  deleteallvisible: boolean = false;

  @ContentChild('maingrid')
  grid: XtGridComponent;

  @HostBinding('class.xt-embedded')
  @Input()
  embedded: boolean = false;

  @Input()
  searchStyle: string;

  //@Input()
  //enable: boolean = true;

  @HostBinding('class.xt-disabled')
  @Input()
  disabled: boolean = false;

  @HostBinding('class.xt-readonly')
  @Input()
  readonly: boolean = false;

  //@HostBinding('class.xt-invisible')
  //@Input()
  //invisible: boolean = false;

  @Output()
  public actionclicked = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  handleClick(action: string) {
    this.actionclicked.emit({ action: action });
  }
}
