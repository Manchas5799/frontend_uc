import { Component, OnInit, ViewChild, AfterViewInit, Input, HostBinding, DoCheck, EventEmitter, Output, ComponentFactoryResolver, Type } from '@angular/core';
import { MatTable } from '@angular/material/table';
import { Router, ActivatedRoute, UrlSegment, NavigationBehaviorOptions } from '@angular/router';
import { GridFormComponent } from '../grid-form/grid-form.component';
import { ServiceBase } from '../../../svc/ServiceBase';
import { forkJoin, Observable } from 'rxjs';
import { XtFieldCellComponent } from '../../controls/xt-field-cell/xt-field-cell.component';
import { InjectorInstance } from '../../../app.module';
import { GlobalsComponent } from '../../../globals.component';
import { XtGridComponent } from '../../controls/xt-grid/xt-grid.component';
import { XtFieldPanelComponent } from '../../controls/xt-field-panel/xt-field-panel.component';
//import * as XLSX from 'xlsx';
import * as moment from 'moment';
import { EditBaseComponent } from '../edit-base.component';
import { MatDialog } from '@angular/material/dialog';
import { DialogIntegranteEditComponent } from '../../../edit-forms/dialog-integrante/dialog-integrante.component';
//import { DACAnexoService } from '../../../svc/services/DACAnexo.service';
//import { Constants } from '../../../common/constant';
//import { DACDeclaracionService } from '../../../svc/services/DACDeclaracion.service';

@Component({
  selector: 'app-grid-base',
  template: ``,
  styles: []
})
export class GridBaseComponent implements OnInit, AfterViewInit, DoCheck {

  enProceso = false;

  modalEdit: boolean = true;
  @Input() title: string = null;
  @Input() keyname: string = null;
  @Input() keyvalue: number = 0;
  @Input() id: string;
  @Input() skipValidation: boolean = false;
  @Input() disabled: boolean = false;
  @Input() insertvisible: boolean = true;
  @Input() deletevisible: boolean = true;
  @Input() deleteMultiple: boolean = false;
  @Input() alwaysVisible: boolean = false;
  @Input() ShouldSave: boolean = true; //flag para indicar si se guarda la grilla o no al darle al botón Guardar
  public $: any = (window as any)["jQuery"]

  @Output() public _action = new EventEmitter();
  @Output() public _saveForm = new EventEmitter();
  @Output() public _saveGrid = new EventEmitter();
  @Input() createInGrid: boolean = false;

  @HostBinding('class.xt-invisible')
  @Input()
  invisible: boolean = false;

  @ViewChild(MatTable)
  public grid: MatTable<any>;

  @ViewChild(XtGridComponent)
  public maingrid: XtGridComponent;

  @ViewChild(GridFormComponent)
  public formComp: GridFormComponent;

  public pntUpdEstadoInsertNoInicianPantalla = [
    ['EstadoResultadoN'],
    ['EstadoResultadoF'],
    ['MovimientoExist'],
    ['Inmueble'],
    ['Depreciacion'],
    ['Dividendo'],
    ['CambioPatrimonio'],
    ['ImpuestoEmpresa'],
    ['BalanceComprobacion'],
    ['ProdCarbonDestino'],
    ['Ubicacion'],
    ['PantallaSondaje'],
    ['TitularMinero']
  ];

  public pntDeleteAllCustomActualizarAnexo = [ //pantallas producción obtenida, destino, carbonífera y prod carbon destino.
    ['PantallaProduccionObtenida'],
    ['PantallaProduccionDestino'],
    ['PntProduccionNoMetalica'],
    ['ProdCarbonDestino'],
  ];
  idTable: string;
  public service: ServiceBase;
  protected router: Router;
  protected route: ActivatedRoute;
  //DACDeclaracionInt
  private _data: Array<any>;

  public entName: string;

  protected globals: GlobalsComponent = InjectorInstance.get(GlobalsComponent);

  public componentFactoryResolver: ComponentFactoryResolver;
  public dialog: MatDialog

  get data(): Array<any> {
    return this._data;
  }
  set data(value: Array<any>) {
    this._data = value;

    if (this.grid) {
      this.grid.dataSource = value;
    }

    if (this.maingrid) {
      this.maingrid.data = value;
    }
  }

  ngOnInit(): void {
  }

  ngDoCheck() {
    if (this.data) {
      this.dataRendered();
    }
  }
  beforeInsert(): boolean {
    return true;
  }

  beforeSearchButton(): boolean {
    return true;
  }

  beforePushInCreateInGrid(obj: any) {
    return obj;
  }
  afterDeleteAll() {

  }
  fnModoLecturaPersonalized() {

  }

  urlDestino(): any {
    return null;
  }
  onActionClicked($event: any) {
    switch ($event.action) {
      case 'insert':

        if (this.modalEdit) {
          let edit = this.componentFactoryResolver.resolveComponentFactory<EditBaseComponent>(this.editComponent());
          let dialogRef = this.dialog.open(DialogIntegranteEditComponent
            , {
              disableClose: true,
              data: null
            }
          );
          let componentRef = dialogRef.componentInstance.vcRef.createComponent<EditBaseComponent>(edit)
          componentRef.instance.loadNew(this.keyname, this.keyvalue);
          dialogRef.afterClosed().subscribe(result => {
            //llamar al search para actualizar la grilla
            this.search();
          });
        }
        else {
          if (this.urlDestino() == null) {
            const segments: UrlSegment[] = this.route.snapshot.url;
            let rutaPadre = "";
            //if (!segments[2]) {
            //  rutaPadre = segments[0].path + "/" + segments[1].path;
            //} else if (!segments[1]) {
            //  rutaPadre = segments[0].path + "/" + segments[1].path + "/" + segments[2].path;
            //} else {
            rutaPadre = segments[0].path;
            //}
            let lRutaPadre = (rutaPadre.split("?")[0]).length;
            rutaPadre = this.router.url.substring(0, this.router.url.split("?")[0].length - lRutaPadre);

            //if (!segments[2]) {
            //  this.router.navigateByUrl(`${rutaPadre}edit-forms/${this.entName}/${idParam}`);
            //} else {.
            let rutaFinal = `${rutaPadre}${segments[0].path}-edit/0`;
            this.router.navigateByUrl(rutaFinal);
            //this.router.navigateByUrl(`${rutaPadre}edit-forms/${this.entName}/0?keyname=${this.keyname}&keyvalue=${this.keyvalue}`, { queryParams: { keyname: this.keyname, keyvalue: this.keyvalue } });
            window.scrollTo(0, 0);

          }
        }

        break;
      case 'export':
        this.exportarExcel();
        break;
      case 'deleteAll':
        if (confirm("¿Está seguro que desea eliminar todos los registros?")) {
          let obsList = new Array<Observable<Object>>();
          obsList.push(this.service.delByKey(this.keyname, this.keyvalue, this.globals.currentUser.User));
          forkJoin(obsList).subscribe(() => {
            this.search(true);
            this.globals.notify('Los registros fueron eliminados');
            this.afterDeleteAll();
            //llamar funcion para actualizar estado
            this.customActualizarEstado();
            setTimeout(() => {
              this.fnActualizarEstado(null, 'DA');
            }, 0);
          });
          //this.service.delByKey(this.keyname, this.keyvalue, this.globals.currentUser.User).subscribe(data => {
          //  this.search();
          //  this.globals.notify('Los registros fueron eliminados');
          //  //llamar funcion para actualizar estado
          //  this.fnActualizarEstado(this.keyname);
          //});
        }
        break;
      case 'delete':
        let rows = this.formComp.grid.selectedRows();
        if (confirm(`¿Está seguro que desea eliminar estos ${rows.length} registros seleccionados?`)) {
          let obsList = new Array<Observable<Object>>();
          rows.forEach(r => {
            obsList.push(this.service.del(r.Id, this.globals.currentUser.User));
          })
          forkJoin(obsList).subscribe(() => {
            this.search();
            this.globals.notify('Los registros fueron eliminados');
            //llamar funcion para actualizar estado
            this.customActualizarEstado();
            setTimeout(() => {
              this.fnActualizarEstado(null, 'DA');
            }, 0);

          });
        }

        //this.exportarExcel();
        break;
      case 'search':
        if (this.beforeSearchButton()) {
          this.search();
          this.globals.notify('Lista actualizada');
          this.afterSearch();
          //alert('Lista actualizada');
        }
        break;
      default:
    }
  }
  afterSearch() {

  }
  fnActualizarEstado(idRow: string = null, tipoEliminacion: string = null): any {
    return null;
    //if (!this.keyname) return;
    //if (this.keyname.toLowerCase().includes("pantalla") || this.keyname.toLowerCase().startsWith("pnt") || this.pntUpdEstadoInsertNoInicianPantalla.find(d => d[0] == this.keyname)) {
    //  if (tipoEliminacion === null && this.data.length >= this.formComp.minRows) {
    //    return;
    //  }
    //  if (this.pntDeleteAllCustomActualizarAnexo.find(d => d[0] == this.keyname)) {
    //    if (this.keyname == "PntProduccionNoMetalica" && !this.router.url.includes("Carbonifera")) {
    //      console.log("continua");
    //    } else {
    //      return
    //    }
    //  }
    //  let messageIncompleto: any;
    //  if (this.formComp.minRows !== 0) messageIncompleto = `Debe ingresar al menos ${this.formComp.minRows} registro(s) en la sección para que se encuentre completa.`;
    //  let pnt = this.buscarPnt(this.keyname);
    //  if (pnt === undefined) return;

    //  let srvAnexo = InjectorInstance.get(DACAnexoService);
    //  let srv = InjectorInstance.get(`${pnt}Service`);

    //  srv.sel(idRow === null ? this.keyvalue : idRow).subscribe((data2: any) => { //OBTENER LA DATA PARA CAPTURAR EL IDANEXO
    //    //messageIncompleto = messageIncompleto.replaceAll('ñ', '{').replaceAll('ñ', '{').replaceAll('<br>', '}').replaceAll('.', '_').replaceAll(';', ']').replaceAll(':', '[').replaceAll(' ', '-').replaceAll(',', '|');
    //    srvAnexo.updEstado(data2.DeclaracionAnexo, Constants.ESTADO_INCOMPLETO, messageIncompleto).subscribe((data3: any) => {
    //      //if (data3.toLoweCase() == 'ok') {

    //      this.reiniciarIndice();
    //      this.reiniciarObservacion(data2.DeclaracionAnexo, messageIncompleto);
    //      //let $ = this.$;
    //      //let indice = document.getElementById(`da${data2.DeclaracionAnexo}`);
    //      //if (indice) {
    //      //  let classIndice = indice?.classList.toString().split(' ');
    //      //  let findEstado = classIndice.find(cl => cl.startsWith("xt-est-"));

    //      //  this.$(indice).removeClass(findEstado)
    //      //  this.$(indice).addClass(`xt-est-Incompleto`)
    //      //}
    //      //}
    //    })

    //  });

    //}
  }
  public reiniciarIndice() {
    //let srvD = InjectorInstance.get(DACDeclaracionService);
    //srvD.reiniciarIndice();
  }
  public reiniciarObservacion(declaracionAnexo: number, messageIncompleto: string) {
    //let srvD = InjectorInstance.get(DACDeclaracionService);
    //srvD.reiniciarObservacion(declaracionAnexo);
    this._action.emit({ "DeclaracionAnexo": declaracionAnexo, "messageIncompleto": messageIncompleto });

  }
  buscarPnt(keyname: any) {
    let pnt;
    let nietos = [
      ["ITMAccionista", 'PNTTitularMinero', 'TitularMinero'],
      ["ITMResponsable", 'PNTTitularMinero', 'TitularMinero'],
      ["ITMConcesion", 'PNTConcesion', 'PantallaConcesion'],
      ["ITMInversion", 'PNTITMInversion', 'PantallaInversion'],
      ["ITMEstudio", 'PNTITMEstudio', 'PantallaEstudio'],
      ["FINActivoCorriente", 'PNTSituacionFinanciera', 'PantallaSituacionFinanciera'],
      ["FINEstadoResultadoN", 'PNTEstadoResultadoN', 'EstadoResultadoN'],
      ["FINEstadoResultadoF", 'PNTEstadoResultadoF', 'EstadoResultadoF'],
      ["FINMovimientoExist", 'PNTMovimientoExist', 'MovimientoExist'],
      ["FINInmueble", 'PNTInmueble', 'Inmueble'],
      ["FINDepreciacion", 'PNTDepreciacion', 'Depreciacion'],
      ["FINMetalicaRG", 'PNTMetalicaRG', 'PntMetalicaRegimenGeneral'],
      ["FINMetalicaDetalleRG", 'FINMetalicaRG', 'MetalicaRG'],
      ["FINMetalicaPPMPMA", 'PNTMetalicaPPMPMA', 'PantallaMetalicaPPMPMA'],
      ["FINVentaNoMetalica", 'PNTVentaNoMetalica', 'PantallaVentaNoMetalica'],
      ["FINVentaIndustrial", 'PNTVentaIndustrial', 'PantallaVentaIndustrial'],
      ["FINCostoProduccion", 'PNTVentaIndustrial', 'PantallaCostoProduccion'],
      ["FINVentaCarbonifero", 'PNTVentaCarbonifero', 'PantallaVentaCarbonifero'],
      ["FINOroAluvial", 'PNTOroAluvial', 'PantallaOroAluvial'],
      ["FINDividendo", 'PNTDividendo', 'Dividendo'],
      ["FINCambioPatrimonio", 'PNTCambioPatrimonio', 'CambioPatrimonio'],
      ["FINImpuestoEmpresa", 'PNTImpuestoEmpresa', 'ImpuestoEmpresa'],
      ["FINGastoPersonal", 'PNTGastoPersonal', 'PntGastoPersonal'],
      ["FINCategoriaPersonal", 'PNTCategoriaPersonal', 'PntCategoriaPersonal'],
      ["FINBalanceComprobacion", 'PNTBalanceComprobacion', 'BalanceComprobacion'],
      ["EXPBalanceMetalurgico", 'PNTBalanceMetalurgico', 'PntBalanceMetalurgico'],
      ["EXPReservaNoMetalica", 'PNTReservaNoMetalica', 'PantallaReservaNoMetalica'],
      //["EXPReservaCarbonifera", 'PNTReservaCarbonifera', 'PantallaReservaMetalica'],
      ["EXPProduccionNoMetalica", 'PNTProduccionNoMetalica', 'PntProduccionNoMetalica'],
      ["EXPProduccionCarbonifera", 'PNTProduccionCarbonifera', 'PntProduccionNoMetalica'],
      ["EXPProdCarbonDestino", 'PNTProdCarbonDestino', 'ProdCarbonDestino'],
      //["EXPCompra", 'PNTOrigenCompra', 'PantallaOrigenCompra'],
      ["EXPProduccionObtenida", 'PNTProduccionObtenida', 'PantallaProduccionObtenida'],
      //["EXPProduccionDestino", 'PNTProduccionDestino', 'PantallaProduccionDestino'],
      ["EXPReserva", 'PNTReservaMetalica', 'PantallaReservaMetalica'],
      ["EXPRecursoMetalico", 'PNTRecursoMetalico', 'PantallaRecursoMetalico'],
      ["EXPRecursoNoMetalico", 'PNTRecursoNoMetalico', 'PantallaRecursoNoMetalico'],
      ["EXPRecursoCarbonifero", 'PNTRecursoCarbonifero', 'PantallaRecursoCarbonifero'],
      ["ADSPrograma", 'PNTProgramaLargoPlazo', 'PantallaPrograma'],
      ["ADSActividad", 'PNTBalanceSocialAct', 'PntBalanceSocialActividad'],
      ["ADSActividadImpacto", 'ADSActividad', 'Actividad'],
      ["ASAAreaSuperficial", 'PNTAreaSuperActMinera', 'PntAreaSuperactividadMinera'],
      ["ASAAreaSuperficial", 'PNTAreaSuperUsoMinero', 'PntAreaSuperUsoMinero'],
      ["ISOPerforacionRealizada", 'PNTPerforacionRealizada', 'PantallaPerforacionRealizada'],
      ["ISOGeologiaSuperficie", 'PNTGeologiaSuperficie', 'PntGeologiaSuperficie'],
      ["ISOGeoquimicaSuper", 'PNTGeoquimicaSuper', 'PntGeoquimicaSuperficie'],
      ["ISOVetaAflorante", 'PNTVetaAflorante', 'PantallaVetaAflorante'],
      ["ISOUbicacion", 'PNTUbicacion', 'Ubicacion'],
      ["ISOSondaje", 'PNTSondaje', 'PantallaSondaje'],
      ["ISOConcesion", 'PNTISOConcesion', 'PNTISOConcesion']
    ];


    nietos.forEach(n => {
      console.log(n)
      if (n[2] == keyname) {
        if (this.entName === "EXPReservaCarbonifera")
          pnt = "PNTReservaCarbonifera"
        else if (this.entName === "EXPProduccionNoMetalica")
          pnt = "PNTProduccionNoMetalica"
        else {
          pnt = n[1];
          return
        }
      }

    })

    return pnt;
  }
  ngAfterViewInit() {
    if (this.disabled) {
      this.formComp.disabled = true;
      this.formComp.insertvisible = false;
    }
    if (!this.insertvisible) {
      this.formComp.insertvisible = false;
    }
    if (!this.deletevisible) {
      this.formComp.deletevisible = false;
    }
    this.configureAccess();
    setTimeout(() => {
      if (this.formComp) {
        this.formComp.embedded = this.keyname != null;
      }
    }, 0);
    //this.grid.setHeaderRowDef

    //Toolbar events
    if (this.formComp)
      this.formComp.actionclicked.subscribe($event => {
        this.onActionClicked($event);
      });


    //Grid events
    if (this.maingrid) {
      this.maingrid.DeleteRow.subscribe($event => {
        if (this.beforeDelete($event.row))
          this.deleteRow($event.id, true, $event.row, $event.messageDelete);
      });
      this.maingrid.EditRow.subscribe($event => {
        this.editRow($event.id, $event.row);
      });
      this.maingrid.PageChange.subscribe($event => {
        this.pageChange($event.page);
      });
      this.maingrid.fields.changes.subscribe(_ => this.deleteFileGrid())
    }

    //this.configureColumns();
    this.populateLookups();
    this.search();

  }

  public beforeDelete(row: any) {
    return true;
  }

  public deleteFileGrid() {
    this.maingrid.fields.forEach(f => {
      if (f === undefined) return
      if (f.datatype?.toLowerCase() == "file") {
        f.deleteFile.subscribe($event => {
          let idLaserfiche = $event.FieldPanel.filenoderef;
          this.EliminarFilesGrids(idLaserfiche, $event.FieldPanel).subscribe(data => {
            if (data) $event.FieldPanel.value = null
          });
        });
      }
    });
  }
  public saveEmit() {
    this._saveForm.emit();

  }
  public saveGridEmit() {
    this._saveGrid.emit();

  }
  public EliminarFilesGrids(idLaserfiche: any, fieldPanel: XtFieldPanelComponent): Observable<boolean> {
    //let fileValue = fieldPanel?.fileValue;
    console.log(this)
    var resp = new Observable<boolean>((observer) => {
      if (confirm(`Se va a eliminar el archivo subido. Esta acción es irreversible`)) {
        let row = fieldPanel.ctx.row; //obtengo la fila de fieldPanel
        this.service.deleteFile(idLaserfiche, row.Id, fieldPanel.ctx.col.source).subscribe(data => {
          this._action.emit({ eliminacionGrid: true });
          observer.next(true);
          observer.complete();
        })
      } else {
        observer.next(false);
        observer.complete();
        //return
      }

    });
    return resp;
  }

  public exportarExcel(): any {
    return null;
    //console.log(this);
    //var contenedor = document.getElementById("Grid" + this.entName);
    //if (contenedor) {
    //  console.log("contenedor", contenedor);
    //  //var element = (<HTMLTableElement>contenedor.childNodes[contenedor.childNodes.length - 1]);
    //  let ws: XLSX.WorkSheet;
    //  //if (this.maingrid.pageStyle == "client") { //paginacion en true
    //  var dataEnd: any[] = [];
    //  this.data.forEach(dato => {
    //    let newDato
    //    this.maingrid.columns.forEach(column => {

    //      newDato = { ...newDato, [`${column.title}`]: dato[column.textSource] }
    //    });
    //    dataEnd.push(newDato);
    //  });
    //  console.log(dataEnd)
    //  //let element = document.getElementById('excel-table');
    //  ws = XLSX.utils.json_to_sheet(dataEnd);

    //  //} else {
    //  //  ws = XLSX.utils.table_to_sheet(element);
    //  //}

    //  /* generate workbook and add the worksheet */
    //  const wb: XLSX.WorkBook = XLSX.utils.book_new();
    //  XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    //  /* save to file */
    //  XLSX.writeFile(wb, `${this.entName} - ${moment().format('DD-MM-YYYY')}.xlsx`);

    //}
    //else {
    //  alert("Necesita añadir un Id en el grid de " + this.entName);
    //}
  }

  private configureColumns() {
    if (this.keyname != '') {
      //var cols: string[] = this.grid._headerRowDefs[this.grid._headerRowDefs.length - 1].columns;
      //cols = cols.filter(e => e != this.keyname);

      //this.grid._headerRowDefs[this.grid._headerRowDefs.length - 1].columns = cols;

      //this.grid._rowDefs[0].columns = cols;
    }
  }

  public search(deleteAll: boolean = false): any {
    console.log("search grid base")
    if (!this.shouldRender) return;

    var obsList = this.getCustomData(deleteAll);

    if (obsList == null) {
      if (this.formComp.pageStyle == "server") {
        let ent: any = { PageNumber: this.formComp.currentPage, PageSize: this.formComp.pageSize }
        if (this.keyname && this.keyvalue) {
          ent[this.keyname] = this.keyvalue;
        }
        obsList = this.service.selByFilters(ent);

      } else if (this.keyname) {
        console.log("GridBase getBy");
        console.log("GridBase this.keyname", this.keyname);
        console.log("GridBase this.keyvalue", this.keyvalue);
        obsList = this.service.getBy(this.keyname, this.keyvalue);
      } else {
        console.log("GridBase selAll");
        obsList = this.service.selAll();
      }
    }
    return obsList.subscribe((data: any) => {
      console.log("GridBase search", data);
      data.forEach((d: any, i: any) => d.__position = i + 1)
      this.dataLoading(data);
      this.data = data;
      this.configureColumns();
      this.dataLoaded();
      this.formComp.grid.search();//para filtrar la grilla por el search de la cabecera
    });
  }
  dataLoading(data: any[]) {

  }
  public searchObs(): Observable<boolean> { //copia de search, devuelve un observable, para no modificar el search original y tener que cambiar en todos los lugares que se necesite, este se usa solo para ativar o desactivar el toggle

    return null;
    //var resp = new Observable<boolean>((observer) => {

    //  if (!this.shouldRender) return;

    //  var obsList = this.getCustomData();

    //  if (obsList == null) {
    //    if (this.formComp.pageStyle == "server") {
    //      let ent = { PageNumber: this.formComp.currentPage, PageSize: this.formComp.pageSize }
    //      if (this.keyname && this.keyvalue) {
    //        ent[this.keyname] = this.keyvalue;
    //      }
    //      obsList = this.service.selByFilters(ent);

    //    } else if (this.keyname) {
    //      console.log("GridBase getBy");
    //      console.log("GridBase this.keyname", this.keyname);
    //      console.log("GridBase this.keyvalue", this.keyvalue);
    //      obsList = this.service.getBy(this.keyname, this.keyvalue);
    //    } else {
    //      console.log("GridBase selAll");
    //      obsList = this.service.selAll();
    //    }
    //  }
    //  return obsList.subscribe((data: any) => {
    //    console.log("GridBase search", data);
    //    data.forEach((d, i) => d.__position = i + 1)
    //    this.dataLoading(data);
    //    this.data = data;
    //    this.configureColumns();
    //    this.dataLoaded();
    //    this.formComp.grid.search();//para filtrar la grilla por el search de la cabecera

    //    observer.next(data);
    //    observer.complete();
    //    return;
    //  });

    //});

    //return resp;
  }

  public lockfields(state: boolean) {
    this.maingrid.fields.forEach(item => {
      item.disabled = state;
    });
  }

  mergeDataTable2(tableId: any, columns: any[] = []) {
    var contenedor = document.getElementById(tableId);
    console.log("contenedor", contenedor);
    var table = (<HTMLTableElement>contenedor.childNodes[contenedor.childNodes.length - 1]);

    console.log("mergeDataTable2");
    console.log("contenedor", contenedor);
    console.log("table", table);

    /// console.log("table bodies count", table.tBodies[0].rows.length);
    if (table) {
      console.log("table bodies count", table.tBodies[0].rows.length);
      let valor_anterior = "";
      let contador_repetido = 0;
      columns.sort(function (a, b) {
        return b - a;
      });
      console.log("recorriendo columnas", columns.length);
      columns.forEach(column => {

        console.log("recorriendo filas", table.tBodies[0].rows.length);
        for (var r = 0, n = table.tBodies[0].rows.length; r < n; r++) {

          let valor = table.tBodies[0].rows[r].cells[column].innerHTML;

          if (valor_anterior === valor) {
            contador_repetido++;
          }

          console.log("table length", table.tBodies[0].rows.length);
          console.log("r", r);
          if (contador_repetido > 0 && (valor_anterior != valor)) {
            console.log("Mesclando");
            console.log("contador_repetido", contador_repetido);
            console.log("i", r - contador_repetido - 1);
            console.log("column", column);
            console.log("==============================================");
            console.log("valor_anterior", valor_anterior);
            console.log("valor", valor);
            this.mergeCol2(table, r - contador_repetido - 1, column, contador_repetido);
            contador_repetido = 0;
          }
          if (contador_repetido > 0 && table.tBodies[0].rows.length - 1 == r) {
            console.log("Mesclando");
            console.log("contador_repetido", contador_repetido);
            console.log("i", r - contador_repetido - 1);
            console.log("column", column);
            console.log("==============================================");
            console.log("valor_anterior", valor_anterior);
            console.log("valor", valor);
            this.mergeCol2(table, r - contador_repetido, column, contador_repetido);
            contador_repetido = 0;
          }
          valor_anterior = valor;
        }
      });
    }
  }

  mergeCol2(t: any, i: any, column: any, merge: any) {
    try {
      //var t = (<HTMLTableElement>document.getElementById(tableId));
      var n = t.tBodies[0].rows[i].cells[column].getAttribute('rowspan') || 1;
      t.tBodies[0].rows[i].cells[column].setAttribute('rowspan', n += merge);
      console.log("eliminando i", i);
      console.log("eliminando merge", merge);
      for (let index = i; index < i + merge; index++) {
        console.log("eliminando", t.tBodies[0].rows[index + 1].cells[column]);
        t.tBodies[0].rows[index + 1].deleteCell(column);
      }
    } catch (error) {
      console.log(error)
    }
  }

  addHeaderTable = (tableId: any, configHeader: any) => {
    try {
      console.log("addHeaderTable")
      var contenedor = document.getElementById(tableId);
      var table = (<HTMLTableElement>contenedor.childNodes[contenedor.childNodes.length - 1]);
      // var table = (<HTMLTableElement>document.getElementById(tableId));
      if (table) {
        /* if (table.tHead.rows.length == 1) {*/
        var row = table.tHead.insertRow(0);
        configHeader.forEach((element: any) => {
          var cell = row.insertCell(element.position);
          cell.innerHTML = element.title;
          cell.colSpan = element.merge;
          cell.className = "mat-header-row mat-header-cell centrar add-header";
        });
        /* }*/
      }
    }
    catch (error) {
      console.log('Error al crear header ', error);
    }
  }

  addFooterTable = (tableId: any, configHeader: any) => {
    try {
      var contenedor = document.getElementById(tableId);
      var table = (<HTMLTableElement>contenedor.childNodes[contenedor.childNodes.length - 1]);
      // var table = (<HTMLTableElement>document.getElementById(tableId));
      if (table) {
        var tableFoot = table.createTFoot();


        if (tableFoot.rows.length == 0) {
          var row = tableFoot.insertRow(0);
          configHeader.forEach((element: any) => {
            var cell = row.insertCell(element.position);
            cell.innerHTML = element.title;
            cell.colSpan = element.merge;

            console.log("footer align: " + element.align)
            switch (element.align) {
              case 'left':
                cell.className = "mat-header-row mat-header-cell izquierda";
                break;
              case 'right':
                cell.className = "mat-header-row mat-header-cell derecha";
                break;
              default:
                cell.className = "mat-header-row mat-header-cell centrar";
                break;
            }

          });
        }
      }
    }
    catch (error: any) {
      console.log('Error al crear footer ' + error.stack);
    }
  }

  public dataLoaded() {

  }

  public dataRendered() {

  }

  public editUrl(row: any): any {
    return null;
  }

  /**
   * Permite editar una fila determinada.
   * @param id
   */
  public editComponent(): Type<EditBaseComponent> {
    return null;
  }
  editRow(id: number, row: any): void {
    //debugger
    if (this.modalEdit) {
      let edit = this.componentFactoryResolver.resolveComponentFactory<EditBaseComponent>(this.editComponent());
      let dialogRef = this.dialog.open(DialogIntegranteEditComponent
        , {
          disableClose: true,
          data: row
        }
      );
      let componentRef = dialogRef.componentInstance.vcRef.createComponent<EditBaseComponent>(edit)
      componentRef.instance.loadId(row.Id);
      dialogRef.afterClosed().subscribe(result => {
        //llamar al search para actualizar la grilla
        this.search();
      });
      //componentRef.instance.modalAct();

    }
    else {
    if (this.editUrl(row) == null) {
      let idParam = id.toString();
      if (this.formComp && this.formComp.readonly) idParam += '_r';
      const segments: UrlSegment[] = this.route.snapshot.url;
      let rutaPadre = "";
      //if (!segments[2]) {
      //  rutaPadre = segments[0].path + "/" + segments[1].path;
      //} else if (!segments[1]) {
      //  rutaPadre = segments[0].path + "/" + segments[1].path + "/" + segments[2].path;
      //} else {
      rutaPadre = segments[0].path;
      //}
      let lRutaPadre = (rutaPadre.split("?")[0]).length;
      rutaPadre = this.router.url.substring(0, this.router.url.split("?")[0].length - lRutaPadre);

      //if (!segments[2]) {
      //  this.router.navigateByUrl(`${rutaPadre}edit-forms/${this.entName}/${idParam}`);
      //} else {
      let rutaFinal = `${rutaPadre}${segments[0].path}-edit/${idParam}`;
      this.router.navigateByUrl(rutaFinal);
      //}


      window.scrollTo(0, 0);
      //this.router.navigateByUrl(`${rutaPadre}/edit-forms/${this.entName}/${idParam}?keyname=${this.keyname}&keyvalue=${this.keyvalue}`);

    }

    }

  }

  customActualizarEstado() {

  }

  deleteRow(id: number, ask: boolean = true, row: any = null, message: string) {
    if (!ask || confirm(message)) {
      var audUser = null;
      if (id !== null)
        this.service.del(id, audUser).subscribe(data => {
          //this.search();
          let newData = this.data.filter(d => !Object.is(d, row) && d.Id != -row.Id); //eliminar elemento con id negativo (2.4.2)
          newData.forEach((nd, i) => nd.__position = i + 1);
          this.data = newData;
          this.rowDeleted(row, this.data);
          //llamar funcion para actualizar estado
          if (id > 0) {
            this.customActualizarEstado();
            setTimeout(() => {
              this.fnActualizarEstado(row[this.keyname]);
            }, 0);
          }
        });
      else {
        let newData = this.data.filter(d => !Object.is(d, row))
        newData.forEach((nd, i) => nd.__position = i + 1);
        this.data = newData;
        this.rowDeleted(row, this.data);
      }

    }
  }

  findField(position: number, columId: string): XtFieldPanelComponent {
    return this.maingrid.columnFields(columId).find(colF => colF.ctx.row?.__position == position);
  }

  rowDeleted(row: any, data: any) {

  }

  pageChange(page: number) {
    this.formComp.currentPage = page;
    this.search();
    console.log(this.data)
  }

  public FillDropDown(
    field: XtFieldCellComponent,
    observableList: Observable<Object>
  ) {
    if (field) {
      observableList.subscribe((data: any) => {
        field.datasource = data;

        if (field.nextVal != null) {
          field.value = field.nextVal;
          field.nextVal = null;
        }
      });
    }
  }
  public FillDropDownData(field: XtFieldCellComponent, data: any) {
    if (field) {
      field.datasource = data;
      if (field.nextVal != null) {
        field.value = field.nextVal;
        field.nextVal = null;
      }
    }
  }

  @HostBinding('style.display')
  get displayAttr(): string {
    return this.shouldRender ? 'block' : 'none';
  }

  get shouldRender(): boolean {
    if (this.alwaysVisible) return true
    return this.keyname == null || this.keyvalue != 0;
  }

  getCustomData(deleteAll: boolean = false): Observable<Object> {
    return null;
  }

  populateLookups() {

  }

  configureAccess() {
  }

  fileUrl(fileinfo: string) {
    if (fileinfo == '') return '';

    let parts = fileinfo.split('[||]');
    return `/api/CAPAreaEfectivaProyecto/getnodefile?filename=${parts[0]}&noderef=${parts[1]}`;
  }

  fileName(fileinfo: string) {
    if (fileinfo == '') return '';

    let parts = fileinfo.split('[||]');
    return parts[0];
  }

  fileExists(fileinfo: string) {
    return fileinfo != '';
  }

  serializeChanges(columns: string[]): string { //columns a exportar
    //let ce = this.changedEntities();
    //separado registro, separador campos
    return this.changedEntities(false, true).map(e => {
      let ar = [e.Id];
      if (this.keyname) ar.push(e[this.keyname]) //valor de la fk si es que hay
      columns.forEach(c => ar.push(e[c]));
      return ar.join('[|]');
    }).join('[||]');
  }

  changedEntities(includeFields: boolean = false, allFields: boolean = false): Array<any> {

    let grid = this.maingrid;
    if (grid && grid.fields.length != 0) {
      let newIdx = -10000;
      let ents = new Array<any>(); //new Map<number, object>();
      let attrs = new Array<string>();
      grid.fields.forEach(field => {
        if (field.ctx.search === true) return;
        if (field.ctx.row.Id === null) {
          field.ctx.row.Id = newIdx;
          newIdx--;
        }
        let ent = ents.find(item => item.Id === field.ctx.row.Id);
        if (ent === undefined) {
          //ent = field.ctx.row;
          ent = {};
          Object.assign(ent, field.ctx.row);
          if (this.keyname) {
            ent[this.keyname] = this.keyvalue;
            //if (field.datatype == "file") { //si es un file, se crea la propiedad ..._file = file
            //  var file = field.getFile();
            //  ent[field.id.substr(5, field.id.length - 6) + "_file"] = file;

            //  //ent[field.id.substr(5) + "_file"] = file;
            //  //includeFields = true;
            //}
            //console.log("ent[field.id.substr(5, field.id.length-6) +", ent)
          }
          if (ent.changed === undefined) ent.changed = false;// ent.changed = allFields; //? allFields : false;
          if (includeFields) ent.fields = new Array<XtFieldPanelComponent>();
          ents.push(ent);
        }
        ent.changed = ent.changed || field.isDirty;
        if (includeFields) ent.fields.push(field);
        let attr = field.ctx.col.source;
        ent[attr] = field.value;
        if (attrs.find(item => item == attr) === undefined) attrs.push(attr);
      });

      ents.forEach(ent => {

        if (ent.Id <= -10000)
          ent.Id = null;
      });

      if (allFields) return ents;
      console.log(ents.filter(ent => ent.changed));
      return ents.filter(ent => ent.changed);
    }

    return [];
  }

  getValues() {
    //var resp = new Object();
    //var fields = this.getFormFields();
    //for (var i = 0; i <= fields.length - 1; i++) {
    //  if (fields[i]) {
    //    resp[fields[i].getColumnId()] = fields[i].value;
    //    if (fields[i].datatype == "file") {
    //      var file = fields[i].FieldPanel.getFile();
    //      resp[fields[i].getColumnId() + "_file"] = file;
    //    }
    //  }
    //}

    //if (this.id != 0) {
    //  resp["Id"] = this.id;
    //} else if (this.keyname) {
    //  resp[this.keyname] = this.keyvalue;
    //}

    //return resp;
  }

}
