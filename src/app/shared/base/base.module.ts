import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ControlsModule } from '@app/shared/controls/controls.module';
import { GridFormComponent } from './grid-form/grid-form.component';
import { EditFormComponent } from './edit-form/edit-form.component';
import { GridBaseComponent } from './grid-base/grid-base.component';
import { EditBaseComponent } from './edit-base.component';
import { MaterialModule } from '../material/material.module';

@NgModule({
  declarations: [
    GridFormComponent,
    EditFormComponent,
    GridBaseComponent,
    EditBaseComponent
  ],
  imports: [
    CommonModule,
    ControlsModule,
    MaterialModule
  ],
  exports: [
    GridFormComponent,
    EditFormComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BaseModule { }
