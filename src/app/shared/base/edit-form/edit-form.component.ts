import { Component, OnInit, Input, ViewChild, Output, EventEmitter, ContentChildren, QueryList } from '@angular/core';
//import { DACDeclaracionService } from '@svc/services/DACDeclaracion.service';
import { XtGroupPanelComponent } from '../../controls/xt-group-panel/xt-group-panel.component';

@Component({
  selector: 'xt-edit-form',
  templateUrl: './edit-form.component.html',
  styleUrls: ['./edit-form.component.scss']
})
export class EditFormComponent implements OnInit {
  @Input() title1: string;
  @Input() title2: string;

  //@Input() caminodemigas: string = null; //debe llegar y mostrarse
  //@Input() title1Code: string = null; //debe llegar y mostrarse
  @Input() toolbarVisible: boolean = true;

  //reiniciarEncabezadoFn() {
  //  this.dacService.caminodemigas = "";
  //  this.dacService.title1Code = "";
  //}
  //caminodemigasFn() {
  //  this.caminodemigas = this.dacService.caminodemigas;
  //  return this.caminodemigas;
  //}
  //title1CodeFn() {
  //  this.title1Code = this.dacService.title1Code;
  //  return this.title1Code;
  //}

  //@Input() observacionAnexo: string;

  @Input() notaVisible: boolean = true;
  @Input() message: string;
  @Input() messageStyle: string;
  @Input() messageEnd: string;
  @Input() messageEndStyle: string;

  @Output()
  public actionclicked = new EventEmitter();
  @ViewChild('form1')
  formElem: any;

  @ContentChildren(XtGroupPanelComponent, { descendants: true }) public fieldsGroup: QueryList<XtGroupPanelComponent>;


  mostrarFinalizar: boolean = false;
  mostrarAbrir: boolean = false;
  @Input() mostrarSave: boolean = true;
  @Input() salirNoGrabar: boolean = false;
  @Input() textSalirNoGrabar: string = "Cerrar sin Grabar";
  @Input() mostrarSaveClose: boolean = true;
  @Input() mostrarCloseIntranet: boolean = false;
  public mostrarValidacion: boolean = false;
  public mostrarEnviar: boolean = false;
  public mostrarEnviarDoc: boolean = false;
  public mostrarClose: boolean = true;
  public mostrarBotonesPersonalizados: boolean = true;

  constructor() { }

  ngOnInit(): void {
    //this.caminodemigas = this.dacService.caminodemigas
    //this.title1Code = this.dacService.title1Code
  }

  handleClick(action: string) {
    this.actionclicked.emit({ action: action });
  }

}
