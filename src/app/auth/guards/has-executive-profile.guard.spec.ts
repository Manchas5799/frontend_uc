import { TestBed } from '@angular/core/testing';

import { HasExecutiveProfileGuard } from './has-executive-profile.guard';

describe('HasExecutiveProfileGuard', () => {
  let guard: HasExecutiveProfileGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(HasExecutiveProfileGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
