import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UcProfile } from '../models/UcProfile';

@Injectable({
  providedIn: 'root'
})
export class HasExecutiveProfileGuard implements CanActivate {
  constructor(
    private router: Router
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const profileJson = localStorage.getItem('uc-profile') || '{}';
    const profile: UcProfile = JSON.parse(profileJson);
    if (profile && profile.type === environment.executiveType) {
      return true;
    }
    this.router.navigate(['/', '401']);
    return false;
  }

}
