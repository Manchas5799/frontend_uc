import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { UcAuthService } from '../../services/uc-auth.service';

@Component({
  selector: 'app-login-dev',
  templateUrl: './login-dev.component.html',
  styleUrls: ['./login-dev.component.sass']
})
export class LoginDevComponent implements OnInit {
  errorMessage = '';
  waitRedirect = false;
  loading = false;
  loggedIn = false;

  constructor(
    private ucAuthService: UcAuthService,
    private authService: AuthService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.initUi();
    this.authService.getProfile().subscribe((profile) => {
      if (profile) {
        if (profile.level && profile.level !== 'no-level') {
          this.router.navigate(['/', profile.type, profile.level]);
        } else {
          this.router.navigate(['/', 'auth', 'escoger-perfil']);
        }
      }
    } );
    
  }

  singInUC(user:string): void {
    // console.log(this.googleUser);
    if (!user) {
      this.errorMessage = 'Ingrese un usuario';
      return;
    }
    this.loading = true;
    this.errorMessage = '';
    this.ucAuthService.loginDev(user)
      .then((account) => {
        this.loading = false;
        this.authService.setProfile(account.jwt, account.profile);
      })
      .catch((error) => {
        console.log(error);
        if (error.status === 401) {
          this.errorMessage = error.error;
        } else {
          this.errorMessage = 'Tenemos un error, intentelo mas tarde';
        }
      }).finally(() => {
        this.loading = false;
      });
  }

  initUi(): void {
    document.body.classList.add('uc-login-layout');
  }
}