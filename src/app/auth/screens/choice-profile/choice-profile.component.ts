import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { UcProfile } from '../../models/UcProfile';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'auth-choice-profile',
  templateUrl: './choice-profile.component.html',
  styleUrls: ['./choice-profile.component.sass']
})
export class ChoiceProfileComponent implements OnInit {
  levels: string[] = [];
  campuses: string[] = [];
  profile?: UcProfile;
  errorMessage = '';
  constructor(
    private authService: AuthService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.initUi();
    console.log("choice")
    this.authService.getProfile().subscribe((profile) => {
      if (profile) {
        this.profile = profile;
        if (profile.levels && profile.levels.length > 0) {
          this.levels = profile.levels;
        }
        if (profile.campuses && profile.campuses.length > 0) {
          this.campuses = profile.campuses;
        }
      } else {
        this.router.navigate(['/', 'auth', 'ingresar']);
      }
    } );
  }

  setLevel(level: string) {
    const newLevel = this.authService.setLevel(level);
    if(newLevel) {
      this.router.navigate(
        ['/', this.profile?.type, this.profile?.level]);
    } else {
      this.errorMessage = 'No se pudo cambiar el nivel';
    }
    
  }
  getImgByLevel(level: string): string {
    switch (level) {
      case environment.pregradoLevel:
        return '/assets/img/logos/logo-horizontal-black.svg';
        case environment.posgradoLevel:
        return '/assets/img/logos/logo-posgrado.svg';
        case environment.continuaLevel:
        return '/assets/img/logos/logo-continua.svg';
        default:
          return '/assets/img/logos/logo-horizontal-black.svg';
    }
  }
  initUi(): void {
    document.body.classList.remove('uc-login-layout');
    document.body.classList.add('uc-choice-profile-layout');
  }
}
