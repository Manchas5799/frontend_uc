import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChoiceProfileComponent } from './choice-profile.component';

describe('ChoiceProfileComponent', () => {
  let component: ChoiceProfileComponent;
  let fixture: ComponentFixture<ChoiceProfileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChoiceProfileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChoiceProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
