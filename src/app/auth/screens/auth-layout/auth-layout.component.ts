import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'auth-layout',
  templateUrl: './auth-layout.component.html',
  styleUrls: ['./auth-layout.component.sass']
})
export class AuthLayoutComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    document.body.classList.remove();
    document.body.classList.add('hold-transition');
    document.body.classList.add('login-page');
  }

}
