import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GoogleLoginProvider, SocialAuthService } from 'angularx-social-login';
import { AuthService } from '../../services/auth.service';
import { UcAuthService } from '../../services/uc-auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {
  errorMessage = '';
  waitRedirect = false;
  googleUser: any;
  loading = false;
  loggedIn = false;

  constructor(
    private socialAuthService: SocialAuthService,
    private ucAuthService: UcAuthService,
    private authService: AuthService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.initUi();
    this.authService.getProfile().subscribe((profile) => {
      if (profile) {
        if (profile.level && profile.level !== 'no-level') {
          this.router.navigate(['/', profile.type, profile.level]);
        } else {
          this.router.navigate(['/', 'auth', 'escoger-perfil']);
        }
      }
    } );
    this.socialAuthService.authState.subscribe((gooleUser) => {
      this.googleUser = gooleUser;
      this.loggedIn = (gooleUser != null);
      this.singInUC();
    });
  }

  signInWithGoogle(): void {
    this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  signOutInGoogle(): void {
    this.socialAuthService.signOut();
  }

  singInUC(): void {
    const token = this.googleUser.idToken;
    this.loading = true;
    this.errorMessage = '';
    console.log('token is ', token);
    this.ucAuthService.login(token)
      .then((account) => {
        this.loading = false;
        this.authService.setProfile(account.jwt, account.profile);
      })
      .catch((error) => {
        console.log(error);
        if (error.status === 401) {
          this.errorMessage = error.error;
        } else {
          this.errorMessage = 'Tenemos un error, intentelo mas tarde';
        }
      }).finally(() => {
        this.loading = false;
      });

  }

  initUi(): void {
    // document.body.classList.remove();
    document.body.classList.add('uc-login-layout');
  }

}
