import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthLayoutComponent } from './screens/auth-layout/auth-layout.component';
import { ChoiceProfileComponent } from './screens/choice-profile/choice-profile.component';
import { LoginComponent } from './screens/login/login.component';
import { LoginDevComponent } from './screens/login-dev/login-dev.component';
import { LogoutComponent } from './screens/logout/logout.component';
import { AuthRoutingModule } from './auth-routing.module';



@NgModule({
  declarations: [
    AuthLayoutComponent,
    ChoiceProfileComponent,
    LoginComponent,
    LoginDevComponent,
    LogoutComponent
  ],
  imports: [
    AuthRoutingModule,
    CommonModule,
  ]
})
export class AuthModule { }
