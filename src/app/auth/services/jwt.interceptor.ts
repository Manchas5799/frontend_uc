import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable()
export class JWTInterceptor {

  constructor() {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token: string = localStorage.getItem('jwt') || 'none';
    request = request.clone({
      setHeaders: {
        // https://tools.ietf.org/html/rfc6750
        Authorization: `bearer ${token}`,
      }
    });
    return next.handle(request);
  }

}
