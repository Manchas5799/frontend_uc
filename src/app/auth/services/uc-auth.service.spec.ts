import { TestBed } from '@angular/core/testing';

import { UcAuthService } from './uc-auth.service';

describe('UcAuthService', () => {
  let service: UcAuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UcAuthService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
