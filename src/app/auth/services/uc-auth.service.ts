import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { AccountResponse } from '../models/AccountResponse';
import { AuthAppModule } from '../models/AuthUiModule';
import { UcProfile } from '../models/UcProfile';

@Injectable({
  providedIn: 'root'
})
export class UcAuthService {
  constructor(private http: HttpClient) { }

  login(googleToken: string): Promise<AccountResponse> {
    return this.http.post<AccountResponse>(
      `${environment.apiUrl}/api/v1/auth/login-executive`, { googleToken })
      .toPromise();
  }

  loginDev(user: string): Promise<AccountResponse> {
    if (environment.production == true) {
      throw new Error('Cannot login here in production');
    }
    return this.http.post<AccountResponse>(
      `${environment.apiUrl}/api/v1/auth/login-executive-dev`, { user })
      .toPromise();
  }

  refrestToken(): Promise<any> {
    return this.http.post<any>(
      `${environment.apiUrl}auth/refresh-token`, {})
      .toPromise();
  }
}
