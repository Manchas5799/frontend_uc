import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { BehaviorSubject, Observable } from 'rxjs';
import { AuthAppModule, AuthUiModule } from '../models/AuthUiModule';
import { UcProfile } from '../models/UcProfile';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private profileSubject = new BehaviorSubject<UcProfile |null>(null);
  
  constructor() {
    const profile = JSON.parse(
      localStorage.getItem('uc-profile') || '{}') || null;
    const token = localStorage.getItem('jwt');
    if (token && profile) {
      const helper = new JwtHelperService();
      const isExpired = helper.isTokenExpired(token);
      if (!isExpired) {
        this.profileSubject.next(profile);
      } else {
        localStorage.removeItem('jwt');
        localStorage.removeItem('uc-profile');
      }
    }
    
  }

  getProfile(): Observable<UcProfile | null> {
    return this.profileSubject;
  }

  setProfile(jwt: string, profile: UcProfile) {
    if (jwt && profile) {
      const helper = new JwtHelperService();
      const isExpired = helper.isTokenExpired(jwt);
      if (!isExpired) {
        this.registerProfile(profile);
        this.registerJwt(jwt);
        this.profileSubject.next(profile);
      } else {
        throw new Error('Token is expired');
      }
    }
  }

  setLevel(level: string): string | null {
    const profile = this.profileSubject.value;
    if (profile && profile.levels.includes(level)) {
      profile.level = level;
      this.registerProfile(profile);
      this.profileSubject.next(profile);
      return level;
    }
    return null;
  }

  cleanProfile() {
    localStorage.clear();
    this.profileSubject.next(null);
  }

  private registerJwt(jwt: string) {
    localStorage.setItem('jwt', jwt);
  }

  private registerProfile(profile: UcProfile) {
    localStorage.setItem('uc-profile', JSON.stringify(profile));
  }

}
