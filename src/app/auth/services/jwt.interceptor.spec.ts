import { TestBed } from '@angular/core/testing';

import { JWTInterceptor } from './jwt.interceptor';

describe('JwtService', () => {
  let service: JWTInterceptor;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(JWTInterceptor);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
