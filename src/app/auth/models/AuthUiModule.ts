export interface AuthUiModule {
  code: string;
  name: string;
  description: string;
  url: string;
  icon: string;
  color: string;
}

export interface AuthAppModule {
  moduleName: string;
  moduleDescription: string;
  campus: string;
  uri: string;
  icon: string;
  menu: string;
}

