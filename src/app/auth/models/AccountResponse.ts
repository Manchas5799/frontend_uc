import { UcProfile } from "./UcProfile";

export interface AccountResponse {
    jwt: string;
    profile: UcProfile;
    executive?: UcProfile;
}