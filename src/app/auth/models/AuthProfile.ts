export interface AuthProfile {
  code: string;
  type: string;
  level: string;
}