export interface UcProfile {
    code: string;
    id : string;
    name: string;
    familyName: string;
    givenName: string;
    type: string;
    levels: string[];
    level: string;
    campuses: string[];
    campus: string;
    photoUrl: string;
}