import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { environment } from 'src/environments/environment';
import { AuthLayoutComponent } from './screens/auth-layout/auth-layout.component';
import { ChoiceProfileComponent } from './screens/choice-profile/choice-profile.component';
import { LoginDevComponent } from './screens/login-dev/login-dev.component';
import { LoginComponent } from './screens/login/login.component';
import { LogoutComponent } from './screens/logout/logout.component';

const routes: Routes = [
  {
    path: '', component: AuthLayoutComponent, 
    children: [
      { path: '', redirectTo: 'ingresar', pathMatch: 'full' },
      { path: 'ingresar', component: environment.production? LoginComponent: LoginDevComponent },
      { path: 'salir', component: LogoutComponent },
      { path: 'escoger-perfil', component: ChoiceProfileComponent },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
