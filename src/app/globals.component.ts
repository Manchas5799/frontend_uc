import { Injectable, InjectionToken, Injector, NgZone, OnInit } from '@angular/core';
//import { ILoginResponse } from './login/models/login.response.model';
import { MatSnackBar, MatSnackBarConfig, MatSnackBarRef } from '@angular/material/snack-bar';

import { LiveAnnouncer } from '@angular/cdk/a11y';
import { BreakpointObserver } from '@angular/cdk/layout';
import { NoopScrollStrategy, Overlay, OverlayContainer, ScrollDispatcher, ScrollStrategyOptions, ViewportRuler } from '@angular/cdk/overlay';
import { Platform } from '@angular/cdk/platform';
import { HttpBackend, HttpClient, HttpHandler, HttpXhrBackend, ɵHttpInterceptingHandler } from '@angular/common/http';
import { InjectorInstance } from './app.module';
import { ILoginResponse } from './models/login.model';

@Injectable()
export class GlobalsComponent implements OnInit {
  
  constructor(private matSnackBar: MatSnackBar) {
  }

  ngOnInit(): void {
  }

  public currentUser: ILoginResponse;
  public tipoSite: string;
  public dialogsAbiertos: number;
  //{
  //  //return null;
  ////  var currentUserString = localStorage.getItem("currentUser");
  ////  if (currentUserString) {
  ////    var resp: ILoginResponse = JSON.parse(currentUserString);
  ////    return resp;
  ////  }
  //}


  public notify(message: string, title: string = null) {
    //const $injectorx = Injector.create([
    //  SOLSolicitudComponent,
    //  HttpClient
    //]);

    //const $injector = Injector.create([
    //  SOLSolicitudComponent,
    //  HttpClient,
    //  { provide: HttpHandler, useClass: ɵHttpInterceptingHandler },
    //  HttpXhrBackend,
    //  { provide: HttpBackend, useExisting: HttpXhrBackend },
    //  BrowserXhr,
    //  { provide: XhrFactory, useExisting: BrowserXhr },
    //]);


    //const injector = Injector.create({
    //  providers: [
    //    { provide: HttpClient, deps: [HttpHandler] },
    //    { provide: HttpHandler, useValue: new HttpXhrBackend({ build: () => new XMLHttpRequest }) },
    //  ],
    //});


    //const $injector = Injector.create({
    //  providers: [
    //    { provide: HttpHandler, useClass: ɵHttpInterceptingHandler }
    //  ]
    //});
    //$injector.get(SOLSolicitudComponent);

    //this.snackBar = $injector.get(MatSnackBar);
    //const $injector = Injector.create({
    //  providers: [
    //    {
    //      provide: MatSnackBar, deps: [Overlay, MatSnackBarRef, MatSnackBarConfig, BreakpointObserver, LiveAnnouncer, NgZone]
    //    },
    //    { provide: Overlay },
    //    { provide: ScrollStrategyOptions, useValue: new NoopScrollStrategy() },
    //    { provide: ScrollDispatcher },
    //    { provide: NgZone, useValue: new NgZone({ enableLongStackTrace: false, shouldCoalesceEventChangeDetection: false }) },
    //    { provide: Platform },
    //    { provide: ViewportRuler },
    //    { provide: InjectionToken, useValue: new InjectionToken<MatSnackBarConfig<any>>('DocumentToken') },
    //    { provide: OverlayContainer },
    //  ]
    //});
    //this.snackBar = $injector.get(MatSnackBar);
    // this.matSnackBar = InjectorInstance.get(MatSnackBar);

    var config: MatSnackBarConfig = {
      duration: 50000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
      panelClass: ['custom-snackbar']
    }

    this.matSnackBar.open(message, 'Ok', config);
  }
}
