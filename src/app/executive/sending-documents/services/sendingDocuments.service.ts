import { HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Webinar } from '../models/webinar';
import { WebinarPDF } from '../models/webinarPDF';
import { Email } from '../models/email';
import { EmailResponse } from '../models/emailResponse';
import { ProcedureResponse } from '../models/ProcedureResponse';
import { generatedDocumentCRUD } from '../models/generatedDocumentCRUD';
import { HttpHeaders } from '@angular/common/http';
import { Observable} from 'rxjs';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class SendingDocumentsService {

  constructor(
    private client: HttpClient
  ) { }
    
  sendEmail(data: Email): Promise<EmailResponse> {
    let headers = new HttpHeaders({
      'x-token': 'hola123xDxDxD'});
    let options = { headers: headers };

    return this.client.post<EmailResponse>(
      `http://172.16.3.45:4000/mail/send`,data,options)
      .toPromise();
  }

  generatedDocumentCRUD(data: generatedDocumentCRUD): Promise<ProcedureResponse> {
    return this.client.post<ProcedureResponse>(
      `${environment.apiUrl}/api/v1/sendingdocuments/generateddocumentscrud`,data)
      .toPromise();
  }

  generatedDocumentMassiceCRUD(data: generatedDocumentCRUD[]): Promise<ProcedureResponse> {
    return this.client.post<ProcedureResponse>(
      `${environment.apiUrl}/api/v1/sendingdocuments/generateddocumentsmassivecrud`,data)
      .toPromise();
  }

  getWebinar(data: any): Promise<Webinar[]> {
    return this.client.patch<Webinar[]>(
      `${environment.apiUrl}/api/v1/sendingdocuments/webinar`,data)
      .toPromise();
  }

  generateWebinarPDF(data: any): Promise<ProcedureResponse> {
    return this.client.post<ProcedureResponse>(
      `${environment.apiUrl}/api/v1/sendingdocuments/webinargenerator`,data)
      .toPromise();
  }

  generateConferenciaPDF(data: any): Promise<ProcedureResponse> {
    return this.client.post<ProcedureResponse>(
      `${environment.apiUrl}/api/v1/sendingdocuments/conferenciagenerator`,data)
      .toPromise();
  }

  generateDocumentPDF(data: Webinar): Promise<ProcedureResponse> {
    return this.client.post<ProcedureResponse>(
      `${environment.apiUrl}/api/v1/sendingdocuments/documentgenerator`,data)
      .toPromise();
  }

  generateDocumentMassivePDF(data: Webinar[]): Promise<ProcedureResponse> {
    return this.client.post<ProcedureResponse>(
      `${environment.apiUrl}/api/v1/sendingdocuments/documentgeneratormassive`,data)
      .toPromise();
  }

  downloadPDF(data: any): Observable<any> {
    return this.client.patch(`${environment.apiUrl}/api/v1/sendingdocuments/downloadPDF`, data, { responseType: 'blob' }).pipe(
      map((res) => {
        let newBlob: Blob = res;
        return new Blob([res], { type: newBlob.type });
      })
    );
  }


}
