export interface generatedDocumentCRUD {
    id: number;
    dni: string;
    seccion: string;
    archivo: string;
    tipoConstancia: number;
    estado: number;
    razonAnulacion: string;
    crud: string;
    nombreEstudiante?: string;
    nombrePrograma?: string;
}


