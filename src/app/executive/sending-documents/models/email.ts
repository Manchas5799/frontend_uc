import { AuthUiModule } from "src/app/auth/models/AuthUiModule";

export interface Email {
  user: String;
  to: String;
  subject: String;
  body: String;
  from: String;
  emailDev: String;
}