export interface WebinarPDF {
    tipoDocumento:string;
    studentName: string;
    nombreWebinar: string;
    dateDocument: Date;
    fecha: string;
    bEvento: number;
    nombreEvento: string;
    dni: string;
    seccion: string;
}