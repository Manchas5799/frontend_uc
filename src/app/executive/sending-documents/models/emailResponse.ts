import { AuthUiModule } from "src/app/auth/models/AuthUiModule";

export interface EmailResponse {
  status: String;
  tracer: String;
}