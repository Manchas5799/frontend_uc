export interface Webinar {
    dni: string;
    alumno: string;
    sede: string;
    tipoEscuela: string;
    escuela: string;
    apto: number;
    estado: number;
    archivo: string;
    selected: boolean;
    fecini: Date;
    fecfin: Date;
    asignaturas: string;
    correo: string;
    seccion: string;
    tipoconstancia: number;
    horas: number;
    codigoFormato: string;
}



