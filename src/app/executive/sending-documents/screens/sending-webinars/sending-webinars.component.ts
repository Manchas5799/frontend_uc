import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Webinar } from '../../models/webinar';
import { SendingDocumentsService } from '../../services/sendingDocuments.service';
import { WebinarPDF } from '../../models/webinarPDF';
import { generatedDocumentCRUD } from '../../models/generatedDocumentCRUD';
import { Email } from '../../models/email';
@Component({
  selector: 'app-sending-webinars',
  templateUrl: './sending-webinars.component.html',
  styleUrls: ['./sending-webinars.component.sass']
})
export class SendingWebinarsComponent implements OnInit {
  tipoConstancia: number = 1; //1->Webinar/Conferencia | 2->Const. Participación | 3->Certif.Curso Cerrado | 4-> Certif. Curso Abierto
  color = 'green';
  level = '';
  typeSearchFilter: string = 'bySection';
  showTable: boolean = false; 
  showMassiveControls: boolean = false;
  tempChecked1: boolean = false;
  tempChecked2: boolean = false;
  tempChecked3: boolean = false;
  tempChecked4: boolean = false;
  tempTotalCheked: boolean = false;
  tempGenerado: boolean = false;
  listWebinars: Webinar[] = [];
  documentPDF: WebinarPDF = {
    "tipoDocumento":"0",
    "studentName":"",
    "nombreWebinar":"",
    "dateDocument":new Date(),
    "fecha":"",
    "bEvento":0,
    "nombreEvento":"",
    "dni":"",
    "seccion":""
  }
  generatedDocument: generatedDocumentCRUD = {
    "id":0,
    "dni":"",
    "seccion":"",
    "archivo":"",
    "tipoConstancia":0,
    "estado":0,
    "razonAnulacion":"",
    "crud":""
  }

  inputDNI: string="";
  inputSection: string="";
  masiveOperation: string="0"
  massiveSelector: boolean = false;
  finding: boolean = false;
  executingMassive: boolean = false;
  dataMasiva: Webinar[]=[];
  dataGeneradaMasiva: generatedDocumentCRUD[]=[]; 
  constructor(private route: ActivatedRoute,private sendingDocumentsService: SendingDocumentsService) {}
  opMassive=false;
  ngOnInit(): void {
    this.level = this.route.snapshot.data['level'];
  }

  getPDF(filename:string){
    var file = {"file":filename};
    this.sendingDocumentsService.downloadPDF(file)
    .subscribe(
      {
        next: (result) =>{
          if(result){
            const fileDownloadName: string = filename+".pdf";
            const fileURL = URL.createObjectURL(result);
            const a: HTMLAnchorElement = document.createElement('a') as HTMLAnchorElement;
            a.href = fileURL;
            a.download = fileDownloadName;
            a.target = "_blank";
            document.body.appendChild(a);
            a.click();
          }
        }
      }
    );   
 }

  generarPDF(massive=false){
    var dd = String(new Date(this.documentPDF.dateDocument).getDate()).padStart(2, '0');
    var mm = String(new Date(this.documentPDF.dateDocument).getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = new Date(this.documentPDF.dateDocument).getFullYear();
    this.documentPDF.fecha = dd + " de " + this.getSpanishMonth(mm) + " de " + yyyy;
    if(this.documentPDF.tipoDocumento=='webinar'){
      this.sendingDocumentsService.generateWebinarPDF(this.documentPDF).then(rpta=>{
        if(!massive) this.reloadList();
      });
    }
    else{
      this.sendingDocumentsService.generateConferenciaPDF(this.documentPDF).then(rpta=>{
        if(!massive) this.reloadList();
      });
    }
  }

  generarMasivoPDF(){
    var dd = String(new Date(this.documentPDF.dateDocument).getDate()).padStart(2, '0');
    var mm = String(new Date(this.documentPDF.dateDocument).getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = new Date(this.documentPDF.dateDocument).getFullYear();
    this.documentPDF.fecha = dd + " de " + this.getSpanishMonth(mm) + " de " + yyyy;
    this.documentPDF.seccion = this.inputSection;
    this.showTable = false;
    this.executingMassive = true;
    for(var i = 0; i<this.listWebinars.length; i++){
      if(this.listWebinars[i].selected && this.listWebinars[i].estado==0 && this.listWebinars[i].apto){
        this.documentPDF.studentName = this.listWebinars[i].alumno;
        this.documentPDF.dni = this.listWebinars[i].dni;
        this.generarPDF(true);
      }
    }
    this.reloadList();
  }

  archivarPDF(massive=false){
    this.generatedDocument.tipoConstancia = this.tipoConstancia;
    this.generatedDocument.estado = 0;
    this.generatedDocument.crud="AN";
    this.generatedDocument.seccion=this.inputSection;
    this.sendingDocumentsService.generatedDocumentCRUD(this.generatedDocument).then(rpta=>{
      if(!massive) this.reloadList();
    });
  }

  archivarMasivoPDF(){
    this.showTable = false;
    this.executingMassive = true;
    for(var i = 0; i<this.listWebinars.length; i++){
      if(this.listWebinars[i].selected && this.listWebinars[i].estado==1){
        this.generatedDocument.dni = this.listWebinars[i].dni;
        this.archivarPDF(true);
      }
    }
    this.reloadList();
  }

  enviarPDF(massive=false){
    this.generatedDocument.tipoConstancia = this.tipoConstancia;
    this.generatedDocument.estado = 2;
    this.generatedDocument.crud="NTF";
    this.generatedDocument.seccion=this.inputSection;
    this.generatedDocument.tipoConstancia;

    console.log(this.generatedDocument);

    let data:Email = {
      "user": "PCAMARGO",
      "to":"test@gmail.com",
      "subject": "CONSTANCIA WEBINAR GENERADA",
      "body": "Estimado JOSÉ ANTONIO RIOS MONTERREY, Su certificado Webinar ha sido generado.",
      "from": "UNIVERSIDAD CONTINENTAL",
      "emailDev": "joserios.sistemas@gmail.com"
    };
 
    
    
    this.sendingDocumentsService.sendEmail(data);
    this.sendingDocumentsService.generatedDocumentCRUD(this.generatedDocument).then(rpta=>{
      if(!massive) this.reloadList();
    });
  }

  enviarMasivoPDF(){
    this.showTable = false;
    this.executingMassive = true;
    for(var i = 0; i<this.listWebinars.length; i++){
      if(this.listWebinars[i].selected && this.listWebinars[i].estado==1){
        this.generatedDocument.dni = this.listWebinars[i].dni;
        this.enviarPDF(true);
      }
    }
    this.reloadList();
  }

  reloadList(){
    this.documentPDF = {
      "tipoDocumento":"0",
      "studentName":"",
      "nombreWebinar":"",
      "dateDocument":new Date(),
      "fecha":"",
      "bEvento":0,
      "nombreEvento":"",
      "dni":"",
      "seccion":""
    }
    this.find();
  }

  getSpanishMonth(n: String):String{
    console.log(n);
    switch(n){
      case"01": return "Enero";break;
      case"02": return "Febrero";break;
      case"03": return "Marzo";break;
      case"04": return "Abril";break;
      case"05": return "Mayo";break;
      case"06": return "Junio";break;
      case"07": return "Julio";break;
      case"08": return "Agosto";break;
      case"09": return "Septiembre";break;
      case"10": return "Octubre";break;
      case"11": return "Noviembre";break;
      case"12": return "Diciembre";break;
    }
    return "";
  }

  setTypeSearchFilters(pType:string): void{
    this.showTable = false;
    this.showMassiveControls = false;
    this.typeSearchFilter = pType;
  }

  find(): void{
    var tipoBusqueda = this.typeSearchFilter == "byStudentCode" ? "porDni" : "porSeccion";
    var dni = this.inputDNI;
    var seccion = this.inputSection;
    this.getWebinar(tipoBusqueda,dni,seccion);
  }

  getWebinar(tipoBusqueda:string,dni:string,seccion:string) {
    this.finding = true;
    this.finding = true;
    this.showTable = false;
    const data: any ={
      tipoBusqueda: tipoBusqueda,
      dni: dni,
      seccion: seccion,
      tipoConstancia: this.tipoConstancia
    };
    this.sendingDocumentsService.getWebinar(data)
    .then(webinars=>{
      if(webinars[0].dni == "0"){
        alert("La sección ingresada no corresponde un programa de Webinar/Conferencia.");
        this.finding = false;
        return;
      }
      this.finding = false;
      this.listWebinars = webinars;
      this.showTable = true;
      this.executingMassive = false;
      this.listWebinars.forEach(webinar=>webinar.selected=false);
      this.massiveSelector = false;
    })
    .catch(err => {
      console.log(err);
    });
  }

  massiveOperation(): void{
    this.listWebinars.forEach(webinar=>webinar.selected=this.massiveSelector);
    this.showMassiveControls = this.massiveSelector;
  }

  reviewMassive(): void{
    let count: number = 0; 
    let countUnselected: number = 0;
    this.showMassiveControls = false;
    for(let i = 0; i < this.listWebinars.length; i++){
      if(this.listWebinars[i].selected){
        count++;;
      }
    }
    if(count>=1){
      this.showMassiveControls = true;
    }
    if(countUnselected>0) this.massiveSelector = true;
    else this.massiveSelector = false;
  }
  
}
