import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-sending-documents',
  templateUrl: './sending-documents.component.html',
  styleUrls: ['./sending-documents.component.sass']
})
export class SendingDocumentsComponent implements OnInit {
  color = 'green';
  level = '';
  constructor(private route: ActivatedRoute) {}
  
  ngOnInit(): void {
    this.level = this.route.snapshot.data['level'];
  }

}
