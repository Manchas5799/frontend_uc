import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApplicationService } from 'src/app/executive/attention/services/application.service';
import { GetDataEnum } from 'src/app/executive/models/get-data.enum';
import { DocumentRequest } from 'src/app/executive/models/request.model';

@Component({
  selector: 'app-status-process',
  templateUrl: './status-process.component.html',
  styleUrls: ['./status-process.component.sass']
})
export class StatusProcessComponent implements OnInit {
  color = 'blue';
  level = '';
  status = GetDataEnum;
  requestState: GetDataEnum = GetDataEnum.NOTHING;
  application?: DocumentRequest;
  constructor(
    private route: ActivatedRoute,
    private applicationService: ApplicationService,
    ) {
    this.level = this.route.snapshot.data['level'];
  }

  ngOnInit(): void {
  }

  searchRequest(code: string) {
    this.requestState = GetDataEnum.LOADING;
    console.log('>>>>', code);
    this.applicationService.getApplication(this.level, +code)
    .then((application) => {
      this.application = application;
      this.requestState = GetDataEnum.DONE;
    })
    .catch((error) => {
      this.requestState = GetDataEnum.ERROR;
    });
  }

}
