import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApplicationService } from 'src/app/executive/attention/services/application.service';
import { GetDataEnum } from 'src/app/executive/models/get-data.enum';
import { ReportFilter } from 'src/app/executive/models/report-filter.model';
@Component({
  selector: 'app-generated-report',
  templateUrl: './generated-report.component.html',
  styleUrls: ['./generated-report.component.sass']
})
export class GeneratedReportComponent implements OnInit {
  color = 'blue';
  level = '';
  status = GetDataEnum;
  reportPrereqState: GetDataEnum = GetDataEnum.NOTHING;
  reportState: GetDataEnum = GetDataEnum.NOTHING;
  reports: ReportFilter[] = [];
  report?: ReportFilter;
  showReport = false;
  constructor(
    private route: ActivatedRoute,
    private applicationService: ApplicationService,
    ) {
    this.level = this.route.snapshot.data['level'];
  }

  ngOnInit(): void {
    this.loadLocalReports();
    
  }
  
  getReportData(filter: ReportFilter) {
    console.log(filter.startAt, filter.endAt);
    this.reportState = GetDataEnum.LOADING;
    this.applicationService.searchApplications(this.level, filter).then((data) => {
      filter.data = data;
      this.pushReport(filter);
      this.reportState = GetDataEnum.DONE;
    }).catch(() => {
      this.reportState = GetDataEnum.ERROR;
    });

    
  }

  pushReport(report: ReportFilter) {
    //this.reports.push(report);
    this.reports.unshift(  {
        startAt: new Date(),
        endAt: new Date(),
        createdAt: new Date(),
        createdBy: 'rmatos',
        data: report.data ?? [],
      }
    );
    localStorage.setItem('request-reports-list', JSON.stringify(this.reports));
  }

  removeReport(newList: ReportFilter[]) {
    this.reports = newList;
    localStorage.setItem('request-reports-list', JSON.stringify(this.reports));
  }

  loadLocalReports() {
    const str = localStorage.getItem('request-reports-list');
    const reports = JSON.parse(str ?? '[]') ?? [];
    this.reports = reports;
  }

  previzualizeReport(filter: ReportFilter) {
    console.log('show filter', filter);
    this.report = filter;
    this.showReport = true;
  }
}
