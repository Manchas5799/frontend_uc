import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestReportsMainComponent } from './request-reports-main.component';

describe('RequestReportsMainComponent', () => {
  let component: RequestReportsMainComponent;
  let fixture: ComponentFixture<RequestReportsMainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RequestReportsMainComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestReportsMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
