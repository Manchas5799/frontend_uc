import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-request-reports-main',
  templateUrl: './request-reports-main.component.html',
  styleUrls: ['./request-reports-main.component.sass']
})
export class RequestReportsMainComponent implements OnInit {
  color = 'blue';
  level = '';
  constructor(private route: ActivatedRoute) {
    this.level = this.route.snapshot.data['level'];
  }
  ngOnInit(): void {
  }

}
