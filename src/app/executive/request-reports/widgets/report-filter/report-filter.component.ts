import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ApplicationService } from 'src/app/executive/attention/services/application.service';
import { GetDataEnum } from 'src/app/executive/models/get-data.enum';
import { ReportFilter } from 'src/app/executive/models/report-filter.model';

@Component({
  selector: 'ui-report-filter',
  templateUrl: './report-filter.component.html',
  styleUrls: ['./report-filter.component.sass']
})
export class ReportFilterComponent implements OnInit {
  @Input()
  color: string = 'blue';
  @Input()
  level: string = 'none';
  @Input()
  submitLabel = 'Generar reorte';
  @Output()
  submit = new EventEmitter<ReportFilter>();
  today = new Date();
  filter: ReportFilter = {
    startAt: new Date(),
    endAt: new Date(),
    campus: '',
    state: 0,
    office: 0,
    document: 0,
    
    //documentState: 0,
    term: '',
    dni: '',
    program: '',
    attendedBy: '',
    studentName: '',
  };
  programType = 0;
  filterData: any = {};
  status = GetDataEnum;
  requestState: GetDataEnum = GetDataEnum.LOADING;
  programs: any[] = [];
  applications: any[] = [];

  constructor(
    private applicationService: ApplicationService,
  ) { }

  ngOnInit(): void {

    this.applicationService.getFilterData(this.level)
    .then((data) => {
      this.filterData = data;
      this.requestState = GetDataEnum.DONE;
    }).catch(() => {
      this.requestState = GetDataEnum.ERROR;
    });
    
  }
  
  getReportData() {
    console.log('inside filter data:',this.filter);
    this.submit.emit(this.filter);
  }

  changeTypeProgram(typeCode: string) {
    this.programs = this.filterData.programs
      .filter((type: any) => type.code === typeCode)[0].programs;;
  }
}
