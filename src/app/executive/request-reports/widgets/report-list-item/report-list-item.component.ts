import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ReportFilter } from 'src/app/executive/models/report-filter.model';

@Component({
  selector: 'ui-report-list-item',
  templateUrl: './report-list-item.component.html',
  styleUrls: ['./report-list-item.component.sass']
})
export class ReportListItemComponent implements OnInit {
  @Input()
  reports: ReportFilter[] = [];
  @Output()
  removeItem = new EventEmitter<ReportFilter[]>();

  @Output()
  show = new EventEmitter<ReportFilter>();
  
  constructor() { }

  ngOnInit(): void {
  }

  // remove item from reports and emit event
  remove(index: number) {
    this.reports.splice(index, 1);
    this.removeItem.emit(this.reports);
  
  }

  showReport(index: number) {
    console.log('emit show report');
    
    const report = this.reports[index];
    this.show.emit(report);
  }

  download(index: number) {
    
  }

}
