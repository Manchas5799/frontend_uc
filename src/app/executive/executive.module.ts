import { CommonModule } from "@angular/common";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { RouterModule } from "@angular/router";
import { ExecutiveRoutingModule } from "./executive-routing.module";
import { ExecutiveComponent } from "./executive.component";
import { ExecutiveHomeComponent } from './home/executive-home/executive-home.component';
import { ExecutiveLayoutComponent } from './home/executive-layout/executive-layout.component';
import { AttentionMainComponent } from './attention/screens/attention-main/attention-main.component';
import { ToolsMainComponent } from './tool/screens/tools-main/tools-main.component';
import { RequestReportsMainComponent } from './request-reports/screens/request-reports-main/request-reports-main.component';
import { AdministrationMainComponent } from './administration/screens/administration-main/administration-main.component';
import { UpdateStudentFormComponent } from './update-student/screens/update-student-form/update-student-form.component';
import { AttentionDetailsComponent } from './attention/screens/attention-details/attention-details.component';
import { SearchStudentComponent } from './tool/screens/search-student/search-student.component';
import { GeneratedReportComponent } from './request-reports/screens/generated-report/generated-report.component';
import { StatusProcessComponent } from './request-reports/screens/status-process/status-process.component';
import { ManagePermissionsComponent } from './administration/screens/manage-permissions/manage-permissions.component';
import { ManageMenusComponent } from './administration/screens/manage-menus/manage-menus.component';
import { GeneralStatementComponent } from './administration/screens/general-statement/general-statement.component';
import { UiModule } from "../ui/ui.module";
import { JWTInterceptor } from "../auth/services/jwt.interceptor";
import { BannerComponent } from './tool/screens/banner/banner.component';
import { BannerUiComponent } from "./tool/widgets/banner-ui/banner-ui.component";
import { AttentionDisplayComponent } from './attention/screens/attention-display/attention-display.component';
import { RequestInfoComponent } from './attention/widgets/request-info/request-info.component';
import { RequestTraceComponent } from './attention/widgets/request-trace/request-trace.component';
import { RequestListComponent } from './attention/widgets/request-list/request-list.component';
import { AtentionPhotosComponent } from "./attention/screens/atention-photos/atention-photos.component";
import { SendingDocumentsComponent } from './sending-documents/screens/sending-documents-main/sending-documents.component'
import { SendingWebinarsComponent } from './sending-documents/screens/sending-webinars/sending-webinars.component'
import { SendingParticipationComponent } from './sending-documents/screens/sending-participation/sending-participation.component'
import { SendingClosedProgramComponent } from './sending-documents/screens/sending-closedprogram/sending-closedprogram.component'
import { SendingOpenedProgramComponent } from './sending-documents/screens/sending-openedprogram/sending-openedprogram.component'
import { TestComponent } from "../grid-forms/requests/screens/test/test.component";
import { RequestsComponent } from "../grid-forms/requests/screens/requests-main/requests.component";
import { MaterialModule } from "../shared/material/material.module";
import { ControlsModule } from "../shared/controls/controls.module";
import { BaseModule } from "../shared/base/base.module";
import { SharedModule } from "../shared/shared.module";
import { RequestsSummaryComponent } from "../grid-forms/requests/screens/requests-summary-main/requests-summary.component";
import { RequestsRAComponent } from "../grid-forms/requests/screens/requests-ra-main/requests-ra.component";
import { RequestsRASummaryComponent } from "../grid-forms/requests/screens/requests-ra-summary-main/requests-ra-summary.component";
import { PROProcesoGraduacionComponent } from '../grid-forms/PROProcesoGraduacion/PROProcesoGraduacion.component';
import { PROProcesoGraduacionEditComponent } from "../edit-forms/PROProcesoGraduacion/PROProcesoGraduacion.component";
import { IntegranteEditComponent } from "../edit-forms/Integrante/Integrante.component";
import { IntegranteComponent } from "../grid-forms/Integrante/Integrante.component";
import { DialogIntegranteEditComponent } from "../edit-forms/dialog-integrante/dialog-integrante.component";
import { AssignRolComponent } from './requests/widgets/assign-rol/assign-rol.component';
import { EntrevistasComponent } from "../edit-forms/Entrevistas/Entrevistas.component";
import { DialogEntrevistaEditComponent } from "../edit-forms/dialog-entrevista/dialog-entrevista.component";
import { DialogConfirmEditComponent } from "../edit-forms/dialog-confirm/dialog-confirm.component";
import { PEREstudianteEditComponent } from "../edit-forms/PEREstudiante/PEREstudiante.component";
import { PEREstudianteComponent } from "../grid-forms/PEREstudiante/PEREstudiante.component";

@NgModule({
  declarations: [
    ExecutiveComponent,
    ExecutiveHomeComponent,
    ExecutiveLayoutComponent,
    AttentionMainComponent,
    ToolsMainComponent,
    RequestReportsMainComponent,
    AdministrationMainComponent,
    UpdateStudentFormComponent,
    AttentionDetailsComponent,
    SearchStudentComponent,
    GeneratedReportComponent,
    StatusProcessComponent,
    ManagePermissionsComponent,
    ManageMenusComponent,
    GeneralStatementComponent,
    BannerComponent,
    BannerUiComponent,
    AttentionDisplayComponent,
    RequestInfoComponent,
    RequestTraceComponent,
    RequestListComponent,
    AtentionPhotosComponent,
    SendingDocumentsComponent,
    SendingWebinarsComponent,
    SendingParticipationComponent,
    SendingClosedProgramComponent,
    SendingOpenedProgramComponent,
    TestComponent,
    RequestsComponent,
    RequestsSummaryComponent,
    RequestsRAComponent,
    RequestsRASummaryComponent,
    PROProcesoGraduacionComponent,
    PROProcesoGraduacionEditComponent,
    IntegranteEditComponent,
    IntegranteComponent,
    DialogIntegranteEditComponent,
    AssignRolComponent,
    EntrevistasComponent,
    DialogEntrevistaEditComponent,
    DialogConfirmEditComponent,
    PEREstudianteEditComponent,
    PEREstudianteComponent
  ],
  imports: [
    MaterialModule,
    ControlsModule,
    //MatCheckboxModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ExecutiveRoutingModule,
    UiModule,
    BaseModule,
    SharedModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JWTInterceptor,
      multi: true,
    },
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ExecutiveModule { }
