import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { environment } from 'src/environments/environment';
import { HasExecutiveProfileGuard } from '../auth/guards/has-executive-profile.guard';
import { IsLoginGuard } from '../auth/guards/is-login.guard';
import { AdministrationMainComponent } from './administration/screens/administration-main/administration-main.component';
import { GeneralStatementComponent } from './administration/screens/general-statement/general-statement.component';
import { ManageMenusComponent } from './administration/screens/manage-menus/manage-menus.component';
import { ManagePermissionsComponent } from './administration/screens/manage-permissions/manage-permissions.component';
import { AtentionPhotosComponent } from './attention/screens/atention-photos/atention-photos.component';
import { AttentionDetailsComponent } from './attention/screens/attention-details/attention-details.component';
import { AttentionDisplayComponent } from './attention/screens/attention-display/attention-display.component';
import { AttentionMainComponent } from './attention/screens/attention-main/attention-main.component';
import { ExecutiveComponent } from './executive.component';
import { ExecutiveHomeComponent } from './home/executive-home/executive-home.component';
import { ExecutiveLayoutComponent } from './home/executive-layout/executive-layout.component';
import { GeneratedReportComponent } from './request-reports/screens/generated-report/generated-report.component';
import { RequestReportsMainComponent } from './request-reports/screens/request-reports-main/request-reports-main.component';
import { StatusProcessComponent } from './request-reports/screens/status-process/status-process.component';
import { BannerComponent } from './tool/screens/banner/banner.component';
import { SearchStudentComponent } from './tool/screens/search-student/search-student.component';
import { ToolsMainComponent } from './tool/screens/tools-main/tools-main.component';
import { UpdateStudentFormComponent } from './update-student/screens/update-student-form/update-student-form.component';
import { SendingDocumentsComponent } from './sending-documents/screens/sending-documents-main/sending-documents.component'
import { SendingWebinarsComponent } from './sending-documents/screens/sending-webinars/sending-webinars.component'
import { SendingParticipationComponent } from './sending-documents/screens/sending-participation/sending-participation.component'
import { SendingClosedProgramComponent } from './sending-documents/screens/sending-closedprogram/sending-closedprogram.component'
import { SendingOpenedProgramComponent } from './sending-documents/screens/sending-openedprogram/sending-openedprogram.component'
import { RequestsComponent } from '../grid-forms/requests/screens/requests-main/requests.component'
import { TestComponent } from '../grid-forms/requests/screens/test/test.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { RequestsSummaryComponent } from '../grid-forms/requests/screens/requests-summary-main/requests-summary.component';
import { RequestsRAComponent } from '../grid-forms/requests/screens/requests-ra-main/requests-ra.component';
import { RequestsRASummaryComponent } from '../grid-forms/requests/screens/requests-ra-summary-main/requests-ra-summary.component';
import { PROProcesoGraduacionComponent } from '../grid-forms/PROProcesoGraduacion/PROProcesoGraduacion.component';
import { PROProcesoGraduacionEditComponent } from '../edit-forms/PROProcesoGraduacion/PROProcesoGraduacion.component';
import { EntrevistasComponent } from '../edit-forms/Entrevistas/Entrevistas.component';

const routes: Routes = [
  {
    path: '', component: ExecutiveLayoutComponent,
    canActivate: [IsLoginGuard, HasExecutiveProfileGuard],
    children: [
      { path: '', redirectTo: `/auth/escoger-perfil`, pathMatch: 'full' },
      { path: 'posgrado', data: { level: 'posgrado' },
        children: [
          { path: '', component: ExecutiveHomeComponent,  }, // /administrativo/postgrado
          { path: 'atenciones', component: AttentionMainComponent, },
          { path: 'atenciones/solicitud-estudiante/:requestId', component: AttentionDisplayComponent},
          { path: 'atenciones/solicitud-estudiante', component: AttentionDetailsComponent},
          { path: 'atenciones/fotos', component: AtentionPhotosComponent},
          { path: 'herramientas', component: ToolsMainComponent, },
          { path: 'herramientas/cambiar-estudiante', component: SearchStudentComponent, },
          { path: 'herramientas/banner', component: BannerComponent, },
          { path: 'reportes', component: RequestReportsMainComponent, },
          { path: 'reportes/generar-reporte', component: GeneratedReportComponent, },
          { path: 'reportes/estado-tramite', component: StatusProcessComponent},
          { path: 'administracion', component: AdministrationMainComponent, },
          { path: 'administracion/administrar-permisos', component: ManagePermissionsComponent, },
          { path: 'administracion/administrar-menus', component: ManageMenusComponent, },
          { path: 'administracion/comunicado-general', component: GeneralStatementComponent, },
          { path: 'emision-documentos-pg', component: SendingDocumentsComponent, },
          { path: 'emision-documentos-pg/sending-webinars', component: SendingWebinarsComponent, },
          { path: 'emision-documentos-pg/sending-participation', component: SendingParticipationComponent, },
          { path: 'emision-documentos-pg/sending-closedprogram', component: SendingClosedProgramComponent, },
          { path: 'emision-documentos-pg/sending-openedprogram', component: SendingOpenedProgramComponent, },
          { path: 'test', component: TestComponent, },
          { path: 'solicitudes', component: RequestsComponent, },
          { path: 'solicitudes-summary', component: RequestsSummaryComponent, },
          { path: 'solicitudes-ra', component: RequestsRAComponent, },
          { path: 'solicitudes-ra-summary', component: RequestsRASummaryComponent, },
          { path: 'proceso-graduacion', component: PROProcesoGraduacionComponent, },
          { path: 'proceso-graduacion-edit/:id', component: PROProcesoGraduacionEditComponent, },
          { path: 'entrevistas', component: EntrevistasComponent, },
          // { path: 'actualizar-estudiante', component: UpdateStudentFormComponent},
        ]
      },
      { path: 'continua', data: { level: 'continua' },
        children: [
          { path: '', component: ExecutiveHomeComponent,  },
          { path: 'herramientas', component: ToolsMainComponent, },
          { path: 'herramientas/cambiar-estudiante', component: SearchStudentComponent, },
          { path: 'herramientas/banner', component: BannerComponent, },
          { path: 'administracion', component: AdministrationMainComponent, },
          { path: 'administracion/administrar-permisos', component: ManagePermissionsComponent, },
          { path: 'administracion/administrar-menus', component: ManageMenusComponent, },
          { path: 'administracion/comunicado-general', component: GeneralStatementComponent, },
          { path: 'actualizar-estudiante', component: UpdateStudentFormComponent},
        ]
      },
    ],
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    MatCheckboxModule
  ],
  exports: [RouterModule],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ExecutiveRoutingModule { }
