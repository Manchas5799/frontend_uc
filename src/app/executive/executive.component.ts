import { Component } from '@angular/core';

@Component({
  selector: 'executive-root',
  template: 'Executive<router-outlet></router-outlet>',
})
export class ExecutiveComponent {
  title = 'UCPortal';
}
