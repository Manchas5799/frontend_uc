export interface PortalModule {
  id: number;
  name: string;
  description: string;
}