export interface Campus {
  id: string;
  code: string;
  name: string;
}