import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-administration-main',
  templateUrl: './administration-main.component.html',
  styleUrls: ['./administration-main.component.sass']
})
export class AdministrationMainComponent implements OnInit {
  color = 'orange';
  level = '';
  constructor(private route: ActivatedRoute) {
    this.level = this.route.snapshot.data['level'];
  }

  ngOnInit(): void {
  }

}
