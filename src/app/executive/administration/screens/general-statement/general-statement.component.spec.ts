import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralStatementComponent } from './general-statement.component';

describe('GeneralStatementComponent', () => {
  let component: GeneralStatementComponent;
  let fixture: ComponentFixture<GeneralStatementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GeneralStatementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralStatementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
