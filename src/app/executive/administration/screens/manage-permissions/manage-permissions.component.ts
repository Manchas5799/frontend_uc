import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Campus } from '../../models/Campus';
import { PortalModule } from '../../models/PortalModule';
import { AuthorizationService } from '../../services/authorization.service';
import { CampusService } from '../../services/campus.service';

@Component({
  selector: 'app-manage-permissions',
  templateUrl: './manage-permissions.component.html',
  styleUrls: ['./manage-permissions.component.sass']
})
export class ManagePermissionsComponent implements OnInit {
  color = 'blue';
  loading = true;
  modules: PortalModule[] = [];
  moduleSelected?: PortalModule ;
  users?: any[];
  level = '';
  campuses: Campus[] = [];
  campusSelected?: Campus;
  constructor(
    private authorizationService: AuthorizationService,
    private campusService: CampusService,
    private route: ActivatedRoute) {
    this.level = this.route.snapshot.data['level'];
  }

  ngOnInit(): void {
    this.campusService.getCampus().then(campuses => {
      this.campuses = campuses;
    }).catch(err => {});
    this.authorizationService.getModules(this.level).then(modules => {
      this.modules = modules;
      this.loading = false;
      console.log(this.modules);
    }).catch(err => {});
  }

  selectModule(module: PortalModule) {
    this.moduleSelected = module;
    this.campusSelected = undefined;
    this.users = undefined;
  }
  selectCampus(campus: Campus) {
    this.campusSelected = campus;
    this.users = undefined;
    if (this.moduleSelected) {
      this.getUsers(this.moduleSelected.id, campus.id);
    }
  }

  getUsers(moduleId: number, campusId: string) {
    this.authorizationService.getUsersInModule(moduleId, campusId).then(users => {
      this.users = users;
    }).catch(err => {});
  }

  addUser(user: string) {
    console.log(user);
    if (this.moduleSelected && this.campusSelected) {
      this.authorizationService.addUserToModule(user, this.moduleSelected?.id, this.campusSelected.id).then((res) => {
        console.log(res);
        if (this.moduleSelected && this.campusSelected) {
          this.getUsers(this.moduleSelected.id, this.campusSelected.id);
        }
      }).catch(err => {});
    }
  }

  removeUser(userId: string) {
    if (this.moduleSelected && this.campusSelected) {
      this.authorizationService.removeUserFromModule(
        userId, this.moduleSelected.id, this.campusSelected.id).then((res) => {
        console.log(res);
        if (this.moduleSelected && this.campusSelected) {
          this.getUsers(this.moduleSelected.id, this.campusSelected.id);
        }
      }).catch(err => {});
    }
  }
}
