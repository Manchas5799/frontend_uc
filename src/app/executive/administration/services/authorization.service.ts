import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { PortalModule } from '../models/PortalModule';
import { StatusResponse } from '../models/StatusResponse';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {

  constructor(private http: HttpClient) { }

  getModules(level: string): Promise<PortalModule[]> {
    return this.http.get<PortalModule[]>(
        `${environment.apiUrl}/api/v1/authorization/${level}/modules`)
      .toPromise();
  }

  getUsersInModule(moduleId: number, campus: string) {
    return this.http.get<any[]>(
        `${environment.apiUrl}/api/v1/authorization/campus/${campus}/module/${moduleId}/users`)
      .toPromise();
  }

  addUserToModule(userId: string, moduleId: number, campus: string)
  : Promise<StatusResponse> {
    const data = {moduleId, userId, campus};
    return this.http.post<StatusResponse>(
        `${environment.apiUrl}/api/v1/authorization/modules/add-user`, data)
      .toPromise();
  }

  removeUserFromModule(userId: string, moduleId: number, campus: string)
  : Promise<StatusResponse> {
    const data = {moduleId, userId, campus};
    return this.http.post<StatusResponse>(
        `${environment.apiUrl}/api/v1/authorization/modules/remove-user`, data)
      .toPromise();
  }
}
