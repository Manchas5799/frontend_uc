import { Injectable } from '@angular/core';
import { Campus } from '../models/Campus';

@Injectable({
  providedIn: 'root'
})
export class CampusService {

  constructor() { }

  getCampus(): Promise<Campus[]> {
    return new Promise((resolve, _) =>{
      resolve([
        {id: 'S01', code: 'HYO', name: "Huancayo"},
        {id: 'F01', code: 'ARQP', name: "Arequipa"},
        {id: 'F02', code: 'CUZ', name: "Cuzco"},
        {id: 'F03', code: 'LIM', name: "Lima"},
      ]);
    }); 
  }
}
