import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthUiModule } from 'src/app/auth/models/AuthUiModule';
import { ModulesService } from '../services/modules.service';

@Component({
  selector: 'app-executive-home',
  templateUrl: './executive-home.component.html',
  styleUrls: ['./executive-home.component.sass']
})
export class ExecutiveHomeComponent implements OnInit {
  level: string = '';
  campus: string = '';
  modules: AuthUiModule[] = [];
  
  constructor(
    private route: ActivatedRoute,
    private modulesService: ModulesService,
  ) {
    this.level = this.route.snapshot.data['level'];
   }

  ngOnInit(): void {
    this.modulesService.getModules().subscribe(modules => {
      this.modules = modules ?? [];
    });
    const campuses = this.modulesService.getCapmuses();
    this.modulesService.getCampus().subscribe(campus => {
      this.campus = campus || campuses[0];
    });
  }

}
