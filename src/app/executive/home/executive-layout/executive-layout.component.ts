import { Component, OnInit } from '@angular/core';
import { AuthUiModule } from 'src/app/auth/models/AuthUiModule';
import { AuthService } from 'src/app/auth/services/auth.service';
import { DinamicScriptLoaderService } from 'src/app/ui/services/dinamic-script-loader.service';
import { ModulesService } from '../services/modules.service';

@Component({
  selector: 'app-executive-layout',
  templateUrl: './executive-layout.component.html',
  styleUrls: ['./executive-layout.component.sass']
})
export class ExecutiveLayoutComponent implements OnInit {
  level: string = '';
  campuses: string[] = [];
  campus: string = '';
  modules: AuthUiModule[] = [];
  constructor(
    private scriptLoader: DinamicScriptLoaderService,
    private authService: AuthService,
    private modulesService: ModulesService,
    ) {
      // this.level = this.route.snapshot.data['level'];
    }

  ngOnInit(): void {
    this.initUI();
    console.log("exec")
    this.authService.getProfile().subscribe(profile => {
      this.level = profile?.level ?? '';
      this.modulesService.loadModules(this.level).then(data => {
        this.modulesService.setModules(data);
        this.campuses = this.modulesService.getCapmuses();
      }).catch(error => {});
    });
    this.modulesService.getModules().subscribe(modules => {
      this.modules = modules ?? [];
    });
    this.modulesService.getCampus().subscribe(campus => {
      this.campus = campus || this.campuses[0];
    });

  }

  initUI() {
    // hold-transition sidebar-mini layout-fixed
    document.body.className = '';
    document.body.classList.add('hold-transition');
    document.body.classList.add('sidebar-mini');
    document.body.classList.add('layout-fixed');
    if (window.innerWidth < 992) {
      document.body.classList.add('sidebar-closed');
      document.body.classList.add('sidebar-collapse');
      document.body.classList.remove('sidebar-open');
    }

    // document.body.classList.add('uc-layout-student');
    this.scriptLoader.load('adminlte-init').then(data => {
      console.log('script load success', data);
      const preloader: any = document.getElementsByClassName('preloader')[0];
      if (preloader) {
        setTimeout(() => {
          preloader.style.height = '0';
          setTimeout(function () {
            preloader.classList.add('hide');
            preloader.style.display = 'none';
          }, 200);
        }, 500);
      }
    }).catch(error => {
      console.log('script load error', error);
    });
  }
}
