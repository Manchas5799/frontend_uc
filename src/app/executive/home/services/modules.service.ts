import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { AuthAppModule, AuthUiModule } from 'src/app/auth/models/AuthUiModule';
import { environment } from 'src/environments/environment';
import { ExecutivePermission } from '../../models/permission';

@Injectable({
  providedIn: 'root'
})
export class ModulesService {
  private modulesSubject = new BehaviorSubject<AuthUiModule[] | null>(null);
  private campusSubject = new BehaviorSubject<string | null>(null);
  private permissionsSubject = new BehaviorSubject<ExecutivePermission[]>([]);
  private modules: AuthUiModule[] = [];
  constructor(private http: HttpClient) {
    // load modules
    const modules = JSON.parse(
      localStorage.getItem('uc-modulles') || '[]') || null;
    if (modules) {
      this.setModules(modules);
    }
  }

  getModules(): Observable<AuthUiModule[] | null> {
    return this.modulesSubject;
  }

  setModules(appModules: AuthAppModule[]) {
    const permission: ExecutivePermission[] = [];
    //const uiModules: AuthUiModule[] = [];
    const campuses = [...new Set(appModules.map(m => m.campus))];
    campuses.forEach(campus => {
      permission.push({
        campus: campus,
        modules: appModules
          .filter((mod: AuthAppModule) => mod.campus === campus)
          .map<AuthUiModule>((mod: AuthAppModule) =>  {
            return {
              code: mod.moduleName,
              name: mod.menu,
              description: mod.moduleName,
              url: mod.uri,
              icon: mod.icon,
              color: 'orange',
            }
          })
      });
    });
    console.log(permission);
    localStorage.setItem('uc-executive-permissions', JSON.stringify(permission));
    this.permissionsSubject.next(permission);
    const campus = campuses.length > 0 ? campuses[0] : null;
    this.campusSubject.next(campus);
    if (campus) {
      const modules: AuthUiModule[] = permission
        .find(p => p.campus === campus)?.modules ?? [];
      this.modulesSubject.next(modules);
    }
  }

  loadModules(level: string): Promise<AuthAppModule[]> {
    //return of([]).toPromise();
    return this.http.get<AuthAppModule[]>(
      `${environment.apiUrl}/api/v1/executive/${level}/modules`)
      .toPromise();
  }

  getCapmuses(): string[] {
    const permissions: ExecutivePermission[] = JSON.parse(
      localStorage.getItem('uc-executive-permissions') || '[]') || [];
    // campuses
    return permissions.map<string>((p: ExecutivePermission) => p.campus);
  }

  getCampus(): Observable<string | null> {
    return this.campusSubject;
  }

  setCampus(campus: string) {
    const permissions: ExecutivePermission[] = JSON.parse(
      localStorage.getItem('uc-executive-permissions') || '[]') || [];
    const campuses = permissions
    .map<string>((p: ExecutivePermission) => p.campus);
    if ( campuses.includes(campus) ) {
      this.campusSubject.next(campus);
      const modules: AuthUiModule[] = permissions
        .find(p => p.campus === campus)?.modules ?? [];
      this.modulesSubject.next(modules);
    } else {
      throw new Error("Campus not found");
    }
  }
}
