import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtentionPhotosComponent } from './atention-photos.component';

describe('AtentionPhotosComponent', () => {
  let component: AtentionPhotosComponent;
  let fixture: ComponentFixture<AtentionPhotosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtentionPhotosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtentionPhotosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
