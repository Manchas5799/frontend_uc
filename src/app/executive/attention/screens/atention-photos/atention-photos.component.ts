import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmationService, MessageService } from 'primeng/api';
import { UcProfile } from 'src/app/auth/models/UcProfile';
import { AuthService } from 'src/app/auth/services/auth.service';
import { GetDataEnum } from 'src/app/executive/models/get-data.enum';
import { PhotoRequest } from 'src/app/executive/models/photoRequest';
import { DocumentRequest } from 'src/app/executive/models/request.model';
import { environment } from 'src/environments/environment';
import { ApplicationService } from '../../services/application.service';
import { DocumentService } from '../../services/document.service';
import { OfficeService } from '../../services/office.service';
import { PhotoService } from '../../services/photo.service';

@Component({
  selector: 'app-atention-photos',
  templateUrl: './atention-photos.component.html',
  styleUrls: ['./atention-photos.component.sass'],
  providers: [MessageService, ConfirmationService],
})
export class AtentionPhotosComponent implements OnInit {
  showFilterDialog = false;
  level = '';
  profile: UcProfile | null = null;
  offices: any[] = [];
  documents: any[] = [];
  applications: DocumentRequest[] = [];
  filterApplications: DocumentRequest[] = [];
  myApplications: DocumentRequest[] = [];
  status = GetDataEnum;
  requestState: GetDataEnum = GetDataEnum.LOADING;
  tinyFilter = {
    criticaly: '',
    office: '',
    documentType: '',
  };
  color: string = 'purple';
  requests: PhotoRequest[] = [];
  apiPhoto = environment.apiUrl + '/api/v1/personal/photo/';
  showLoadingDialog = false;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private officeService: OfficeService,
    private documentService:DocumentService,
    private applicationService: ApplicationService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private photoService: PhotoService,
  ) {}

  ngOnInit(): void {
    this.level = this.route.snapshot.data['level'];
    this.authService.getProfile().subscribe((profile) => {
      this.profile = profile;
    });
    this.getRequests();
  }
  getRequests() {
    this.requests = [];
    this.requestState = GetDataEnum.LOADING;
    this.photoService.getRequests(this.level)
    .then((requests) => {
      requests.forEach(req => {
        req.url = 'data:image/png;base64,' + req.photo;
        req.show = false;
      });
      this.requests = requests;
      this.requestState = GetDataEnum.DONE;
    })
    .catch((err) => {
      this.requestState = GetDataEnum.ERROR;
    });
  }

  updateRequest(request: PhotoRequest, status: string) {

    this.confirmationService.confirm({
      message: status == 'approved'
        ? 'Esta seguro de actualizar su foto de perfil.'
        : 'Esta seguro de rechazar la actualización foto de perfil.',
      accept: () => {
          this.processupdateRequest(request, status);
      }});
    
  }
  processupdateRequest(request: PhotoRequest, status: string) {
    console.log('updateRequest', status);
    this.showLoadingDialog = true;
    this.photoService.updateRequest(this.level, request.id, status)
      .then( (data) => console.log('ok', status))
      .catch((err) => console.log('err'))
      .finally(() => {
        this.showLoadingDialog = false;
        this.getRequests();
      });
  }
}
