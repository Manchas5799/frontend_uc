import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AttentionMainComponent } from './attention-main.component';

describe('AttentionMainComponent', () => {
  let component: AttentionMainComponent;
  let fixture: ComponentFixture<AttentionMainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AttentionMainComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AttentionMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
