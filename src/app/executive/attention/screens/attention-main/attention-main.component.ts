import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-attention-main',
  templateUrl: './attention-main.component.html',
  styleUrls: ['./attention-main.component.sass']
})
export class AttentionMainComponent implements OnInit {
  color = 'purple';
  level = '';
  constructor(private route: ActivatedRoute) {}
  
  ngOnInit(): void {
    this.level = this.route.snapshot.data['level'];
  }

}
