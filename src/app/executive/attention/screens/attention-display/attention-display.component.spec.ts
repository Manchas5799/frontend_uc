import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AttentionDisplayComponent } from './attention-display.component';

describe('AttentionDisplayComponent', () => {
  let component: AttentionDisplayComponent;
  let fixture: ComponentFixture<AttentionDisplayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AttentionDisplayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AttentionDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
