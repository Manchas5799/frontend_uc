import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GetDataEnum } from 'src/app/executive/models/get-data.enum';
import { DocumentRequest } from 'src/app/executive/models/request.model';
import { ApplicationService } from '../../services/application.service';
import { OfficeService } from '../../services/office.service';

@Component({
  selector: 'app-attention-display',
  templateUrl: './attention-display.component.html',
  styleUrls: ['./attention-display.component.sass']
})
export class AttentionDisplayComponent implements OnInit {

  request?: DocumentRequest;
  color: string = 'purple';
  status = GetDataEnum;
  requestState: GetDataEnum = GetDataEnum.LOADING;
  level = '';
  code: number = 0;
  offices: any[] = [];
  application: any;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private applicationService: ApplicationService,
    private officeService: OfficeService,
  ) { }

  ngOnInit(): void {
    this.level = this.route.snapshot.data['level'];
    this.code = +this.route.snapshot.params['requestId'];
    this.officeService.getOffices(this.level).then((offices) => {
      this.offices = offices;
      this.getApplication();
    });
    
  }

  getApplication() {
    this.applicationService.getApplication(this.level, this.code)
    .then((application) => {
      this.application = application;
      this.requestState = GetDataEnum.DONE;
      this.getStates(application.documentId, application.requestStateID);
    })
    .catch((error) => {
      this.requestState = GetDataEnum.ERROR;
    });
  }

  getStates(documentId: number, stateId: number) {
    this.applicationService.getPosibleStates(this.level, documentId, stateId)
    .then((states) => {
      this.application.states = states;
    })
    .catch((error) => {
      this.requestState = GetDataEnum.ERROR;
    });
  }

  updateState(next: number, message: string) {
    const data: any = {
      next,
      message,
      "userCode": "usuariope",
    };
    this.applicationService.updateApplicationState(this.level, this.code, data)
    .then((state) => {
      console.log(state);
      this.application = null;
      this.requestState = GetDataEnum.LOADING;
      this.getApplication();
    })
    .catch((error) => {
      this.requestState = GetDataEnum.ERROR;
    });
  }

  getOfficeName(id: number): string {
    if (id === null) return "----";
    return this.offices.find(o => o.id === id).name;
  }

}
