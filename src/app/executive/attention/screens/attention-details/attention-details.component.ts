import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UcProfile } from 'src/app/auth/models/UcProfile';
import { AuthService } from 'src/app/auth/services/auth.service';
import { GetDataEnum } from 'src/app/executive/models/get-data.enum';
import { ReportFilter } from 'src/app/executive/models/report-filter.model';
import { DocumentRequest } from 'src/app/executive/models/request.model';
import { environment } from 'src/environments/environment';
import { ApplicationService } from '../../services/application.service';
import { DocumentService } from '../../services/document.service';
import { OfficeService } from '../../services/office.service';

@Component({
  selector: 'app-attention-details',
  templateUrl: './attention-details.component.html',
  styleUrls: ['./attention-details.component.sass']
})
export class AttentionDetailsComponent implements OnInit {
  showFilterDialog = false;
  level = '';
  profile: UcProfile | null = null;
  offices: any[] = [];
  documents: any[] = [];
  applications: DocumentRequest[] = [];
  filterApplications: DocumentRequest[] = [];
  myApplications: DocumentRequest[] = [];
  status = GetDataEnum;
  requestState: GetDataEnum = GetDataEnum.LOADING;
  tinyFilter = {
    criticaly: '',
    office: '',
    documentType: '',
  };
  color: string = 'purple';
  
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private officeService: OfficeService,
    private documentService:DocumentService,
    private applicationService: ApplicationService,
  ) { }
  
  ngOnInit(): void {
    this.level = this.route.snapshot.data['level'];
    this.authService.getProfile().subscribe((profile) => {
      this.profile = profile;
    });
    let emptyFilter = {
      "startAt":"2022-02-25T21:46:57.605Z",
      "endAt":"2022-02-25T21:46:57.605Z",
      "campus":"",
      "state":0,
      "office":0,
      "document":0,
      "term":"",
      "dni":"",
      "program":"",
      "attendedBy":"",
      "studentName":""
    }
    Promise.all(
      [
        this.officeService.getOffices(this.level),
        this.documentService.getdocuments(this.level),
        this.applicationService.searchApplications(this.level, emptyFilter)
      ]
    ).then(([offices, documents, applications]) => {
      this.offices = offices;
      this.documents = documents;
      this.processApplications(applications);
      console.log(this.offices, this.documents);
      this.requestState = GetDataEnum.DONE;
    }).catch(() => {});
    
  }

  display(request: any): void {
    //console.log(['/administrativo',this.level,'atenciones','solicitud-estudiante', requestId]);
    this.router.navigate(['/administrativo',this.level,'atenciones','solicitud-estudiante', request.id]);
  }

  showFilter(): void {
    this.showFilterDialog = !this.showFilterDialog;
  }

  getReportData(filter: ReportFilter) {
    this.applicationService.searchApplications(this.level, filter)
    .then((data) => {
      console.log(data);
      this.processApplications(data);
    }).catch(() => {});
  }

  processApplications(applications: any[]) {
    this.applications = applications;
    this.filterApplication();
  }
  
  change(event: any) {
    console.log('change', this.tinyFilter);
    this.filterApplication();
  }

  filterApplication() {
    this.filterApplications = [];
    let filterData: DocumentRequest[] = this.applications;
    if (this.tinyFilter.documentType !== '') {
      filterData = filterData.filter(ap => ap.document.id === +this.tinyFilter.documentType);
    }
    if (this.tinyFilter.office !== '') {
      filterData = filterData.filter(ap => ap.officeId === +this.tinyFilter.office);
    }
    if (this.tinyFilter.criticaly !== '') {
      filterData = filterData.filter(ap => this.getColor(ap.requestDate, ap.document.estimatedDays) === this.tinyFilter.criticaly);
    }
    this.filterApplications = filterData;
    // my requests
    const pidm = this.profile?.id;
    this.myApplications  = filterData.filter((application) => (application as any).attendedBy === pidm);
  }

  getColor(startAt: string | Date, totalDays: number) {
    if (typeof startAt === 'string') startAt = new Date(startAt);
    const now = new Date();
    const days = Math.floor((now.getTime() - startAt.getTime()) / (1000 * 60 * 60 * 24));
    // get pecentage
    if (days > totalDays) {
      return 'red';
    }
    const percent = days/totalDays * 100;

    if (percent >= environment.requestCriticality.red) {
      return 'red';
    } else if (percent >= environment.requestCriticality.yellow) {
      return 'yellow';
    } else {
      return 'green';
    }
  }
}
