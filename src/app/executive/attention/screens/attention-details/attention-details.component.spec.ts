import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AttentionDetailsComponent } from './attention-details.component';

describe('AttentionDetailsComponent', () => {
  let component: AttentionDetailsComponent;
  let fixture: ComponentFixture<AttentionDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AttentionDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AttentionDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
