import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { GetDataEnum } from 'src/app/executive/models/get-data.enum';
import { DocumentRequest } from 'src/app/executive/models/request.model';
import { ApplicationService } from '../../services/application.service';

@Component({
  selector: 'app-request-trace',
  templateUrl: './request-trace.component.html',
  styleUrls: ['./request-trace.component.sass']
})
export class RequestTraceComponent implements OnInit {
  @Input()
  request?: DocumentRequest;
  @Input()
  level = 'posgrado';
  @Input()
  color = 'blue';
  steps: any[] = [];
  status = GetDataEnum;
  requestState: GetDataEnum = GetDataEnum.NOTHING;
  constructor(private applicationService: ApplicationService) { }

  ngOnInit(): void {
    this.loadData();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.request.firstChange) return;
    if (changes.request.currentValue.id !== changes.request.previousValue.id) {
      this.loadData();
    }
  }

  loadData() {
    if (this.request) {
      this.requestState = GetDataEnum.LOADING;
      this.applicationService.log(this.level, this.request.id).then(
        (data) => {
          this.steps = data;
          this.requestState = GetDataEnum.DONE;
        }
      ).catch((e) => this.requestState = GetDataEnum.ERROR);
    }
  }


}
