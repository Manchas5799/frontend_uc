import { Component, Input, OnInit } from '@angular/core';
import { DocumentRequest } from 'src/app/executive/models/request.model';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-request-info',
  templateUrl: './request-info.component.html',
  styleUrls: ['./request-info.component.sass']
})
export class RequestInfoComponent implements OnInit {
  @Input()
  request?: DocumentRequest;
  @Input()
  color = 'blue';
  criticaly: string = 'green';
  untilDays = '';
  description = '';
  apiUrl = environment.apiUrl;
  constructor() { }

  ngOnInit(): void {
    if (this.request) {
      this.criticaly = this.getColor(this.request.requestDate, this.request.document.estimatedDays);
      this.untilDays = this.getUntilDays(this.request.requestDate, this.request.document.estimatedDays);
      this.description = this.request.metaData.description;
    }
  }

  getColor(startAt: string | Date, totalDays: number) {
    if (typeof startAt === 'string') startAt = new Date(startAt);
    const now = new Date();
    const days = Math.floor((now.getTime() - startAt.getTime()) / (1000 * 60 * 60 * 24));
    // get pecentage
    if (days > totalDays) {
      return 'red';
    }
    const percent = days/totalDays * 100;

    if (percent >= environment.requestCriticality.red) {
      return 'red';
    } else if (percent >= environment.requestCriticality.yellow) {
      return 'yellow';
    } else {
      return days +'green';
    }
  }

  getUntilDays(startAt: string | Date, totalDays: number): string {
    if (typeof startAt === 'string') startAt = new Date(startAt);
    const now = new Date();
    const days = Math.floor((now.getTime() - startAt.getTime()) / (1000 * 60 * 60 * 24));
    const until = totalDays - days;
    if (until < 0) {
      return `Vencio hace ${Math.abs(until)} días`;
    } else if (until === 0) {
      return 'Vence hoy';
    } else {
      return `Vence en ${until} días`;
    }
  }

}
