import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-request-list',
  templateUrl: './request-list.component.html',
  styleUrls: ['./request-list.component.sass']
})
export class RequestListComponent implements OnInit {
  @Input()
  applications: any[] = [];
  @Input()
  color: string = 'purple';
  @Output()
  onSelect = new EventEmitter<any>();
  constructor() { }

  ngOnInit(): void {
  }

  selectApplication(appication: any) {
    console.log('selectApplication', appication);
    this.onSelect.emit(appication);
  }

  getColor(startAt: string | Date, totalDays: number) {
    if (typeof startAt === 'string') startAt = new Date(startAt);
    const now = new Date();
    const days = Math.floor((now.getTime() - startAt.getTime()) / (1000 * 60 * 60 * 24));
    // get pecentage
    if (days > totalDays) {
      return 'black';
    }
    const percent = days/totalDays * 100;

    if (percent >= environment.requestCriticality.red) {
      return 'red';
    } else if (percent >= environment.requestCriticality.yellow) {
      return 'yellow';
    } else {
      return 'green';
    }
  }

  getUntilDays(startAt: string | Date, totalDays: number): string {
    if (typeof startAt === 'string') startAt = new Date(startAt);
    const now = new Date();
    const days = Math.floor((now.getTime() - startAt.getTime()) / (1000 * 60 * 60 * 24));
    const until = totalDays - days;
    if (until < 0) {
      return `Vencio hace ${Math.abs(until)} días`;
    } else if (until === 0) {
      return 'Vence hoy';
    } else {
      return `Vence en ${until} días`;
    }
  }

}
