import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApplicationService {

  constructor(private http: HttpClient) { }

  searchApplications(level: string, filter: any): Promise<any[]> {
    return this.http.post<any[]>(
        `${environment.apiUrl}/api/v1/application/${level}/search`
        , {
            ...filter, 
            state: +filter.state,
            document: +filter.document,
            office: +filter.office,
          })
      .toPromise();
  }

  getApplication(level: string, code: number): Promise<any> {
    return this.http.get<any[]>(
        `${environment.apiUrl}/api/v1/application/${level}/${code}`)
      .toPromise();
  }

  getFilterData(level: string): Promise<any[]> {
    return this.http.get<any[]>(
        `${environment.apiUrl}/api/v1/application-utils/${level}/filter-data`)
      .toPromise();
  }

  updateApplicationState(level: string, requestId: number, data: any): Promise<any> {
    return this.http.put<any>(
        `${environment.apiUrl}/api/v1/application/${level}/${requestId}`
        , data)
      .toPromise();
  }

  getPosibleStates(level: string, documentId: number, stateId: number): Promise<any[]> {
    return this.http.get<any[]>(
        `${environment.apiUrl}/api/v1/application/${level}/document/${documentId}/state/${stateId}`)
      .toPromise();
  }


  log(level: string, code: number): Promise<any> {
    return this.http.get<any>(
        `${environment.apiUrl}/api/v1/application/${level}/${code}/seguimiento`)
      .toPromise();
  }

}
