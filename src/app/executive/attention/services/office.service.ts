import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OfficeService {
  constructor(private http: HttpClient) { }

  getOffices(level: string): Promise<any[]> {
    return this.http.get<any[]>(
        `${environment.apiUrl}/api/v1/application-utils/${level}/offices`)
      .toPromise();
  }
}
