import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { PhotoRequest } from '../../models/photoRequest';

@Injectable({
  providedIn: 'root'
})
export class PhotoService {
  constructor(private http: HttpClient) { }

  getRequests(level: string): Promise<PhotoRequest[]> {
    return this.http.get<any[]>(
        `${environment.apiUrl}/api/v1/profile-photo/${level}/requests`)
      .toPromise();
  }

  updateRequest(level: string, id: number, state: string): Promise<{status: string}> {
    return this.http.put<any>(
        `${environment.apiUrl}/api/v1/profile-photo/${level}/requests`,
        { id, state })
      .toPromise();
  }
}
