import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DocumentService {

  constructor(private http: HttpClient) { }

  getdocuments(level: string, campus: string = "S01"): Promise<any[]> {
    return this.http.get<any[]>(
        `${environment.apiUrl}/api/v1/application-utils/${level}/${campus}/MEA/documents`)
      .toPromise();
  }
}
