import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UcProfile } from 'src/app/auth/models/UcProfile';
import { environment } from 'src/environments/environment';
import { StudentInfoService } from '../../services/student-info.service';

@Component({
  selector: 'app-search-student',
  templateUrl: './search-student.component.html',
  styleUrls: ['./search-student.component.sass']
})
export class SearchStudentComponent implements OnInit {
  level: string = '';
  loading = false;
  color = "blue";
  studentPortalUrl = environment.studentPortalUrl;
  studentFound: UcProfile | null = null;
  constructor(
    private route: ActivatedRoute,
    private studentInfoService: StudentInfoService
  ) {
    this.level = this.route.snapshot.data['level'];
   }

  ngOnInit(): void {
  
  }

  searchStudent(studentCode: string) {
    this.loading = true;
    this.studentInfoService.get(studentCode).then( data => {
      this.loading = false;
      this.studentFound = data;
    }).catch(err => {});
  }

  getToken() {
    if (this.studentFound){
      this.studentInfoService.createToken(this.studentFound?.code, this.level).then( data => {
        console.log(data);
        const jsonData = JSON.stringify(data);
        const base64 = btoa(jsonData);
        window.open(
          `${environment.studentPortalUrl}/auth/ingresar-como-estudiante?q=${base64}`,
          "_blank");


      }).catch(err => {});

    }
  }

}
