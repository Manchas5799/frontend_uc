import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GetDataEnum } from 'src/app/executive/models/get-data.enum';
import { TemplateEnum } from 'src/app/executive/models/template.enum';
import { Banner, BannerProperties } from '../../models/banner';
import { BannerService } from '../../services/banner.service';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.sass']
})
export class BannerComponent implements OnInit {
  color = 'orange';
  level = '';
  bannersState: GetDataEnum = GetDataEnum.NOTHING;
  banners: Banner[] = [];
  properties: BannerProperties = {
    image: '',
    header: '',
    content: '',
    footer: '',
    template: '',
    title: '',
  };
  templates: any[] = [
    {code: TemplateEnum.imageOnly, name: 'Solo imagen'},
    {code: TemplateEnum.thalf, name: '50/50 horizontal'},
    {code: TemplateEnum.t3q, name: '70/30 horizontal'},
  ];
  template?: any;
  constructor(
    private route: ActivatedRoute,
    private bannerService: BannerService,
  ) {
    this.level = this.route.snapshot.data['level'];
  }

  ngOnInit(): void {
    this.getBanners();
  }

  setTpl(value: string) {
    this.template = this.templates.find( (tpl) => tpl.code === value)[0];
    this.properties.template = value;
  }
  getBanners() {
    this.bannersState = GetDataEnum.LOADING;
    this.bannerService.getBanners(this.level).then( (data) => {
        this.banners = data;
        this.bannersState = GetDataEnum.DONE;
      }).catch( (error) => {
        console.log(error);
        this.bannersState = GetDataEnum.ERROR;
      }
    );
  }
}