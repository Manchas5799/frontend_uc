import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-tools-main',
  templateUrl: './tools-main.component.html',
  styleUrls: ['./tools-main.component.sass']
})
export class ToolsMainComponent implements OnInit {
  color = 'orange';
  level = '';
  constructor(private route: ActivatedRoute) {
    this.level = this.route.snapshot.data['level'];
  }
  
  ngOnInit(): void {
  }

}
