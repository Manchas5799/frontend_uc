import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AccountResponse } from 'src/app/auth/models/AccountResponse';
import { UcProfile } from 'src/app/auth/models/UcProfile';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StudentInfoService {

  constructor(private http: HttpClient) { }

  get(studentCode: string): Promise<UcProfile> {
    return this.http.get<UcProfile>(
      `${environment.apiUrl}/api/v1/executive/student-info/${studentCode}`).toPromise();
  }

  createToken(studentCode: string, level: string): Promise<AccountResponse> {
    const data: any = {
      studentCode, adminKey: '', level
    };
    return this.http.post<AccountResponse>(
      `${environment.apiUrl}/api/v1/auth/${level}/login-as-student`, data).toPromise();
  }
}
