import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Banner } from '../models/banner';

@Injectable({
  providedIn: 'root'
})
export class BannerService {

  constructor(private http: HttpClient) { }

  getBanners(level: string): Promise<Banner[]> {
    return this.http.get<Banner[]>(
        `${environment.apiUrl}/api/v1/application/${level}/banner`)
      .toPromise();
  }
}
