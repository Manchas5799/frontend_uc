import { Component, Input, OnInit } from '@angular/core';
import { TemplateEnum } from 'src/app/executive/models/template.enum';
import { BannerProperties } from '../../models/banner';

@Component({
  selector: 'app-banner-ui',
  templateUrl: './banner-ui.component.html',
  styleUrls: ['./banner-ui.component.sass']
})
export class BannerUiComponent implements OnInit {
  @Input()
  properties?: BannerProperties;
  types = TemplateEnum;
  constructor() { }

  ngOnInit(): void {
  }

}
