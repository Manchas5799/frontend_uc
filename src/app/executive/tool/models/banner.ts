export interface Banner {
  id: number;
  title: string;
  content: string;
  startAt: string;
  endAt: string;
}

export interface BannerProperties {
  image: string;
  title: string;
  header: string;
  content: string;
  footer: string;
  template: string;
}