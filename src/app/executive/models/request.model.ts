export interface DocumentRequest {
  id: number;
  levelCode: string;
  campus: string;
  requestDate: Date;
  
  // requestDescription: string;
  officeId: number;
  term: string;
  requestStateID: number;
  stateName: string;
  executiveNames?: string;
  
  document: Document;
  student: {
    pidm: number,
    dni: string,
    names: string,
    email?: string,
    celphone?: string,
  },
  programType: {
    code: string,
    name: string,
  },
  program: {
    code: string,
    name: string,
  },
  state: ApplicationState,
  metaData: ApplicationMetaData,
}
export interface ApplicationState {
  actionable: any;
}
export interface RequestState {
  id: number;
  name: string;
}

export interface Document {
  id: number;
  name: string;
  estimatedDays: number;
  generateDebt: boolean;
  generateDocument: boolean;
}


export interface ApplicationMetaData {
  description: string;
  publicData: {
    level: string;
    programType: string;
    program: string;
    section: string;
    campus: string;
    studentId: string;
    studentCode: string;
  };
  privateData: {
    pdfUrl: string;
  };
}