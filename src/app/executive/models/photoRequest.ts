export interface PhotoRequest {
  id: number;
  studentId: number;
  status: string;
  level: string;
  photo: string;
  url?: string;
  createAt?: string;
  name?: string;
  show?: boolean;
  lastname?: string;
}