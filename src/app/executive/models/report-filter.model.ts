export interface ReportFilter {
  campus?: string;
  state?: number;
  office?: number;
  document?: number;
  program?: string;
  term?: string;
  dni?: string;
  attendedBy?: string;
  studentName?: string;
  startAt: Date;
  endAt: Date;
  data?: any[];
  createdAt?: Date;
  createdBy?: string;
  // documentState?: number;
  // programType?: number;
}