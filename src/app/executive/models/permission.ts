import { AuthUiModule } from "src/app/auth/models/AuthUiModule";

export interface ExecutivePermission {
  campus: string;
  modules: AuthUiModule[];
}