export enum GetDataEnum {
  NOTHING = 0,
  LOADING = 1,
  DONE = 2,
  ERROR = 3,
}