import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignRolComponent } from './assign-rol.component';

describe('AssignRolComponent', () => {
  let component: AssignRolComponent;
  let fixture: ComponentFixture<AssignRolComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AssignRolComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignRolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
