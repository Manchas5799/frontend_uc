import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { forkJoin, Observable, of } from 'rxjs';
import { GlobalsComponent } from '../../../../globals.component';
import { XtFieldCellComponent } from '../../../../shared/controls/xt-field-cell/xt-field-cell.component';
import { RequestsService } from '../../../../svc/services/Requests.service';

@Component({
  selector: 'app-assign-rol',
  templateUrl: './assign-rol.component.html',
  styleUrls: ['./assign-rol.component.sass'],
})
export class AssignRolComponent implements OnInit {
  usersToSelect: { Id: number; RowText: string }[] = [];
  optionSelected: 'Equipo' | 'Usuario' = 'Usuario';

  //User
  @ViewChild('user')
  public user: XtFieldCellComponent;

  constructor(
    private readonly dialogRef: MatDialogRef<AssignRolComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public globals: GlobalsComponent,
    protected POSPostulacionSvc: RequestsService
  ) { }

  ngOnInit(): void {
    //console.log("dialog data", this.data) //selected rows
    this.getUsers();
  }

  getUsers(): void {
    this.POSPostulacionSvc.selPersonasByTipo('08').subscribe((dat: any) => {
      console.log("getUsers", dat)
      this.usersToSelect = dat.map((x: any) => ({
        Id: x.IDPersona,
        RowText: x.NombreCompleto,
      }));
    })
  }

  close(): void {
    this.dialogRef.close({ flag: false });
  }
  changeOptionSelected(option: 'Equipo' | 'Usuario'): void {
    this.optionSelected = option;
  }
  save(): void {
    if (!this.user.value) return this.globals.notify('Debe seleccionar una persona o equipo')
    if (this.data.RA) {
      this.data.selectedRows.forEach((dat: any) => {
        dat.AsignadoRA = this.user.value
      })
      const requests = this.data.selectedRows.map((dat: any) =>
        this.POSPostulacionSvc.updAsignarRA({ Id: dat.Id, AsignadoRA: dat.AsignadoRA })
      );

      forkJoin(requests).subscribe({
        next: (data) => {
          console.log('data', data);
        },
        complete: () => {
          console.log("forkjoin RA completo: ")
          this.globals.notify('Se ha asignado correctamente')
          this.dialogRef.close({ flag: true });
        },
      });
    }
    else {
      this.data.selectedRows.forEach((dat: any) => {
        dat.AsignadoC = this.user.value
      })
      const requests = this.data.selectedRows.map((dat: any) =>
        this.POSPostulacionSvc.updAsignar({ Id: dat.Id, AsignadoC: dat.AsignadoC })
      );
      forkJoin(requests).subscribe({
        next: (data) => {
          console.log('data', data);
        },
        complete: () => {
          console.log("forkjoin completo: ")
          this.globals.notify('Se ha asignado correctamente')
          this.dialogRef.close({ flag: true });
        },
      });
    }
  }
}
