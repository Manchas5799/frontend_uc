import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
/**
 * Clase que permite implementar rutas para la invocación de los componentes
 *
 * @descripción: Permite implementar rutas para la invocación de los componentes del paquete "dashboard"
 *
 * @author: hdiaz
 * @version: 1.0
 * @fecha_de_creación: 23/09/2021
  */
const routes: Routes = [
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GridFormsRoutingModule { }
