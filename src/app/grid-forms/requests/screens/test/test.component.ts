import { Component, OnInit, ViewChild } from '@angular/core';
import { GridBaseComponent } from '../../../../shared/base/grid-base/grid-base.component';
import { XtFieldCellComponent } from '../../../../shared/controls/xt-field-cell/xt-field-cell.component';
import { RequestsService } from '../../../../svc/services/Requests.service';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.sass']
})
export class TestComponent extends GridBaseComponent implements OnInit {

  //FieldSearch
  @ViewChild("FieldSearch")
  public FieldSearch: XtFieldCellComponent;
  constructor(
    public service: RequestsService,

  ) {
    super();
  }
  
  ngOnInit(): void {
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();
  }
}
