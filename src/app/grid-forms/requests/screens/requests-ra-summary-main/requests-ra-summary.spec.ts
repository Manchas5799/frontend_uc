import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestsRASummaryComponent } from './requests-ra-summary.component';

describe('RequestsRASummaryComponent', () => {
  let component: RequestsRASummaryComponent;
  let fixture: ComponentFixture<RequestsRASummaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RequestsRASummaryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestsRASummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
