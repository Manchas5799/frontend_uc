import { Component, OnInit } from '@angular/core';
//import { GridBaseComponent } from '@shared/base/grid-base/grid-base.component';
import { ActivatedRoute } from '@angular/router';
import { RequestsService } from '@app/svc/services/Requests.service';
import { InterviewService } from '@app/svc/services/Interview.service';
import * as moment from 'moment';
import { AuthService } from '@app/auth/services/auth.service';
import { UcProfile } from '@app/auth/models/UcProfile';

@Component({
  selector: 'app-requests-ra-summary',
  templateUrl: './requests-ra-summary.component.html',
  styleUrls: ['./requests-ra-summary.component.sass'],
})
export class RequestsRASummaryComponent implements OnInit {
  public datSolicitudes: any[] = [];
  public datEntrevistas: any[] = [];
  public fechaActual: any = moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
  profile?: UcProfile;
  constructor(
    public service: RequestsService,
    private readonly authService: AuthService,
    public entrevistaSvc: InterviewService,
    protected route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.initResources();
  }

  initResources() {
    this.getProfile();
    this.getRequestsSummary();
  }

  fnValidarDocumento = (item: any) => {
    alert('Validar documento');
  };

  fnProgramarHorario = () => {
    alert('Programar horarios');
  };

  private getProfile(): void {
    this.authService
      .getProfile()
      .subscribe((profile: UcProfile) => (this.profile = profile));
  }

  private getRequestsSummary(): void {
    this.service.selAll().subscribe((dat: any) => {
      this.datSolicitudes = dat?.filter((d: any) =>
        ['-1055', '-1074'].includes(d.EstadoPostulacion)
      );
      console.log('this.datSolicitudes', this.datSolicitudes);
      //this.data.push({ 'Fecha': 'dd/MM/yyyy HH:mm:ss', 'NombreCompleto': 'Test', 'Programa_text': "Programa de ..." }) //Quitar esto cuando se empiece a ingresar la data
      //this.data.push({ 'Fecha': 'dd/MM/yyyy HH:mm:ss', 'NombreCompleto': 'Test', 'Programa_text': "Programa de ..." }) //Quitar esto cuando se empiece a ingresar la data
    });
  }
}
