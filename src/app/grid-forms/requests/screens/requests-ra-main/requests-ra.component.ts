import { Component, OnInit, ViewChild } from '@angular/core';
//import { GridBaseComponent } from '@shared/base/grid-base/grid-base.component';
import { ActivatedRoute } from '@angular/router';
import {
  RequestInterviewStatus,
  RequestsService,
} from '@app/svc/services/Requests.service';
import { Observable, forkJoin, of } from 'rxjs';
import { GridBaseComponent } from '../../../../shared/base/grid-base/grid-base.component';
import { XtFieldCellComponent } from '../../../../shared/controls/xt-field-cell/xt-field-cell.component';
import { MatDialog } from '@angular/material/dialog';
import * as moment from 'moment';
import { AssignRolComponent } from '@app/executive/requests/widgets/assign-rol/assign-rol.component';

@Component({
  selector: 'app-requests-ra',
  templateUrl: './requests-ra.component.html',
  styleUrls: ['./requests-ra.component.sass'],
})
export class RequestsRAComponent extends GridBaseComponent implements OnInit {
  color = 'green';
  level = '';
  //FieldSearch
  @ViewChild('FieldSearch')
  public FieldSearch: XtFieldCellComponent;
  constructor(
    public service: RequestsService,
    protected route: ActivatedRoute,
    private readonly matDialogService: MatDialog
  ) {
    super();
  }

  ngOnInit(): void {
    this.level = this.route.snapshot.data['level'];
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();
  }

  getCustomData(): Observable<Object> {
    var values: Object = {
      Search: this.FieldSearch?.value || null,
    };
    return new Observable<Object>((observer) => {
      this.service.selByFilters(values).subscribe((data2: any) => {
        data2.forEach((element: any) => {
          element.Fecha = moment(element.FechaEntrevista).format('DD/MM/YYYY');
        });
        observer.next(data2);
        observer.complete();
      });
    });
  }
  fnAsignar(): any {
    if (this.formComp.grid.selectedRows().length == 0) return this.globals.notify('Debe seleccionar al menos una solicitud');
    let dialogRef = this.matDialogService.open(AssignRolComponent, {
      disableClose: true,
      data: {
        selectedRows: this.formComp.grid.selectedRows(),
        RA: true
      },
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result.flag) {
        //llamar al search para actualizar la grilla
        this.search();
      }
    });
  }

  fnDesactivarEntrevista(): any {
    const data: { Id: number }[] = this.formComp.grid.selectedRows();
    const ids = data.map((x) => x.Id);

    if (ids.length === 0) {
      this.globals.notify('Debe seleccionar al menos una solicitud');
      return;
    }

    const answer = confirm(
      '¿Está seguro de desactivar la entrevista a las solicitudes seleccionadas?'
    );

    if (!answer) {
      return;
    }
    const requests = ids.map((id) =>
      this.service.updEstadoEntrevista({
        id,
        estado: RequestInterviewStatus.EntrevistaDesactivada,
      })
    );

    forkJoin(requests).subscribe({
      next: (data) => {
        console.log('data', data);
      },
      complete: () => {
        this.search();
      },
    });
  }

  fnActivarEntrevista(): any {
    console.log('fnActivarEntrevista');
    const data: { Id: number }[] = this.formComp.grid.selectedRows();
    const ids = data.map((x) => x.Id);

    if (ids.length === 0) {
      this.globals.notify('Debe seleccionar al menos una solicitud');
      return;
    }

    const answer = confirm(
      '¿Está seguro de activar la entrevista a las solicitudes seleccionadas?'
    );

    if (!answer) {
      return;
    }

    const requests = ids.map((id) =>
      this.service.updEstadoEntrevista({
        id,
        estado: RequestInterviewStatus.EntrevistaActiva,
      })
    );

    forkJoin(requests).subscribe({
      next: (data) => {
        console.log('data', data);
      },
      complete: () => {
        this.search();
      },
    });
  }
}
