import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestsRAComponent } from './requests-ra.component';

describe('RequestsRAComponent', () => {
  let component: RequestsRAComponent;
  let fixture: ComponentFixture<RequestsRAComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RequestsRAComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestsRAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
