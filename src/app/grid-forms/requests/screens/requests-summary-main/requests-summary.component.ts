import { forkJoin } from 'rxjs';
import { Component, OnInit } from '@angular/core';
//import { GridBaseComponent } from '@shared/base/grid-base/grid-base.component';
import { ActivatedRoute } from '@angular/router';
import { RequestsService } from '@app/svc/services/Requests.service';
import { RequestsCountService } from '@app/svc/services/RequestsCount.service';
import { GridBaseComponent } from '../../../../shared/base/grid-base/grid-base.component';
import { dataRequestsCount, dataTest } from './data-test';
import { AuthService } from '@app/auth/services/auth.service';
import { UcProfile } from '@app/auth/models/UcProfile';
import { EstadoPostulacion } from '@app/executive/models/constants';

@Component({
  selector: 'app-requests-summary',
  templateUrl: './requests-summary.component.html',
  styleUrls: ['./requests-summary.component.sass'],
})
export class RequestsSummaryComponent implements OnInit {
  public datSolicitudes: any[] = [];
  public datCount: any = { validarDocumentos: 0, validados: 0, enTotal: 0 };
  public profile!: UcProfile;
  constructor(
    public service: RequestsService,
    private readonly authService: AuthService,
    public postulacionCountSvc: RequestsCountService,
    protected route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    // this.datSolicitudes = dataTest;
    // this.datCount = dataRequestsCount;
    this.initResources();
    this.getProfile();
  }

  fnValidarDocumento = (item: any) => {
    alert('Validar documento');
  };

  private getProfile(): void {
    this.authService
      .getProfile()
      .subscribe((profile: any) => (this.profile = profile));
  }

  private initResources(): void {
    forkJoin({
      requests: this.service.selAll(),
      requestsCount: this.postulacionCountSvc.selCounts(),
    }).subscribe({
      next: ({
        requests,
        requestsCount,
      }: {
        requests: any;
        requestsCount: any;
      }) => {
        this.datSolicitudes = requests
          ?.filter(
            (d: any) => d.EstadoPostulacion == EstadoPostulacion.Pendiente
          )
          .map((item: any) => {
            let { NombreCompleto, ApellidoPaterno, ApellidoMaterno } = item;
            let nombres = NombreCompleto.split(' ');

            return {
              ...item,
              NombreCompleto: `${this.changeWord(nombres[0])} ${this.changeWord(
                nombres[1]
              )} ${this.changeWord(ApellidoPaterno)} ${this.changeWord(
                ApellidoMaterno
              )}`,
            };
          });
        this.datCount = requestsCount;
      },
      error: (err) => {},
    });
  }

  // change word from uppercase to capitalize
  private changeWord(word: string): string {
    return word.charAt(0).toUpperCase() + word.slice(1).toLowerCase();
  }
}
