export interface Solicitud {
  Id: number;
  NombreCompleto: string;
  Programa_text: string;
  Fecha: string;
}

//dame 5 ejemplos
export const dataTest: Solicitud[] = [
  {
    Id: 1,
    NombreCompleto: 'Juan Perez Perez',
    Programa_text: 'Ingenieria de Sistemas',
    Fecha: '2021-09-01 12:00:00',
  },
  {
    Id: 2,
    NombreCompleto: 'Juan Perez Salvaje',
    Programa_text: 'Ingenieria de Sistemas',
    Fecha: '2021-09-01 12:00:00',
  },
  {
    Id: 3,
    NombreCompleto: 'Juan Perez Perez',
    Programa_text: 'Ingenieria de Sistemas',
    Fecha: '2021-09-01 12:00:00',
  },
  {
    Id: 4,
    NombreCompleto: 'Amilia Perez Perez',
    Programa_text: 'Ingenieria de Sistemas',
    Fecha: '2021-09-01 12:00:00',
  },
  {
    Id: 5,
    NombreCompleto: 'Perdo Perez Perez',
    Programa_text: 'Ingenieria de Sistemas',
    Fecha: '2021-09-01 12:00:00',
  },
];

export const dataRequestsCount: {
  validarDocumentos: number;
  validados: number;
  enTotal: number;
} = {
  validarDocumentos: 15,
  validados: 500,
  enTotal: 515,
};
