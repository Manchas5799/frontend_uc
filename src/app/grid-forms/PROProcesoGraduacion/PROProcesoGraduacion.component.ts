import { Component, OnInit, ViewChild } from '@angular/core';
import { GridBaseComponent } from '@shared/base/grid-base/grid-base.component';
import { PROProcesoGraduacionService } from '@svc/PROProcesoGraduacion.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { XtFieldCellComponent } from '../../shared/controls/xt-field-cell/xt-field-cell.component';
import * as moment from 'moment';
import { PEREstudianteService } from '../../svc/services/PEREstudiante.service';
import { DetalleParametroService } from '../../svc/services/DetalleParametro.service';

@Component({
  selector: 'app-proprocesograduacion',
  templateUrl: './PROProcesoGraduacion.component.html',
  styleUrls: ['./PROProcesoGraduacion.component.css']
  })
  export class PROProcesoGraduacionComponent extends GridBaseComponent implements OnInit {
  
  public entName: string = 'PROProcesoGraduacion';
  //FieldEstudiante
  @ViewChild("FieldEstudiante")
  public FieldEstudiante: XtFieldCellComponent;
  //FieldTituloTesis
  @ViewChild("FieldTituloTesis")
  public FieldTituloTesis: XtFieldCellComponent;
  //FieldGestionTesis
  @ViewChild("FieldGestionTesis")
  public FieldGestionTesis: XtFieldCellComponent;
  //FieldFechaInscripcionIni
  @ViewChild("FieldFechaInscripcionIni")
  public FieldFechaInscripcionIni: XtFieldCellComponent;
  //FieldFechaInscripcionFin
  @ViewChild("FieldFechaInscripcionFin")
  public FieldFechaInscripcionFin: XtFieldCellComponent;

  constructor(
    public service: PROProcesoGraduacionService,
    public PEREstudianteSvc: PEREstudianteService,
    public DetalleParametroSvc: DetalleParametroService,
    protected router: Router,
    protected route: ActivatedRoute
  ) {
    super();
  }
  
  ngOnInit(): void {
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();
    this.modalEdit = false

    super.FillDropDown(this.FieldEstudiante, this.PEREstudianteSvc.selAll());
    super.FillDropDown(this.FieldGestionTesis, this.DetalleParametroSvc.selxParametro(this.FieldGestionTesis.listid));
  }
  
  getCustomData(): Observable<Object> {
    console.log("getCustomData")
    var values: Object = {
      Estudiante: this.FieldEstudiante.value ? this.FieldEstudiante.value : null,
      TituloTesis: this.FieldTituloTesis.value ? this.FieldTituloTesis.value : null,
      GestionTesis: this.FieldGestionTesis.value ? this.FieldGestionTesis.value  : null,
      //FechaInscripcionIni: this.FieldFechaInscripcionIni.value ? moment(this.FieldFechaInscripcionIni.value).format('DD/MM/YYYY HH:mm:ss') : null,
      FechaInscripcionIni: this.FieldFechaInscripcionIni.value ? this.FieldFechaInscripcionIni.value : null,
      FechaInscripcionFin: this.FieldFechaInscripcionFin.value ? this.FieldFechaInscripcionFin.value : null,
    };
    let resp = new Observable<Object>(observer => {
      this.service.selByFilters(values).subscribe((data2: any) => {
        observer.next(data2);
        observer.complete();
      })
    });
    return resp;
  }
}
