import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PROProcesoGraduacionComponent } from './PROProcesoGraduacion.component';

describe('PROProcesoGraduacionComponent', () => {
  let component: PROProcesoGraduacionComponent;
  let fixture: ComponentFixture<PROProcesoGraduacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PROProcesoGraduacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PROProcesoGraduacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
