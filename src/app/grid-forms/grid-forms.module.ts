import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
//import { ControlsModule } from '@app/shared/controls/controls.module';
//SharedModule,
//  ControlsModule,
import { GridFormsRoutingModule } from './grid-forms.routing.module';
import { FormsModule } from '@angular/forms';
import { ControlsModule } from '../shared/controls/controls.module';

let griFormsComponentes: any[] =
  [
  ];

@NgModule({
  declarations: griFormsComponentes,
  imports: [
    CommonModule,
    SharedModule,
    GridFormsRoutingModule,
    FormsModule,
    ControlsModule,
  ],
  exports: griFormsComponentes,
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GridFormsModule { }
