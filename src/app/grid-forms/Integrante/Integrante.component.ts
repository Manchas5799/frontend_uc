import { Component, OnInit, AfterViewInit, ComponentFactoryResolver, Type } from '@angular/core';
import { GridBaseComponent } from '@shared/base/grid-base/grid-base.component';
import { IntegranteService } from '@svc/Integrante.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { DialogIntegranteEditComponent } from '../../edit-forms/dialog-integrante/dialog-integrante.component';
import { IntegranteEditComponent } from '../../edit-forms/Integrante/Integrante.component';
import { EditBaseComponent } from '../../shared/base/edit-base.component';
import { PEREstudianteService } from '../../svc/services/PEREstudiante.service';
import { PEREstudianteEditComponent } from '../../edit-forms/PEREstudiante/PEREstudiante.component';

@Component({
  selector: 'app-integrante',
  templateUrl: './Integrante.component.html',
  styleUrls: ['./Integrante.component.css']
  })
  export class IntegranteComponent extends GridBaseComponent implements OnInit {
  
  public entName: string = 'Integrante';

  constructor(
    public service: IntegranteService,
    public PEREstudianteSvc: PEREstudianteService,
    protected router: Router,
    public dialog: MatDialog,
    protected route: ActivatedRoute,
    public componentFactoryResolver: ComponentFactoryResolver
  ) {
    super();
  }

  urlDestino(): Type<EditBaseComponent> {
    return IntegranteEditComponent;
  }

  editComponent(): Type<EditBaseComponent> {
    return IntegranteEditComponent; 
  }

  editUrl(row: any) {
    console.log("this.route.snapshot.url",this.route.snapshot.url)
    console.log("row",row)
    let dialogRef = this.dialog.open(DialogIntegranteEditComponent
      , {
        disableClose: true,
        data: row
      }
    );
    dialogRef.afterClosed().subscribe(result => {
      //llamar al search para actualizar la grilla
      this.search();
    });
    return 'abrirPopUp'
  }

  ngOnInit(): void {
  }

  getInfoEstudiante(row: any): any {
    //levantar modal para mostrar la información del estudiante (solo lectura)
    let dialogRef = this.dialog.open(PEREstudianteEditComponent
      , {
        disableClose: true,
        data: {},
        //height: '250px',
        //width: '400px',
      }
    );
    dialogRef.afterClosed().subscribe(result => {
      
    });
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();
  }
}
