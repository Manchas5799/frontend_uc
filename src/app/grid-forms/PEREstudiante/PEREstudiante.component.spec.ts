import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PEREstudianteComponent } from './PEREstudiante.component';

describe('PEREstudianteComponent', () => {
  let component: PEREstudianteComponent;
  let fixture: ComponentFixture<PEREstudianteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PEREstudianteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PEREstudianteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
