﻿import { Component, OnInit, AfterViewInit } from '@angular/core';
import { GridBaseComponent } from '@shared/base/grid-base/grid-base.component';
import { PEREstudianteService } from '@svc/PEREstudiante.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-perestudiante',
  templateUrl: './PEREstudiante.component.html',
  styleUrls: ['./PEREstudiante.component.css']
  })
  export class PEREstudianteComponent extends GridBaseComponent implements OnInit {
  
  public entName: string = 'PEREstudiante';

  constructor(
    public service: PEREstudianteService,
    protected router: Router,
    protected route: ActivatedRoute
  ) {
    super();
  }
  
  ngOnInit(): void {
  }

  ngAfterViewInit() {
    super.ngAfterViewInit();
  }
}
