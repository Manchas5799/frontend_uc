import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { environment } from 'src/environments/environment';
import { P401Component } from './ui/screens/p401/p401.component';
import { P404Component } from './ui/screens/p404/p404.component';
import { LocationStrategy, PathLocationStrategy } from '@angular/common';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        redirectTo: '/auth/ingresar',
        pathMatch: 'full',
      },
      {
        path: 'auth',
        loadChildren: () =>
          import('./auth/auth.module').then((m) => m.AuthModule),
      },
      {
        path: environment.executiveType,
        loadChildren: () =>
          import('./executive/executive.module').then((m) => m.ExecutiveModule),
      },
      {
        path: 'comercial',
        loadChildren: () =>
          import('./administrative/administrative.module').then(
            (m) => m.AdministrativeModule
          ),
      },
    ],
  },
  // Http errors routes
  { path: '404', component: P404Component },
  { path: '401', component: P401Component },
  { path: '**', redirectTo: '/404' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  providers: [{ provide: LocationStrategy, useClass: PathLocationStrategy }],
  exports: [RouterModule],
})
export class AppRoutingModule {}
